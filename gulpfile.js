// 1. LIBRARIES
// - - - - - - - - - - - - - - -

var gulp = require('gulp'),
    rimraf = require('rimraf'),
    runSequence = require('run-sequence'),
    autoprefixer = require('gulp-autoprefixer'),
    sass = require('gulp-ruby-sass'),
    uglify = require('gulp-uglify'),
    exit = require('gulp-exit'),
    concat = require('gulp-concat'),
    connect = require('gulp-connect'),
    jshint = require('gulp-jshint'),
    path = require('path'),
    nodemon = require('gulp-nodemon'),
    _ = require('lodash');

// 2. SETTINGS VARIABLES
// - - - - - - - - - - - - - - -

// Foundation 5
// Sass will check these folders for files when you use @import.
var foundationSassPaths = [
    'client/assets/scss/site',
    'bower_components/foundation/scss',
    'bower_components/selectize/dist/css/selectize.css'
];
// These files include Foundation for Apps and its dependencies
var foundationJS = [
    'bower_components/fastclick/lib/fastclick.js',
    'bower_components/viewport-units-buggyfill/viewport-units-buggyfill.js',
    'bower_components/tether/tether.js',
    'bower_components/foundation/js/vendor/jquery.js',
    'bower_components/foundation/js/vendor/jquery.cookie.js',
    'bower_components/foundation/js/foundation.js',
    'bower_components/foundation/js/foundation/**/*.js',
    'bower_components/angular/angular.js',
    'bower_components/angular-resource/angular-resource.js',
    'bower_components/angular-touch/angular-touch.js',
    'bower_components/angular-sanitize/angular-sanitize.js',
    'bower_components/angular-animate/angular-animate.js',
    'bower_components/angular-ui-router/release/angular-ui-router.js',
    'bower_components/angular-cookies/angular-cookies.js',
    'bower_components/angular-local-storage/dist/angular-local-storage.js',
    'bower_components/moment/min/moment-with-locales.js',
    'bower_components/moment-timezone/builds/moment-timezone-with-data.js',
    'bower_components/ng-flow/dist/ng-flow-standalone.js',
    'bower_components/selectize/dist/js/standalone/selectize.js',
    'client/assets/js/site/datetimepicker.js',
    'client/assets/js/site/canvas-to-blob.js',
    'client/assets/js/site/load-image.all.min.js',
    'client/assets/js/site/owl.carousel.js',
    'bower_components/autofill-event/src/autofill-event.js',
    'bower_components/ngAutocomplete/src/ngAutocomplete.js',
    'client/assets/js/site/websockhop.js',
    'bower_components/fullcalendar/dist/fullcalendar.min.js',
    'bower_components/tinymce-dist/tinymce.js',
    'bower_components/angular-ui-tinymce/src/tinymce.js'
];


var siteJS = {
    "common": [
        './client/app.js',
        './client/modules/core/**/*.js',
        './client/modules/users/**/*.js'
    ],
    "dashboard": [
        './client/modules/dashboard/**/*.js',
        './client/modules/dashboard/**/**/*.js'
    ],
    "market-admin": [
        './client/modules/market-admin/**/*.js',
        './client/modules/market-admin/**/**/*.js'
    ],
    "home": [],
    "search": [
        './client/modules/search/**/*.js'
    ],
    "yumquotes": [
        './client/modules/yumquotes/**/*.js'
    ],
    "excludes": [
        '!./client/modules/**/tests/**/*.*'
    ]
};

var copyPaths = [
    './client/**/*.*',
    './client/modules/**/views/**/*.*',
    '!./client/app.js',
    '!./client/modules/**/*.js',
    '!./client/templates/**/*.*',
    '!./client/assets/{scss,js}/**/*.*'
];


// 3. TASKS
// - - - - - - - - - - - - - - -

var isRunning = false;

// js linter
gulp.task('lint', function () {
    return gulp.src(['./server/**/*.js', './server/**/*.json'])
        .pipe(jshint());
});

// Cleans the build directory
gulp.task('clean', function (cb) {
    rimraf('./build', cb);
});

// Copies user-created files typically views & images
gulp.task('copy', function () {
    return gulp.src(copyPaths, {
        base: './client/'
    }).pipe(gulp.dest('./build'));
});

// Compiles Sass
gulp.task('sass', function () {
    return gulp.src('./client/assets/scss/site/inchefs.scss')
        .pipe(sass({
            loadPath: foundationSassPaths,
            style: 'compact',
            container: 'site',
            bundleExec: true
        }))
        .on('error', function (e) {
            console.log(e);
        })
        .pipe(autoprefixer({
            browsers: ['> 1%', 'last 2 versions', 'Firefox 14', 'Opera 11.1', 'ie 10']
        }))
        .pipe(gulp.dest('./build/assets/css/'));
});


gulp.task('uglify-lib', function () {
    gulp.src(foundationJS)
        .pipe(uglify({
            mangle: true
        }).on('error', function (e) {
            console.log(e);
        }))
        .pipe(concat('zurb.min.js'))
        .pipe(gulp.dest('./build/assets/js/'));
    return gulp.src('bower_components/foundation/js/vendor/modernizr.js')
        .pipe(gulp.dest('./build/assets/js/'));
});

gulp.task('uglify-app', function () {

    var val;
    var groups = JSON.parse(JSON.stringify(siteJS));
    var commonGroup = JSON.parse(JSON.stringify(siteJS["common"]));
    var excludesGroup = JSON.parse(JSON.stringify(siteJS["excludes"]));

    delete groups["common"];
    delete groups["excludes"]

    for (group in groups) {
        var listofFiles = _.union(commonGroup, groups[group], excludesGroup);
        val = gulp.src(listofFiles)
            .pipe(uglify({
                mangle: true
            }).on('error', function (e) {
                console.log(e);
            }))
            .pipe(concat(group + '.min.js'))
            .pipe(gulp.dest('./build/assets/js/'));
    }
    return val;
});


// Builds your entire app once, without starting a server
gulp.task('build', function (cb) {
    runSequence('clean', ['copy', 'sass', 'uglify-lib', 'uglify-app'], function () {
        console.log("Successfully built.");
        cb(undefined);
    });
});

// Starts a test nodemon server, which you can view at http://localhost:3000
gulp.task('start', ['build'], function () {
    nodemon({
        script: 'inchefs.js',
        ext: 'server.view.html server*js json',
        env: {'NODE_ENV': 'development'},
        ignore: ['./build/**', './client/', 'node_modules/**', 'bower_components/**']
    })
        //.on('change', ['lint'])
        .on('restart', function () {
            console.log('restarted!');
        });
});

// Default task: builds your app, starts a server, and recompiles assets when they change
gulp.task('default', ['build', 'start'], function () {

    // Watch Sass
    gulp.watch('./client/assets/scss/site/*.scss', ['sass']);

    // Watch JavaScript && JSON
    var listOfFiles = _.map(siteJS);

    gulp.watch(foundationJS, ['uglify-lib']);

    gulp.watch(listOfFiles, ['uglify-app']);

    // Watch static files
    gulp.watch(copyPaths, ['copy']);
});
