# InChefs Node UI

This is the Node standalone app, handling web UI & talking to inChefs's api. It's powered by Node, Gulp, Angular, and libsass along with Foundation for Apps.
## Requirements

You'll need the following software installed to get started.

  * [Node.js](http://nodejs.org): Use the installer provided on the NodeJS website.
  * [Git](http://git-scm.com/downloads): Use the installer for your OS.
    * Windows users can also try [Git for Windows](http://git-for-windows.github.io/).
  * [Ruby](https://www.ruby-lang.org/en/): Use the installer for your OS. For Windows users, [JRuby](http://jruby.org/) is a popular alternative.
    * With Ruby installed, run `gem install bundler sass`.
  * [Gulp](http://gulpjs.com/) and [Bower](http://bower.io): Run `[sudo] npm install -g gulp bower`

## Get Started

Clone this repository

```bash
git clone git@bitbucket.org:nolines/inchefs-node-ui.git inchefs-node-ui
```

Change into the directory.

```bash
cd inchefs-node-ui
```

Install the dependencies. Running `npm install` will also automatically run `bower install` after. If you're running Mac OS or Linux, you may need to run `sudo npm install` instead, depending on how your machine is configured. Running `bundle` will install the correct version of Sass for the template.

```bash
npm install
bower install
bundle
```

While you're working on your project, run:

```bash
gulp
```

### If you encountered this error

```
Application loaded using the "development" environment configuration
{ Error: Cannot find module '../build/Release/bson'
    at Function.Module._resolveFilename (module.js:472:15)
    at Function.Module._load (module.js:420:25)
    at Module.require (module.js:500:17)
    at require (internal/module.js:20:19)
    at Object.<anonymous> (/Users/aldrin/Documents/past-projects/wanteet-folder/inchefs-node-ui/node_modules/bson/ext/index.js:15:10)
    at Module._compile (module.js:573:32)
    at Object.Module._extensions..js (module.js:582:10)
    at Module.load (module.js:490:32)
    at tryModuleLoad (module.js:449:12)
    at Function.Module._load (module.js:441:3) code: 'MODULE_NOT_FOUND' }
js-bson: Failed to load c++ bson extension, using pure JS version
```

Change Line 10 of this file `...node_modules/bson/ext/index.js`

to this:

```
bson = require('bson');
```


This will compile the Sass and assemble your Angular app. **Now go to `localhost:3000` in your browser to see it in action.**

To run the compiling process once, without watching any files:

```bash
npm start build
```