
var config = require('../../config/config');
var request = require('request');
var debug = require('debug')('wanteet|claim|profile');

exports.claim = function(req, res, next){
    var marketId = req.headers['x-market-id'] || req.market || 'wanteet';
    var prospectId = req.prospectId;


    if(!prospectId){
        res.status(404);
        next();
        return;
    }

    request.get(config.inchefs.publicUrl + "/prospects/claim/profile/" + prospectId, {
            headers: {
                'Content-Type': 'application/json',
                'X-Market-ID': marketId
            }
        },
        function (err, response, body) {

            if (err) {
                console.error(err);
            }

            if (typeof response !== 'undefined' && response.statusCode == 200) {
                var data = JSON.parse(body);

                String.prototype.repeat = function (num) {
                    return new Array(num + 1).join(this);
                }
                var email = data.email;
                var emailArr = email.split('@');
                var maskedEmail = email[0] + "*".repeat(emailArr[0].length - 1) + "@" + emailArr[1];

                res.render('user/registration',{
                    email: maskedEmail,
                    market: {slug: marketId, name: marketId}
                });
            }else{
                res.status(500);
                next();
            }
        });

};


exports.verify = function(req, res, next){

    var marketId = req.headers['x-market-id'] || req.market || 'wanteet';

    var prospectId = req.prospectId;
    var claimToken = req.claimToken;
    var prospectEmail = req.prospectEmail;

    debug(prospectId);
    debug(claimToken);

    var url = config.inchefs.publicUrl + "/bootstrap/" + marketId;
    var headers = {
        'Content-Type': 'application/json',
        'X-Market-ID': marketId
    };

    request.get(url, {
            headers: headers
        },
        function (err, response, body) {
            if (err) {
                debug(err);
            }
            if (response.statusCode == 200) {
                var bootStrap = JSON.parse(body);

                var data = {
                    user: req.user || null,
                    claimData: {prospectId: prospectId, claimToken: claimToken, prospectEmail: prospectEmail},
                    request: req,
                    market: bootStrap.market,
                    stripe: bootStrap.payments.stripe,
                    userProfile: bootStrap.profile
                }

                if (bootStrap.services) {
                    data['services'] = JSON.stringify(bootStrap.services);
                }
                if (bootStrap.globals) {
                    data['globals'] = JSON.stringify(bootStrap.globals);
                }

                res.render('prospect/register-prospect', data);

            } else {
                debug(response.statusCode);
                res.status(500);
                next();
            }
        });
};

