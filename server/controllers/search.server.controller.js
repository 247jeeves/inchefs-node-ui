var request = require('request'),
    config = require('../../config/config');
var debug = require('debug')('wanteet|search|server');

var marketId, requestedService, refId, queryText, location, isLocationSearch, locationGeo;

exports.index = function (req, res) {


    marketId = req.market || req.headers['x-market-id'] || 'wanteet';
    //console.log("ushakiz search controller market "+marketId+":"+req.location+":"+req.query.location);
    requestedService = req.service || req.query.service;
    refId = req.query.referenceId;
    queryText = req.query.queryText;
    // ushakiz isLocationSearch = (req.location || req.query.location) ? true : false;
    isLocationSearch = false;
    //TODO: Bring GeoIP here instead of hardcoding the default location
    if (isLocationSearch) {
        location = req.location || req.query.location || undefined;
        location = location.replace(/ /g, '+');
    }
    //console.log("ushakiz location is "+location);

    if (location) {
        findGeo(location, function (locationCoords) {
            searchResults(locationCoords, function (data) {
                takeAction(data, req, res, function () {
                    debug("Search executed with Location");
                });
            }, function(response, body){
                debug("search Failed");
                debug(response.statusCode);
                debug(body);
            });
        }, function(response, body){
            debug("findGeo Failed");
            debug(response.statusCode);
            debug(body);
        });
    } else {
        searchResults(undefined, function (data) {
            takeAction(data, req, res, function () {
                debug("Search executed without Location");
            });
        }, function(response, body){
            debug("search Failed");
            debug(response.statusCode);
            debug(body);
        });
    }
};

var findGeo = function (location, success, failure) {
    request.get(config.inchefs.publicUrl + "/search/geo/" + location, {
        headers: {
            'Content-Type': 'application/json',
            'X-Market-ID': marketId
        }
    }, function (err, response, body) {
        if (err) {
            console.log(err);
        }
        if (response.statusCode == 200) {

            locationGeo = JSON.parse(body);

            var geometry = locationGeo.results[0].geometry;

            var lat;
            var lon;
            if (geometry.hasOwnProperty('bounds')) {
                lat = geometry.bounds.northeast.lat;
                lon = geometry.bounds.northeast.lng;
            } else if (geometry.hasOwnProperty('viewport')) {
                lat = geometry.viewport.northeast.lat;
                lon = geometry.viewport.northeast.lng;
            } else if (geometry.hasOwnProperty('location')) {
                lat = geometry['location'].lat;
                lon = geometry['location'].lng;
            }

            var locationCoords = "" + parseFloat(lat).toFixed(7) + ","
                + parseFloat(lon).toFixed(7);

            success(locationCoords);
        } else {
            failure(response, body);
        }
    });
}

var searchResults = function (locationCoords, success, failure) {


    var searchRequest = config.inchefs.publicUrl + "/search/"
        + "?service=" + requestedService + "&market=" + marketId + '&fetchMarket=true';

    if (queryText) {
        searchRequest += '&query=' + queryText;
    }

    if (locationCoords) {
        searchRequest += '&l=' + locationCoords;
    }

    request.get(searchRequest, {
        headers: {
            'Content-Type': 'application/json',
            'X-Market-ID': marketId
        }
    }, function (err, response, body) {
        if (err) {
            console.log(err);
        }
        if (response.statusCode == 200) {
            var data = JSON.parse(body);

            success(data);
        } else {
            failure(response, body);
        }
    });
};

var takeAction = function (data, req, res, cb) {
    if ((data.searchResultCollection.total > 0)
        || (data.searchResultCollection.total == 0 && refId)) {

        var model = {
            user: req.user,
            request: req,
            refId: refId,
            service: requestedService,
            services: JSON.stringify({}),
            isHyperLocal: isLocationSearch,
            market: data.market,
            title: data.market.name + ' - ' + (data.market.tagline || 'Search Results')
        }

        if(isLocationSearch){
            model['location'] = JSON.stringify(locationGeo);
        }

        res.render('search/index', model);

    } else {

        var redrUrl = '/place-request/' + requestedService;

        if (isLocationSearch) {
            redrUrl += "?l=" + location;
        }
        res.redirect(redrUrl);
    }

    cb();
}

