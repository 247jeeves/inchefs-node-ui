'use strict';

var request = require('request'),
    config = require('../../config/config'),
    keystone = require('keystone'),
    Page = keystone.list('Page'),
    ObjectId = require('mongoose').Types.ObjectId,
    debug = require('debug')('wanteet|market-admin|server');
exports.dashboard = function (req, res) {

    var User = require('mongoose').model('User');
    var marketId = req.headers['x-market-id'] || req.market || 'wanteet';

    if (req.user) {


        request.get(config.inchefs.baseUrl + "/users/" + req.user.id
            + "/bootstrap", {
                headers: {
                    'Content-Type': 'application/json',
                    'X-Auth-Token': req.user.accessToken,
                    'X-Market-ID': marketId
                }
            },
            function (err, response, body) {
                if (err) {
                    debug(err);
                    res.redirect('/' + marketId + '/auth/signout');
                }
                if (typeof response !== 'undefined' && response.statusCode == 200) {
                    var bootStrapData = JSON.parse(body);

                    //Update User's Roles
                    User.findOne({username: bootStrapData.user.username}, function (err, user) {
                        if (err) {
                            debug(err);
                            return;
                        } else {
                            // Update existing user with latest accessToken
                            for (var key in bootStrapData.user) {
                                if (bootStrapData.user.hasOwnProperty(key)) {
                                    user[key] = bootStrapData.user[key];
                                }
                            }
                            user.save(function (err) {
                                if (err) {
                                    debug("Error in Update");
                                    debug(err);
                                    return;
                                } else {
                                    res.render('market-admin/index', {
                                        user: user,
                                        request: req,
                                        userProfile: bootStrapData.profile,
                                        services: JSON.stringify(bootStrapData.services),
                                        stripe: bootStrapData.payments.stripe,
                                        globals: JSON.stringify(bootStrapData.globals),
                                        market: bootStrapData.market,
                                        currencies: bootStrapData.currencies,
                                        title: bootStrapData.market.name + ' - ' + 'Market Admin Dashboard'
                                    });
                                }
                            });
                        }
                    });
                } else {
                    debug("There was a problem with request - Signing Out");
                    res.redirect('/auth/signout');
                }
            });

    } else {
        res.redirect("/#!/signin?r=");
    }
};


exports.setupDomain = function (req, res, next) {

    var debug = require('debug')('wanteet|market-admin|setupDomain');
    var config = require('../../config/config');
    var marketId = req.body['market'];
    var domains = req.body['domains'];
    domains = domains.join(' ');
    debug(domains);

    var nginxConfigPath = config.nginx.configPath;
    var nginxConfigName = marketId + '.conf';
    var tempPath = config.nginx.tempPath;
    var fs = require('fs');
    var path = require('path');
    var sys = require('sys'),
        exec = require('child_process').exec;

    if (fs.existsSync(nginxConfigPath + nginxConfigName)) {
        //Modify existing
        fs.readFile(nginxConfigPath + nginxConfigName, function (err, data) {
            if (err) {
                console.error(err);
                res.status(500).send(err);
            }
            var fileContents = data.toString();
            var nginxConfigData = fileContents.replace(/server_name.*;/g, 'server_name ' + domains + ';');

            fs.writeFile(tempPath + nginxConfigName, nginxConfigData, function (err) {
                if (err) {
                    console.err(err);
                    return res.status(500).send(err);
                }

                //console.log('file written successfully - moving to nginx & reloading');
                exec('sudo mv -f ' + tempPath + nginxConfigName + ' ' + nginxConfigPath + '; sudo nginx -s reload', function (error, stdout, stderr) {
                    if (!error) {
                        return res.status(200).send({
                            message: "Domain Mapping updated Successfully!"
                        });
                    } else {
                        console.error(error);
                    }
                });
            });
        });

    } else {
        //Make New
        var appUrl = config.nginx.appUrl;
        var appRootPath = config.nginx.appRootPath;
        var nginxPort = config.nginx.port;
        var logPath = config.nginx.logPath + marketId + '.log';
        var nginxConfigData = "\
            # " + marketId + " domain configuration \n\
            upstream " + marketId + " {\n\
                server " + appUrl + "; \n\
                keepalive 8; \n\
            }\n\
            \n\
            server { \n\
                client_max_body_size 8M; \n\
                listen 0.0.0.0:" + nginxPort + "; \n\
                server_name " + domains + "; \n\
                access_log " + logPath + "; \n\
                \n\
                location / { \n\
                    root " + appRootPath + "; \n\
                    proxy_set_header X-Real-IP $remote_addr; \n\
                    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for; \n\
                    proxy_set_header Host $http_host; \n\
                    proxy_set_header X-NginX-Proxy true; \n\
                    proxy_set_header X-Market-ID \"" + marketId + "\"; \n\
                    proxy_http_version 1.1; \n\
                    proxy_set_header Upgrade $http_upgrade; \n\
                    proxy_set_header Connection \"upgrade\"; \n\
                    proxy_pass http://" + appUrl + "/; \n\
                    proxy_redirect off; \n\
                }\n\
            }";

        fs.writeFile(tempPath + nginxConfigName, nginxConfigData, function (err) {
            if (err) {
                console.err(err);
                return res.status(500).send(err);
            }

            //console.log('file written successfully - moving to nginx & reloading');
            exec('sudo chown root:root ' + tempPath + nginxConfigName + ';sudo mv -f ' + tempPath + nginxConfigName + ' ' + nginxConfigPath + '; sudo nginx -s reload', function (error, stdout, stderr) {
                if (!error) {
                    //console.log("Domain Mapping created Successfully!");
                    return res.status(201).send({
                        message: "Domain Mapping created Successfully!"
                    });
                } else {
                    console.error(error);
                }
            });

        });
    }
};


var getDefaultPage = function (slug, cb) {
    Page.model.findOne()
        .where('slug', slug).where('parent', 'wanteet').exec(function (err, page) {
            if (err) {
                debug('Page not found - default-home');
                debug(err);
                return;
            }
            if (page) {
                cb(page);
            }
        });
}

var setupDefaultPage = function (slug, market, title, title_slug, cb) {

    getDefaultPage(slug, function (defaultPage) {

        var newPage = Page.model({
            title: title,
            parent: market.slug,
			slug: slug,
            content: {
                data: defaultPage.content.full
            }
        });

        debug(title + "ukizhake  Page created " + newPage);
        newPage.save(function (err) {
            if (err) {
                console.error(err);
            }
            debug(title + " Page created for - " + market.slug);
			
			Page.model.findOne()
			.where('parent', market.slug).where('title', title).exec( function(err, page) {
				if(err){
					debug(err);
				}
	
				if(page) {
					page.slug = title_slug;
	
					page.save( function (err){
						if(err){
							debug("setupDefaultPage page save" + err)
						}
						debug(title + " Page updated for - " + market.slug + " and title slug as " + title_slug);
						cb()
						
					} )

				}

			} )
            
        });
    });

};

exports.setupPages = function (req, res, next) {
    var marketId = req.body['market'],
        marketName = req.body['name'],
        debug = require('debug')('wanteet|market-admin|setupPages');

    request.get(config.inchefs.publicUrl + "/bootstrap/" + marketId, {
            headers: {
                'Content-Type': 'application/json',
                'X-Market-ID': marketId
            }
        },
        function (err, response, body) {

            if (typeof response !== 'undefined' && response.statusCode == 200) {
                var bootStrapData = JSON.parse(body);
                var market = bootStrapData.market;

                //Setup HomePage
                setupDefaultPage('default-home', market, market.slug, market.slug , function () {
                    //console.log('Home created');
                });
                setupDefaultPage('default-about-us', market, 'About Us', 'about-us', function () {
                    //console.log('About created');
                });
                setupDefaultPage('default-terms', market, 'Terms of Service', 'terms-of-service', function () {
                    //console.log('Terms created');
                });
                setupDefaultPage('default-contact-us', market, 'Contact Us', 'contact-us' ,function () {
                    //console.log('Contact created');
                });
                setupDefaultPage('default-help', market, 'Help', 'help' ,function () {
                    //console.log('Help created');
                });
                setupDefaultPage('default-tracy', market, 'Tracy', 'tracy' , function () {
                    //console.log('Tracy created');
                });
                setupDefaultPage('default-privacy-policy', market, 'Privacy Policy', 'privacy-policy' , function () {
                    //console.log('Tracy created');
                });
                setupDefaultPage('default-how-it-works', market, 'How it works', 'how-it-works' ,function () {
                    //console.log('Help created');
                });

            }

        });

    return res.status(201).send({
        message: 'Pages setup successfully!'
    });

};

exports.updatePage = function (req, res, next) {

    var pageId = req.pageId;
    var marketId = req.headers['x-market-id'] || req.market || 'wanteet';

    var title = req.body["title"];
    var content = req.body["content"];
    var keywords = req.body["keywords"];

    Page.model.findOne()
        .where('_id', ObjectId(pageId.toString())).where('parent', marketId).exec(function (err, page) {
            if (err) {
                debug(err);
                res.status(422).send(err);
                return;
            }
            if (page) {

                page.title = title;
                page.content.data = content;
                page.keywords = keywords;
                page.parent = marketId;

                page.save(function(err){
                    if(err){
                        res.status(422).send(err);
                        return;
                    }else{
                        res.status(200).send(page);
                        return;
                    }
                });

            } else {
                res.status(404).send({message: 'Not Found'});
                return;
            }
        });

};

exports.getPages = function(req, res, next){

    var marketId = req.headers['x-market-id'] || req.market || 'wanteet';

    debug(marketId);

    Page.model.find()
        .where('parent', marketId).exec(function (err, pages) {
            if (err) {
                debug(err);
                res.status(422).send(err);
                return;
            }
            if (pages.length > 0) {

                res.status(200).send(pages);
                return;

            } else {
                res.status(404).send({message: 'Not Found'});
                return;
            }
        });
}

exports.getPage = function (req, res, next) {

    var pageId = req.pageId;
    var marketId = req.headers['x-market-id'] || req.market || 'wanteet';

    if (!pageId) {
        res.status(404).send({message: 'Not Found'});
        return;
    }

    Page.model.findOne()
        .where('_id', ObjectId(pageId.toString())).where('parent', marketId).exec(function (err, page) {
            if (err) {
                debug('Page not found - default-home');
                debug(err);
                res.status(422).send(err);
                return;
            }
            if (page) {

                res.status(200).send(page);
                return;

            } else {
                res.status(404).send({message: 'Not Found'});
                return;
            }
        });
};

exports.createPage = function (req, res, next) {

    var title = req.body["title"];
    var content = req.body["content"];
    var keywords = req.body["keywords"];
    var marketId = req.headers['x-market-id'] || req.market || 'wanteet';

    if(title){
        var newPage = Page.model({
            title: title,
            parent: marketId,
            keywords: keywords,
            content: {
                data: content
            }
        });

        newPage.save(function (err) {
            if (err) {
                debug(err);
                res.status(422).send(err);
            }else{
                debug(title + " Page created for - " + marketId);
                res.status(201).send(newPage);
            }
        });
    }else{
        res.status(422).send({message: "Title is missing"});
    }
};

exports.deletePage = function (req, res, next) {
    var pageId = req.id;
    var marketId = req.headers['x-market-id'] || req.market || 'wanteet';

    if (!pageId) {
        res.status(404).send({message: 'Not Found'});
        return;
    }

    Page.model.findOne()
        .where('_id', ObjectId(pageId.toString())).where('parent', marketId).exec(function (err, page) {
            if (err) {
                debug('Page not found - default-home');
                debug(err);
                res.status(422).send(err);
                return;
            }
            if (page) {

                page.remove(function (err) {
                    if (err) {
                        res.status(500).send(err);
                        return;
                    } else {
                        res.status(200).send({message: 'OK'});
                        return;
                    }

                });
            } else {
                res.status(404).send({message: 'Not Found'});
                return;
            }
        });
};
