'use strict';

exports.dashboard = function (req, res) {

    var debug = require('debug')('wanteet|dashboard|server');
    var User = require('mongoose').model('User');
    var marketId = req.headers['x-market-id'] || req.market || 'wanteet';
   //console.log("!!!!!! Sanjeet - Inside the dashboard controller marketId is : " + marketId); 
    
    if (req.user) {
        var request = require('request'),
            config = require('../../config/config');

        request.get(config.inchefs.baseUrl + "/users/" + req.user.id
            + "/bootstrap", {
                headers: {
                    'Content-Type': 'application/json',
                    'X-Auth-Token': req.user.accessToken,
                    'X-Market-ID': marketId
                }
            },
            function (err, response, body) {
                if (err) {
                    debug(err);
                    res.redirect('/' + marketId + '/auth/signout');
                }
                if (typeof response !== 'undefined' && response.statusCode == 200) {
                    var bootStrapData = JSON.parse(body);

                    //Update User's Roles
                    User.findOne({username: bootStrapData.user.username}, function (err, user) {
                        if (err) {
                            debug(err);
                            return;
                        } else {
                            // Update existing user with latest accessToken
                            for (var key in bootStrapData.user) {
                                if (bootStrapData.user.hasOwnProperty(key)) {
                                    user[key] = bootStrapData.user[key];
                                }
                            }
                            user.save(function (err) {
                                if (err) {
                                    debug("Error in Update");
                                    debug(err);
                                    return;
                                } else {
                                    debug(user);
                                    res.render('dashboard/index', {
                                        user: user,
                                        request: req,
                                        userProfile: bootStrapData.profile,
                                        services: JSON.stringify(bootStrapData.services),
                                        stripe: bootStrapData.payments.stripe,
                                        globals: JSON.stringify(bootStrapData.globals),
                                        market: bootStrapData.market,
                                        title: bootStrapData.market.name + ' - ' + 'User Dashboard'
                                    });
                                }
                            });
                        }
                    });
                } else {
                    debug(response.statusCode);
                    res.redirect('/auth/signout');
                }
            });

    } else {
        res.redirect("/#!/signin?r=");
    }
};


/**
 * This method is only meant to refresh the User data for session
 * e.g. incase a user's granted a new role, the session data shall need an update
 * Important!!! Only use this if user is logged in!
 * @param req
 * @param res
 */

exports.refreshSession = function (req, res) {

    var debug = require('debug')('wanteet|reload');
    var User = require('mongoose').model('User');
    var marketId = req.headers['x-market-id'] || req.market || 'wanteet';

    if (req.user) {

        //After session refresh we need to redirect user to a page
        var fromUrl = decodeURIComponent(req.query.from);
        var toUrl = decodeURIComponent(req.query.to);

        debug(fromUrl);
        debug(toUrl);

        var request = require('request'),
            config = require('../../config/config');

        request.get(config.inchefs.baseUrl + "/validate", {
            headers: {
                'Content-Type': 'application/json',
                'X-Auth-Token': req.user.accessToken,
                'X-Market-ID': marketId
            },
            json: true
        }, function (err, response, body) {
            if (err) {
                debug(err);
                res.redirect(fromUrl);
                return;
            }

            if (response.statusCode != 200) {
                debug(response.statusCode);
                debug(body);
                res.redirect(fromUrl);
                return;
            }

            if (!body) {
                debug('No body received from api :(');
                res.redirect(fromUrl);
                return;
            }

            debug(body);

            User.findOne({username: body.username}, function (err, user) {
                if (err) {
                    debug(err);
                    res.redirect(fromUrl);
                    return;
                } else {
                    // Update existing user with latest accessToken
                    for (var key in body) {
                        if (body.hasOwnProperty(key)) {
                            user[key] = body[key];
                        }
                    }

                    user.save(function (err) {
                        if (err) {
                            debug("Error in Update");
                            res.redirect(fromUrl);
                            return;
                        } else {
                            //Success! redirect to redr url
                            debug("Old Permissions");
                            debug(req.user.permissions);
                            debug("New Permission");
                            debug(user.permissions);
                            res.redirect(toUrl);
                        }
                    });
                }
            });
        });
    }
};
