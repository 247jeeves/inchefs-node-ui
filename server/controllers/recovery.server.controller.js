
exports.forgot = function (req, res) {

    var posts = require('../extcache/blog.json');
    var upcomingSpecials = require('../extcache/upcoming-specials.json');
    var featured = require('../extcache/featured.json');
    var pageTop = require('../extcache/home-page-top.json');

    var request = require('request'),
        config = require('../../config/config');

    var truncate = function (str) {
        return (str.length > 22) ? str.substring(0, 22) + "..." : str;
    };

    request.get(config.inchefs.publicUrl + "/services", {
            headers: {
                'Content-Type': 'application/json'
            }
        },
        function (err, response, body) {
            if (err) {
                console.log(err);
            }
            if (response.statusCode == 200) {

                var truncate = function (str) {
                    return (str.length > 22) ? str.substring(0, 22) + "..." : str;
                };

                res.render('home/index', {
                    user: req.user || null,
                    request: req,
                    posts: posts.slice(0, 8),
                    pageTop: pageTop,
                    specials: upcomingSpecials,
                    featured: featured,
                    services: JSON.parse(body),
                    truncate: truncate
                });
            } else {
                console.log(response.statusCode);
                //console.log(body);
            }
        });
};

exports.confirm = function (req, res) {

    var posts = require('../extcache/blog.json');
    var upcomingSpecials = require('../extcache/upcoming-specials.json');
    var featured = require('../extcache/featured.json');
    var pageTop = require('../extcache/home-page-top.json');

    var request = require('request'),
        config = require('../../config/config');

    var truncate = function (str) {
        return (str.length > 22) ? str.substring(0, 22) + "..." : str;
    };

    request.get(config.inchefs.publicUrl + "/services", {
            headers: {
                'Content-Type': 'application/json'
            }
        },
        function (err, response, body) {
            if (err) {
                console.log(err);
            }
            if (response.statusCode == 200) {

                var truncate = function (str) {
                    return (str.length > 22) ? str.substring(0, 22) + "..." : str;
                };

                res.render('home/index', {
                    user: req.user || null,
                    request: req,
                    posts: posts.slice(0, 8),
                    pageTop: pageTop,
                    specials: upcomingSpecials,
                    featured: featured,
                    services: JSON.parse(body),
                    truncate: truncate
                });
            } else {
                console.log(response.statusCode);
                console.log(body);
            }
        });
};

exports.verify = function(req, res){

};
