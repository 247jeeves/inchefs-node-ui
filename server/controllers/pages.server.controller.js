'use strict';

/**
 * Module dependencies.
 */
var _ = require('lodash');

/**
 * Extend user's controller
 */

console.log("Sanjeet At the start of the Pages.Server controller");

module.exports = _.extend(
	require('./pages/pages.profile.server.controller'),
	require('./pages/pages.package.server.controller'),
	require('./pages/pages.landing.server.controller'),
	require('./pages/pages.request.server.controller'),
	require('./pages/pages.prospect.server.controller')
);
