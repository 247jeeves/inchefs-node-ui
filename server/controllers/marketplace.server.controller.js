'use strict';

//TODO: REFACTOR THIS !!! This for integration with external sources
exports.marketplace = function (req, res, next) {

    var debug = require('debug')('wanteet|marketplace|server');

    var marketName = req.headers['x-market-id'] || req.marketName;
    var accessToken = req.accessToken;
    if (!req.user) {

        req.body = {
            username: accessToken,
            token: accessToken
        };
        debug(req.body);
        var passport = require('passport');
        passport.authenticate('market', function (err, user, info) {
            if (err || !user) {
                debug(err);
                debug(info);
                res.status(400).send(info);
            } else {
                // Remove sensitive data before login
                user.password = undefined;
                user.salt = undefined;

                req.login(user, function (err) {
                    if (err) {
                        debug(err);
                        res.status(400).send(err);
                    } else {
                        res.redirect('/' + marketName);
                    }
                });
            }
        })(req, res, next);

    } else {
        res.redirect('/' + marketName);
    }
};


/**
 * This method is only meant to refresh the User data for session
 * e.g. incase a user's granted a new role, the session data shall need an update
 * Important!!! Only use this if user is logged in!
 * @param req
 * @param res
 */

exports.refreshSession = function (req, res) {

    var debug = require('debug')('wanteet|reload');
    var User = require('mongoose').model('User');

    if (req.user) {

        //After session refresh we need to redirect user to a page
        var fromUrl = decodeURIComponent(req.query.from);
        var toUrl = decodeURIComponent(req.query.to);

        debug(fromUrl);
        debug(toUrl);

        var request = require('request'),
            config = require('../../config/config');

        request.get(config.inchefs.baseUrl + "/validate", {
            headers: {
                'Content-Type': 'application/json',
                'X-Auth-Token': req.user.accessToken
            },
            json: true
        }, function (err, response, body) {
            if (err) {
                debug(err);
                res.redirect(fromUrl);
                return;
            }

            if (response.statusCode != 200) {
                debug(response.statusCode);
                debug(body);
                res.redirect(fromUrl);
                return;
            }

            if (!body) {
                debug('No body received from api :(');
                res.redirect(fromUrl);
                return;
            }

            User.findOne({username: body.username}, function (err, user) {
                if (err) {
                    debug(err);
                    res.redirect(fromUrl);
                    return;
                } else {
                    // Update existing user with latest accessToken
                    for (var key in body) {
                        if (body.hasOwnProperty(key)) {
                            user[key] = body[key];
                        }
                    }

                    user.save(function (err) {
                        if (err) {
                            debug("Error in Update");
                            res.redirect(fromUrl);
                            return;
                        } else {
                            //Success! redirect to redr url
                            res.redirect(toUrl);
                        }
                    });
                }
            });
        });
    }
};
