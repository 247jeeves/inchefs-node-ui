/**
 * Module dependencies.
 */
exports.productBySlug = function(req, res) {

	var slug = req.params.slug;
	var restclient = require('../../util/restclient');
	restclient.getJSON('http://localhost:8080/item/'+slug,function(data){
		res.render('product/index', {
			user: req.user || null,
			request: req,
			product: data,
			market: req.market
		});
	}, function(error){
		console.log(error);
	});
};
