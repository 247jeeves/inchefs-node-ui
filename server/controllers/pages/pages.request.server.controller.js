exports.requestByDisplayId = function (req, res) {

    var displayId = req.params.displayId;
    var marketId = req.headers['x-market-id'] || req.market || 'wanteet';
    var restclient = require('../../util/restclient');
    var config = require('../../../config/config');

    //if(req.user === undefined || req.user === null){
    //    console.log("Assigned some dummy user");
    //    req.user = {id:0};
    //}


    restclient.getJSON(config.inchefs.publicUrl + "/requests/" + marketId + "/" + displayId,
        function (data) {
            res.render('request/index', {
                user: req.user || null,
                request: req,
                requestObj: data,
                market: {slug: marketId},
                title: data.displayId + ' - ' + marketId
            });
        }, function (error) {
            res.status(404).send('Not Found');
        });
};
