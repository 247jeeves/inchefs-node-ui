/**
 * Module dependencies.
 */
exports.prospectBySlug = function (req, res, next) {

    var debug = require('debug')('wanteet|prospect');
    var slug = req.params.slug;
    var config = require('../../../config/config');
    var marketId = req.headers['x-market-id'] || req.market || 'wanteet';

    var request = require('request');

    request.get(config.inchefs.publicUrl + "/prospects/" + slug, {
            headers: {
                'Content-Type': 'application/json',
                'X-Market-ID': marketId
            }
        },
        function (err, response, body) {

            if (err) {
                console.error(err);
            }

            if (typeof response !== 'undefined' && response.statusCode == 200) {
                var data = JSON.parse(body);

                var serviceArr = [];
                if (data.hasOwnProperty('characteristics')) {
                    var marketServices = data.market.settings.services;
                    var services = data.characteristics;
                    if (services.length > 0) {
                        services.map(function (obj) {
                            debug(obj);
                            for (i = 0; i < marketServices.length; i++) {
                                debug((marketServices[i].name == obj.name));
                                if (marketServices[i].name == obj.name) {
                                    serviceArr.push(obj.name);
                                    break;
                                }
                            }
                        });
                    }
                }


                res.render('prospect/index', {
                    user: req.user || null,
                    request: req,
                    prospect: data,
                    claimProfileToken: new Buffer(data.slug).toString('base64'),
                    servicesArr: serviceArr,
                    title: data.displayName + ' - ' + data.market.name,
                    market: data.market
                });
            }else{
                res.status(404);
                next();
                return;
            }
        });

};
