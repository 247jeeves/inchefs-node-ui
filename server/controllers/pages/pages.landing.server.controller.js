var keystone = require('keystone');

exports.pageBySlug = function (req, res, next) {

    var debug = require('debug')('wanteet|pages|pageBySlug');
    var slug = req.params.slug;
    var marketId = req.headers['x-market-id'] || req.market || 'wanteet';
    var currency = require('../../util/currency');
    if (!slug) {
        slug = marketId;
    }

    //console.log("1111 Usha At the Pages Landing Controller - Slug: "+slug + " Market Id "+ marketId+" keystone" +keystone);
    var debugKeystone = keystone.list('Page').model.findOne()
    .where('slug', slug).where('parent', marketId);


    var keyStoneValue =   keystone.list('Page').model.findOne()
    .where('slug', slug).where('parent', marketId).exec(function (err, page) {
        debug(err);
        if (err) {
            debug(err);
            console.error("2222 pages landing errorr getting page  "+err);
            res.status(404);
            return;
        }

        //Redirect to usual page
        if (!page) {
            console.error("2222 pages landing got usual page  null page ");
            next();
            return;
        }


        //Check the status of page - unpublished pages shall be only visible to admin
        if (page.state !== 'published') {
            if (req.user) {
                keystone.list('User').model.findOne()
                    .where('id', req.user.id).exec(function (err, user) {
                        if (err) {
                            console.error(err);
                            res.status(404);
                            return;
                        }
                        if (!(user && user.canAccessKeystone)) {
                            res.status(404);
                            return;
                        }
                    });
            } else {
                res.status(404).send('Not Found');
                return;
            }
        }

        var swig = require('swig');

        var request = require('request'),
            config = require('../../../config/config');

        var apiUrl;
        var headers;

        if (!req.user) {
            apiUrl = config.inchefs.publicUrl + "/bootstrap/" + marketId;
            headers = {
                'Content-Type': 'application/json',
                'X-Market-ID': marketId
            };
        } else {
            apiUrl = config.inchefs.baseUrl + "/users/" + req.user.id + "/bootstrap";
            headers = {
                'Content-Type': 'application/json',
                'X-Auth-Token': req.user.accessToken,
                'X-Market-ID': marketId
            };
        }

        request.get(apiUrl, {
                headers: headers
            },
            function (err, response, body) {
                if (err) {
                    console.error(err);
                }

                if (response.statusCode == 200) {

                    var truncate = function (str) {
                        return (str.length > 22) ? str.substring(0, 22) + "..." : str;
                    };

                    var bootStrap = JSON.parse(body);

                    var user = req.user;

                    if (Object.keys(bootStrap.user).length > 0) {
                        user = bootStrap.user;
                    }

                    var marketData = bootStrap.market;
                    var plans = [];
                    for (var type in marketData.settings.plans) {
                        for (var plan in marketData.settings.plans[type]) {
                            var obj = marketData.settings.plans[type][plan];
                            obj.title = plan;
                            plans.push(obj);
                        }
                    }

                    for (var i = 1; i < plans.length; i++) {
                        for (var j = 0; j < plans.length; j++) {
                            if (plans[i].amount < plans[j].amount) {
                                var tmp = plans[i];
                                plans[i] = plans[j];
                                plans[j] = tmp;
                            }
                        }
                    }

                    marketData.sortedPlans = plans;

                    var data = {
                        locals: {
                            posts: require('../../extcache/blog.json'),
                            packages: require('../../extcache/upcoming-specials.json'),
                            featured: require('../../extcache/featured.json'),
                            services: bootStrap.market.settings.services,
                            market: marketData,
                            listings: bootStrap.listings
                        }
                    };
                    //$timeout(function () {
                    //    $("#tags").selectize({
                    //        valueField: 'name',
                    //        labelField: 'description',
                    //        searchField: 'description',
                    //        delimiter: ',',
                    //        maxItems: '10',
                    //        options: bootStrap.services,
                    //        sortField: {
                    //            field: 'description',
                    //            direction: 'asc'
                    //        },
                    //        onChange: function (value) {
                    //            if (value == null) {
                    //                $scope.services = [];
                    //            } else {
                    //                $scope.services = value;
                    //            }
                    //        }
                    //
                    //    });
                    //}, 5);

                    var pageContent = swig.render(page.content.full, data);
                    res.render('page/index', {
                        user: user || null,
                        request: req,
                        pageContent: pageContent,
                        keywords: page.keywords,
                        description: page.description,
                        title: page.title + ' - ' + bootStrap.market.settings.tagline || bootStrap.market.name,
                        truncate: truncate,
                        bootStrap: bootStrap,
                        services: bootStrap.services,
                        formatCurrency: currency.formatCurrency,
                        market: bootStrap.market
                    });

                } else {

                    debug(response.statusCode);
                    debug(body);

                    var data = {
                        locals: {
                            posts: require('../../extcache/blog.json'),
                            packages: require('../../extcache/upcoming-specials.json'),
                            featured: require('../../extcache/featured.json')
                        }
                    };
                    //$timeout(function () {
                    //    $("#tags").selectize({
                    //        valueField: 'name',
                    //        labelField: 'description',
                    //        searchField: 'description',
                    //        delimiter: ',',
                    //        maxItems: '10',
                    //        options: bootStrap.services,
                    //        sortField: {
                    //            field: 'description',
                    //            direction: 'asc'
                    //        },
                    //        onChange: function (value) {
                    //            if (value == null) {
                    //                $scope.services = [];
                    //            } else {
                    //                $scope.services = value;
                    //            }
                    //        }
                    //
                    //    });
                    //}, 5);
                    var pageContent = swig.render(page.content.full, data);
                    res.render('page/index', {
                        user: req.user || null,
                        request: req,
                        pageContent: pageContent,
                        keywords: page.keywords,
                        services: bootStrap.services,
                        description: page.description,
                        formatCurrency: currency.formatCurrency,
                        title: page.title + ' - ' + 'Wanteet'
                    });
                }
            });
    });

    //console.log("88888 Usha - Page Landing Controller - Skipping Keystone function " + JSON.stringify(keyStoneValue,"",2));    
};
