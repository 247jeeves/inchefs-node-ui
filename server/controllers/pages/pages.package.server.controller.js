exports.packageBySlug = function(req, res) {

    var debug = require('debug')('wanteet|profile');
    var slug = req.params.slug;
    var config = require('../../../config/config');
    var marketId = req.headers['x-market-id'] || req.market || 'wanteet';
    var currency = require('../../util/currency');

    var request = require('request');


    request.get(config.inchefs.publicUrl + "/packages/" + slug, {
            headers: {
                'Content-Type': 'application/json',
                'X-Market-ID': marketId
            }
        },
        function (err, response, body) {

            if (err) {
                res.status(404).send('Not Found');
            }

            if (typeof response !== 'undefined' && response.statusCode == 200) {
                var data = JSON.parse(body);

                res.render('package/index', {
                    user: req.user || null,
                    request: req,
                    pkg: data,
                    market: data.market,
                    formatCurrency: currency.formatCurrency,
                    title: data.name + ' - ' + data.market.name
                });
            } else {
                res.status(404).send('Not Found');
            }
        });
};
