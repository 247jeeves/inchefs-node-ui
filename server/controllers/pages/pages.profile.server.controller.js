/**
 * Module dependencies.
 */
exports.profileBySlug = function (req, res) {

    var debug = require('debug')('wanteet|profile');
    var slug = req.params.slug;
    var config = require('../../../config/config');
    var marketId = req.headers['x-market-id'] || req.market || 'wanteet';

    var currency = require('../../util/currency');
    var request = require('request');

    //console.log("Sanjeet At the Pages Profile Server Controller");

    request.get(config.inchefs.publicUrl + "/users/" + slug, {
            headers: {
                'Content-Type': 'application/json',
                'X-Market-ID': marketId
            }
        },
        function (err, response, body) {

            if (err) {
                console.err(err);
            }

            if (typeof response !== 'undefined' && response.statusCode == 200) {
                var data = JSON.parse(body);

                var serviceArr = [];
                if (data.sellerProfile && data.sellerProfile.hasOwnProperty('services')) {
                    var marketServices = data.market.settings.services;
                    var services = data.sellerProfile.services;
                    if (services.length > 0) {
                        services.map(function (obj) {
                            debug(obj);
                            for (i = 0; i < marketServices.length; i++) {
                                debug((marketServices[i].name == obj.name));
                                if (marketServices[i].name == obj.name) {
                                    serviceArr.push(obj.name);
                                    break;
                                }
                            }
                        });
                    }
                }

                res.render('profile/index', {
                    user: req.user || null,
                    request: req,
                    profile: data,
                    servicesArr: serviceArr,
                    publicProfile: JSON.stringify(data),
                    formatCurrency: currency.formatCurrency,
                    title: data.displayName + ' - ' + data.market.name,
                    market: data.market
                });
            }
        });

};
