'use strict';

/**
 * Module dependencies.
 */
var _ = require('lodash');

/**
 * Extend user's controller
 */
module.exports = _.extend(
	require('./users/users.zohoapi.server.controller'),
	require('./users/users.salesforce.server.controller'),
	require('./users/users.authentication.server.controller'),
	require('./users/users.microsoft.server.controller'),
	require('./users/users.authorization.server.controller'),
	require('./users/users.password.server.controller'),
	require('./users/users.profile.server.controller')
);
