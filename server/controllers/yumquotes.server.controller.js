/**
 * Module dependencies.
 */
exports.request = function (req, res) {
    config = require('../../config/config');

    //console.log('Server Yumquotes Controller - Setting Content to - ' + req.service);

    var marketId = req.headers['x-market-id'] || req.market || 'wanteet';
    var requestVar = require('request');

    requestVar.get(config.inchefs.publicUrl + "/bootstrap/" + marketId, {
        headers: {
            'Content-Type': 'application/json'
        }
    }, function (err, response, body) {

        if (err) {
            debug(err);
        }
        if (response.statusCode == 200) {

            var truncate = function (str) {
                return (str.length > 22) ? str.substring(0, 22) + "..." : str;
            };
            var bootStrap = JSON.parse(body);

            res.render('yumquotes/request', {
                user: req.user,
                request: req,
                service: req.service,
                market: bootStrap.market
            });
        } else {
            debug(response.statusCode);
        }

    });
};

