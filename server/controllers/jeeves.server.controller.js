'use strict';

var _ = require('lodash'),
    mongoose = require('mongoose'),
    request = require('request'),
    url = require('url'),
    fs = require('fs'),
    rp = require('request-promise'),
    userController = require('../../server/controllers/users.server.controller'),
    querystring = require('querystring'),
    User = mongoose.model('User');

/***************************************
 *  To store  Jeeves Engine Credential
 *  as AdditionalProvider Inside Mongo
 *************************************/
exports.jeevesRegistration = function(req, res) {
	//console.log(req.body);
	var marketId = req.body.data.marketId;
	      if(req.body!=""){
               var  profile  = req.body.data;
		//console.log(profile);
		/******************************
		*Prepare provider data for saving the profile
		************************************/

		var providerUserProfile = {
                        firstName: profile.first_name,
                        lastName: profile.last_name,
                        displayName: profile.full_name,
                        email: profile.email,
                        username: profile.username,
                        provider: marketId,
                        providerIdentifierField: 'id',
	                jeevesPassword: profile.password,
                    };
			//console.log(providerUserProfile); // Print the information - just for checking
			saveJeevesEnginePassword(req, res, providerUserProfile, marketId);
		}// End of if 


};
/**********************
* To save jeevesengine password
************************/

var saveJeevesEnginePassword = function(req, res, providerUserProfile, market){
	var dataToSave =  { jeevesengine: providerUserProfile };
	//console.log(dataToSave);
	User.findOneAndUpdate({username:providerUserProfile.email},
	{$set:{"additionalProvidersData":dataToSave }},function(err,doc){
	console.log("!!!!!!!!!!!!!!! err => " + JSON.stringify(err,"",3));
	//console.log("!!!!!!!!!!!!!!! Sanjeet doc => " + JSON.stringify(doc,"" , 3));
	});

}
