/**
 * Module dependencies.
 */
var keystone = require('keystone');

exports.placeRequest = function(req, res, next){

    var request = require('request'),
        config = require('../../config/config');
    var marketId = req.headers['x-market-id'] || req.market || 'wanteet';
    var apiUrl;
    var headers;

    if (!req.user) {
        apiUrl = config.inchefs.publicUrl + "/bootstrap/" + marketId;
        headers = {
            'Content-Type': 'application/json',
            'X-Market-ID': marketId
        };
    } else {
        apiUrl = config.inchefs.baseUrl + "/users/" + req.user.id + "/bootstrap";
        headers = {
            'Content-Type': 'application/json',
            'X-Auth-Token': req.user.accessToken,
            'X-Market-ID': marketId
        };
    }

    request.get(apiUrl, {
            headers: headers
        },
        function (err, response, body) {
            if (err) {
                console.error(err);
            }
            if (response.statusCode == 200) {

                var bootStrap = JSON.parse(body);

                var user = req.user;

                if (Object.keys(bootStrap.user).length > 0) {
                    user = bootStrap.user;
                }

                res.render('home/place-request', {
                    user: user || null,
                    request: req,
                    title: bootStrap.market.name + ' - Place Request',
                    service: req.service,
                    market: bootStrap.market
                });

            } else {

                debug(response.statusCode);
                debug(body);

                res.render('home/place-request', {
                    user: req.user || null,
                    request: req,
                    title: 'Wanteet - Place Request',
                    market: {slug: 'wanteet'}
                });
            }
        });


};


exports.getEnvironment = function(req, res, next){
	var debug = require('debug')('wanteet|home|server');	
	if(process.env.NODE_ENV == 'production'){
		res.send({"env":"production"})
	}else{
		res.send({"env":"qa"})
	}
};

exports.createYourMarket = function (req, res, next) {
    var debug = require('debug')('wanteet|home|server');

    var marketId = req.headers['x-market-id'] || req.market || 'wanteet';

    var request = require('request'),
        config = require('../../config/config');

    var url = config.inchefs.publicUrl + "/bootstrap/" + marketId;
    var headers = {
        'Content-Type': 'application/json'
    };

    if (req.user) {
        url = config.inchefs.baseUrl + '/users/' + req.user.id + '/bootstrap';
        headers['X-Market-ID'] = marketId;
        headers['X-Auth-Token'] = req.user.accessToken;
    }

    request.get(url, {
            headers: headers
        },
        function (err, response, body) {
            if (err) {
                debug(err);
            }
            if (response.statusCode == 200) {
                var bootStrap = JSON.parse(body);

                var data = {
                    user: req.user || null,
                    request: req,
                    market: bootStrap.market,
                    stripe: bootStrap.payments.stripe,
                    userProfile: bootStrap.profile
                }

                if (bootStrap.services) {
                    data['services'] = JSON.stringify(bootStrap.services);
                }

                //if (bootStrap.markets) {
                //    data['markets'] = JSON.stringify(bootStrap.markets);
                //}
                if (bootStrap.globals) {
                    data['globals'] = JSON.stringify(bootStrap.globals);
                }

                res.render('home/create-your-market', data);

            } else {
                debug(response.statusCode);
            }
        });

};
        exports.createYourCustomerServiceCenter = function (req, res, next) {
            var debug = require('debug')('wanteet|home|server');
        
            var marketId = req.headers['x-market-id'] || req.market || 'wanteet';
        
            var request = require('request'),
                config = require('../../config/config');
        
            var url = config.inchefs.publicUrl + "/bootstrap/" + marketId;
            var headers = {
                'Content-Type': 'application/json'
            };

            if (req.user) {
                url = config.inchefs.baseUrl + '/users/' + req.user.id + '/bootstrap';
                headers['X-Market-ID'] = marketId;
                headers['X-Auth-Token'] = req.user.accessToken;
            }
        
            request.get(url, {
                    headers: headers
                },
                function (err, response, body) {
                    if (err) {
                        debug(err);
                    }
                    if (response.statusCode == 200) {

                        var bootStrap = JSON.parse(body);
                        var data = {
                            user: req.user || null,
                            request: req,
                            market: bootStrap.market,
                            stripe: bootStrap.payments.stripe,
                            userProfile: bootStrap.profile
                        }
        
                        if (bootStrap.services) {
                            data['services'] = JSON.stringify(bootStrap.services);
                        }
        
                        //if (bootStrap.markets) {
                        //    data['markets'] = JSON.stringify(bootStrap.markets);
                        //}
                        if (bootStrap.globals) {
                            data['globals'] = JSON.stringify(bootStrap.globals);
                        }

                        res.render('home/create-your-customer-service-center', data);
        
                    } else {
                        debug(response.statusCode);
                    }
            });
};
