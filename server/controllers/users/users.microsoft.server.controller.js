'use strict';

/**
 * Module dependencies.
 */
var _ = require('lodash'),
    errorHandler = require('../errors.server.controller'),
    mongoose = require('mongoose'),
    request = require('request'),
    url = require('url'), 
    fs = require('fs'),
    querystring = require('querystring'),
    rp = require('request-promise'),    
    config = require('../../../config/config'),
    User = mongoose.model('User'),
  LocalStorage = require('node-localstorage').LocalStorage,
  localStorage = new LocalStorage('./scratch');

/**********************************
* To get the bootstrap information of the market
* using the marketId from the req to further get the
* social Token for Microsoft Outlook
************************/

var getMicrosoftTokens = function(req, callback){

	var options = {
		method:'GET',
		uri: config.inchefs.publicUrl + '/bootstrap/' + req.headers['x-market-id'],
		json:true,	
		};
	rp(options).then(function (body){
		//console.log("body getmicrosoftotkens "+body);
		callback(body);
	});

}

/******************************************************
*To trigger the Microsoft OAuth2 Authorization for authorization to
*there User Profile and Outlook Calendar
**************************/
exports.microsoftAuth = function(req, res){
	//Get Zoho Id, Client Secret , CallBack URL
	getMicrosoftTokens(req, function(body){
		 var microsoft = body.market.settings.social.microsoft;
		 var authURL = "https://login.microsoftonline.com/common/oauth2/v2.0/authorize?client_id=" +
                microsoft.clientId + "&response_type=code&redirect_uri=" + microsoft.callbackURL + 
              "&response_mode=query&scope=openid%20offline_access%20https%3A%2F%2Fgraph.microsoft.com%2Fuser.read%20Calendars.ReadWrite%20https://outlook.office.com/calendars.readwrite.shared";
	//console.log(authURL);
	res.redirect(authURL);
	})

}

/*********************************************************
 * To receive the authorization code and to get the further access
 * refresh token from the microsoft and saving the tokens to mongodb
 ******************************************************/

exports.microsoftAuthCallback = function(req, res){
   var fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl; //Get the full url 
   var parsedUrl = url.parse(fullUrl); // Parse the data received as query string including the grant code / authorization code
   var parsedQs = querystring.parse(parsedUrl.query); // Get the Query String as Array
   var grantCode = parsedQs.code;
   getMicrosoftTokens(req, function(body){
          var microsoft = body.market.settings.social.microsoft; 
          //console.log("!!!!!!!!!!! Sanjeet Microsoft Info: "+JSON.stringify(microsoft));
          var marketAdminEmail = body.market.settings.owner.user;
          var postUrl = "https://login.microsoftonline.com/common/oauth2/v2.0/token";
          //console.log(postUrl);
          var options = {
                method:'POST',
                url: postUrl,
                json:true,
                headers: {
                    'content-type': 'application/x-www-form-urlencoded',
                    'X-Market-ID':'247jeeves',
                    'X-Auth-Token': localStorage.getItem("ls.accessToken")
                },
                form: {
                    "grant_type":"authorization_code",
                    "client_id" : microsoft.clientId,
                    "code" : grantCode,
                    "client_secret": microsoft.clientSecret,
                    "redirect_uri":microsoft.callbackURL,
                    "scope":"https://outlook.office.com/calendars.readwrite.shared"
                }
          };
          rp(options).then(function (_body){
                //console.log(JSON.stringify(_body));
               if( _body.refresh_token != null){
                        var dataToSave = {"access_token":_body.access_token, "refresh_token":_body.refresh_token};
                        User.findOneAndUpdate({username:marketAdminEmail}, //Update Both Access and Refresh Token  into the Database
                        {$set:{"additionalProvidersData.microsofttokens":dataToSave }},function(err,doc){
                                console.log("!!!!!!!!!!!!!!! err => " + JSON.stringify(err,"",3));
                                //console.log("!!!!!!!!!!!!!!! Sanjeet doc => " + JSON.stringify(doc,"" , 3));
                                res.redirect('/');
                        }); 
               }else{
                        User.findOneAndUpdate({username:marketAdminEmail}, //Update the Access Token Only into the Database
                        {$set:{"additionalProvidersData.microsofttokens.access_token":_body.access_token }},function(err,doc){
                                console.log("!!!!!!!!!!!!!!! err => " + JSON.stringify(err,"",3));
                                //console.log("!!!!!!!!!!!!!!! Sanjeet doc => " + JSON.stringify(doc,"" , 3));
                                res.redirect('/');
                        });

               }
         });
   });
}


var microsoftRefreshToken = exports.microsoftRefreshToken =  function (req, res){  // Function connected with the express route as API
/*************************************************************************
 * Step 1: Get the refresh token from the mongo database
 *      using the email id.
 *
 * Step 2: Get the cLient Id and Client Secret of the zoho
 *        using the Bootstrap public api of the wanteet using the marketId
 *
 * Step 3: Build the url having the query string to generate the new access token
 *      and redirect the user to it, but further for this it requires the user to be logged in into his zoho account.
 *
 * Step 4: The callback function of the zoho will get the access_token and will update it 
 *      in the mongo database.  
 ************************************************************************/

        getMicrosoftTokens(req, function(body){ // _zoho contains the client Id and Secret of Zoho and market owner email Id
                        var microsoft = body.market.settings.social.microsoft;
                        var email = body.market.settings.owner.user;
                        microsoftTokenMongo(email, function(microsofttokens){ //microsofttokens contain the refresh & access token earlier saved in our mongo
            
                          var postUrl = "https://login.microsoftonline.com/common/oauth2/v2.0/token";
			  //console.log(postUrl);
			  var options = {
				method:'POST',
				url: postUrl,
				json:true,
				headers: {
				    'content-type': 'application/x-www-form-urlencoded',
				    'X-Market-ID':'247jeeves',
				},
				form: {
				    "grant_type":"refresh_token",
				    "client_id" : microsoft.clientId,
				    "refresh_token" : microsofttokens.refresh_token,
				    "client_secret": microsoft.clientSecret
				}
			  };
			rp(options).then(function(_body){
				 User.findOneAndUpdate({username:email}, //Update the Access Token Only into the Database
		                {$set:{"additionalProvidersData.microsofttokens.access_token":_body.access_token }},function(err,doc){
		                        console.log("!!!!!!!!!!!!!!! err => " + JSON.stringify(err,"",3));
		                        //console.log("!!!!!!!!!!!!!!! Sanjeet Microsoft Refresh TOken Received doc=>" + JSON.stringify(doc,"" , 3));
		                        res.send({"status":"success"});
		                });
			}, function(_error){
				console.log("!!!!! Error Log Refresh Token function microsoft")
				res.send({"status":"failed"});
			});
                });
        });

}

/*****************************************************
 * Get Users Calendar Event from our database
 ****************************************************/

var getCalendarEvents  = function(req, callback){
	var options = {
	 "method": "GET",
	"headers":{
		"X-Market-ID":req.headers['x-market-id'],
		"X-Auth-Token": req.headers['x-auth-token']
	},
	 "url":  config.inchefs.baseUrl+"/users/"+req.params.id+"/calendar/events",
	 "json": true,
	}

	rp(options).then(function(_body){
		callback(_body);
	});
}


/***********************************************************
 * Function for posting the wanteet calendar events to 
 * user's Outlook Calendar
 * ------------------------------------------------------
 *Step: 1 - Check Event is already synced on outlook
 * or not
 *---------------------------------------------------------
 *Step: 2 -  Do Post Event If Event is not already there
 * on outlook or synced earlier
 *----------------------------------------------------------
 *Step: 3 - Update the status of event our our backend
 * or on our wanteet calendar
 ***********************************************************/
exports.syncOutlookEvents = function(req, res){
      		getMicrosoftTokens(req, function(body){ //For getting market admin email using marketId in the header
			  var marketAdminEmail = body.market.settings.owner.user;
			 //console.log("!!!!!! Sanjeet Got the marketAdminEmail "+marketAdminEmail);
			  microsoftTokenMongo(marketAdminEmail, function(microsofttokens){ //For getting microsoft tokens from users collection
				getCalendarEvents(req, function(events){ //Get Calendar Events from the Database
					//console.log("!!!!! Sanjeet Got the events");
					var results = events.results;
					//console.log("Array Length is "+ results.length);
					for(var idx in results){
						if(!isOutlookEventSynced(results[idx])){
							//console.log("!!! Sanjeet Passed the isSynced Function: false")
							postOutlookEvent(req, results[idx], microsofttokens.access_token, function(_body){
								//console.log("!!!!! Sanjeet Posted the Event Successfull got the body in callback");
							},function(_err){
								console.log("!!!!! Posted the Event - Got Error - Might be refresh token expired");
								console.log(_err);
								res.send({"status":"failed"});
							});
						}
						if(idx == results.length){
							res.send({"status":"success"});
						}
					}
					res.send({"status":"success"});
				});
			  });
		});
}


/**************************************************
 * Function to post Outlook Event
 * @param calendarEvent : it contains the start and end
 * 	timestamp along with timezone
 * @param accessToken
 * @param - success and error - callback function
 **************************************************/
var postOutlookEvent = function(req, calendarEvent, accessToken, success, error){
	var startTime = (calendarEvent.start); //Split the timestamp and the timezone
	var endTime = (calendarEvent.end); 

	var header = {
	 	"Authorization": "Bearer " + accessToken ,
		"Content-Type": "application/json"
	};

	var options = {
		"method": "POST",
		"url": "https://outlook.office.com/api/v2.0/me/events",
		"headers":header,
		"json": true,
		"body": {
		  "Subject": calendarEvent.title,
		  "Start": {
		      "DateTime": startTime,
		      "TimeZone": "Pacific Daylight Time"
		  },
		  "End": {
		      "DateTime": endTime,
		      "TimeZone": "Pacific Daylight Time"
		  }
		}
	}

	rp(options).then(function(body){
		//console.log("!!!! Sanjeet Post Event Outlook - Success");
		//console.log(body);
		updateWanteetCalendarEvent(req, calendarEvent, body.Organizer.EmailAddress.Address);
		success(body);
	}, function(err){
		console.log("!!!! Post Event Outlook - Err");
		console.log(err);
		error(err);
	});
}


/*****************************************************
 * For getting the access_token and refresh_token from the 
 * database using marketAdmin Email
 ***************************************************/	
var microsoftTokenMongo = function(email, callback){
        User.find({"username": email},function(err, doc){
                //console.log("!!!! Inside the Microsoft Token Mongo");
                //console.log(doc[0]);
                callback(doc[0].additionalProvidersData.microsofttokens);
        });
}


/***********************************************
*@event - event of wanteet calendar
*@calendarName - name of the calendar (from success body of outlook post)
*@organizerEmail - email of the outlook account (from success body of outlook post)
*************************************************/
var updateWanteetCalendarEvent = function(req, calendarEvent, organizerEmail){
		//console.log("!!!!! Sanjeet - Inside the Update Calendar Event");
		//console.log("!!!! Sanjeet Event id is : " + calendarEvent.id);
		//Update the calendar
		var calendarEventUpdate = {
			"start":calendarEvent.start,
			"end":calendarEvent.end,
			"title": calendarEvent.title,
			"options": {
                 		"outlookSync":true,
		                "outlookEmail": organizerEmail
                	}
		}	
		
		//Header of the request
		var headers = {
			"X-Market-Id": req.headers['x-market-id'],
			"X-Auth-Token": req.headers['x-auth-token'],
			"Content-Type":"application/json",
		}	
		//Other parameters of the request	
		var options = {
			"url": config.inchefs.baseUrl + "/users/"+ req.params.id + "/calendar/events/" + calendarEvent.id,
			"method": "PUT",
			"headers":headers,
			"json": true,
			 body: calendarEventUpdate
		}

		rp(options).then(function(_body){
			//Success Case
			//console.log(_body);
			
		},function(err){
			//Err Log
			console.log(err);
		});
}


/**************************************************
*Function returns boolean value - true means synced,
* false means not synced
* Function to find out event is synced or not.
************************************************/
var isOutlookEventSynced = function(eventCalendar){
	//If event is new and has not been synced. Hence no option would be added/updated regarding sync info
	//console.log("!!!!! Sanjeet Inside the isOutlookEvnentSynced")
	//console.log("Event id is : "+ eventCalendar.id);
	if(eventCalendar.options == null){
		return false;
	}else{
		//If first integration is with google then outlook will not be present, therefore, not synced
		if(eventCalendar.options.outlookSync == null){
			return false;

		}

		//If first integration is outlook and event is synced
		if(eventCalendar.options.outlookSync){
			return true;
		}
	     
	}
}

