var _ = require('lodash'),
        errorHandler = require('../errors.server.controller'),
        mongoose = require('mongoose'),
        request = require('request'),
        url = require('url'),
        fs = require('fs'),
        querystring = require('querystring'),
        rp = require('request-promise'),
        config = require('../../../config/config'),
        User = mongoose.model('User'),
        moment = require('moment'),
        Lead = mongoose.model('Lead');

var debug = require('debug')('wanteet|salesforce');

/**********************************
* To get the bootstrap information of the market
* using the marketId from the req to further get the
* social Token for Salesforce
************************/

var getSFTokens = function (req, callback) {

        var options = {
                method: 'GET',
                uri: config.inchefs.publicUrl + '/bootstrap/' + req.headers['x-market-id'],
                json: true,
        };
        rp(options).then(function (body) {
                callback(body);
        });

}

/******************************************************
*To trigger the Salesforce OAuth2 Authorization for access to
*there salesforce CRM Panel
**************************/
exports.sfAuthCrm = function (req, res) {
	//Get Zoho Id, Client Secret , CallBack URL
	getSFTokens(req, function (body) {
		var salesforce = body.market.settings.social.salesforce;
		var authURL = "https://login.salesforce.com/services/oauth2/authorize?scope=full%20refresh_token&client_id=" + salesforce.clientID + "&response_type=code&prompt=consent&redirect_uri=" + salesforce.callbackURL;
		res.redirect(authURL);
	})

}

/*****************************************************
 *To save the salesforce accessToken and refreshTOken into the 
 * mongo db of the admin of  service center
 *************************************************/
exports.sfAuthCrmCallback = function (req, res) {
	var fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl; //Get the full url 
	var parsedUrl = url.parse(fullUrl); // Parse the data received as query string including the grant code / authorization code
	var parsedQs = querystring.parse(parsedUrl.query); // Get the Query String as Array
	var grantCode = parsedQs.code;

	getSFTokens(req, function (body) {
		var salesforce = body.market.settings.social.salesforce;
		
		var marketAdminEmail = body.market.settings.owner.user;
		var postUrl = "https://login.salesforce.com/services/oauth2/token?code=" + grantCode + "&redirect_uri=" + salesforce.callbackURL + "&client_id=" + salesforce.clientID + "&client_secret=" + salesforce.clientSecret + "&grant_type=authorization_code"; //Prepare the Url for getting the Access & Refresh Token

		var options = {
			method: 'POST',
			url: postUrl,
			json: true,
		};
		rp(options).then(function (_body) {
			debug("response received from callback" + JSON.stringify(_body));
	console.log("response received from callback" + JSON.stringify(_body));
			if (_body.refresh_token != null) {
				var dataToSave = { "access_token": _body.access_token, "refresh_token": _body.refresh_token, "instance_url":_body.instance_url };
				User.findOneAndUpdate({ username: marketAdminEmail }, //Update Both Access and Refresh Token  into the Database
					{ $set: { "additionalProvidersData.sftokens": dataToSave } }, function (err, doc) {
						console.log("247jeeves error => " + JSON.stringify(err, "", 3));
						res.redirect('/market-admin#!/salesforce/crm');
					});
			} else {
				User.findOneAndUpdate({ username: marketAdminEmail }, //Update the Access Token Only into the Database
					{ $set: { "additionalProvidersData.sftokens.access_token": _body.access_token } }, function (err, doc) {
						console.log("247jeeves err => " + JSON.stringify(err, "", 3));
						res.redirect('/market-admin#!/salesforce/crm');
					});

			}
		});
	});
}

/***************************************
 *For checking salesforce tokens exist inside mongodb (permissions exist)
 * ****************************************/
exports.needSFAuth = function (req, res) {
	var username = req.body.username;
	sfTokenMongo(username, function (sftokens) {
		if (sftokens) {
			res.send({ "status": "success" });
		} else {
			res.send({ "status": "error" });
		}
	})
}

var sfTokenMongo = function (email, callback) {
	User.find({ "username": email }, function (err, doc) {
		callback(doc[0].additionalProvidersData.sftokens);
	});
};

//To get the last sync timestamp of the salesforce synchronization

var sfLastSyncTimestamp = function (email, callback) {
	User.find({ "username": email }, function (err, doc) {
		if(err){
			debug("247jeeves error - sf last sync timestamp ")
			debug(err)	
		}
		debug(doc);
		callback(doc[0].additionalProvidersData.last_sf_sync_event);
	});
};

var saveSFLastSyncTimestamp = function(email, timestamp, callback){
	User.findOneAndUpdate({ "username": email }, {
		$set : {"additionalProvidersData.last_sf_sync_event": timestamp}}, 
		function (err, doc) {
			if(err){
			    debug("247jeeves error - salesforce save timestamp"+ JSON.stringify(err))
				callback({"status": "error"})
			}else{
				callback({"status":"success"})
			}
	});
}


var deleteLeadsAfterSync = function(leads, marketId, callback){
    debug("salesforce deleteLeadsUponSync leads are ", leads)
    debug("salesforce deleteLeadsUponSync length of leads", leads.length)
	for (var i=0; i < (leads.length); i++){
	 	 debug("salesforce deleteUponSync lead using i: " + i, leads[i] )
		 Lead.find({"email": leads[i].richInput.Email, "marketId":marketId},  function (err, lead){
		 	var conversations = lead[0].conversations;
			 debug("salesforce conversations are "+ JSON.stringify(conversations));
		     for(var j=0; j < conversations.length; j++){
     		  	 if(conversations[j]){
	        		   deleteConversation(conversations[j].conversationId);
	       		}
	   		 }
			debug("salesforce deleteUponSync lead using i: " + i, leads[i] )
	     });
         Lead.find({"email": leads[i].richInput.Email, "marketId":marketId}).remove().exec();
    }//end for loop
    callback({"status":"success"});
}

var getLeads = function (req, callback) {
	Lead.find({ "marketId": req.headers['x-market-id'] }, function (err, result) {
		callback(result);
	})
}

var getMarketOwnerLeads = function (marketId, callback) {
	Lead.find({ "marketId": marketId }, function (err, result) {
		debug("getMarketOwner leads result" + JSON.stringify(result))
		if(result){
			callback(result);
		}else{
			callback(null)
		}
	})
}


exports.getLeadsAPI = function (req, res) {
	debug("ushakiz getLeadsAPI " + req);
	getLeads(req, function (body) {
		debug("ushakiz getLeadsAPI " + body);
		res.send(body);
	});
}

//Function to check last sync timestamp of salesforce with the created at timestamp of lead
// It give the leads whose timestamp is greater than of salesforce sync
var getLeadsForSync = function (requestData, callback) {
		debug("!!!!! Sanjeet getLeadsForSync requestData received is " + JSON.stringify(requestData));
		getMarketOwnerLeads(requestData.marketId, function (ourLeads) {
			//If last sync time available then do filteration using timestamp
			//else return the leads as it is.
			if (ourLeads && requestData.lastSyncTime) {
				var newLeadsForSync = [];

				for (var i in ourLeads) {
					var syncTimestamp = new Date(requestData.lastSyncTime).getTime() / 1000
					var leadTimestamp = new Date(ourLeads[i].created) / 1000
					var leadUpdateTimestamp = new Date(ourLeads[i].updated) / 1000
					var leadUpdateTimestamp = parseInt(leadUpdateTimestamp)
					var syncTimestamp = parseInt(syncTimestamp);
					var leadTimestamp = parseInt(leadTimestamp);

					if (leadUpdateTimestamp >  syncTimestamp && ourLeads[i].sfId) {
						newLeadsForSync.push(ourLeads[i]);
					} else if (leadTimestamp > syncTimestamp) {
						newLeadsForSync.push(ourLeads[i]);
					}

					 if (i == (ourLeads.length - 1)) {
						 debug("getLEadsForSYnc returned Data lastSyncNot Empty : newLeadsForSync" + JSON.stringify(newLeadsForSync))
		                 callback(newLeadsForSync);
					}
				}
			} else {
				callback(ourLeads, "");
			}
		})
}

/****
 * To prepare the data field according to the sales request
 ****/
var getNewLeadsReadyForSync = function(ourLeads, callback){
		var leadsToSync = [];
        if (ourLeads && (ourLeads.length != 0)){
		for(var i in ourLeads){
			if (ourLeads[i].employeesNumber != "" && ourLeads[i].designation) {
				var uniqueLead = {
					"method" : "PATCH",
					"url":"/services/data/v40.0/sobjects/Lead/External_ID_Email__c/"+ourLeads[i].email,
					"richInput":{
						"FirstName": ourLeads[i].firstName,
						"LastName": ourLeads[i].lastName,
						"Email": ourLeads[i].email,
						"Description": "No_of_Employees: " + ourLeads[i].employeesNumber + "\n Timezone: "+ ourLeads[i].timezone ,
						"Title": ourLeads[i].designation,
						"Company":"."
					}
				};
				leadsToSync.push(uniqueLead);
			}// Push the unique lead to array of unique leads
			else {
				var uniqueLead = {
					"method" : "PATCH",
					"url":"/services/data/v40.0/sobjects/Lead/External_ID_Email__c/"+ourLeads[i].email,
					"richInput":{
						"FirstName": ourLeads[i].firstName,
						"LastName": ourLeads[i].lastName,
						"Email": ourLeads[i].email,
						"Company":"."
					}
				};
				leadsToSync.push(uniqueLead);
			}
		}
        }
		callback(leadsToSync);
}


/**********************************************
 * To generate random id 
 *********************************/
var generateReferenceId = function(){
	return Math.floor(Math.random() * 100000000 + 99999999);
}

exports.sfSyncAPI = function (req, res) {
	getSFTokens(req, function (body) {
		var requestData = {
			"email": body.market.settings.owner.user,
			"marketId": req.headers['x-market-id'],
			"lastSyncTime":"",
			"deleteUponSync": body.market.settings.social.salesforce.deleteLeadUponSync,
			"sfAppCredentials": body.market.settings.social.salesforce,
			"uniqueLeads":"",
			"allLeadsWithoutBatches":"",
			"newLeadsSyncBatchId": ""
		};
		sfTokenMongo(requestData.email, function (sftokens) {
		 sfLastSyncTimestamp(requestData.email, function (lastSyncTime) { //get last sync event
				debug("lastSyncTime " + JSON.stringify(lastSyncTime));
				if(typeof lastSyncTime !== "undefined"){
					requestData.lastSyncTime = lastSyncTime;
				}
				debug("request data " + JSON.stringify(requestData));
				getLeadsForSync(requestData, function (newLeadsForSync) {
			    debug("sfAPISync leads for sync receied are ")
    			//For Syncing the New Leads
						getNewLeadsReadyForSync(newLeadsForSync, function(leadsToSync){
							sfRefreshToken(requestData, function(sftokens){
									if (leadsToSync.length != 0) {
										requestData.allLeadsWithoutBatches = leadsToSync;

										//New Leads Batch operation 
										createBatches(leadsToSync, function(leadBatches){
											for(var i in leadBatches){
												debug("!!!!!!! Sanjeet Lead Batches " + JSON.stringify(leadBatches[i]))
												requestData.uniqueLeads = leadBatches[i];
												requestData.newLeadsSyncBatchId = i;
												sfSyncNewLeads(requestData, sftokens, function(batchId){
													if( batchId == (leadBatches.length -1)){
														doSFEventSync(requestData, sftokens, res);
													}
												});
											}
										})
									}else{
										debug("Not lead data available to do contact sync operation, so now doing sync event");
										doSFEventSync(requestData, sftokens, res);
									}
							}, function(err){
								debug("sfSyncAPI: sfRefreshToken Error ", err)
								res.redirect('/market-admin#!/salesforce/crm')
							})
						
						})
				})
			})
		})
	})
}


var sfSyncNewLeads = function (requestData, sftokens, callback) {
	//Prepare Centact to send
	if (requestData.uniqueLeads){
		var sfInputData = { "batchRequests": requestData.uniqueLeads };
		var options = {
			method: 'POST',
			uri: sftokens.instance_url + "/services/data/v42.0/composite/batch",
			headers: {
				'Authorization': 'Bearer ' + sftokens.access_token
			},
			body: sfInputData,
			json: true // Automatically stringifies the body to JSON
		};

		rp(options).then(function (_res) {
			debug("sfSyncNewLeads success result is " , _res.results)
			requestData.sfSyncNewLeadsResults = _res.results;
			saveContactIdsInsideMongo(requestData, callback);

		}).catch(function (err) {
			debug("sfSyncNewLeads err " + JSON.stringify(err));
			callback(requestData.newLeadsSyncBatchId)
		});
    }
};



var sfRefreshToken = exports.sfRefreshToken = function (requestData, success, error) {  // Function connected with the express route as API
	/*************************************************************************
	 * Step 1: Get the refresh token from the mongo database
	 *      using the email id.
	 *
	 * Step 2: Get the cLient Id and Client Secret of the salesforce
	 *        using the Bootstrap public api of the wanteet using the marketId
	 *
	 * Step 3: Build the url having the query string to generate the new access token through post request.
	 *
	 * Step 4: The success callback function of the request promise (rp) will get the access_token and will update it 
	 *      in the mongo database.  
	 ************************************************************************/
		sfTokenMongo(requestData.email, function (sftokens) { //sftokens contain the refresh and access token earlier saved in our mongo
			var accessTokenGenURL = "https://login.salesforce.com/services/oauth2/token?refresh_token=" + sftokens.refresh_token + "&grant_type=refresh_token&client_id=" + requestData.sfAppCredentials.clientID + "&client_secret=" + requestData.sfAppCredentials.clientSecret;
			var options = {
				method: 'POST',
				uri: accessTokenGenURL,
				json: true,
			};
			rp(options).then(function (_success) {
			debug("Data refresh token function" + JSON.stringify(_success));
			User.findOneAndUpdate({ username: requestData.email }, //Update the Access Token Only into the Database
					{ $set: { "additionalProvidersData.sftokens.access_token": _success.access_token } }, function (err, doc) {
						debug("!!!!!!!!!!!!!!! Sanjeet inside sfRefreshToken err => " + JSON.stringify(err, "", 3));
					    debug("!!!!!!!!!!!!!!! Sanjeet inside sfRefreshToken doc => " + JSON.stringify(doc, "", 3));
						if (!err) {
							success({ "status": "success", "access_token":_success.access_token, "refresh_token": sftokens.refresh_token , "instance_url": sftokens.instance_url});	//Success Callback
						} else {

							error({ "status": "error" });  //Error Callback
						}
					});

			}, function (_error) {
				debug("!!!!!! Error Salesforce Refresh Token");
				debug(_error);
				error({ "status": "error" });  //Error Callback
			})
		});
}

/*************************************************************
 * To create batches for the leads and the events
 * **********************************************************/

var createBatches = function(syncItems, callback){
    var totalItems = syncItems.length;
    var syncBatches = [];
    debug('Inside the createBatches total items received for batch making', totalItems)
    if(totalItems <= 25){
        syncBatches.push(syncItems);
        debug("Inside the createBatches first block")
        callback(syncBatches);
    }else{
        var count = 0;
        var subBatch = []; //Will contains max 25 items
        debug("Inside the createBatches second block")

        // remainining items
        var remainder = totalItems % 25;
        var totalBatches = totalItems / 25;
        debug("Inside the createBatches totalBatches excluding remainder batch ", Math.floor(totalBatches))
        debug('Inside the createBatches items in last batch remainder batch', remainder)

        for(var i in syncItems){
            count++;
            subBatch.push(syncItems[i]);
            if(count == 25){
                syncBatches.push(subBatch); //push the batch of 25 items in the main syncBatches array
                subBatch = []; //reset the subBatch
                count = 0; //reset the count
            }
          
            if(i > ((syncItems.length - remainder)-1)){
                if(i == (syncItems.length-1)) {
                    syncBatches.push(subBatch)    
                }
            }
            
           
        }
        callback(syncBatches);
    }
}

/**************************************************
Sub-Function for the saveContactsIds for saving the
contact_ids 
**************************************************/
var saveContactIdsInsideMongo = function (requestData, callback) {
	var syncResults = requestData.sfSyncNewLeadsResults;
	var uniqueLeads =  requestData.uniqueLeads;
	if (requestData.uniqueLeads && requestData.sfSyncNewLeadsResults) {
		for (var i in requestData.uniqueLeads) {
			Lead.findOneAndUpdate({ "email": uniqueLeads[i].richInput.Email, "marketId": requestData.marketId}, { $set: { "sfId": syncResults[i].result.id } }, function (err, doc) {
				if(err){
					debug("saveContactIdsInsideMongo: Updating the sfId err => " + JSON.stringify(err, "", 3));
				}
			})
		}
	}
	callback(requestData.newLeadsSyncBatchId)
	
}


/**************************************************
For getting the leadEvents for further doing the 
sync to the crm
***************************************************/
		
var getLeadEvents = function (requestData, data) {
	var leadEventsToSync = []; // Master array to save all the leadEvents

			getMarketOwnerLeads(requestData.marketId, function (ourLeads) {
                if (ourLeads){
		
				for(var i = 0; i < ourLeads.length; i++){
					var leadEvents = ourLeads[i].events;
				     debug("!!!!!!!Sanjeet LeadEvent received is ", leadEvents)	
					 if (leadEvents  && leadEvents.length != 0){	
							var syncTimestamp = 0;

							if (requestData.lastSyncTime){
								syncTimestamp = new Date(requestData.lastSyncTime).getTime() / 1000
							}
						
						var leadEventTimestamp = new Date(leadEvents[0].createdAt) / 1000
						var leadEventUpdateTimestamp = new Date(leadEvents[0].updated) / 1000
						debug("keadEventTimestamp after unix conv function ", leadEventTimestamp)	
		    		    debug("Comparing " + leadEventTimestamp + " with " + syncTimestamp)
		
     		if ((parseInt(leadEventTimestamp) > parseInt(syncTimestamp)) || (parseInt(leadEventUpdateTimestamp) > parseInt(syncTimestamp))){

                                                                var leadEvent = {
   																		"attributes" : {"type" : "Event", "referenceId": ""+ generateReferenceId()},
                                                                        "StartDateTime": leadEvents[0].startDate,
                                                                        "EndDateTime": leadEvents[0].endDate,
                                                                        "Subject": leadEvents[0].eventTitle,
                                                                        "WhoId": ourLeads[i].sfId,
                                                                }
                                                                leadEventsToSync.push(leadEvent)
                                                        }
											}
					}
                }
		        debug("!!!!!! leadEventsToSync ", leadEventsToSync)
			   	data(leadEventsToSync)
			});
}


//FOr getting the all the lead Conversations from our database to Sync
var getLeadConversationsToSync = function (requestData, data) {
				getMarketOwnerLeads(requestData.marketId, function (ourLeads) {
                    if (ourLeads != null){
				        var leadConversations = [];
					    var i=0; 
					for (i=0; i < ourLeads.length; i++) {
						if (ourLeads[i]  && ourLeads[i].conversations) {
							var allConversationIds = ourLeads[i].conversations;
							var j=0;
							for (j=0; j < allConversationIds.length; j++) {
								if (allConversationIds[j].conversationId) {
									getConversationFromRasa(allConversationIds[j].conversationId, i, requestData.lastSyncTime, function (_i, _conv) {
										if (_conv && _conv.conversation != ""){
              
										   var eventData = {
											"attributes" : {"type" : "Event", "referenceId": ""+ generateReferenceId()},
									        "StartDateTime": _conv.first_conv_timestamp,
											"EndDateTime": _conv.last_conv_timestamp,
											"Subject": "Service conversation with " + ourLeads[_i].firstName +" " + ourLeads[_i].lastName + "("+ ourLeads[_i].email +")",
											"Description": _conv.conversation,
											"WhoId": ourLeads[_i].sfId,
											"IsAllDayEvent": false
											
										    };
										    leadConversations.push(eventData);
										}
									});
									getConversationFromRasaUI(allConversationIds[j].conversationId, i, requestData.lastSyncTime, function (_i, _conv) {
										if (_conv && _conv.conversation != ""){
										    var eventData = {
											"attributes" : {"type" : "Event", "referenceId": ""+ generateReferenceId()},
									       	"StartDateTime": _conv.first_conv_timestamp,
											"EndDateTime": _conv.last_conv_timestamp,
											"Subject": "FAQ conversation with " + ourLeads[_i].firstName +" " + ourLeads[_i].lastName + "("+ ourLeads[_i].email +")",
											"Description": _conv.conversation,
											"WhoId": ourLeads[_i].sfId,
											"IsAllDayEvent": false
	
										    };
                                            leadConversations.push(eventData);
										}
									}); 
								}
							}

						}
						if (i == (ourLeads.length - 1)) {
							setTimeout(function () {
								data(leadConversations);
							}, 3000);
						}

					}
		}
	});
}


/**
 *Note:  conversationId is senderId for the below function
 * **/
var getConversationFromRasa = function (senderId, i, last_sync_timestamp, data) {
	debug("!!!!! Sanjeet Get COnverastion From Rasa " + senderId +" values of i is:  " + i + " "+ last_sync_timestamp);
	var options = {
		method: 'POST',
		uri: 'https://faq.engine.247jeeves.com/conversations/' + senderId,
		body:{
			"last_sync_timestamp": last_sync_timestamp
		},
		json: true
	}

	rp(options).then(function (_res) {
		debug("!!!!! Sanjeet conversation from rasa data" + JSON.stringify(_res.result))
		if (_res && (_res.status == 'success')) {
			data(i, _res.result);
		} else {
			data(i, null);
		}
	});

}


/**
 *Note:  conversationId is senderId for the below function, i is the for loop iterator to keep track of leads data
 * **/
var getConversationFromRasaUI = function (senderId, i, last_sync_timestamp, dataCallback) {
	debug("!!!!! Sanjeet Get COnverastion From RasaUI" + senderId +" values of i is:  " + i + " "+ last_sync_timestamp);
	var options = {
		method: 'POST',
		uri: 'https://faq.engine.247jeeves.com/rasaUIConversations/'+senderId,
		body:{
			"last_sync_timestamp": last_sync_timestamp
		},
		json: true
	}

	rp(options).then(function (_res) {
		debug("!!!!! Sanjeet conversation from rasaui data" + JSON.stringify(_res))
		if (_res.status == 'success') {
            dataCallback(i, _res.result);
        } else {
            dataCallback(i, null);
        }
	});

}

/**************************************************
For doing the synchronization of the contact events
using the sfIds of the contacts
**************************************************/

var sfSyncLeadEvents = function (requestData, sftokens, callback) {
		var sfInputData = { "records": requestData.leadEvents };
		debug("sfSyncLeadEvents data " + JSON.stringify(sfInputData));
		var options = {
			method: 'POST',
			uri: sftokens.instance_url + '/services/data/v34.0/composite/tree/Event/',
			headers: {
				'Authorization': 'Bearer ' + sftokens.access_token
			},
			body: sfInputData,
			json: true // Automatically stringifies the body to JSON
		};

		rp(options).then(function (_res) {
			callback(requestData.leadEventsBatchId)
		}).catch(function (err) {
			debug("sfSyncLeadEvents Error - " + JSON.stringify(err));
			debug("sfSyncLeadEvents Error batchId " + requestData.leadEventsBatchId)
			callback(requestData.leadEventsBatchId)
		});
}


/**************************************************
 * For Event Synchronization
 * **********************************************/

var doSFEventSync = function(requestData, sftokens, res){
    getLeadEvents(requestData, function (leadEvents) {
        debug("lead events for sync before pushing conversations into it ", leadEvents);
    	getLeadConversationsToSync(requestData, function (leadConversations) {
       	debug("!!!!! Sanjeet allleadConversations are ", leadConversations)

        if ((leadConversations) && (leadConversations.length != 0)){
            for (var i in leadConversations) {
                leadEvents.push(leadConversations[i]);
            }
        }
		debug("Events for syc after pushing conversations are : " + JSON.stringify(leadEvents));

			//New Leads Batch operation 
			if(leadEvents.length != 0){
				createBatches(leadEvents, function(leadBatches){
					for(var i in leadBatches){
							requestData.leadEvents = leadBatches[i];
							requestData.leadEventsBatchId = i;
							sfSyncLeadEvents(requestData, sftokens, function(batchId){
										if( batchId == (leadBatches.length -1)){
												//Update timestamp    
											var timeNow = moment().format('lll');
											saveSFLastSyncTimestamp(requestData.email, timeNow, function(_res){
													if(_res && (_res.status == "error")){
														debug("sfSyncLeadEvents savetimestamp error");
													}
											});
		
											debug("deleteUponSync value is ", requestData.deleteUponSync);
											if(requestData.deleteUponSync){
													debug("Salesforce:- Delete Upon Sync: true");
													deleteLeadsAfterSync(requestData.allLeadsWithoutBatches, requestData.marketId, function(callback){
													debug("deleteUponSync Enabled - Leads Deleted Successfully!");
												});
											}
										}
							});
						}
						res.redirect('/market-admin#!/salesforce/crm');
					})
			}else{
				res.redirect('/market-admin#!/salesforce/crm');
			}
		

           
        });
    });
}
