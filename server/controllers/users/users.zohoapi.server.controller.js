'use strict';

/**
 * Module dependencies.
 */
var _ = require('lodash'),
	errorHandler = require('../errors.server.controller'),
	mongoose = require('mongoose'),
	request = require('request'),
	url = require('url'),
	fs = require('fs'),
	querystring = require('querystring'),
	rp = require('request-promise'),
	config = require('../../../config/config'),
	User = mongoose.model('User'),
	moment = require('moment-timezone'),
	Lead = mongoose.model('Lead');

var debug = require('debug')('wanteet|zoho');
/**********************************
* To get the bootstrap information of the market
* using the marketId from the req to further get the
* social Token for Zoho
************************/

var getZohoTokens = function (req, callback) {

	var options = {
		method: 'GET',
		uri: config.inchefs.publicUrl + '/bootstrap/' + req.headers['x-market-id'],
		json: true,
	};
	rp(options).then(function (body) {
		debug(req.headers['x-market-id']);
		callback(body);
	});

}

/******************************************************
*To trigger the Zoho OAuth2 Authorization for access to
*there Zoho CRM Panel
**************************/
exports.zohoAuthCrm = function (req, res) {
	//Get Zoho Id, Client Secret , CallBack URL
	getZohoTokens(req, function (body) {
		var zoho = body.market.settings.social.zoho;
		var authURL = "https://accounts.zoho.com/oauth/v2/auth?scope=ZohoCRM.modules.ALL,ZohoCRM.settings.ALL"
			+ "&client_id=" + zoho.clientID + "&response_type=code&prompt=consent&access_type=offline&redirect_uri=" + zoho.callbackURL;
		res.redirect(authURL);
	})

}


/*****************************************************
 *To save the zoho accessToken and refreshTOken into the 
 * mongo db of the admin of  service center
 *************************************************/
exports.zohoAuthCrmCallback = function (req, res) {
	var fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl; //Get the full url 
	var parsedUrl = url.parse(fullUrl); // Parse the data received as query string including the grant code / authorization code
	var parsedQs = querystring.parse(parsedUrl.query); // Get the Query String as Array
	var grantCode = parsedQs.code;

	getZohoTokens(req, function (body) {
		var zoho = body.market.settings.social.zoho;
		debug(zoho);
		var marketAdminEmail = body.market.settings.owner.user;
		var postUrl = "https://accounts.zoho.com/oauth/v2/token?code=" + grantCode + "&redirect_uri=" + zoho.callbackURL + "&client_id=" + zoho.clientID + "&client_secret=" + zoho.clientSecret + "&grant_type=authorization_code"; //Prepare the Url for getting the Access & Refresh Token
		debug(postUrl);
		var options = {
			method: 'POST',
			url: postUrl,
			json: true,
		};
		rp(options).then(function (_body) {
			debug(JSON.stringify(_body));
			if (_body.refresh_token != null) {
				var dataToSave = { "access_token": _body.access_token, "refresh_token": _body.refresh_token };
				User.findOneAndUpdate({ username: marketAdminEmail }, //Update Both Access and Refresh Token  into the Database
					{ $set: { "additionalProvidersData.zohotokens": dataToSave } }, function (err, doc) {
						debug("247jeeves error => " + JSON.stringify(err, "", 3));
						res.redirect('/market-admin#!/emailer/zohoCRM');
					});
			} else {
				User.findOneAndUpdate({ username: marketAdminEmail }, //Update the Access Token Only into the Database
					{ $set: { "additionalProvidersData.zohotokens.access_token": _body.access_token } }, function (err, doc) {
						debug("247jeeves err => " + JSON.stringify(err, "", 3));
						res.redirect('/market-admin#!/emailer/zohoCRM');
					});

			}
		});
	});
}



//To get the last sync timestamp of the zoho synchronization

var zohoLastSyncTimestamp = function (email, callback) {
	User.find({ "username": email }, function (err, doc) {
		if(err){
			debug("247jeeves error - zoho last sync timestamp ")
			debug(err)	
		}
		debug(doc);
		callback(doc[0].additionalProvidersData.last_zoho_sync_event);
	});
};

var saveZohoLastSyncTimestamp = function(email, timestamp, callback){
	User.findOneAndUpdate({ "username": email }, {
		$set : {"additionalProvidersData.last_zoho_sync_event": timestamp}}, 
		function (err, doc) {
			if(err){
			    debug("247jeeves error - zoho save timestamp"+ JSON.stringify(err))
				callback({"status": "error"})
			}else{
				callback({"status":"success"})
			}
	});
}


var zohoTokenMongo = function (email, callback) {
	User.find({ "username": email }, function (err, doc) {
		debug(doc[0]);
		callback(doc[0].additionalProvidersData.zohotokens);
	});
};


var getLeads = function (req, callback) {
	Lead.find({ "marketId": req.headers['x-market-id'] }, function (err, result) {
		callback(result);
	})
}

var getMarketOwnerLeads = function (marketId, callback) {
	Lead.find({ "marketId": marketId }, function (err, result) {
		debug("getMarketOwner leads result" + JSON.stringify(result))
		if(result){
			callback(result);
		}else{
			callback(null)
		}
	})
}


exports.getLeadsAPI = function (req, res) {
	debug("ushakiz getLeadsAPI " + req);
	getLeads(req, function (body) {
		debug("ushakiz getLeadsAPI " + body);
		res.send(body);
	});
}

/****************************
 * To delete the data complete of the lead
 *  under GDPR policy
 *  *************************/

exports.deleteLead = function(req, res){

// Step-1 Delete all the conversations from the faq and service bot.
// Step -2 Delete the lead from the service center owner lead collection.
	
	var marketId = req.body.marketId;
	var email = req.body.email;

	debug("deleteLEad email received to delete is ", email);
	debug("deleteLead market received is ", marketId);

   Lead.find({"email": email, "marketId":marketId},  function (err, lead){
   		debug("lead is ", lead[0])	
	    var conversations = lead[0].conversations;
	    debug("conversations are "+ JSON.stringify(conversations));
	    for(var i=0; i < conversations.length; i++){
				if(conversations[i]){
					deleteConversation(conversations[i].conversationId);
				}
		}
		Lead.find({"email": email, "marketId":marketId}).remove().exec();
		res.send({"status":"success"});
  });
}


var deleteConversation = function(sender_id){
   var options = {
        method: 'DELETE',
        uri: 'https://faq.engine.247jeeves.com/conversations/' + sender_id,
        json: true,
     };

    rp(options);
}



var deleteLeadsAfterSync = function(leads, marketId, callback){
    debug("deleteLeadsUponSync leads are ", leads)
    debug("deleteLeadsUponSync length of leads", leads.length)
	for (var i=0; i < (leads.length); i++){
	 	 debug("deleteUponSync lead using i: " + i, leads[i] )
		 Lead.find({"email": leads[i].Email, "marketId":marketId},  function (err, lead){
		 	var conversations = lead[0].conversations;
			 debug("conversations are "+ JSON.stringify(conversations));
		     for(var j=0; j < conversations.length; j++){
     		  	 if(conversations[j]){
	        		   deleteConversation(conversations[j].conversationId);
	       		}
	   		 }
			debug("deleteUponSync lead using i: " + i, leads[i] )
	     });
         Lead.find({"email": leads[i].Email, "marketId":marketId}).remove().exec();
    }//end for loop
    callback({"status":"success"});
}


/*****************************
* To save the contact Information in 
* the mongo database
******************************/
var saveLead = function (req, res) {
	var contact = new Lead({
		firstName: req.body.first_name,
		lastName: req.body.last_name,
		email: req.body.email,
		marketId: req.body.marketId,
		timezone: req.body.timezone,
	});
    /*
	debug("LEad details are: ", {
                firstName: req.body.first_name,
                lastName: req.body.last_name,
                email: req.body.email,
                marketId: req.body.marketId,
				timezone: req.body.timezone,
				conversations : { "conversationId": req.body.conversationId }
        });

	*Working of this function
	 1. Check for the email already exist for the owner or not.
						Case: If not exist
			Then create a new lead and on success further add the conversation id, else show error
			Case: If exist
		 Then simply look for the conversation id for the same lead email exist or not
								 Case: If conversationId exist for the leadEmail
			 do nothing simply show response to user data already exist
								 Case: If conversationId don't exist
												 add new conversationId into the the lead record.
	* */

Lead.findOne({ "email": req.body.email, "marketId": req.body.marketId }, function (err, lead) {
	
	if(err){
		res.status(500).send({"status":"error", "description": "Database error occured"})
    }

   	Lead.findOne({"conversations.conversationId": req.body.conversationId}, function(err, conversation){
	
	    if(err){
          res.status(500).send({"status":"error", "description": "Database error occured"})
        }


		if (conversation && lead){
		  res.status(500).send({"status":"error", "description":"conversationId and email already exist"})
		}
		else if (conversation != null && lead == null){
			res.status(406).send({"status":"error", "description":"conversationId already exist"})
		}
		else if (lead == null && conversation == null) { //If No Data Exist
			contact.save(function (err, result) {
				if (err) {
					debug("Wanteet error - Inside the the contact save")
					debug(err);
					res.json({ "status": "error" });
				}
				else {
					debug(result);
					Lead.findOneAndUpdate({ "email": req.body.email, "marketId": req.body.marketId },
						{ $push: { "conversations": { "conversationId": req.body.conversationId } } }, function (_err, _doc) {
					        debug(_doc);
							if (_doc) {
								res.status(200).send({ "status": "success", "message": "New Lead added with conversation id" });
							}else {
							    debug("Wanteet error - inside the findOne and Update the conversationId after save contactId")
						       	debug(_err);
								res.status(500).send({"status":"error"});
							}
						});
				}
			});
		} else { //Lead not equal to null and conversation is null (new one)
			Lead.findOne({ "email": req.body.email, "marketId": req.body.marketId, "conversations.conversationId": req.body.conversationId },
				function (err, doc) {
					debug(err);
					if (err) {
						debug("!!!!! Wanteet error - - update the conversationId" + JSON.stringify(err));
						res.status(500).json({ "status": "error", "description": "error while finding conversationId" });
					} else {
						if (doc == null) {
							var conversationData = { "conversationId": req.body.conversationId }
							Lead.update({ "email": req.body.email, "marketId": req.body.marketId },
								{ $set:{"firstName": req.body.first_name, "lastName": req.body.last_name}, $push: { "conversations": conversationData } }, function (_err, _doc) {
									debug(_doc);
									debug(_err);
									if (doc) {
										res.status(200).send({ "status": "success", "message": "updated the conversation id" });
									} else {
										res.status(500).send({ "status": "error" });
									}
								}); //End query 
						} else {
							res.status(500).json({ "status": "error", "description": "lead email already exist" });
						}
					} //else
				});
		}//else
	     });
	});
}





/****************************************
 *Save the lead details
 ***************************************/
exports.updateLead = function (req, res) {
	var leadEmail = req.body.email;
	var empNumber = req.body.empNum;
	var empDesignation = req.body.empDesignation;
	var marketId = req.body.marketId;
	Lead.findOneAndUpdate({ "email": leadEmail, "marketId": marketId },
		{ $set: { "employeesNumber": empNumber, "designation": empDesignation, "updated": Date.now() } }, function (err, doc) {
			debug(doc);
			debug(err);
			if (doc) {
				res.status(200).send({ "status": "success", "message": "Lead details updated" });
			} else {
				res.status(500).send(err);
			}
		});
}


/****************************************
 *Update the lead event details
 ***************************************/
exports.updateLeadEvents = function (req, res) {
        var leadEmail = req.body.email;
        var startDate = req.body.startDate;
        var endDate = req.body.endDate;
        var eventTitle = req.body.eventTitle;
        var marketId = req.body.marketId;
        var timezone = req.body.timezone;
        //Check whether the startDateTime is not in the past (less than current users time)
        var startDateUTC = parseInt((new Date(startDate).getTime() / 1000).toFixed(0))
        debug("Timezone received is ", timezone)
        var timeNow = moment.tz(timezone).format();
        debug("Timenow is ", timeNow)
        var timeNowUTC =  (new Date(timeNow).getTime() / 1000)
        var timeDifference = (startDateUTC - timeNowUTC)
        debug("Current Date is ",timeNowUTC)
        debug("Event Date is ", startDateUTC)
        debug("time difference is ", timeDifference)
        if(timeDifference > 3600){
                Lead.findOneAndUpdate({ "email": leadEmail, "marketId": marketId },
                        { $set: { "events": { "startDate": startDate, "endDate": endDate, "eventTitle": eventTitle, "updated": Date.now() } } }, function (err, doc) {
                                debug(doc);
                                debug(err);
                                if (doc) {
                                        res.status(200).send({ "status": "success", "message": "Lead Event details updated" });
                                } else {
                                        res.status(500).send(err);
                                }
                        });
        } else{
                res.status(406).send({"status":"error", "message":"Event datetime difference should be greater than 24hrs than current users time"})   
        }
}

/***************************************
 *For checking zohotoken exist inside mongodb (permissions exist)
 * ****************************************/
exports.needZohoAuth = function (req, res) {
	var username = req.body.username;
	zohoTokenMongo(username, function (zohotokens) {
		if (zohotokens) {
			res.send({ "status": "success" });
		} else {
			res.send({ "status": "error" });
		}
	})
}


//Function to check last sync timestamp of zoho with the created at timestamp of lead
// It give the leads whose timestamp is greater than of zoho sync
var getLeadsForSync = function (requestData, callback) {
		debug("!!!!! Sanjeet getLeadsForSync requestData received is " + JSON.stringify(requestData));
		getMarketOwnerLeads(requestData.marketId, function (ourLeads) {
			//If last sync time available then do filteration using timestamp
			//else return the leads as it is.
			if (ourLeads && requestData.lastSyncTime) {
				var newLeadsForSync = [];
				for (var i in ourLeads) {
					var syncTimestamp = new Date(requestData.lastSyncTime).getTime() / 1000
					var leadTimestamp = new Date(ourLeads[i].created) / 1000
					var leadUpdateTimestamp = new Date(ourLeads[i].updated) / 1000
					var leadUpdateTimestamp = parseInt(leadUpdateTimestamp)
					var syncTimestamp = parseInt(syncTimestamp);
					var leadTimestamp = parseInt(leadTimestamp);
					debug("comparing if" + leadTimestamp + "  >  " + syncTimestamp)
				
			    	if (leadUpdateTimestamp >  syncTimestamp && ourLeads[i].zohoId) {
                         newLeadsForSync.push(ourLeads[i]);
                    } else if (leadTimestamp > syncTimestamp) {
						newLeadsForSync.push(ourLeads[i]);
					}

					 if (i == (ourLeads.length - 1)) {
						 debug("getLEadsForSYnc returned Data lastSyncNot Empty : newLeadsForSync" + JSON.stringify(newLeadsForSync))
		                 callback(newLeadsForSync);
					}
				}
			} else {
				callback(ourLeads, "");
			}
		})
}


/*************************************************************
 * To create batches for the leads and the events
 * **********************************************************/

var createBatches = function(syncItems, callback){
    var totalItems = syncItems.length;
    var syncBatches = [];
    debug('Inside the createBatches total items received for batch making', totalItems)
    if(totalItems <= 100){
        syncBatches.push(syncItems);
        debug("Inside the createBatches first block")
        callback(syncBatches);
    }else{
        var count = 0;
        var subBatch = []; //Will contains max 100 items
        debug("Inside the createBatches second block")

        // remainining items
        var remainder = totalItems % 100;
        var totalBatches = totalItems / 100;
        debug("Inside the createBatches totalBatches excluding remainder batch ", Math.floor(totalBatches))
        debug('Inside the createBatches items in last batch remainder batch', remainder)

        for(var i in syncItems){
            count++;
            subBatch.push(syncItems[i]);
            if(count == 100){
                syncBatches.push(subBatch); //push the batch of 100 items in the main syncBatches array
                subBatch = []; //reset the subBatch
                count = 0; //reset the count
            }
          
            if(i > ((syncItems.length - remainder)-1)){
                if(i == (syncItems.length-1)) {
                    syncBatches.push(subBatch)    
                }
            }
            
           
        }
        callback(syncBatches);
    }
}

/****
 * To prepare the data field according to the zoho request
 ****/
var getNewLeadsReadyForSync = function(ourLeads, callback){
		var leadsToSync = [];
        if (ourLeads && (ourLeads.length != 0)){
		for(var i in ourLeads){
			if (ourLeads[i].employeesNumber != "" && ourLeads[i].designation != "" ) {
				var uniqueLead = {
					"First_Name": ourLeads[i].firstName,
					"Last_Name": ourLeads[i].lastName,
					"Email": ourLeads[i].email,
					"Description": "No_of_Employees: " + ourLeads[i].employeesNumber+ "\n Timezone: "+ ourLeads[i].timezone ,
					"Designation": ourLeads[i].designation
				};
				leadsToSync.push(uniqueLead);
			}// Push the unique lead to array of unique leads
			else {
				var uniqueLead = {
					"First_Name": ourLeads[i].firstName,
					"Last_Name": ourLeads[i].lastName,
					"Email": ourLeads[i].email,
				};
				leadsToSync.push(uniqueLead);
			}
		}
        }
		callback(leadsToSync);
}

exports.zohoSyncAPI = function (req, res) {
	getZohoTokens(req, function (body) {
		var requestData = {
			"email": body.market.settings.owner.user,
			"marketId": req.headers['x-market-id'],
			"deleteUponSync": body.market.settings.social.zoho.deleteLeadUponSync,
			"lastSyncTime":"",
			"zohoAppCredentials": body.market.settings.social.zoho,
			"allLeadsWithoutBatches":"",
			"newLeadsSyncResponse":"",
			"newLeadsForSync":"",
			"newLeadsSyncBatchId": ""
		};
		zohoTokenMongo(requestData.email, function (zohotokens) {
		 zohoLastSyncTimestamp(requestData.email, function (lastSyncTime) { //get last sync event
				debug("lastSyncTime " + JSON.stringify(lastSyncTime));
				if(typeof lastSyncTime !== "undefined"){
					requestData.lastSyncTime = lastSyncTime;
				}
				debug("request data " + JSON.stringify(requestData));
				getLeadsForSync(requestData, function (newLeadsForSync) {
					    debug("zohoAPISync leads for sync receied are ")
						//For Syncing thie New Leads
						getNewLeadsReadyForSync(newLeadsForSync, function(leadsToSync){
						requestData.allLeadsWithoutBatches = leadsToSync;
     					zohoRefreshToken(requestData, function(zohotokens){
					     	if (leadsToSync.length != 0) {
								debug("Leads received for sync are: " + JSON.stringify(leadsToSync));

      							//New Leads Batch operation 
								createBatches(leadsToSync, function(leadBatches){
									for(var i in leadBatches){
									    requestData.newLeadsForSync = leadBatches[i];
										requestData.newLeadsSyncBatchId = i;
										zohoSyncLeads(requestData, zohotokens, function(batchId){
											
											if( batchId == (leadBatches.length -1)){
												doZohoEventSync(requestData, zohotokens, res);
											}

										});
									}

								})
							}else{
							    debug("Not lead data available to do contact sync operation, so now doing sync event");
								doZohoEventSync(requestData, zohotokens, res);
							}

						}, function(refreshErr){
							debug("Inside zohoSyncAPI refresh Token Err")
						    res.redirect('/market-admin#!/emailer/zohoCRM')
						})					
					})
				})
			})
		})	 
	})
}


var zohoSyncLeads = function (requestData, zohotokens, callback) {
	//Prepare Centact to send
	if (requestData.newLeadsForSync){
	
		debug("Inside zohoSyncLeads BatchIds is ", requestData.newLeadsSyncBatchId);

	    var uniqueLeads = requestData.newLeadsForSync;
		var zohoInputData = { "data": uniqueLeads,"duplicate_check_fields":["Email"], "trigger": "approval" };
		var options = {
			method: 'POST',
			uri: 'https://www.zohoapis.com/crm/v2/Leads/upsert',
			headers: {
				'Authorization': 'Zoho-oauthtoken ' + zohotokens.access_token
			},
			body: zohoInputData,
			json: true // Automatically stringifies the body to JSON
		};

	rp(options).then(function (_res) {
		requestData.newLeadsSyncResponse = _res.data; //For saving the zohoLead Ids without making get calls
		debug("zohoSyncLeads response is" + JSON.stringify(_res.data, "" , 3));
		zohoSaveLeadIds(zohotokens, requestData, callback);

	}).catch(function (err) {
		debug(err);
		debug("!!!! zohoSyncLeads function err");
	    res.redirect('/market-admin#!/emailer/zohoCRM');
	});
    }
};


/******************************************************
Function for saving LeadsIDs from the response of sync or
for saving leadIds in our mongo database for further
doing the event synchronization
*******************************************************/
var zohoSaveLeadIds = function (zohotokens, requestData, callback) {
	
	var leads = requestData.newLeadsForSync; //Leads that already got synced up
    var syncResponse = requestData.newLeadsSyncResponse; 

	for(var i in leads){
			Lead.findOneAndUpdate({ "email": leads[i].Email, "marketId": requestData.marketId},
			 { $set: { "zohoId": syncResponse[i].details.id } }, function (err, doc) {
					if(err){
					  debug("zohoSaveLeadIds Updating the zohoId err => " + JSON.stringify(err, "", 3));
					}
					  debug("zohoSaveLeadIds Updating the zohoId success => " + JSON.stringify(doc, "", 3));
			})
    }
	callback(requestData.newLeadsSyncBatchId)

}



/**************************************************
 * For Event Synchronization
 * **********************************************/

var doZohoEventSync = function(requestData, zohotokens, res){
    getLeadEvents(requestData, function (leadEvents) {
        debug("lead events for sync before pushing conversations into it ", leadEvents);
    	getLeadConversationsToSync(requestData, function (leadConversations) {
			
        	debug("!!!!! Sanjeet allleadConversations are ", leadConversations)

            if ((leadConversations) && (leadConversations.length != 0)){
              for (var i in leadConversations) {
                 leadEvents.push(leadConversations[i]);
              }
            }
	
	            debug("Events for syc after pushing conversations are : " + JSON.stringify(leadEvents));
       			debug("doZohoEventSync before zohoSyncLeadEvents")

		
			 //New Leads Batch operation 
                createBatches(leadEvents, function(leadBatches){
                     for(var i in leadBatches){
                            requestData.leadEvents = leadBatches[i];
                            requestData.leadEventsBatchId = i;
                            zohoSyncLeadEvents(requestData, zohotokens, function(batchId){
                                         if( batchId == (leadBatches.length -1)){
			 									//Update timestamp    
										     var timeNow = moment().format('lll');
										     saveZohoLastSyncTimestamp(requestData.email,timeNow, function(_res){
							              		  if(_res && (_res.status == "error")){
								                      debug("zohoSyncLeadEvents savetimestamp error");
        		        						   }
								     	     });
	
										     debug("deleteUponSync value is ", requestData.deleteUponSync);
										     if(requestData.deleteUponSync){
							            		  debug("!!!!! Sanjeet Delete Upon Sync true");
									              deleteLeadsAfterSync(requestData.allLeadsWithoutBatches, requestData.marketId, function(callback){
							         		        debug("deleteUponSync Enabled - Leads Deleted Successfully!");
									             });
							        		 }
                                          }
                             });
                      }
					res.redirect('/market-admin#!/emailer/zohoCRM');
                })



        });
    });
}




/**************************************************
For getting the leadEvents for further doing the 
sync to the crm
***************************************************/
		
var getLeadEvents = function (requestData, data) {
	var leadEventsToSync = []; // Master array to save all the leadEvents

			getMarketOwnerLeads(requestData.marketId, function (ourLeads) {
                if (ourLeads){
		
   				for(var i = 0; i < ourLeads.length; i++){
					var leadEvents = ourLeads[i].events;
				     debug("!!!!!!!Sanjeet LeadEvent received is ", leadEvents)	
					 if (leadEvents  && leadEvents.length != 0){	
							var syncTimestamp = 0;

							if (requestData.lastSyncTime){
								syncTimestamp = new Date(requestData.lastSyncTime).getTime() / 1000
							}
						
						var leadEventTimestamp = new Date(leadEvents[0].createdAt) / 1000
						var leadEventUpdateTimestamp = new Date(leadEvents[0].updated) / 1000
						debug("keadEventTimestamp after unix conv function ", leadEventTimestamp)	
		    		    debug("Comparing " + leadEventTimestamp + " with " + syncTimestamp)
		
     		if ((parseInt(leadEventTimestamp) > parseInt(syncTimestamp)) || (parseInt(leadEventUpdateTimestamp) > parseInt(syncTimestamp))){

                                                                var leadEvent = {
                                                                        "Start_DateTime": moment(leadEvents[0].startDate).format(),
                                                                        "End_DateTime": moment(leadEvents[0].endDate).format(),
                                                                        "Event_Title": leadEvents[0].eventTitle,
                                                                        "What_Id": ourLeads[i].zohoId,
                                                                        "$se_module": "Leads"
                                                                }
                                                                leadEventsToSync.push(leadEvent)
                                                        }
											}
					}
                }
		        debug("!!!!!! leadEventsToSync ", leadEventsToSync)
			   	data(leadEventsToSync)
			});
}


//FOr getting the all the lead Conversations from our database to Sync
var getLeadConversationsToSync = function (requestData, data) {
				getMarketOwnerLeads(requestData.marketId, function (ourLeads) {
                    if (ourLeads != null){
				        var leadConversations = [];
					    var i=0; 
					for (i=0; i < ourLeads.length; i++) {
						if (ourLeads[i]  && ourLeads[i].conversations) {
							var allConversationIds = ourLeads[i].conversations;
							var j=0;
							for (j=0; j < allConversationIds.length; j++) {
								if (allConversationIds[j].conversationId) {
									getConversationFromRasa(allConversationIds[j].conversationId, i, requestData.lastSyncTime, function (_i, _conv) {
										if (_conv && _conv.conversation != ""){
              
										   var eventData = {
									        "Start_DateTime": moment(_conv.first_conv_timestamp).format(),
											"End_DateTime": moment(_conv.last_conv_timestamp).format(),
											"Event_Title": "Service conversation with " + ourLeads[_i].firstName +" " + ourLeads[_i].lastName + "("+ ourLeads[_i].email +")",
											"Description": _conv.conversation,
											"What_Id": ourLeads[_i].zohoId,
											"$se_module": "Leads"
										    };
										    leadConversations.push(eventData);
										}
									});
									getConversationFromRasaUI(allConversationIds[j].conversationId, i, requestData.lastSyncTime, function (_i, _conv) {
										if (_conv && _conv.conversation != ""){
										    var eventData = {
									        "Start_DateTime": moment(_conv.first_conv_timestamp).format(),
											"End_DateTime": moment(_conv.last_conv_timestamp).format(),
											"Event_Title": "FAQ conversation with " + ourLeads[_i].firstName +" " + ourLeads[_i].lastName + "("+ ourLeads[_i].email +")",
											"Description": _conv.conversation,
											"What_Id": ourLeads[_i].zohoId,
											"$se_module": "Leads"
										    };
                                            leadConversations.push(eventData);
										}
									}); 
								}
							}

						}
						if (i == (ourLeads.length - 1)) {
							setTimeout(function () {
								data(leadConversations);
							}, 3000);
						}

					}
		}
	});
}


/**
 *Note:  conversationId is senderId for the below function
 * **/
var getConversationFromRasa = function (senderId, i, last_sync_timestamp, data) {
	debug("!!!!! Sanjeet Get COnverastion From Rasa " + senderId +" values of i is:  " + i + " "+ last_sync_timestamp);
	var options = {
		method: 'POST',
		uri: 'https://faq.engine.247jeeves.com/conversations/' + senderId,
		body:{
			"last_sync_timestamp": last_sync_timestamp
		},
		json: true
	}

	rp(options).then(function (_res) {
		debug("!!!!! Sanjeet conversation from rasa data" + JSON.stringify(_res.result))
		if (_res && (_res.status == 'success')) {
			data(i, _res.result);
		} else {
			data(i, null);
		}
	});

}


/**
 *Note:  conversationId is senderId for the below function, i is the for loop iterator to keep track of leads data
 * **/
var getConversationFromRasaUI = function (senderId, i, last_sync_timestamp, dataCallback) {
	debug("!!!!! Sanjeet Get COnverastion From RasaUI" + senderId +" values of i is:  " + i + " "+ last_sync_timestamp);
	var options = {
		method: 'POST',
		uri: 'https://faq.engine.247jeeves.com/rasaUIConversations/'+senderId,
		body:{
			"last_sync_timestamp": last_sync_timestamp
		},
		json: true
	}

	rp(options).then(function (_res) {
		debug("!!!!! Sanjeet conversation from rasaui data" + JSON.stringify(_res))
		if (_res.status == 'success') {
            dataCallback(i, _res.result);
        } else {
            dataCallback(i, null);
        }
	});

}
/**************************************************
For doing the synchronization of the contact events
using the zohoIds of the contacts
**************************************************/

var zohoSyncLeadEvents = function (requestData, zohotokens, callback) {
		var zohoInputData = { "data": requestData.leadEvents };
		debug("Inside zohoSyncLeadEvents data is " + JSON.stringify(zohoInputData));
		var options = {
			method: 'POST',
			uri: 'https://www.zohoapis.com/crm/v2/Events',
			headers: {
				'Authorization': 'Zoho-oauthtoken ' + zohotokens.access_token
			},
			body: zohoInputData,
			json: true // Automatically stringifies the body to JSON
		};

		rp(options).then(function (_res) {
			debug("Inside zohoSyncLeadEvents success batchId" + requestData.leadEventsBatchId)
			callback(requestData.leadEventsBatchId)
		}).catch(function (err) {
			debug("Inside zohoSyncLeadEvents error trace code is " + err.statusCode);
			debug("zohoSyncLeadEvents error batchId " + requestData.leadEventsBatchId)
			callback(requestData.leadEventsBatchId)
		});
}



var zohoRefreshToken = exports.zohoRefreshToken = function (requestData, success, error) {  // Function connected with the express route as API
	/*************************************************************************
	 * Step 1: Get the refresh token from the mongo database
	 *      using the email id.
	 *
	 * Step 2: Get the cLient Id and Client Secret of the zoho
	 *        using the Bootstrap public api of the wanteet using the marketId
	 *
	 * Step 3: Build the url having the query string to generate the new access token through post request.
	 *
	 * Step 4: The success callback function of the request promise (rp) will get the access_token and will update it 
	 *      in the mongo database.  
	 ************************************************************************/
		zohoTokenMongo(requestData.email, function (zohotokens) { //zohotokens contain the refresh and access token earlier saved in our mongo
			var accessTokenGenURL = "https://accounts.zoho.com/oauth/v2/token?refresh_token=" + zohotokens.refresh_token + "&grant_type=refresh_token&client_id=" + requestData.zohoAppCredentials.clientID + "&client_secret=" + requestData.zohoAppCredentials.clientSecret;
			var options = {
				method: 'POST',
				uri: accessTokenGenURL,
				json: true,
			};
			rp(options).then(function (_success) {
			debug("Data refresh token function" + JSON.stringify(_success));
			User.findOneAndUpdate({ username: requestData.email }, //Update the Access Token Only into the Database
					{ $set: { "additionalProvidersData.zohotokens.access_token": _success.access_token } }, function (err, doc) {
						debug("!!!!!!!!!!!!!!! Sanjeet inside zohoRefreshToken err => " + JSON.stringify(err, "", 3));
					    debug("!!!!!!!!!!!!!!! Sanjeet inside zohoRefreshToken doc => " + JSON.stringify(doc, "", 3));
						if (!err) {
							success({ "status": "success", "access_token":_success.access_token, "refresh_token": zohotokens.refresh_token });	//Success Callback
						} else {

							error({ "status": "error" });  //Error Callback
						}
					});

			}, function (_error) {
				debug("!!!!!! Error Zoho Refresh Token");
				debug(_error);
				error({ "status": "error" });  //Error Callback
			})
		});
}

exports.rasaZohoAPI = function (req, res) {
	saveLead(req, res);
}



