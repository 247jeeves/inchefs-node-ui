'use strict';

/**
 * Module dependencies.
 */
var _ = require('lodash'),
    errorHandler = require('../errors.server.controller'),
    mongoose = require('mongoose'),
    passport = require('passport'),
    User = mongoose.model('User'),
    config = require('../../../config/config'),
    nodemailer = require('nodemailer'),
    async = require('async'),
    crypto = require('crypto');
var debug = require('debug')('wanteet|change-password|');

/**
 * Forgot for reset password (forgot POST)
 */
exports.forgot = function (req, res, next) {
    async.waterfall([
        // Generate random token
        function (done) {
            crypto.randomBytes(20, function (err, buffer) {
                var token = buffer.toString('hex');
                done(err, token);
            });
        },
        // Lookup user by username
        function (token, done) {
            if (req.body.username) {
                User.findOne({
                    username: req.body.username
                }, '-salt -password', function (err, user) {
                    if (!user) {
                        return res.status(400).send({
                            message: 'No account with that username has been found'
                        });
                    } else if (user.provider !== 'local') {
                        return res.status(400).send({
                            message: 'It seems like you signed up using your ' + user.provider + ' account'
                        });
                    } else {
                        user.resetPasswordToken = token;
                        user.resetPasswordExpires = Date.now() + 3600000; // 1 hour

                        user.save(function (err) {
                            done(err, token, user);
                        });
                    }
                });
            } else {
                return res.status(400).send({
                    message: 'Username field must not be blank'
                });
            }
        },
        function (token, user, done) {
            res.render('templates/reset-password-email', {
                name: user.displayName,
                appName: config.app.title,
                url: 'http://' + req.headers.host + '/auth/reset/' + token
            }, function (err, emailHTML) {
                done(err, emailHTML, user);
            });
        },
        // If valid email, send reset email using service
        function (emailHTML, user, done) {
            var smtpTransport = nodemailer.createTransport(config.mailer.options);
            var mailOptions = {
                to: user.email,
                from: config.mailer.from,
                subject: 'Password Reset',
                html: emailHTML
            };
            smtpTransport.sendMail(mailOptions, function (err) {
                if (!err) {
                    res.send({
                        message: 'An email has been sent to ' + user.email + ' with further instructions.'
                    });
                }

                done(err);
            });
        }
    ], function (err) {
        if (err) return next(err);
    });
};

/**
 * Reset password GET from email token
 */
exports.validateResetToken = function (req, res) {
    User.findOne({
        resetPasswordToken: req.params.token,
        resetPasswordExpires: {
            $gt: Date.now()
        }
    }, function (err, user) {
        if (!user) {
            return res.redirect('/#!/password/reset/invalid');
        }

        res.redirect('/#!/password/reset/' + req.params.token);
    });
};

/**
 * Reset password POST from email token
 */
exports.reset = function (req, res, next) {

    res.render('user/change-password', {
        user: req.user || null,
        token: req.token
    });

};

exports.forgetPassword = function (req, res) {

    var email = req.body.email;
    var request = require('request');
    var marketId = req.headers['x-market-id'] || req.market || 'wanteet';

    debug(email);

    request.get(config.inchefs.baseUrl + "/forgot-password?email=" + email,
        {
            headers: {
                'Content-Type': 'application/json',
                'X-Market-ID': marketId
            }
        },
        function (err, response, body) {
            if (err) {
                console.log(err);
            }

            if (response.statusCode == 200) {
                res.render('user/reset-password-instr');
            }
        }
    )

}

/**
 * Change Password
 */
exports.changePassword = function (req, res) {
    // Init Variables
    var passwordDetails = req.body;
    var token = passwordDetails.token;
    var newPassword = passwordDetails.newPassword;
    var confirmPassword = passwordDetails.confirmPassword;
    var marketId = req.headers['x-market-id'] || req.market || 'wanteet';

    var request = require('request');

    request.post(config.inchefs.baseUrl + "/reset-password",
        {
            headers: {
                'Content-Type': 'application/json',
                'X-Auth-Token': token,
                'X-Market-ID': marketId
            },
            json: true,
            body: {
                token: token,
                newPassword: newPassword,
                confirmPassword: confirmPassword
            }
        },
        function (err, response, body) {
            debug(response.statusCode);
            debug(body);

            if (err) {
                console.log(err);
            }

            if (response.statusCode == 401) {
                res.render('user/change-password', {
                    token: token,
                    error: "Invalid Request! Your password reset request is invalid, please start again"
                });
            }

            if (response.statusCode == 422) {

                var errors = {};
                var errList = body.errors;

                for (var idx in errList) {
                    errors[errList[idx].field] = errList[idx].message;
                }

                res.render('user/change-password', {
                    token: token,
                    errors: errors
                });
            }

            if (response.statusCode == 200) {

                request.post(config.inchefs.baseUrl + "/logout", {
                    headers: {
                        'Content-Type': 'application/json',
                        'X-Auth-Token': token
                    },
                    json: true
                }, function (err, response, body) {
                    if (err) {
                        console.log(err);
                    }
                    //Successfully Logged out!
                    if (response.statusCode == 200) {
                        console.log('logout success!');
                    }
                });

                //Non blocking logout from node server
                req.logout();
                res.render('user/reset-password-success');
            }
        }
    )
};
