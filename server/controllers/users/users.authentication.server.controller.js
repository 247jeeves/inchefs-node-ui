'use strict';

/**
 * Module dependencies.
 */
var _ = require('lodash'),
    errorHandler = require('../errors.server.controller'),
    FacebookStrategy = require('passport-facebook').Strategy,
    ZohoStrategy = require('passport-zoho-crm').Strategy,
    GoogleStrategy = require('passport-google-oauth').OAuth2Strategy,
    LinkedInStrategy = require('passport-linkedin-oauth2').Strategy,
    mongoose = require('mongoose'),
    passport = require('passport'),
    request = require('request'),
    config = require('../../../config/config'),
    User = mongoose.model('User');

/**
 * Signup
 */
exports.signup = function (req, res, next) {
    // For security measurement we remove the roles from the req.body object
    delete req.body.roles;

    // Init Variables
    //var user = new User();
    var userObj = req.body;
    userObj.terms = true;
    var message = null;
    //console.log(userObj);
    var marketId = req.headers['x-market-id'] || req.market || 'wanteet';

    request.post(config.inchefs.baseUrl + "/users", {
        headers: {
            'Content-Type': 'application/json',
            'X-Market-ID': marketId
        },
        json: true,
        body: userObj
    }, function (err, response, body) {
        if (err) {
            console.log(err);
            return res.status(400).send({
                errors: [errorHandler.getErrorMessage(err)]
            })
        }
        if (response.statusCode == 422) {
            return res.status(422).send({
                errors: body.errors
            });
        }

        return res.status(201).send({
            message: "User created Successfully!"
        });


        ////Successfully Registered! - Auto Login
        //if(response.statusCode == 201){
        //	 signin(req, res, next);
        //}
    });
};

/**
 * Signin after passport authentication
 */
var signin = exports.signin = function (req, res, next) {
    passport.authenticate('local', function (err, user, info) {
        if (err || !user) {
            console.log(err);
            res.status(400).send(info);
        } else {
            // Remove sensitive data before login
            user.password = undefined;
            user.salt = undefined;

            req.login(user, function (err) {
                if (err) {
                    console.log(err);
                    res.status(400).send(err);
                } else {
                    res.json(user);
                }
            });
        }
    })(req, res, next);
};

exports.proxy = function (req, res, next) {

    passport.authenticate('proxy', function (err, user, info) {
        if (err || !user) {
            console.log(err);
            res.status(400).send(info);
        } else {
            // Remove sensitive data before login
            user.password = undefined;
            user.salt = undefined;

            req.login(user, function (err) {
                if (err) {
                    console.log(err);
                    res.status(400).send(err);
                } else {
                    res.json(user);
                }
            });
        }
    })(req, res, next);
};

/**
 * Signout
 */
exports.signout = function (req, res) {

    var marketId = req.headers['x-market-id'] || req.market || 'wanteet';

    var redirectUrl = '/';

    //console.log(req.get('host'));

    //
    if (req.get('host').indexOf(marketId) != 0 && marketId != 'wanteet') {
        redirectUrl += marketId;
    }

    if (req.user) {
        //Logout api for api-server
        request.post(config.inchefs.baseUrl + "/logout", {
            headers: {
                'Content-Type': 'application/json',
                'X-Auth-Token': req.user.accessToken,
                'X-Market-ID': marketId
            },
            json: true
        }, function (err, response, body) {
            if (err) {
                console.log(err);
            }

            //Successfully Logged out!
            if (response.statusCode == 200) {

            }
        });
    }

    //Non blocking logout from node server
    req.logout();
    res.redirect(redirectUrl);

};

exports.oauth = function (provider) {
    return function (req, res, next) {

        var marketId = req.headers['x-market-id'] || req.market || 'wanteet';
        var scope = [];
        var strategy = provider;
        //console.log("!!!!!!!!!!!!!! Sanjeet - oauth Provider: "+ provider);

        if (marketId != 'wanteet') {
            strategy += marketId;
            var request = require('request');
            request.get(config.inchefs.publicUrl + "/bootstrap/" + marketId,
                {
                    'Content-Type': 'application/json',
                    'X-Market-ID': marketId
                }, function (err, response, body) {
                    if (err) {
                        console.log("ouath error"+err);
                    }
                    if (response.statusCode == 200) {
                        var bootStrap = JSON.parse(body);
                        var market = bootStrap.market;
                        //console.log(JSON.stringify(market.settings.social));
                        
                        setupStrategy(provider, market, function () {
                            if (provider == 'facebook') {
                                scope = ['email'];
                            } else if (provider == 'google') {
                                scope = [
                                    'https://www.googleapis.com/auth/userinfo.profile',
                                    'https://www.googleapis.com/auth/userinfo.email'
                                ];
                            } else if (provider == 'linkedin') {
                                scope = ['r_emailaddress', 'r_basicprofile'];

                            }else if(provider == 'zohocrm'){
                                scope = ['ZohoCRM.crmdataaccess.ALL'];
                            }

                            passport.authenticate(strategy, {
                                scope: scope,
                                state: marketId
                            })(req, res, next);
                            return;
                        });
                    }
                });

        } else {
            if (provider == 'facebook') {
                scope = ['email'];
            } else if (provider == 'google') {
                scope = [
                    'https://www.googleapis.com/auth/userinfo.profile',
                    'https://www.googleapis.com/auth/userinfo.email'
                ];
            } else if (provider == 'linkedin') {
                scope = ['r_emailaddress', 'r_basicprofile']
            }else if(provider == 'zohocrm'){
                scope = ['ZohoCRM.crmdataaccess.ALL'];
            }

            passport.authenticate(strategy, {
                scope: scope,
                state: marketId
            })(req, res, next);
        }
    }
}
/**
 * OAuth callback
 */
exports.oauthCallback = function (_strategy) {
    return function (req, res, next) {

        var debug = require('debug')('wanteet|oauth|callback');
        var marketId = req.headers['x-market-id'] || req.query.state || 'wanteet';
        var strategy = _strategy;

        if (marketId !== 'wanteet') {
            strategy += marketId;
        }

        passport.authenticate(strategy, function (err, user, redirectURL) {

            var redirectUrl = '/';

            //if (req.get('host').indexOf('wanteet') != -1 && marketId != 'wanteet') {
            //    redirectUrl += marketId + '/';
            //}

            if (!req.user && (err || !user)) {
                return res.redirect(redirectUrl + '#!/signin');
            }

            if (req.user) {
                return res.redirect(redirectUrl + 'dashboard#!/profile/trust');
            }

            req.login(user, function (err) {
                if (err) {
                    debug(err);
                    return res.redirect(redirectUrl + '#!/signin');
                }

                return res.redirect(redirectURL || redirectUrl || '/');
            });
        })(req, res, next);
    };
};

/**
 * Helper function to save or update a OAuth user profile
 */
var saveOAuthUserProfile = exports.saveOAuthUserProfile = function (req, providerUserProfile, done, strategy) {

    var debug = require('debug')('wanteet|oauth|save|profile');

    var marketId = req.headers['x-market-id'] || req.market || 'wanteet';

    if (typeof strategy !== 'undefined') {
        passport.unuse(strategy);
    }

    var user;

    if (!req.user) {

        registerMongoUser(providerUserProfile, marketId, function (user) {
            user.save(function (err) {
                if (err) {
                    console.error(err);
                    return done(null, false, {
                        message: 'Server failure! Please try after some time'
                    });
                }
                return done(err, user);
            });
        });

    } else {
        // User is already logged in, join the provider data to the existing user
        user = req.user;

        // Check if user exists, is not signed in using this provider, and doesn't have that provider data already configured
        if (user.provider !== providerUserProfile.provider && (!user.additionalProvidersData || !user.additionalProvidersData[providerUserProfile.provider])) {

            registerMongoUser(providerUserProfile, marketId, function (user) {
                // Then tell mongoose that we've updated the additionalProvidersData field
                user.markModified('additionalProvidersData');

                // And save the user
                user.save(function (err) {
                    return done(err, user);
                });
            });

        } else {
            return done(new Error('User is already connected using this provider'), user);
        }
    }
};

/**
 * Remove OAuth provider
 */
exports.removeOAuthProvider = function (req, res, next) {
    var user = req.user;
    var provider = req.param('provider');

    if (user && provider) {
        // Delete the additional provider
        if (user.additionalProvidersData[provider]) {
            delete user.additionalProvidersData[provider];

            // Then tell mongoose that we've updated the additionalProvidersData field
            user.markModified('additionalProvidersData');
        }

        user.save(function (err) {
            if (err) {
                return res.status(400).send({
                    message: errorHandler.getErrorMessage(err)
                });
            } else {
                req.login(user, function (err) {
                    if (err) {
                        res.status(400).send(err);
                    } else {
                        res.json(user);
                    }
                });
            }
        });
    }
};


exports.verify = function (req, res, next) {

    var marketId = req.headers['x-market-id'] || req.market || 'wanteet';

    request.get(config.inchefs.baseUrl + "/verify/" + req.token, {
        headers: {
            'Content-Type': 'application/json',
            'X-Market-ID': marketId
        },
        json: true
    }, function (err, response, body) {
        if (err) {
            console.error(err);
            return res.status(400).send({
                errors: [errorHandler.getErrorMessage(err)]
            })
        }
        if (response.statusCode == 422) {
            return res.status(422).send({
                errors: body.errors
            });
        }

        res.render('user/verify', {
            market: req.market
        });
    });
}

exports.registrationSuccess = function (req, res, next) {

    String.prototype.repeat = function (num) {
        return new Array(num + 1).join(this);
    }
    var marketId = req.headers['x-market-id'] || req.market || 'wanteet';
    var email = new Buffer(req.token, 'base64').toString('utf8');
    var emailArr = email.split('@');
    var maskedEmail = email[0] + "*".repeat(emailArr[0].length - 1) + "@" + emailArr[1];
    res.render('user/registration', {
        email: maskedEmail,
        market: {slug: marketId}
    });
}


var registerMongoUser = exports.registerMongoUser =  function (providerUserProfile, market, success) {

    var debug = require('debug')('wanteet|registerMongoUser');

    if (typeof market === 'undefined' || market == '' || market == null) {
        market = 'wanteet';
    }
    request.post(config.inchefs.baseUrl + "/ext/login", {
        headers: {
            'Content-Type': 'application/json',
            'X-Market-ID': market
        },
        json: true,
        body: {
            firstName: providerUserProfile.firstName,
            lastName: providerUserProfile.lastName,
            email: providerUserProfile.email,
            provider: providerUserProfile.provider,
            data: providerUserProfile.providerData
        }
    }, function (err, response, body) {
        if (err) {
            console.error(err);
            return res.status(400).send({
                errors: [errorHandler.getErrorMessage(err)]
            })
        }
        if (response.statusCode == 422) {
            return res.status(422).send({
                errors: body.errors
            });
        }

        User.findOne({username: providerUserProfile.email}, function (err, user) {
            if (err) {
                console.error(err);
            } else {
                // If no user found in cache - create new one
                if (!user) {
                    user = new User(body);
                }
                // Update existing user with latest accessToken
                else {
                    for (var key in body) {
                        if (body.hasOwnProperty(key)) {
                            user[key] = body[key];
                        }
                    }
                }

                if (!user.additionalProvidersData) user.additionalProvidersData = {};
                user.additionalProvidersData[providerUserProfile.provider] = providerUserProfile.providerData;

                success(user);
            }
        });
    });
};

var setupStrategy = function (provider, market, done) {

    var debug = require('debug')('wanteet|setupStrategy');

    switch (provider) {
        case 'facebook':
            passport.use(provider + market.slug, new FacebookStrategy({
                    clientID: market.settings.social.facebook.clientID,
                    clientSecret: market.settings.social.facebook.clientSecret,
                    callbackURL: config.facebook.callbackURL,
                    authorizationURL: 'https://www.facebook.com/v2.5/dialog/oauth',
                    tokenURL: 'https://graph.facebook.com/v2.5/oauth/access_token',
                    profileURL: 'https://graph.facebook.com/v2.5/me',
                    passReqToCallback: true,
                    profileFields: ['id', 'birthday', 'email', 'first_name', 'gender', 'last_name']
                },
                function (req, accessToken, refreshToken, profile, done) {
                    // Set the provider data and include tokens
                    var providerData = profile._json;
                    providerData.accessToken = accessToken;
                    providerData.refreshToken = refreshToken;

                    // Create the user OAuth profile
                    var providerUserProfile = {
                        firstName: profile.name.givenName,
                        lastName: profile.name.familyName,
                        displayName: profile.displayName,
                        email: profile.emails[0].value,
                        username: profile.username,
                        provider: 'facebook',
                        providerIdentifierField: 'id',
                        providerData: providerData
                    };
                    debug(JSON.stringify(providerUserProfile));
                    // Save the user OAuth profile
                    saveOAuthUserProfile(req, providerUserProfile, done, provider + market.slug);
                }
            ));
            break;
            case 'zohocrm':
            passport.use(provider + market.slug, new ZohoStrategy({
                    clientID: config.zohocrm.clientID,
                    clientSecret: config.zohocrm.clientSecret,
                    redirect_URL: config.zohocrm.callbackURL,
                    scope: 'ZohoProfile.userinfo.read',
                    response_type: 'code',
                    access_type: 'offline'
                },
                function (req, accessToken, refreshToken, profile, done) {

                    // Set the provider data and include tokens
                    var providerData = profile._json;
                    providerData.accessToken = accessToken;
                    providerData.refreshToken = refreshToken;

                    // Set the provider data and include tokens
                    var providerData = profile._json;
                    providerData.accessToken = accessToken;
                    providerData.refreshToken = refreshToken;
                    
                    //console.log("!!!!!!!!!! Sanjeet- Inside Zoho Strategy"+ JSON.stringify(profile,"",2));
                  
                    // Create the user OAuth profile
                    var providerUserProfile = {
                        firstName: profile.name.givenName,
                        lastName: profile.name.familyName,
                        displayName: profile.displayName,
                        email: profile.emails[0].value,
                        username: profile.username,
                        provider: 'zohocrm',
                        providerIdentifierField: 'id',
                        providerData: providerData
                    };


                    // Save the user OAuth profile
                    saveOAuthUserProfile(req, providerUserProfile, done, provider + market.slug);
                }
            ));
            break;
        case 'google':
            passport.use(provider + market.slug, new GoogleStrategy({
                    clientID: market.settings.social.google.clientID,
                    clientSecret: market.settings.social.google.clientSecret,
                    callbackURL: config.google.callbackURL,
                    passReqToCallback: true
                },
                function (req, accessToken, refreshToken, profile, done) {

                    // Set the provider data and include tokens
                    var providerData = profile._json;
                    providerData.accessToken = accessToken;
                    providerData.refreshToken = refreshToken;

                    // Create the user OAuth profile
                    var providerUserProfile = {
                        firstName: profile.name.givenName,
                        lastName: profile.name.familyName,
                        displayName: profile.displayName,
                        email: profile.emails[0].value,
                        username: profile.username,
                        provider: 'google',
                        providerIdentifierField: 'id',
                        providerData: providerData
                    };

                    // Save the user OAuth profile
                    saveOAuthUserProfile(req, providerUserProfile, done, provider + market.slug);
                }
            ));
            break;
        case 'linkedin':
            passport.use(provider + market.slug, new LinkedInStrategy({
                    clientID: market.settings.social.linkedin.clientID,
                    clientSecret: market.settings.social.linkedin.clientSecret,
                    callbackURL: config.linkedin.callbackURL,
                    passReqToCallback: true,
                    scope: ['r_emailaddress', 'r_basicprofile']
                },
                function (req, accessToken, refreshToken, profile, done) {
                    // Set the provider data and include tokens
                    var providerData = profile._json;
                    providerData.accessToken = accessToken;
                    providerData.refreshToken = refreshToken;

                    // Create the user OAuth profile
                    var providerUserProfile = {
                        firstName: profile.name.givenName,
                        lastName: profile.name.familyName,
                        displayName: profile.displayName,
                        email: profile.emails[0].value,
                        username: profile.username,
                        provider: 'linkedin',
                        providerIdentifierField: 'id',
                        providerData: providerData
                    };

                    // Save the user OAuth profile
                    saveOAuthUserProfile(req, providerUserProfile, done, provider + market.slug);
                }
            ));
            break;

    }
    done();
};
