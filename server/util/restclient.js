'use strict';

exports.getJSON = function(url,success,failure){

	var request = require('request');
	var debug = require('debug')('wanteet|restclient');

	var options = {
    	url: url,
    	headers: {
        	'Content-type': 'json'
    	}
	};

	function callback(error, response, body) {
    	if (!error && response.statusCode == 200) {
        	var info = JSON.parse(body);

			success(info);
    	}else{
    		failure(error);
    	}
	}

	request(options, callback);

};
