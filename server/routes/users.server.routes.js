'use strict';

/**
 * Module dependencies.
 */
var passport = require('passport');

module.exports = function(app) {
	// User Routes
	var users = require('../../server/controllers/users.server.controller');
	var jeeves = require('../../server/controllers/jeeves.server.controller');
	var jeevesEngine = require('../../server/controllers/webhook.server.controller');
	app.param('market', function (req, res, next, market) {
		req.market = market;
		next();
	});

	app.param('provider', function (req, res, next, provider) {
		req.provider = provider;
		next();
	});

	app.param('id', function (req, res, next, id) {
                req.id = id;
                next();
        });


	app.param('userId', function (req, res, next, userId) {
                req.userId = userId;
                next();
        });
	/// Setting up the users profile api
	app.route('/users/me').get(users.me);
	app.route('/users').put(users.update);
	app.route('/users/accounts').delete(users.removeOAuthProvider);

	// Setting up the users password api
	//app.route('/users/password').post(users.changePassword);
	app.route('/auth/forgot').post(users.forgot);
	app.route('/auth/reset/:token').get(users.validateResetToken);
	app.route('/auth/reset/:token').post(users.reset);

	//To save the jeeves credentials in the mongo (as AdditionalProvider)
	app.route('/jeeves/register').post(jeeves.jeevesRegistration);
	app.route('/jeeves/webhook').post(jeevesEngine.webhook);

	//API Call from Rasa Server
	app.route('/savelead').post(users.rasaZohoAPI);
	app.route('/updateLead').post(users.updateLead);	
	app.route('/updateLeadEvent').post(users.updateLeadEvents);
	app.route('/gdpr/lead/delete').post(users.deleteLead);
	
	//API Call for Zoho CRM 
	app.route('/getLeads').get(users.getLeadsAPI);
	app.route('/checkzohoauth').post(users.needZohoAuth);
	app.route('/zohosync').get(users.zohoSyncAPI);


	//Salesforce
	app.route('/checksfauth').post(users.needSFAuth);
	app.route('/sfsync').get(users.sfSyncAPI);

	// Setting up the users authentication api
	//app.route('/:market/auth/signup').post(users.signup);
	//app.route('/:market/auth/signin').post(users.signin);
	//
	//app.route('/:market/auth/signout').get(users.signout);


	app.route('/auth/proxy').post(users.proxy);

	// Setting the facebook oauth routes
	//app.route('/:market/oauth/facebook').get(users.oauth('facebook'));

	app.route('/auth/facebook/callback').get(users.oauthCallback('facebook'));
	

	// Setting the zoho oauth routes
	//app.route('/:market/auth/zoho').get(users.oauth);
	//app.route('/:market/oauth/zoho').get(users.oauth('zoho'));
	app.route('/auth/zoho/callback').get(users.zohoAuthCrmCallback);
	app.route('/auth/salesforce/callback').get(users.sfAuthCrmCallback);


	// Setting up the Microsoft OAuth2 routes
  	app.route('/auth/microsoft').get(users.microsoftAuth);
	app.route('/auth/microsoft/callback').get(users.microsoftAuthCallback);
	app.route('/auth/microsoft/refresh').get(users.microsoftRefreshToken);
	
	app.route('/outlooksync/:id').get(users.syncOutlookEvents);
	// Setting the twitter oauth routes
	//app.route('/auth/twitter').get(passport.authenticate('twitter'));
	//app.route('/auth/twitter/callback').get(users.oauthCallback('twitter'));

	// Setting the google oauth routes
	//app.route('/:market/auth/google').get(users.oauth);
	//app.route('/:market/oauth/google').get(users.oauth('google'));
	app.route('/auth/google/callback').get(users.oauthCallback('google'));

	// Setting the linkedin oauth routes
	//app.route('/:market/oauth/linkedin').get(users.oauth('linkedin'));
	app.route('/auth/linkedin/callback').get(users.oauthCallback('linkedin'));

	// Setting the github oauth routes
	//app.route('/auth/github').get(passport.authenticate('github'));
	//app.route('/auth/github/callback').get(users.oauthCallback('github'));

	//app.route('/:market/registration-success').get(users.registrationSuccess);

	app.param('token', function(req, res, next, token){
		req.token = token;
		next();
	});

	app.route('/:market/verify/:token').get(users.verify);
	app.route('/reset/:token').get(users.reset);
	app.route('/changePassword').post(users.changePassword);
	app.route('/forgetPassword').post(users.forgetPassword);


	//Direct Domain mapping
	app.route('/auth/signup').post(users.signup);
	app.route('/auth/signin').post(users.signin);
	app.route('/auth/signout').get(users.signout);
	app.route('/oauth/facebook').get(users.oauth('facebook'));
	app.route('/oauth/google').get(users.oauth('google'));
	app.route('/oauth/linkedin').get(users.oauth('linkedin'));
	app.route('/auth/zohocrm').get(users.zohoAuthCrm); // Internal CRM sync
	app.route('/auth/sfcrm').get(users.sfAuthCrm); // Internal CRM sync
	app.route('/registration-success/:token').get(users.registrationSuccess);
	app.route('/verify/:token').get(users.verify);



	// Finish by binding the user middleware
	app.param('userId', users.userByID);


};
