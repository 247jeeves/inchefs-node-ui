'use strict';

module.exports = function(app) {
	// Root routing
	var pages = require('../../server/controllers/claimProspect.server.controller');

	app.param('prospectIdBase64', function (req, res, next, prospectIdBase64) {
		req.prospectId = new Buffer(prospectIdBase64, 'base64').toString('ascii');
		next();
	});

	app.param('claimTokenBase64', function (req, res, next, claimTokenBase64) {
		var dataArr = new Buffer(claimTokenBase64, 'base64').toString('ascii').split(',');
		req.prospectId = dataArr[0];
		req.claimToken = dataArr[1];
		req.prospectEmail = dataArr[2];
		next();
	});

	//For markets with no domain mapping
	app.route('/claim/profile/:prospectIdBase64').get(pages.claim);
	app.route('/claim/verify/:claimTokenBase64').get(pages.verify);

};
