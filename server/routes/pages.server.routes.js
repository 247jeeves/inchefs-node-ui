'use strict';

module.exports = function(app) {
	// Root routing
	var pages = require('../../server/controllers/pages.server.controller');

	app.param('market', function (req, res, next, market) {
		req.market = market;
		next();
	});

	//For markets with no domain mapping
	app.route('/:market/u/:slug').get(pages.profileBySlug);
	app.route('/:market/p/:slug').get(pages.packageBySlug);
	app.route('/:market/r/:displayId').get(pages.requestByDisplayId);
	app.route('/:market/pc/:slug').get(pages.prospectBySlug);

	//Direct domain mappings
	app.route('/u/:slug').get(pages.profileBySlug);
	app.route('/p/:slug').get(pages.packageBySlug);
	app.route('/r/:displayId').get(pages.requestByDisplayId);
	app.route('/pc/:slug').get(pages.prospectBySlug);

	app.route('/').get(pages.pageBySlug);
	app.route('/:slug').get(pages.pageBySlug);

};
