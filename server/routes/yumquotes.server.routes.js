'use strict';

module.exports = function(app) {
	// Root routing
	var yq = require('../../server/controllers/yumquotes.server.controller');

	app.param('market', function (req, res, next, market) {
		req.market = market;
		next();
	});

	app.param('service', function(req,res,next,service){
		req.service = service;
		next();
	})

	app.route('/:market/request').get(yq.request);

	/**
	 * We don't need to use POST with node JS end points, as that's not happening anywhere in the app
	 */
	//app.route('/request').post(yq.request);
	app.route('/:market/request/:service').get(yq.request);

	//Direct domain mappings
	app.route('/request').get(yq.request);
	app.route('/request/:service').get(yq.request);
};
