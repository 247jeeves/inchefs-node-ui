'use strict';

module.exports = function (app) {
    // Root routing
    var dash = require('../../server/controllers/market-admin.server.controller');


    app.param('market', function (req, res, next, market) {
        req.market = market;
        next();
    });

    app.route('/:market/market-admin').get(dash.dashboard);
    app.route('/market-admin').get(dash.dashboard);
    app.route('/market-admin/setup-domain').post(dash.setupDomain);
    app.route('/market-admin/setup-pages').post(dash.setupPages);


    app.param('pageId', function (req, res, next, pageId) {
        req.pageId = pageId;
        next();
    });

    app.route('/market-admin/pages').get(dash.getPages);
    app.route('/market-admin/pages').post(dash.createPage);
    app.route('/market-admin/pages/:pageId').get(dash.getPage);
    app.route('/market-admin/pages/:pageId').put(dash.updatePage);
    app.route('/market-admin/pages/:pageId').delete(dash.deletePage);

    var requireLogin = function (req, res, next) {
        if (req.user) {
            next();
        } else {
            res.redirect('/' + req.market + '/#!/signin');
        }
    };

    app.all('/:market/market-admin*', requireLogin, function (req, res, next) {
        next();
    });

    app.all('/market-admin/**/*', requireLogin, function (req, res, next) {
        res.status(401);
        next();
    });

};
