'use strict';

module.exports = function(app) {

	var search = require('../../server/controllers/search.server.controller');

	app.param('market', function (req, res, next, market) {
		req.market = market;
		next();
	});

	app.param('service', function(req,res,next,service){
		req.service = service;
		next();
	})
	app.param('location', function(req,res,next,location){
		req.location = location;
		next();
	})

	app.route('/:market/search').get(search.index);
	app.route('/:market/search/:service').get(search.index);
	app.route('/:market/search/:service/:location').get(search.index);

	app.route('/search').get(search.index);
	app.route('/search/:service').get(search.index);
	app.route('/search/:service/:location').get(search.index);

};
