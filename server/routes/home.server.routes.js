'use strict';

module.exports = function(app) {
	// Root routing
	var home = require('../../server/controllers/home.server.controller');
	var marketPlace = require('../../server/controllers/marketplace.server.controller');

	// app.route('/').get(home.index);

	app.route('/marketplace/:marketName/:accessToken').get(marketPlace.marketplace);
	app.route('/marketplace/:accessToken').get(marketPlace.marketplace);

	app.param('marketName', function (req, res, next, marketName) {
		req.marketName = marketName;
		next();
	});

	app.param('accessToken', function (req, res, next, accessToken) {
		req.accessToken = accessToken;
		next();
	});

	app.param('service', function (req, res, next, service) {
		req.service = service;
		next();
	});

	app.route('/create-your-market').get(home.createYourMarket);
	app.route('/create-your-customer-service-center').get(home.createYourCustomerServiceCenter);

	app.route('/place-request').get(home.placeRequest);
	app.route('/place-request/:service').get(home.placeRequest);

	app.route('/app/environment').get(home.getEnvironment)

};
