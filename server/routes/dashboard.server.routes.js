'use strict';

module.exports = function(app) {
	// Root routing
	var dash = require('../../server/controllers/dashboard.server.controller');


	app.param('market', function (req, res, next, market) {
		req.market = market;
		next();
	});

	app.route('/:market/dashboard').get(dash.dashboard);
	app.route('/:market/rs').get(dash.refreshSession);
	app.route('/rs').get(dash.refreshSession);

	var requireLogin = function(req, res, next){
		if(req.user){
			next();
		}else{
			res.redirect('#!/signin');
		}
	};

	//Direct Domain mapping
	app.route('/dashboard').get(dash.dashboard);


	app.all('/:market/dashboard*', requireLogin, function (req, res, next) {
	 	next();
	});

	app.all('/dashboard*', requireLogin, function (req, res, next) {
		next();
	});

};
