angular.module('search').controller('SearchController', ['$scope', 'BootStrap', '$state', '$timeout', '$rootScope', 'Menus', '$http', '$filter', 'localStorageService',
    function ($scope, BootStrap, $state, $timeout, $rootScope, Menus, $http, $filter, localStorageService) {
        // This provides BootStrap context.
        $scope.authentication = BootStrap;

        var offset = 0;
        var max = 10;
        $scope.total = 0;
        $scope.isWaiting = false;
        $scope.isEnd = false;
        $scope.results = [];

        //Default location to San Francisco (GLOBAL!)
        var locationGeo = {
            results:[
                {
                    geometry:{
                        bounds:{
                            northeast:{
                                lat: '37.9297707',
                                lng: '-122.3279149'
                            }
                        }
                    },
                    formatted_address: 'San Francisco, CA, USA'
                }
            ]
        };
        $scope.service = 'food';
        $scope.searchSorter = 'price-asc';
        $scope.formattedAddress;
        $scope.budget = 50;
        $scope.sortValues = {};
        $scope.referenceId = undefined;

        var market = $scope.market = BootStrap.market;
        //var market = $scope.market;

        $scope.isHyperLocal = false;

        //Backward Compatibility for Addons
        if($scope.market.settings.addons){
            $scope.isHyperLocal = ($scope.market.settings.addons['hyper_local'])? true: false;
        }

        $scope.searchGridSize = localStorageService.get('currentMarket')["settings"].searchGridSize || 2;

        $('[data-slider]').on('change.fndtn.slider', function () {
            $scope.budget = $('div.range-slider').attr('data-slider');
            if (typeof $('span.range-slider-handle').css('transform') !== 'undefined') {
                $scope.nubPosition = $('span.range-slider-handle').css('transform').split(',')[4].trim();
            } else if (typeof $('span.range-slider-handle').css('-webkit-transform') !== 'undefined') {
                $scope.nubPosition = $('span.range-slider-handle').css('-webkit-transform').split(',')[4].trim();
            } else if (typeof $('span.range-slider-handle').css('-moz-transform') !== 'undefined') {
                $scope.nubPosition = $('span.range-slider-handle').css('-moz-transform').split(',')[4].trim();
            }

            if(!$scope.$$phase) {
                $scope.$apply();
            }
        });

        if($scope.isHyperLocal){
            if($("meta[name='_location_geo']").length){
                locationGeo = JSON.parse($("meta[name='_location_geo']").attr('content'));
            }
            $scope.searchSorter = 'location-asc';
        }

        if($("meta[name='_service']").length){
            $scope.service = $("meta[name='_service']").attr('content');
        }

        if ($("meta[name='_refId']").length) {
            $scope.referenceId = $("meta[name='_refId']").attr('content');
        }

        $scope.formatDate = function (date) {
            return moment(date).format('MM/DD/YYYY hh:mm A');
        };

        // Fetch Request List
        $scope.getSearchResults = function (force) {

            if ($scope.total == 0) {
                $timeout(function () {
                    $(document).foundation();
                }, 100);
            }

            if (force) {
                $scope.total = 0;
                offset = 0;
                //$scope.results = [];
                $scope.isEnd = false;
            }

            if($scope.isEnd){
                return;
            }

            $scope.isWaiting = true;
            $scope.success = "";

            $scope.request = undefined;
            $scope.offer = {};
            $(document).foundation('reflow');
            //$(document).foundation('slider', 'reflow');

            var locationCoords = undefined;

            var searchRequest = $scope.authentication.api.publicUrl + "/search/"
                + "?service=" + $scope.service
                + "&max=" + max + "&offset=" + offset + "&market=" + market.slug;

            if($scope.isHyperLocal){
                locationCoords = "" + parseFloat(locationGeo.results[0].geometry.bounds.northeast.lat).toFixed(7) + ","
                + parseFloat(locationGeo.results[0].geometry.bounds.northeast.lng).toFixed(7);
                $scope.formattedAddress = locationGeo.results[0].formatted_address;
                searchRequest += "&l=" + locationCoords;
            }

            if ($scope.keywords) {
                searchRequest += '&query=' + $scope.keywords;
            }

            console.log(searchRequest);

            $http.get(searchRequest)
                .success(function (response) {
                    if(force){
                        $scope.results = [];
                    }
                    console.log("ushakiz search results "+JSON.stringify(response));
                    $scope.results = $scope.results.concat(response.searchResults);
                    //$scope.sortValues = $scope.sortValues.concat(response.sort);
                    for (idx in response.sort) {
                        $scope.sortValues[idx] = response.sort[idx];
                    }
                    $scope.total = response.total;
                    if (($scope.total == 0) || ($scope.results.length == $scope.total)) {
                        $scope.isEnd = true;
                    }

                    if ($scope.total == 0) {
                        $scope.total = "No";

                    }
                    offset += max;
                    $scope.isWaiting = false;
                    $scope.error = {};

                }).error(function (response) {
                    $scope.isEnd = true;
                    errorHandler(response);
                });
        };

        $scope.setupDialog = function () {

                $("#placeRequest").foundation('reveal', 'open');

                $timeout(function () {
                    angular.element('.close-reveal-modal').on('click', function () {
                        $("#placeRequest").foundation('reveal', 'close');
                    });
                }, 10);
                return;
        };

        $scope.updateLocation = function (elem) {
            if (elem.formattedAddress) {
                $timeout(function(){
                    window.location = '/search/' + $scope.service + '?location=' + elem.formattedAddress;
                },100);
            }
        };

        var errorHandler = function(data){
            console.log("error"+data);
        }

        $scope.initCustomRequestFlow = function(){
            $("#placeRequest").foundation('reveal', 'open');
        }
    }
]);
