angular.module('search').config(['$stateProvider', '$urlRouterProvider',
    function ($stateProvider, $urlRouterProvider) {
        // Redirect to home view when route not found
        $urlRouterProvider.otherwise('/');

        // Home state routing
        $stateProvider.
            state('search', {
                url: '/',
                controller: 'SearchController',
                templateUrl: 'modules/search/views/index.client.view.html'
            });
    }
]);
