angular.module('core').directive('hallFeed', function () {
    return {
        restrict: 'E',
        templateUrl: 'modules/core/views/hall-feed.client.view.html',
        scope: {
            feed: '='
        },
        link: function (scope, element, attrs) {
            console.log('Hall Feed started!');
        }
    }
});
