angular.module('core').config(['$stateProvider', '$urlRouterProvider',
    function ($stateProvider, $urlRouterProvider) {
        // Redirect to home view when route not found
        $urlRouterProvider.otherwise('/');

        // Home state routing
        $stateProvider.

            state('exclusiveListingRequest', {
                views: {
                    'exclusive': {
                        controller: 'YumquotesController',
                        templateUrl: 'modules/yumquotes/views/yumquotes/getExclusiveDetails.client.view.html'
                    }
                },
                url: '/exclusive'
            }).
            state('instantListingRequest', {
                views: {
                    'exclusive': {
                        controller: 'PublicListingController',
                        templateUrl: 'modules/yumquotes/views/yumquotes/getInstantDetails.client.view.html'
                    }
                },
                url: '/instant'
            }).
            state('exclusiveListingRequest.getResults', {
                views: {
                    'exclusive': {
                        templateUrl: 'modules/yumquotes/views/yumquotes/getResults.client.view.html'
                    }
                },
                url: '/success',
                parent: 'exclusiveListingRequest'
            }).

            state('addReviewForm', {
                views: {
                    'addReviewForm': {
                        controller: 'ReviewController',
                        templateUrl: 'modules/core/views/addReview.client.view.html'
                    }
                },
                url: '/add-review'
            }).

            state('hallFeedPost', {
                views: {
                    'hallPost': {
                        controller: 'FirebaseController',
                        templateUrl: 'modules/core/views/hall-feed-post.client.view.html'
                    }
                },
                url: '/hall/:postId'
            });

    }
]);
