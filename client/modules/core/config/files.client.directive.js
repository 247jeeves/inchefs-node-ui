angular.module('core').directive('fileUpload', function () {
    return {
        restrict: 'E',
        scope: {
            fileModel: '=',
            fileComplete: '&'
        },
        templateUrl: function (tElement, tAttrs) {
            var type = tAttrs.type;
            if (typeof type === 'undefined' || type == 'single') {
                return "modules/core/views/file-upload.client.view.html"
            } else if (type == 'multiple') {
                return "modules/core/views/files-upload.client.view.html"
            }
        },
        controller: ['$scope', 'BootStrap', '$http', '$timeout', function ($scope, BootStrap, $http, $timeout) {
            var flowInst = $scope.$flow;
            var imagesToUpload = [];

            $scope.apiBase = BootStrap.api.baseUrl;
            $scope.userId = BootStrap.user.id;
            $scope.market = BootStrap.market;
            $scope.uploadedFile = undefined;
            $scope.toggleDisplay = false;

            $timeout(function () {

                if (typeof $scope.fileModel !== 'undefined' && $scope.fileModel !== null) {
                    $scope.uploadedFile = JSON.parse($scope.fileModel);
                    $scope.toggleDisplay = true;
                }
            }, 100);

            $scope.uploadSuccess = function ($flow, $file, $message) {

                console.log($file);
                console.log($message);

                flowInst = $flow;
                imagesToUpload.push($file.uniqueIdentifier);

                $scope.uploadedFile = JSON.parse($message);

                if ($scope.uploadedFile.status) {
                    $scope.toggleDisplay = false;
                    $flow.files = [];
                    $scope.setErrors = true;
                    $scope.error = JSON.parse($message);
                    console.log($scope.error);
                    $scope.fileModel = $scope.uploadedFile = undefined;
                } else {
                    $scope.fileModel = JSON.stringify($scope.uploadedFile);
                    $scope.setErrors = false;
                    $scope.error = undefined;

                    if (!$scope.fileComplete) {
                        $scope.fileComplete($scope.fileModel);
                    }

                    $(document).foundation('alert', 'reflow');
                    $scope.toggleDisplay = true;
                }
            };

            $scope.deleteFile = function () {
                if (!$scope.uploadedFile) {
                    console.log("Nothing to delete");
                    return;
                }

                $scope.toggleDisplay = false;

                $http.delete(BootStrap.api.baseUrl + "/users/" + $scope.userId
                + "/markets/" + $scope.market.slug
                + "/file/store/" + $scope.uploadedFile.id)
                    .success(function (response) {
                        console.log(response);
                        $scope.uploadedFile = undefined;
                        if (flowInst && flowInst.files) {
                            flowInst.files = [];
                        }
                        $scope.fileModel = undefined;
                    })
                    .error(function (response) {
                        console.log(response);
                        $scope.fileModel = $scope.uploadedFile = undefined;
                    });
            };

            $scope.showProgress = function ($file, $flow) {
                console.log("Progress");
                $scope.setErrors = false;
                $scope.error = undefined;
            }

            $scope.genFilename = function (file) {
                return file.name;
            }


            $scope.$on('flow::error', function (event, $flow, flowFile) {
                console.log(flowFile);
                $scope.toggleDisplay = false;
                $flow.files = [];
                $scope.setErrors = true;
                $scope.error = JSON.parse($message);
                console.log($scope.error);
                $scope.fileModel = $scope.uploadedFile = undefined;
            });

            //$scope.errorFileUpload = function ($file, $message, $flow) {
            //    console.log($file);
            //    console.log($message);
            //    console.log($chunk);
            //    console.log($flow);
            //
            //    $scope.toggleDisplay = false;
            //    $flow.files = [];
            //    $scope.setErrors = true;
            //    $scope.error = JSON.parse($message);
            //    console.log($scope.error);
            //    $scope.fileModel = $scope.uploadedFile = undefined;
            //
            //}

        }],
        link: function (scope, element, attrs, ctrl, transclude) {
            //ctrl.init();
        }
    }
});
