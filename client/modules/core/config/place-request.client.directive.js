angular.module('core').directive('placeRequest', function () {
    return {
        restrict: 'E',
        scope: {
            sellerId: '@seller',
            serviceName : '@service',
            listingId : '@listing',
            listingTitle: '@title',
            exclusiveBudget: '@amount',
            processEvent : '&'
        },
        templateUrl: function(tElement, tAttrs){
            var type = tAttrs.type;
            if(typeof type === 'undefined' || type == 'compact'){
                return "modules/core/views/place-request-compact.client.view.html"
            }else if(type == 'regular'){
                return "modules/core/views/place-request-regular.client.view.html"
            } else if (type == 'questions') {
                return "modules/core/views/place-request-questions.client.view.html"
            }
        },
        controller: 'PlaceRequestController',
        link : function(scope, element, attrs, ctrl, transclude){
            ctrl.init();
        }
    }
});
