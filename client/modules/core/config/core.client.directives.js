angular.module('core').directive('rating', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            attrs.$observe('rating', function (value) {
                var max = 5;
                var rating = Number(value);

                var ratingElement = '';

                for (var i = 0; i < max; i++) {
                    var star = (i < rating) ? 'star' : 'star-o';
                    ratingElement += '<span><i class="fa fa-' + star + '"></i></span>';
                }

                element.html(ratingElement);
            });
        }
    }
});

angular.module('core').directive('createRating', function () {
    return {
        restrict: 'A',
        template: '<ul class="inline-list add-rating" ng-mouseleave="hovered = 0">' +
        '<li ng-repeat="star in stars" ng-click="toggleRating($index)" ng-mouseover="toggleHover($index)">' +
            //'<li ng-repeat="star in stars" ng-class="{\'filled\':$index<ratingValue, \'hovered\':$index<hovered}" ng-click="toggleRating($index)" ng-mouseover="toggleHover($index)">' +
        '<i data-ng-show="$index<ratingValue || $index<hovered" class="fa fa-star"></i><i data-ng-hide="$index<ratingValue || $index<hovered" class="fa fa-star-o"></i>' +
        '</li>' +
        '</ul>',
        scope: {
            ratingValue: '=',
            max: '=',
            readonly: '@'
        },
        link: function (scope, element, attrs) {
            scope.stars = [];
            scope.hovered = scope.ratingValue;

            for (var i = 0; i < scope.max; i++) {
                scope.stars.push(i);
            }
            32

            scope.toggleRating = function (index) {
                if (scope.readonly && scope.readonly === 'true') {
                    return;
                }
                scope.ratingValue = index + 1;
                scope.hovered = 0;
            };

            scope.toggleHover = function (index) {
                if (scope.readonly && scope.readonly === 'true') {
                    return;
                }

                scope.hovered = index + 1;
            };

        }
    }
});

angular.module('core').filter('percentage', [
    function () {
        return function (input) {
            return input + "%";
        }
    }
]);

angular.module('core').directive('ngCustomAccordion', function () {
    return {
        restrict: 'A',
        scope: {
            accordionContent: '='
        },
        link: function (scope, element, attrs) {

            var contentElem = angular.element('#' + attrs.accordionContent);
            var bodyText = element[0].innerHTML;

            function isOpen() {
                return contentElem.hasClass('active');
            }

            function changeIndicator() {
                var indicator = (isOpen()) ? "<i class='fa fa-caret-up'></i>" : "<i class='fa fa-caret-down'></i>";
                element[0].innerHTML = bodyText + " " + indicator;
            }

            changeIndicator();

            element.on('click', function () {
                if (isOpen()) {
                    contentElem.removeClass('active');
                    changeIndicator();
                } else {
                    contentElem.addClass('active');
                    changeIndicator();
                }
            });
        }
    }
});

angular.module('core').directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;

            element.bind('change', function () {
                scope.$apply(function () {
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
}]);
