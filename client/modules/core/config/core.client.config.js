angular.module('core').config(['flowFactoryProvider',
    function (flowFactoryProvider) {

        //Using direct references here as we don't have access to Services here
        var accessToken = window.user.accessToken;
        var market = window._market.slug;

        if (window.location.href.indexOf('dashboard') == -1) {
            flowFactoryProvider.defaults = {
                target: 'https://api.cloudinary.com/v1_1/wanteet-pages/raw/upload?api_key=296198548993374&timestamp=' + new Date().getTime(),
                testChunks: false,
                permanentErrors: [404, 500, 501],
                headers: {'X-Auth-Token': accessToken, 'X-Market-ID': market},
                generateUniqueIdentifier: function (file) {
                    var relativePath = file.relativePath || file.webkitRelativePath || file.fileName || file.name;
                    return file.size + '-' + relativePath.replace(/[^0-9a-zA-Z._-]/img, '');
                }
            };
        }
        //// You can also set default events:
        //flowFactoryProvider.on('catchAll', function (event) {
        //    console.log(event);
        //});
    }
]);
