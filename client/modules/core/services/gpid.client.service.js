//Menu service used for managing  menus
angular.module('core').service('GroupId', ['$timeout', 'localStorageService',

    function ($timeout, localStorageService) {

        var GroupId = (function () {
            var increment = Math.floor(Math.random() * (16777216));
            var gpid = Math.floor(Math.random() * (65536));
            var machine = Math.floor(Math.random() * (16777216));

            var setSessionGpid = function () {
                var cookieList = document.cookie.split('; ');
                for (var i in cookieList) {
                    var cookie = cookieList[i].split('=');
                    var cookieMachineId = parseInt(cookie[1], 10);
                    if (cookie[0] == 'wanteetGpid' && cookieMachineId && cookieMachineId >= 0 && cookieMachineId <= 16777215) {
                        machine = cookieMachineId;
                        break;
                    }
                }
                document.cookie = 'wanteetGpid=' + machine + ';expires=Tue, 19 Jan 2038 05:00:00 GMT;path=/';
            };
            if (typeof (localStorage) != 'undefined') {
                try {
                    var mongoMachineId = parseInt(localStorage['mongoMachineId']);
                    if (mongoMachineId >= 0 && mongoMachineId <= 16777215) {
                        machine = Math.floor(localStorage['mongoMachineId']);
                    }
                    // Just always stick the value in.
                    localStorage['mongoMachineId'] = machine;
                } catch (e) {
                    setSessionGpid();
                }
            }
            else {
                setSessionGpid();
            }

            function GpId() {
                if (!(this instanceof GroupId)) {
                    return new GroupId(arguments[0], arguments[1], arguments[2], arguments[3]).toString();
                }

                if (typeof (arguments[0]) == 'object') {
                    this.timestamp = arguments[0].timestamp;
                    this.machine = arguments[0].machine;
                    this.gpid = arguments[0].gpid;
                    this.increment = arguments[0].increment;
                }
                else if (typeof (arguments[0]) == 'string' && arguments[0].length == 24) {
                    this.timestamp = Number('0x' + arguments[0].substr(0, 8)),
                        this.machine = Number('0x' + arguments[0].substr(8, 6)),
                        this.gpid = Number('0x' + arguments[0].substr(14, 4)),
                        this.increment = Number('0x' + arguments[0].substr(18, 6))
                }
                else if (arguments.length == 4 && arguments[0] != null) {
                    this.timestamp = arguments[0];
                    this.machine = arguments[1];
                    this.gpid = arguments[2];
                    this.increment = arguments[3];
                }
                else {
                    this.timestamp = Math.floor(new Date().valueOf() / 1000);
                    this.machine = machine;
                    this.gpid = gpid;
                    this.increment = increment++;
                    if (increment > 0xffffff) {
                        increment = 0;
                    }
                }
            };
            return GpId;
        })();

        /**
         * Turns a WCF representation of a BSON ObjectId into a 24 character string representation.
         */
        GroupId.prototype.toString = function () {
            if (this.timestamp === undefined
                || this.machine === undefined
                || this.gpid === undefined
                || this.increment === undefined) {
                return 'Invalid ObjectId';
            }

            var timestamp = this.timestamp.toString(16);
            var machine = this.machine.toString(16);
            var gpid = this.gpid.toString(16);
            var increment = this.increment.toString(16);
            return '00000000'.substr(0, 8 - timestamp.length) + timestamp +
                '000000'.substr(0, 6 - machine.length) + machine +
                '0000'.substr(0, 4 - gpid.length) + gpid +
                '000000'.substr(0, 6 - increment.length) + increment;
        };
    }
]);
