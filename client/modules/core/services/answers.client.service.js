angular.module('core').factory('Answers', [
    function () {
        var _this = this;

        var setupAnswers = function (questions, answers) {
            var processedAnswers = {};
            for (idx in answers) {
                var answer = answers[idx];
                if (answer.questionId != null) {
                    processedAnswers[answer.questionId] = {
                        answer: answer.answer,
                        questionId: answer.questionId,
                        meta: answer.meta
                    };

                    if (answer.id) {
                        processedAnswers[answer.questionId]["id"] = answer.id;
                    }

                    if (answer.meta && answer.meta.display === 'list') {
                        var choices = [];
                        questions.forEach(function (entry) {
                            if (entry.id == answer.questionId) {
                                choices = entry.choices;
                                return;
                            }
                        });

                        processedAnswers[answer.questionId].answer = {};
                        var answerList = answer.answer.split(',').map(function (obj) {
                            return obj.trim()
                        });
                        choices.forEach(function (obj, idx) {
                            processedAnswers[answer.questionId].answer[idx] = false;
                            if (answerList.indexOf(obj.trim()) != -1) {
                                processedAnswers[answer.questionId].answer[idx] = obj.trim();
                            }
                        });
                    }
                }
            }
            return processedAnswers;
        };

        var processAnswers = function (answers) {
            var processedAnswers = [];

            for (var idx in answers) {
                var answer;
                if (typeof answers[idx].answer === 'object') {
                    answer = [];
                    for (var jdx in answers[idx].answer) {
                        if (answers[idx].answer[jdx]) {
                            answer.push(answers[idx].answer[jdx]);
                        }
                    }
                    answer = answer.join(',');
                } else {
                    answer = answers[idx].answer;
                }

                var processedAnswer = {
                    questionId: idx,
                    answer: answer
                };

                if (answers[idx].hasOwnProperty('id')) {
                    processedAnswer['id'] = answers[idx].id;
                }

                processedAnswers.push(processedAnswer);
            }
            return processedAnswers;

        };

        _this._data = {
            setupAnswers: setupAnswers,
            processAnswers: processAnswers
        };


        return _this._data;
    }
]);
