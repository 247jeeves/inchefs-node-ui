//Menu service used for managing  menus
angular.module('core').service('Loader', ['$timeout',

    function ($timeout) {

        var message = "";

        this.loadMessage = "<i class='fa fa-circle-o-notch fa-spin'></i> Please Wait...";

        this.successMessage = "<i class='fa fa-check-circle green-color'></i> Successful!";

        this.errorMessage = "<i class='fa fa-times-circle red-color'></i> Error Occurred!";
	
	this.zohoCredentials = "<i class='fa fa-times-circle red-color'></i> Zoho Credentials Not Found, Please add clientId and clientSecret inside partner integration menu.";

        var getMessage = function (message) {
            var messageHTML = '<div id="loader-toast" class="panel toast toast-box top-right radius animated bounceInRight center-content reduced-padding">' +
                '<div class="row">' +
                '<div class="medium-12 columns" id="loader-message">' +
                message +
                '</div>' +
                '</div>' +
                '</div>';
            return messageHTML;
        };

        this.startLoader = function () {
            var loaderHTML = getMessage(this.loadMessage);
            if (!(angular.element('#loader-message').length > 0)) {
                angular.element('body').append(loaderHTML);
            }
        };

        this.stopLoader = function (type) {
            switch (type) {
                case 'success':
                    angular.element('#loader-message').html(this.successMessage);
                    break;
                case 'error':
                    angular.element('#loader-message').html(this.errorMessage);
                    break;
		case 'zohoError':
            	    angular.element('#loader-message').html(this.zohoCredentials);
                    break;
            }

            $timeout(function () {
                angular.element('#loader-toast').removeClass('bounceInRight').addClass('bounceOutRight');
                $timeout(function () {
                    angular.element('#loader-toast').remove();
                }, 1000);
            }, 1000);
        };
    }
]);

