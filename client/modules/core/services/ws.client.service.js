//Menu service used for managing  menus
angular.module('core').service('WS', ['$timeout', '$rootScope',
    function ($timeout, $rootScope) {


        var _api = window._api;
        var _user = window.user || {};
        var _marketSlug = window._market.slug || 'wanteet';
        var _isSubscribed = false;
        var _notifSupported = false;
        var wsh = null;


        if (window.Notification) {
            console.log("Notifications are supported!");
            _notifSupported = true;
        }
        else {
            _notifSupported = false;
            console.log("Notifications are not supported for this Browser/OS version yet.");
        }

        this.init = function () {
            if (_user && _user.hasOwnProperty('accessToken')) {
                wsh = new WebSockHop(_api.subscribeUrl);
                wsh.formatter = new WebSockHop.JsonFormatter();

                wsh.on('opened', function () {
                    console.log('WS Opened!');
                    if (!_isSubscribed) {
                        subscribeToPushpin(_marketSlug + '-all');
                        var privateChannel = _marketSlug + _user.username;
                        privateChannel = privateChannel.replace(/[\@\.]/g, '_');
                        subscribeToPushpin(privateChannel);
                        _isSubscribed = true;
                    }
                });

                wsh.on('message', function (response) {
                    console.log(response);

                    //TODO: Add Entity Specific notification
                    console.log(_notifSupported);
                    console.log(Notification.permission);
                    if (_notifSupported && Notification.permission == 'granted') {
                        generateNotification(response);
                    } else if (_notifSupported && Notification.permission !== 'granted') {
                        Notification.requestPermission();
                    } else {
                        fallbackNotifications(response);
                    }

                    //broadcast event to clients
                    $rootScope.$broadcast('wsUpdate', response);
                });

                wsh.formatter.pingRequest = {type: 'ping'};

                wsh.formatter.handlePong = function (message) {
                    console.log(message);
                    return (message == 'pong'); // return true if message was a pong
                };

                if (_notifSupported) {
                    Notification.requestPermission();
                }
            }
        };

        this.subscribe = function (channel) {
            subscribeToPushpin(channel);
        };

        var subscribeToPushpin = function (channel) {
            //subscribe to a channel
            if (wsh) {
                wsh.request({
                    type: 'subscribe',
                    channel: channel
                }, function (reply) {
                    console.log('Subscribed to - ' + channel);
                });
                console.log('Subscribed to - ' + channel);
            }
        };

        //global check to close wsh cleanly
        window.addEventListener('beforeunload', function (event) {
            console.log('WebSocket connection closed at "unload"');
            if (wsh) wsh.close();
        });

        window.addEventListener('close', function (event) {
            console.log('WebSocket connection closed at "close"');
            if (wsh) wsh.close();
        });

        var generateNotification = function (entity) {
            console.log("Spawn Notif");
            var options = {
                title: entity.title,
                body: entity.text,
                icon: _market.settings.logo || null
            }
            var notification = new Notification(entity.title, options);
            //notification.addEventListener('onclick', function(){
            //
            //});

        }

        var fallbackNotifications = function (entity) {
            //TODO:Implement this
        }

    }
]);
