angular.module('core').controller('PublicListingController',
    ['$scope', 'BootStrap', '$state',
        '$timeout', '$http', '$rootScope', 'PlaceRequest',
        function ($scope, BootStrap, $state, $timeout, $http, $rootScope, PlaceRequest) {

        $scope.authentication = BootStrap;
        $scope.market = BootStrap.market;

        var packageName = location.pathname.split('/');
        packageName = packageName[packageName.length - 1];

        $scope.eventData = {};
        $scope.error = {};
            $scope.packageDetails = {};

            $scope.initScreen = function (budget, service, sellerSlug, listingSlug, listingTitle) {
                $scope.packageDetails = {
                    budget: budget,
                    service: service,
                    sellerSlug: sellerSlug,
                    listingSlug: listingSlug,
                    listingTitle: listingTitle
                };
            }

            $scope.initExclusiveListingRequest = function () {
                PlaceRequest.setupExclusiveRequest($scope.packageDetails);
                $state.go('exclusiveListingRequest');
        };

            $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
                if (toState.name == 'exclusiveListingRequest') {
                    if (!PlaceRequest.getExclusiveRequest().length) {
                        PlaceRequest.setupExclusiveRequest($scope.packageDetails);
                    }
                    $timeout(function () {
                        setupDialog('exclusiveListingRequest', function () {
                            console.log('ExclusiveDialogSetup Complete!');
                        });
                    }, 100);
                }
            });

        $scope.addReview = function () {
            $state.go('addReviewForm');
            $timeout(function () {
                setupDialog('addReviewForm', function () {
                });
            }, 10);
        };

        var setupDialog = function (type, cb) {
            if ($state.current.name === type) {
                $("#" + type).foundation('reveal', 'open');

                $timeout(function () {
                    angular.element('.modal-close-btn').on('click', function () {
                        $state.go('request');
                        $("#" + type).foundation('reveal', 'close');
                    });
                    cb();
                }, 10);
                return;
            }
        };


        $scope.gotoInstantRequest = function () {
            $state.go('instantListingRequest');
        }

        $scope.initInstantRequest = function () {
            $timeout(function () {
                setupDialog('instantListingRequest', function () {
                    // Blocking selection of same day

                    $("input[data-datetime-bind]").datetimepicker({
                        minDate: (new Date()).setDate((new Date()).getDate() + 1),
                        collapse: true,
                        useCurrent: false
                    });

                    var ua = navigator.userAgent;

                    if (ua.match(/(iPhone|iPad)/i)) {
                        $("#dateAndTime").attr('type', 'datetime-local');
                        $('body').on('change', '#dateAndTime', function (e) {

                            $scope.eventData.dateTime = moment($('#dateAndTime').val()).
                                format('YYYY-MM-DDTHH:mm:ss.SSSZ');

                            if (!$scope.$$phase) {
                                $scope.$apply();
                            }
                        });
                    } else {
                        $("#dateAndTime").datetimepicker({
                            minDate: (new Date()).setDate((new Date()).getDate() + 1),
                            collapse: true,
                            useCurrent: false
                        });

                        $('body').on('dp.change', '#dateAndTime', function (e) {
                            $scope.dateAndTime = $('#dateAndTime').val();
                            $scope.eventData.dateTime = $('#dateAndTime').data("DateTimePicker")
                                .date().format('YYYY-MM-DDTHH:mm:ss.SSSZ');
                        });
                    }
                });

            }, 100);
        };

        $scope.postInstantRequest = function () {
            if (!$scope.authentication.user) {
                return;
            }

            $scope.eventData.market = $scope.market.slug;
            $scope.eventData.packageName = packageName;

            $http.post($scope.authentication.api.baseUrl + '/users/'
            + $scope.authentication.user.id + '/events/instant', $scope.eventData)
                .success(function (response) {
                    location.href = '/dashboard#!/orders/pay/' + response[0].order.id;
                })
                .error(function (response) {
                    if (typeof response !== 'undefined') {
                        for (var idx in response.errors) {
                            $scope.error[response.errors[idx]['field']] = {
                                value: response.errors[idx]['value'],
                                message: response.errors[idx]['message']
                            };
                        }
                    }
                });
        }
    }
]);
