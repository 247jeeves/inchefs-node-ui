angular.module('core').controller('ReviewController', ['$scope', 'BootStrap', '$state', '$timeout', '$http', '$rootScope',
    function ($scope, BootStrap, $state, $timeout, $http, $rootScope) {
        // This provides BootStrap context.
        $scope.authentication = BootStrap;

        //$scope.reviews = [];
        $scope.review = {};

        var successCallback = undefined;
        var failureCallback = undefined;

        $scope.formatDate = function (date) {
            return moment(date).format('MM/DD/YYYY hh:mm A');
        };

        var loadReviews = function () {
            console.log($scope.review);
            $http.get($scope.authentication.api.publicUrl + "/reviews?"
            + "id=" + review.id + "&type=" + review.type)
                .success(function (response) {
                    console.log(response);
                    if(typeof successCallback !== 'undefined' && typeof successCallback === 'function'){
                        successCallback(response);
                    }
                })
                .error(function (response) {
                    console.log(response);
                    if(typeof failureCallback !== 'undefined' && typeof failureCallback === 'function'){
                        failureCallback(response);
                    }
                });
        };

        $scope.createReview = function () {
            console.log($scope.review);
        };

        //Event Listener to load reviews
        $scope.$on('initReviews', function (data, args) {
            $scope.review = {
                id: args.forId,
                type: args.type.toString(),
                max: args.max || 10,
                offset: args.offset || 0
            };
            successCallback = args.success;
            failureCallback = args.error;
            loadReviews();
        });

        $rootScope.$on('initCreateReview', function (data, args) {

        });
    }
]);
