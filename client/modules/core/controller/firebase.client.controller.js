angular.module('core').controller('FirebaseController',
    ['$scope', 'BootStrap', '$state',
        '$timeout', '$firebaseObject',
        '$firebaseArray', '$firebaseAuth',
        '$state', '$rootScope',
        function ($scope, BootStrap, $state,
                  $timeout, $firebaseObject,
                  $firebaseArray, $firebaseAuth,
                  $state, $rootScope) {

            $scope.authentication = BootStrap;

            var market = BootStrap.market;

            var ref = new Firebase("https://" + BootStrap.api.hall + ".firebaseio.com");
            var hallRef = ref.child(market.slug);
            $scope.hallUser;
            $scope.hallPost;
            $scope.hallFeed = [];
            $scope.message = {};
            $scope.image = "";
            var flowInst = undefined;
            $scope.showPlaceRequest = false;

            $scope.togglePlaceRequest = function(){
                $scope.showPlaceRequest = !$scope.showPlaceRequest;
            }

            // Handle Post state logic
            $(document).on('closed.fndtn.reveal', '[data-reveal]', function () {
                var modal = $(this);
                if (modal[0].id == 'hallPost') {
                    $state.go('request');
                }
            });

            $scope.uploadSuccess = function ($flow, $file, $message) {
                flowInst = $flow;
                imagesToUpload.push($file.uniqueIdentifier);
                if (flowInst.files.length == imagesToUpload.length) {
                    $scope.isWaiting = false;
                }
            }

            $rootScope.$on('$stateChangeSuccess',
                function (event, toState, toParams, fromState, fromParams) {
                    if (toState.name == "hallFeedPost") {
                        $scope.hallPost = $firebaseObject(hallRef.child('feed').child($state.params.postId));
                        $scope.hallPost.$loaded().then(function () {
                            $('#hallPost').foundation('reveal', 'open');
                        });
                    }
                });


            //Method to check user's membership
            var checkUserMembership = function () {
                if ($scope.authentication.user) {
                    $scope.hallUser = $firebaseObject(hallRef.child('members')
                        .child('' + $scope.authentication.user.id));

                    $scope.hallUser.$loaded().then(function () {
                        if (!$scope.hallUser.displayName) {
                            $scope.hallUser.id = $scope.authentication.user.id;
                            $scope.hallUser.displayName = $scope.authentication.user.displayName;
                            $scope.hallUser.$save();
                        }
                    });
                }
            };

            // Init HallFeed for the given market
            $scope.init = function (success) {

                console.log("Firebase Init!");

                //Check for the current User membership
                checkUserMembership();

                //Load Hall Feed
                $scope.hallFeed = $firebaseArray(hallRef.child('feed'));
                $scope.hallFeed.$loaded().then(function (x) {
                    console.log("Hall Feed Loaded");
                    if (typeof success === 'function') {
                        success();
                    }
                });

            };

            $scope.like = function (postId) {

                if (!$scope.hallUser) {
                    return;
                }
                var userLike = $firebaseObject(hallRef.child('members')
                    .child($scope.hallUser.$id).child('likes').child(postId));
                userLike.$loaded().then(function () {

                    var post = $firebaseObject(hallRef.child('feed').child(postId).child('likeCount'));
                    var likeRef = $firebaseObject(hallRef.child('likes').child(postId).child('' + $scope.hallUser.$id));

                    if (!userLike.$value) {
                        //Add like to User's likes collection
                        userLike.$value = true;
                        userLike.$save();

                        //Increment like count of the post
                        post.$loaded().then(function () {
                            post.$value = (post.$value || 0) + 1;
                            post.$save();
                        });

                        //Capture like timestamp in likes collection
                        likeRef.$loaded().then(function () {
                            likeRef.$id = $scope.hallUser.$id;
                            likeRef.dateTime = new Date().getTime();
                            likeRef.$save();
                        });

                    } else {
                        //Remove like from user's like collection
                        userLike.$remove();

                        //Decrement like count for post
                        post.$loaded().then(function () {
                            post.$value = (post.$value || 0) - 1;
                            post.$save();
                        });

                        //Remove like timestamp from likes collection
                        likeRef.$remove();
                    }
                });

            };

            $scope.share = function (postId) {



                //Log share to firebase
                var userShare = $firebaseObject(hallRef.child('members')
                    .child($scope.hallUser.$id).child('shares').child(postId));
                userShare.$loaded().then(function () {

                    var post = $firebaseObject(hallRef.child('feed').child(postId).child('shareCount'));
                    var shareRef = $firebaseObject(hallRef.child('shares')
                        .child(postId).child('' + $scope.hallUser.$id));

                    //Add share to User's likes collection
                    userShare.$value = true;
                    userShare.$save();

                    //Increment share count of the post
                    post.$loaded().then(function () {
                        post.$value = (post.$value || 0) + 1;
                        post.$save();
                    });

                    //Capture share timestamp in likes collection
                    shareRef.$loaded().then(function () {
                        shareRef.$id = $scope.hallUser.$id;
                        shareRef.dateTime = new Date().getTime();
                        shareRef.$save();
                    });

                });
            };

            var comment = function (postId) {
                //TODO: Implement this
            };

            $scope.deletePost = function (postId) {
                var post = $firebaseObject(hallRef.child('feed').child(postId));
                if (post.author == $scope.authentication.id) {

                }
            };

            //For Hall Feed Screen Input
            $scope.publishPost = function (type, success) {

                //Convert line breaks to <br/>
                $scope.message.content = $scope.message.content.replace(/(?:\r\n|\r|\n)/g, '<br/>');

                var data = {
                    author: $scope.authentication.user.id,
                    authorName: $scope.authentication.user.displayName,
                    content: $scope.message.content,
                    type: type,
                    dateTime: new Date().getTime(),
                    imageUrl: $scope.message.image || null,
                    targetUrl: $scope.message.targetUrl || null
                };

                $scope.hallFeed.$add(data).then(function () {
                    console.log("Published to Hall!");
                    $scope.message = {};
                    if (typeof success === 'function') {
                        success();
                    }
                }).catch(function (error) {
                    console.log(error);
                });
            };

            //Delete the added image - NO PHYSICAL DELETION HERE!
            $scope.deleteImage = function () {
                $scope.message.image = null;
            }

            //Watch the changes to the input box
            $scope.$watch('message.content', function (newVal, oldVal) {
                var content = newVal;
                var urlPattern = new RegExp("([a-zA-Z0-9]+://)?([a-zA-Z0-9_]+:[a-zA-Z0-9_]+@)?([a-zA-Z0-9.-]+\\.[A-Za-z]{2,4})(:[0-9]+)?(/.*)?");
                if (urlPattern.test(content)) {
                    var match = urlPattern.exec(content);
                    $scope.message.image = match[0];
                    $scope.message.content = content.replace(urlPattern, '');
                }
            });

            //Event Listener to publish to Hall
            $scope.$on('publishToHall', function (data, args) {
                $scope.isClosed = args.isClosed || false;
                subject = args.subject;
                $scope.message = {
                    image: args.image || null,
                    content: args.content || null,
                    targetUrl: args.targetUrl || null
                };
                $scope.init(function () {
                    $scope.publishPost(args.postType, args.success);
                });
            });

        }
    ]);
