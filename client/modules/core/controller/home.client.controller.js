angular.module('core').controller('HomeController', ['$scope', 'BootStrap', '$state', '$timeout', 'localStorageService', '$http', 'WS', '$rootScope', '$window' ,
    function ($scope, BootStrap, $state, $timeout, localStorageService, $http, WS, $rootScope, $window) {
        // This provides BootStrap context.
        $scope.authentication = BootStrap;
        $scope.showSignupEmailForm = false;
        //To toggle the 'Signup With Email' form
        $scope.toggleSignupEmailForm = function () {
            $scope.showSignupEmailForm = !$scope.showSignupEmailForm;
        }

        //Event Handler for Login modal
        angular.element(document).on('click', '[data-login-reveal]', function (f) {
            f.preventDefault();
            setupDialog('signin');
        });
        angular.element(document).on('click', '[data-logout-reveal]', function (f) {
                 localStorageService.remove("user");
		 localStorageService.remove("profile");
		 localStorageService.remove("stripe");
                $window.location.href="/auth/signout";
        });

        //Event Handle for Signup modal
        angular.element(document).on('click', '[data-signup-reveal]', function (f) {
            f.preventDefault();
            setupDialog('signup');
        });

        angular.element(document).on('click', '[data-forgot-password-reveal]', function (f) {
            f.preventDefault();
            setupDialog('forgot');
        });

        //Dialog setup for login/signup
        var setupDialog = function (type) {
            if ($scope.showSignupEmailForm) {
                $scope.toggleSignupEmailForm();
            }
            if ($state.current.name === type) {
                triggerReveal(type);
                return;
            }
            $state.go(type);
        };

        //Refresh foundation js state
        //$(document).foundation('dropdown','reflow');

        //if session has expired remove localStorage variables and signout
       var marketName = $window._marketId;
	if(marketName && $scope.authentication && $scope.authentication.user){
		$http({ 
                       url: $scope.authentication.api.baseUrl + "/users/" + $scope.authentication.user.id
                       + "/bootstrap",
                       method: 'get',
                       headers: {
                        'X-Market-ID': marketName,
                        'X-Auth-Token': $scope.authentication.user.accessToken
                       }
                      })
                      .then(function (response) {
                      }, function (_error) {
                          localStorageService.remove("user");
		          localStorageService.remove("profile");
		          localStorageService.remove("stripe");
                          $window.location.href="/auth/signout";
                          console.log("ushakiz error "+response+JSON.stringify(response));
                     });
	} 

        $http.get($scope.authentication.api.publicUrl + "/bootstrap/" + marketName)
            .success(function (response) {
		$http.get("/app/environment").success(function(data) {
			response.market.env = data.env;
			localStorageService.set('subsInfo', response.currSubscription)
			localStorageService.set('currentMarket', response.market);
		})
            }).error(function (response) {
                console.log(response);
            });
        //$scope.currentMarket = JSON.parse(JSON.stringify(response.market))
        if (marketName == 'wanteet') {
            marketName = '';
        }

        //function to open Modal
        var triggerReveal = function (domId) {
            $("#" + domId + "Modal").foundation('reveal', 'open');
        }

        //Event listener to load modal only after the html file is fully downloaded
        $scope.$on('$viewContentLoaded',
            function (event) {
                $timeout(function () {
                    triggerReveal($state.current.name);
                }, 0);
            });

        $scope.submitFeedback = function () {

            $http.post($scope.authentication.api.publicUrl + "/feedback",
                {
                    "name": $scope.fdbkFullName,
                    "email": $scope.fdbkFromEmail,
                    "message": $scope.fdbkMessage,
                    "phone": $scope.fdbkPhone
                })
                .success(function (response) {
                    console.log(response);
                    $('#feedbackForm form[name="feedbackForm"]').hide();
                    $('#feedbackForm div#success').show();
                    $('#feedbackForm div#success > i').addClass('animated rubberBand');

                    $timeout(function () {
                        $('#feedbackForm').removeClass('animated slideInUp').addClass('animated slideInDown');
                        $('#feedbackForm .accordion-navigation > div').addClass('animated fadeOut').removeClass('active');
                        $('#feedbackForm form[name="feedbackForm"]').show();
                        $('#feedbackForm div#success').hide();
                        $scope.fdbkFullName = "";
                        $scope.fdbkFromEmail = "";
                        $scope.fdbkMessage = "";
                        $scope.fdbkPhone = "";
                    }, 2000)

                })
                .error(function (response) {
                    console.log(response);
                });
        }

        $scope.contactUs = function () {
            $http.post($scope.authentication.api.publicUrl + "/feedback",
                {
                    "name": $scope.cntctusFullName,
                    "email": $scope.cntctusFromEmail,
                    "message": $scope.cntctusMessage,
                    "phone": $scope.cntctusPhone
                })
                .success(function (response) {
                    console.log(response);
                })
                .error(function (response) {
                    console.log(response);
                });
        };

        $scope.toggleFeedbackForm = function () {
            if ($('#feedbackForm .accordion-navigation > div').hasClass('active')) {
                $('#feedbackForm').removeClass('animated slideInUp').addClass('animated slideInDown');
                $('#feedbackForm .accordion-navigation > div').addClass('animated fadeOut').removeClass('active');
            } else {
                $('#feedbackForm').removeClass('animated slideOutDown').addClass('animated slideInUp');
                $('#feedbackForm .accordion-navigation > div').removeClass('animated fadeOut').addClass('active');
     	        if ($scope.authentication.user != ""){
	            var url =  "http://tracy.wanteet.com?user_id="+$scope.authentication.user.id+ "&access_token="+$scope.authentication.user.accessToken+ "&market_id="+$scope.authentication.user.marketId;
         	    $("#chatwidget2").attr("src", url);
                }
            }
        }

        $('.pac-container').addClass('needsclick');

        WS.init();

/*	 if(localStorageService.get("redirect")){
		console.log("!!!!!!! Sanjeet Inside the reload function");
		localStorageService.remove("redirect");
		$timeout(function(){
			$window.location.href="/dashboard#!/requests/ayou";
		}, 1500);	
	} **/


        $rootScope.$on('$stateChangeSuccess',
            function (event, toState, toParams, fromState, fromParams) {
                //console.log(toState);
                //console.log(toParams);
                //$http.post("",{})
                //    .success(function(response){
                //
                //    })
                //    .error(function(response){
                //
                //    });
            });
    }
]);
