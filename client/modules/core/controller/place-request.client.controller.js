angular.module('core').controller('PlaceRequestController',
    ['$scope', 'BootStrap', '$state', '$timeout',
        '$rootScope', '$http', 'localStorageService',
        '$location', 'Loader', '$filter', 'PlaceRequest', '$attrs', 'Answers',
        function ($scope, BootStrap, $state, $timeout,
                  $rootScope, $http, localStorageService,
                  $location, Loader, $filter, PlaceRequest, $attrs, Answers) {

            // This provides BootStrap
            $scope.formName = 'EventSubmitForm';
            $scope.authentication = BootStrap;
            $scope.market = BootStrap.market;
            $scope.isHyperLocal = ($scope.market.settings.addons['hyper_local'])? true: false;

            $scope.service = window.service || $scope.serviceName || $scope.market.settings.services[0].name;
            $scope.requestType = "Place Request";
            $scope.requestWord = "Request"
            if(typeof $scope.market.settings.branding !== 'undefined' && $scope.market.settings.services.length == 1){
                if(typeof $scope.market.settings.branding.request_phrase1 !== 'undefined' && typeof $scope.market.settings.branding.request_phrase2 !== 'undefined'){
                    if($scope.market.settings.branding.request_phrase1 && $scope.market.settings.branding.request_phrase2){
                        $scope.requestType = $scope.market.settings.branding.request_phrase1 + " " + $scope.market.settings.branding.request_phrase2
                        $scope.requestWord = $scope.market.settings.branding.request_phrase2
                    }	
                }
            }	

            if($scope.service == ""){
                $scope.service = $scope.market.settings.services[0].name;
            }

            $scope.service = $scope.service.toLowerCase();

            $scope.confNumber = 0;
            $scope.isWaiting = false;

            $scope.eventData = localStorageService.get('fullEventBkp') || {};
            $scope.answers = {};

            var isExclusive = ($scope.sellerId || $scope.listingId) ? true : false;

            var serviceNames = JSON.parse(JSON.stringify(window._market.settings.services));
            var serviceDesc = {};
            for (var idx in serviceNames) {
                if (serviceNames[idx] !== null) {
                    serviceDesc[serviceNames[idx]['name'].toLowerCase()] = serviceNames[idx]['description'];
                }
            }

            $scope.serviceDesc = serviceDesc[$scope.service];

            moment.locale('en');


            this.init = function(){
                //Setup EventData Object
                if ($scope.service) {

                    //Setup Event Data Object if it's empty
                    if (JSON.stringify($scope.eventData) === "{}") {
                        $scope.eventData = {
                            market: $scope.market.slug,
                            services: [
                                {
                                    service: $scope.service
                                }
                            ]
                        };
                    }
                }

                if ($attrs.type == 'questions') {
                    $scope.getQuestions = function (success) {
			console.log("Question api call is ", $scope.authentication.api.publicUrl + "/services/questions?category=" + $scope.service)
                        $http.get($scope.authentication.api.publicUrl + "/services/questions?category=" + $scope.service)
                            .success(function (response) {
                                $scope.questions = response.results;
				console.log("Questions api call response", response);
                                if (response.totalCount == 0) {
                                    $scope.noQuestions = true;
                                }
                                $timeout(function () {
                                    // Blocking selection of same day
                                    $("input[data-datetime-bind]").datetimepicker({
                                        minDate: (new Date()).setDate((new Date()).getDate() + 1),
                                        collapse: true,
                                        useCurrent: false
                                    }).on('dp.change', function (e) {
                                        var currentField = $(e.target);
                                        currentField = currentField[0];
                                        var questionId = currentField.id.match(/([0-9].*)/)[0];
                                        $scope.answers[questionId] = $('#' + currentField.id).val();
                                    });
                                }, 100);

                                $scope.isWaiting = false;
                                $scope.error = {};

                                if ($scope.eventData && $attrs.type == 'questions') {
                                    $scope.answers = Answers.setupAnswers($scope.questions, $scope.eventData.services[0].answers || []);
                                }

                                if (typeof success !== 'undefined') {
                                    success();
                                }
                            }).error(function (response) {
                                console.log(response);
                                $scope.isWaiting = false;
                            });
                    };

                    $scope.getQuestions();
                }

                //Exclusive Mode Setup
                if (isExclusive) {
                    var service;
                    //Get Reference to current Service
                    for (kdx in $scope.eventData.services) {
                        if ($scope.eventData.services[kdx]["service"] === $scope.service) {
                            service = $scope.eventData.services[kdx];
                        }
                    }

                    service["exclusive"] = {};

                    //Set Exclusive SellerId if any
                    if ($scope.sellerId) {
                        console.log("Add Exclusive record for - " + $scope.sellerId);
                        service.exclusive.sellerSlug = $scope.sellerId;
                    }
                    //Set Exclusive ListingId if Any
                    if ($scope.listingId) {
                        service.exclusive.listingSlug = $scope.listingId;
                    }
                    //Set Exclusive Listing Title if Any
                    if ($scope.listingTitle) {
                        service.exclusive.listingTitle = $scope.listingId;
                    }

                }

                //Setup DatePicker controls
                $timeout(function () {
                    // Blocking selection of same day
                    var ua = navigator.userAgent;

                    if (ua.match(/(iPhone|iPad)/i)) {
                        $("#dateAndTime").attr('type', 'datetime-local');
                        $('body').on('change', '#dateAndTime', function (e) {
                            $scope.eventData.dateTime = moment($('#dateAndTime').val()).local().
                                format('YYYY-MM-DDTHH:mm:ss.SSSZ');
                            if (!$scope.$$phase) {
                                $scope.$apply();
                            }
                        });
                    } else {
                        $("#dateAndTime").datetimepicker({
                            minDate: (new Date()).setDate((new Date()).getDate() + 1),
                            collapse: true,
                            useCurrent: false
                        });
                        $('body').on('dp.change', '#dateAndTime', function (e) {
                            console.log('dp change executed');
                            $scope.dateAndTime = $('#dateAndTime').val();
                            $scope.eventData.dateTime = $('#dateAndTime').data("DateTimePicker")
                                .date().local().format('YYYY-MM-DDTHH:mm:ss.SSSZ');
                            console.log($scope.eventData.dateTime);
                        });
                    }
                }, 10);

                $scope.formatDate = function (date) {
                    return moment(date).format('MM/DD/YYYY hh:mm A');
                };

                $scope.formatLocation = function (address) {
                    if (typeof address !== 'undefined') {
                        return address.toLowerCase().replace(/[, ]/g, '-');
                    }
                };

                $scope.serviceName = serviceDesc[$scope.service];
                console.log('Init Place Request Widget');
            };

            var locationHash = '';

            var processAnswers = function () {

                if (!$scope['EventSubmitForm'].$valid) {
                    $scope.setErrors = true;
                    return false;
                }

                return Answers.processAnswers(JSON.parse(JSON.stringify($scope.answers)));
            }

            $scope.processEvent = function () {
                $scope.isWaiting = true;
                locationHash = location.hash;

                if ($attrs.type == 'questions') {
                    if (!processAnswers()) {
                        $scope.isWaiting = false;
                        return;
                    }
                    $scope.eventData.services[0].answers = processAnswers();
                }

                $http.post($scope.authentication.api.baseUrl + "/users/"
                + ($scope.authentication.user.id || "1") + "/events/", $scope.eventData)
                    .success(function (response) {
                        $scope.eventData = response;
                        console.log("ushakiz post request successful "+$scope.eventData);
                        localStorageService.remove('fullEventBkp');

                        var hallContent = '<h5>New Request!</h5>';
                        hallContent += '<p>A request has been placed with following details</p>';
                        hallContent += '<table style="width:100%; margin:0; border:0">';
                        hallContent += '<tbody> ';
                        hallContent += '<tr>';
                        hallContent += '<td width="110">Service</td>';
                        hallContent += '<td>' + $scope.eventData.services[0].service + '</td>';
                        hallContent += '</tr>';
                       if ($scope.eventData.details) {
                            hallContent += '<tr>';
                            hallContent += '<td width="110">Details</td>';
                            hallContent += '<td>' + $scope.eventData.details + '</td>';
                            hallContent += '</tr>';
                        }
                        hallContent += '</tbody></table>';

                        var targetUrl = location.origin + '/' + '/dashboard/#!/';
                        targetUrl += 'requests/request/' + $scope.eventData.services[0].id;
                        targetUrl += '/' + $scope.eventData.services[0].displayId;

                        if (isExclusive) {
                            //$("#excusiveListingRequest").foundation('reveal', 'close');
                            window.location.href = '/dashboard#!/requests/event/' + $scope.eventData.id;

                            //TODO: Do no post to Hall in case it's an exclusive Request (?) [Might need to refactor]
                            return;
                        }

                        //Due to some bug, not able to transfer to child state, hence saving data to localStorage
                        //& deleting it in next state
                        //TODO: Refactor & fix the bug w.r.t child states
                        //localStorageService.set('cReqValObj' + $scope.service, $scope.eventData);

                        $scope.$broadcast('publishToHall', {
                            postType: 0,
                            content: hallContent,
                            targetUrl: targetUrl,
                            success: function () {
                                Loader.stopLoader(true);
                                console.log("ushakiz post request publish to hall ");

                                $scope.isWaiting = false;

                                var url = '/search/' + $scope.service + '?referenceId=' + $scope.eventData.id;
                                //url += '&location=' + $scope.eventData.address;

                                window.location.href = url;
                            }
                        });
                        var url = '/dashboard#!/requests/byou';
                        window.location.href = url;

                        $state.go('requests');
                    })
                    .error(function (response) {
                        $scope.isWaiting = false;
                        $scope.error = {};
                        localStorageService.set('fullEventBkp', $scope.eventData);
                        localStorageService.set('logredir', location.pathname + locationHash);
                        console.log("ushakiz post request error "+response.errors);

                        if (typeof response !== 'undefined') {
                            for (var idx in response.errors) {
                                $scope.error[response.errors[idx]['field']] = {
                                    value: response.errors[idx]['value'],
                                    message: response.errors[idx]['message']
                                };
                            }
                        }
                    });
            };
        }
    ])
;

