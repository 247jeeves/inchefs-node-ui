angular.module('core').controller('PublicProfileController', ['$scope', 'BootStrap', '$state', '$timeout', '$http',
    function ($scope, BootStrap, $state, $timeout, $http) {
        // This provides BootStrap context.
        $scope.authentication = BootStrap;

        var market = BootStrap.market;
        var reviewPaginate = {
            sellerProfile: {
                max: 10,
                offset: 0
            },
            buyerProfile: {
                max: 10,
                offset: 0
            }
        }
        $scope.sellerReviews = [];
        $scope.buyerReviews = [];
        $scope.calendarEvents = [];

        $scope.isWaiting = false;

        var entity = JSON.parse($("meta[name='_publicProfile']").attr('content'));

        console.log(entity);

        $scope.showCalendar = false;
        $scope.showToggle = (market.settings.addons && market.settings.addons['event_calendar']);

        $scope.toggleView = function () {
            $scope.showCalendar = !$scope.showCalendar;
            if ($scope.showCalendar) {
                if ($scope.calendarEvents.length == 0) {
                    initCalendatEvents();
                }
                $('#calendar').fullCalendar('destroy');
                $timeout(function () {
                    renderCalendar();
                }, 10);
            }
        };

        var renderCalendar = function () {
            $('#calendar').fullCalendar({
                fixedWeekCount: false,
                selectable: true,
                //editable: true,
                //selectHelper: true,
                eventLimit: true,
                timezone: 'local',
                select: select,
                eventClick: eventClick
            });

            for (idx in $scope.calendarEvents) {
                $('#calendar').fullCalendar('renderEvent', $scope.calendarEvents[idx], true);
            }
        };

        var eventClick = function (event, jsEvent, view) {
            console.log('eventClick');
            $("#dateAndTime").val(event.start.format('MM/DD/YYYY hh:mm A'));
            $('#dateAndTime').data("DateTimePicker").date(event.start);
        };

        var select = function (start, end, jsEvent, view) {
            console.log('select');
            $("#dateAndTime").val(start.format('MM/DD/YYYY hh:mm A'));
            $('#dateAndTime').data("DateTimePicker").date(start);
        }

        var initCalendatEvents = function () {

            $http.get($scope.authentication.api.publicUrl + "/users/"
                + entity.id + "/calendar/events"
            )
                .success(function (response) {
                    $scope.calendarEvents = response.results;
                    for (idx in $scope.calendarEvents) {
                        $('#calendar').fullCalendar('renderEvent', $scope.calendarEvents[idx], true);
                    }
                    $scope.isWaiting = false;
                })
                .error(function (response) {
                    errorHandler(response);
                });

        }

        $scope.initReviews = function () {
            $scope.isWaiting = true;
            $scope.$broadcast('loadReviews', {
                id: entity.id,
                type: 'buyerProfile',
                paginate: reviewPaginate.buyerProfile,
                success: function (response) {
                    $scope.isWaiting = false;
                    $scope.buyerReviews = response;
                },
                error: function (response) {
                    $scope.isWaiting = false;
                    console.log(response);
                }
            });
        }

        $scope.initReviews();

        console.log($scope.showToggle == true && entity.sellerProfile && entity.sellerProfile.listings.length == 0);

        if($scope.showToggle == true && entity.sellerProfile && entity.sellerProfile.listings.length == 0){
            console.log("In Here");
            $timeout(function(){
                $scope.toggleView();
                $scope.showToggle = false;
            }, 100);
        }
    }
]);
