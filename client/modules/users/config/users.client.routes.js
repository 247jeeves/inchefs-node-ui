// Setting up route
angular.module('users').config(['$stateProvider', '$urlRouterProvider',
    function ($stateProvider, $urlRouterProvider) {
        // Users state routing
        $stateProvider.
            //state('profile', {
            //	url: '/settings/profile',
            //	templateUrl: 'modules/users/views/settings/edit-profile.client.view.html'
            //}).
            //state('password', {
            //	url: '/settings/password',
            //	templateUrl: 'modules/users/views/settings/change-password.client.view.html'
            //}).
            //state('accounts', {
            //	url: '/settings/accounts',
            //	templateUrl: 'modules/users/views/settings/social-accounts.client.view.html'
            //}).
            state('signup', {
                url: '/signup',
                views: {
                    'auth@': {
                        templateUrl: 'modules/users/views/authentication/signup.client.view.html'
                    }
                }
            }).
            state('signin', {
                url: '/signin',
                views: {
                    'auth@': {
                        templateUrl: 'modules/users/views/authentication/signin.client.view.html'
                    }
                }
            }).
            //state('signin', {
            //    url: '/signin',
            //    templateUrl: 'modules/users/views/authentication/signin.client.view.html'
            //}).
            state('forgot', {
                url: '/password/forgot',
                views: {
                    'auth@': {
                        templateUrl: 'modules/users/views/password/forgot-password.client.view.html'
                    }
                }
            }).
            state('reset-invalid', {
                url: '/password/reset/invalid',
                templateUrl: 'modules/users/views/password/reset-password-invalid.client.view.html'
            }).
            state('reset-success', {
                url: '/password/reset/success',
                templateUrl: 'modules/users/views/password/reset-password-success.client.view.html'
            }).
            state('reset', {
                url: '/password/reset/:token',
                templateUrl: 'modules/users/views/password/reset-password.client.view.html'
            }).

            state('createYourMarket', {
                url: '/mcr',
                templateUrl: 'modules/users/views/create-your-market/index.client.view.html',
                controller: 'CreateMarketController'
            }).

            state('createYourMarket.register', {
                url: '/register',
                templateUrl: 'modules/users/views/create-your-market/signup-email.client.view.html',
                parent: 'createYourMarket'
            }).
            state('createYourMarket.subscriptions', {
                url: '/choose-payment-plan',
                templateUrl: 'modules/users/views/subscriptions/subscriptions.client.view.html',
                parent: 'createYourMarket'
            }).
            state('createYourMarket.setupMarket', {
                url: '/setup-market',
                templateUrl: 'modules/users/views/create-your-market/setup-market.client.view.html',
                parent: 'createYourMarket'
            }).
            state('createYourCustomerServiceCenter', {
                url: '/csc',
                templateUrl: 'modules/users/views/create-your-customer-service-center/index.client.view.html',
                controller: 'CreateCustomerServiceCenterController'
            }).
            state('createYourCustomerServiceCenter.register', {
                url: '/register-csc',
                templateUrl: 'modules/users/views/create-your-customer-service-center/signup-email.client.view.html',
                parent: 'createYourCustomerServiceCenter'
            }).
            state('createYourCustomerServiceCenter.subscriptions', {
                url: '/choose-payment-plan-csc',
                templateUrl: 'modules/users/views/subscriptions/subscriptions.client.view.html',
                parent: 'createYourCustomerServiceCenter'
            }).
            state('createYourCustomerServiceCenter.setupCustomerServiceCenter', {
                url: '/setup-customer-service-center',
                templateUrl: 'modules/users/views/create-your-customer-service-center/setup-customer-service-center.client.view.html',
                parent: 'createYourCustomerServiceCenter'
            }).

            state('claimProspectCompletion', {
                url: '/complete-claim-process',
                templateUrl: 'modules/users/views/prospect/signup-email.client.view.html',
                controller: 'ProspectClaimController'
            });

        if (window.location.pathname.indexOf('/create-your-market') != -1) {
            $urlRouterProvider.otherwise('/mcr');
        }
        if (window.location.pathname.indexOf('/create-your-customer-service-center') != -1) {
            $urlRouterProvider.otherwise('/csc');
        }
        if (window.location.pathname.indexOf('/claim/verify') != -1) {
            $urlRouterProvider.otherwise('/complete-claim-process');
        }

    }
]);
