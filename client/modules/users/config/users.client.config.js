// Config HTTP Error Handling
angular.module('users').config(['$httpProvider', 'LoaderProvider',
    function ($httpProvider, Loader) {
        // Set the httpProvider "not authorized" interceptor
        $httpProvider.interceptors.push(['$q', '$location', 'BootStrap', 'Loader',
            function ($q, $location, BootStrap, Loader) {
                return {

                    // optional method
                    'request': function (config) {
                        Loader.startLoader();
                        return config;
                    },

                    // optional method
                    'requestError': function (rejection) {
                        Loader.stopLoader('error');
                        if (canRecover(rejection)) {
                            return responseOrNewPromise
                        }
                        return $q.reject(rejection);
                    },


                    // optional method
                    'response': function (response) {
                        if (response.config.method == 'GET') {
                            Loader.stopLoader('none');
                        } else {
                            Loader.stopLoader('success');
                        }
                        return response;
                    },

                    responseError: function (rejection) {
                        switch (rejection.status) {
                            case 401:
                            case 403:
                            case 404:
                                Loader.stopLoader('none');
                                break;
                            default:
                                Loader.stopLoader('error');
                                break;
                        }

                        switch (rejection.status) {
                            case 401:
                                // Deauthenticate the global user
                                BootStrap.user = null;

                                // Redirect to signin page
                                $location.path('signin');
                                //window.location.href('/#!/signin');
                                break;
                            case 403:
                                // Add unauthorized behaviour
                                BootStrap.user = null;

                                // Redirect to signin page
                                $location.path('signin');
                                //window.location.href('/#!/signin');
                                break;
                        }

                        return $q.reject(rejection);
                    }
                };
            }
        ]);
    }
]);

angular.module('users').run(['$http', 'BootStrap', 'Menus', 'localStorageService',
    function ($http, BootStrap) {

        $http.defaults.headers.common['Content-Type'] = 'text/plain';

        delete $http.defaults.headers.common['X-Requested-With'];

        $http.defaults.headers.common['X-Market-ID'] = BootStrap.market.slug;

        if (BootStrap.user) {
            $http.defaults.headers.common['X-Auth-Token'] = BootStrap.user.accessToken;
        }
    }]);
