angular.module('users').controller('ProspectClaimController',
    ['$scope', 'BootStrap', '$state', '$timeout', '$rootScope', 'Menus', '$http', '$filter', 'Subscription',
        function ($scope, BootStrap, $state, $timeout, $rootScope, Menus, $http, $filter, Subscription) {
            // This provides BootStrap context.
            $scope.authentication = BootStrap;

            $scope.user = $scope.authentication.user;

            $scope.currentMarket = JSON.parse(JSON.stringify(BootStrap.market));

            $scope.error = {};
            $scope.isWaiting = false;

            $scope.market = {};

            $scope.allServices = BootStrap.services;

            $scope.credentials = {
                username: window.prospectEmail,
                terms: true
            };

            $scope.signup = function () {

                console.log($scope.credentials);

                $scope.credentials["market"] = BootStrap.market.slug;
                $scope.credentials["mcrSid"] = Math.floor(Date.now() / 1000);

                var url = $scope.authentication.api.publicUrl + '/prospects/claim/verify/';

                url += window.prospectId + '/' + window.claimToken;

                console.log(url);

                $http.post(url, $scope.credentials).success(function (response) {
                    // If successful we assign the response to the global user model
                    $http.post('/auth/signin', $scope.credentials).success(function (response) {
                        $scope.authentication.user = response;
                        $timeout(function () {
                            window.location.href = '/dashboard';
                        }, 50);
                    });
                    // And redirect the user to message Page


                }).error(function (response) {
                    $scope.error = {};
                    for (var idx in response.errors) {
                        $scope.error[response.errors[idx]['field']] = {
                            value: response.errors[idx]['value'],
                            message: response.errors[idx]['message']
                        };
                    }
                });
            };

            var errorHandler = function (response) {
                console.log(response);
                $scope.error = {};
                if (response && response !== undefined && response.hasOwnProperty('errors')) {
                    for (var idx in response.errors) {
                        $scope.error[response.errors[idx]['field']] = {
                            value: response.errors[idx]['value'],
                            message: response.errors[idx]['message']
                        };
                    }
                }
                $scope.isWaiting = false;
            };

        }
    ]);
