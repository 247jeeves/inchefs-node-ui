angular.module('users').controller('SubscriptionsController', ['$scope', 'BootStrap',
    '$state', '$timeout', '$http', 'localStorageService', 'Subscription', '$window',
    function ($scope, BootStrap, $state, $timeout, $http, localStorageService, Subscription, $window) {
        // This provides BootStrap context.
        $scope.authentication = BootStrap;

        var planType;

        var cb = undefined;

        $scope.gridCols = 3;

        $scope.isAdmin = (BootStrap.user.permissions.indexOf('ROLE_ADMIN') != -1);
        $scope.isSetup = false;

	$scope.market = BootStrap.market;
	$scope.profile = BootStrap.profile;


	$scope.currentSubs = BootStrap.subscription
	$scope.subsInfo = (typeof $scope.currentSubs["market-subscription-info"] !== "undefined") ? $scope.currentSubs["market-subscription-info"] : ''
	console.log("Subscription controller " + $scope.subsInfo)
	$scope.subsInfo = ($scope.subsInfo != '') ? JSON.parse($scope.subsInfo) : ''
	console.log("Updated subsInfo " + JSON.stringify($scope.subsInfo));
	$scope.currentPlan = ($scope.subsInfo != '') ? $scope.subsInfo[0].plan.name : ''
	console.log("subs controller currentPlan " + $scope.currentPlan);
	$scope.planStatus = ($scope.subsInfo != '') ? $scope.subsInfo[0].status: ''
	console.log("subs controller planStatus" + $scope.planStatus);
	$scope.showTrial = $scope.currentSubs["showTrial"];
	console.log("show trial plan " + $scope.showTrial)

	$scope.initScreen = function () {
            if (!$scope.isSetup) {
	         console.log('init subscriptions Subscription controller');
                 console.log('init subscriptions Subscription controller json parameters' + JSON.stringify({
                     marketId: BootStrap.market.slug,
                     planType: 'market_subscription',
                     gridCols: 4
                 }))
                 Subscription.initSubscriptions({
                     marketId: BootStrap.market.slug,
                     planType: 'market_subscription',
                   gridCols: 4
                 });
		console.log("initScreen before setupSubscription" + JSON.stringify(Subscription.getInitSubscription()))
                setupSubscriptions(Subscription.getInitSubscription());
            }
        }

        $scope.init = function (data) {
            console.log("ushakiz init SubscriptionController");
            $scope.market = data.market;
            console.log("Scope market " + JSON.stringify(data.market))	    
            $scope.plantType = planType = data.planType;
            $scope.showTrialPlan = $scope.showTrial;

            if(data.profile && data.profile.showTrial){
                $scope.showTrialPlan = data.profile.showTrial[$scope.market.slug] || true;
            }

            $scope.showTrialPlan = $scope.showTrial;
            var setupSubscription = function () {
                var subscription = undefined;
		if (data.profile && data.profile.subscriptions[$scope.market.slug]) {
                    subscription = {};
                    var userSub = data.profile.subscriptions[$scope.market.slug];
                    subscription[planType] = $scope.market.settings.plans[planType][userSub.name];
                    subscription[planType].name = userSub.name;
                    subscription[planType].leftCount = userSub.leftCount;
                }
                return subscription;
            };

            $scope.currentSubscription = setupSubscription();
            $scope.market['getPlans'] = function () {
                var plans = [];
                for (plan in $scope.market.settings.plans[planType]) {
                    var data = JSON.parse(JSON.stringify($scope.market.settings.plans[planType][plan]));
                    if ($scope.subsInfo && $scope.currentPlan == "wanteet-market-subscription-"+plan.toLowerCase()) {
                        data.subscribed = true;
                    } else {
                        data.subscribed = false;
                    }
                    data.name = plan;
                    plans.push(data);
                }
                return plans;
            };

            $scope.plans = $scope.market.getPlans();
        };

        //Make Subscriptions generic as a callout module
        $scope.$on('setupMarketSubscriptions', function (data, args) {
            console.log("setup market subscriptions 1"+args);
            setupSubscriptions(args);
        });

        var setupSubscriptions = function (data) {
            if(data.gridCols){
                $scope.gridCols = data.gridCols;
            }
            var marketId = data.marketId;
            var planType = data.planType;
            console.log("setup market subscriptions 2 - plantype "+planType);
            if (data.cb) {
                console.log("setup market subscriptions 3 callback data "+data.cb);
                cb = data.cb;
            }

            //Fetch market plans
            $http({
                url: $scope.authentication.api.baseUrl + "/users/" + $scope.authentication.user.id
                + "/bootstrap",
                method: 'get',
                headers: {
                    'X-Market-ID': "247jeeves", //fetch the default market subscription plans
                    'X-Auth-Token': $scope.authentication.user.accessToken
                }
            })
            .success(function (response) {
		
		$scope.market.settings.plans = "";
		$scope.market.settings.plans = response.market.settings.plans;
		
		console.log("Market settings plans are " + JSON.stringify($scope.market.settings.plans));
	
                $scope.init({
                    market: $scope.market,
                    profile: $scope.profile,
                    planType: planType
                });
            })
            .error(function (response) {
                console.log(response);
            });
        }

        // Trigger Confirmation Dialog for Subscription Purchase
        $scope.confirmSubscription = function (planName) {

            $scope.selectedPlan = $scope.market.settings.plans[planType][planName];
            $scope.selectedPlan.name = planName;

            $("#confirmSubscriptionDialog")
                .find('[data-cancel-button]').on('click', function (e) {
                    $("#confirmSubscriptionDialog").foundation('reveal', 'close');
                    $scope.selectedPlan = {};
                    e.preventDefault();
                });

            $("#confirmSubscriptionDialog")
                .find('[data-confirm-button]').on('click', function (e) {
                    $("#confirmSubscriptionDialog").foundation('reveal', 'close');
                    buySubscription(planName);
                    e.preventDefault();
                });


            $timeout(function () {
                $("#confirmSubscriptionDialog").foundation('reveal', 'open');
            }, 10);
            return false;
        };

        $scope.cancelSubscription = function () {

            $("#cancelSubscriptionDialog")
                .find('[data-cancel-button]').on('click', function (e) {
                    $("#cancelSubscriptionDialog").foundation('reveal', 'close');
                    $scope.selectedPlan = {};
                    e.preventDefault();
                });

            $("#cancelSubscriptionDialog")
                .find('[data-confirm-button]').on('click', function (e) {
                    $("#cancelSubscriptionDialog").foundation('reveal', 'close');
                    cancelSubscription();
                    e.preventDefault();
                });


            $timeout(function () {
                $("#cancelSubscriptionDialog").foundation('reveal', 'open');
            }, 10);
            return false;
        };

        var buySubscription = function (planName) {
            var data = {
                market: $scope.market.slug,
                name: planName,
                type: planType
            }

            if ($scope.market.settings['plans'][planType][data.name]['amount'] >= 0.00) {
                initStripePayment(data);
            } else {
                createSubscription(data);
            }

        };

        var createSubscription = function (data) {
             
            console.log("ushakiz creating subscription url "+ JSON.stringify($scope.authentication.user)+" data "+data);
            $http.post($scope.authentication.api.baseUrl + "/users/" + $scope.authentication.user.id
                + "/subscriptions"
                , data)

                .then(function (response) {
		  localStorageService.set('subscription', response.data)
	
		$http({ //user bootstrap api call start
                        url: $scope.authentication.api.baseUrl + "/users/" + $scope.authentication.user.id + "/bootstrap",
			method: 'get',
                         headers: {
                            'X-Market-ID': $scope.market.slug,
                            'X-Auth-Token': $scope.authentication.user.accessToken
			}
		}).then(function(_response){
        	    localStorageService.set('profile', _response.data.profile);
	
    	            $http({ //market bootstrap api call start
	       		        url: $scope.authentication.api.publicUrl + "/bootstrap/" + $scope.market.name,
				method: 'get',
	               		 headers: {
		                    'X-Market-ID': $scope.market.slug
	        	        }
	        	    })
	               	.then(function (_bootstrap) {
				localStorageService.set('subsInfo',_bootstrap.data.currSubscription)
                		if(_response.data.profile.subscriptions.hasOwnProperty($scope.market.slug) || _bootstrap.data.currSubscription.hasOwnProperty(market-subscription-info)){
                        		$window.location.reload();
		                }
        		    },function (error) {
	        	        console.log(error);
		            }); //market bootstrap api call end
          	
			}); // user bootstrap api call end			

        	          // $window.location.reload();
	                }, function (error){
		   console.log("error creating subscription"+response);
	  }); //create subscription api call end

        };

        var cancelSubscription = function () {
            var data = {
                market: $scope.market.slug,
                name: 'dummy'
            }
            $http.delete($scope.authentication.api.baseUrl + "/users/" + $scope.authentication.user.id
            + "/subscriptions/" + $scope.market.slug)
                .success(function (response) {
		    localStorageService.set('subsInfo', '');
                    location.reload();
                })
                .error(function (response) {
                    console.log("cancelSubscription "+response);
                });
        }

        var initStripePayment = function (data) {

            var stripePubKey = window._stripe.public;
            var plan = $scope.market.settings['plans'][planType][data.name];
            var img = $scope.market.settings.logo || '/assets/img/wanteet.png';
            plan.name = plan.name.charAt(0).toUpperCase() + plan.name.substr(1).toLowerCase();

            var description = plan.name + ' Subscription '
                + '($' + plan['amount'].toFixed(2) * 100 / 100
                + ' / ' + plan['type'] + ')';

            if (typeof stripePubKey !== 'undefined') {
                var stripeHandler = StripeCheckout.configure({
                    key: stripePubKey,
                    name: $scope.market.name,
                    image: img,
                    email: $scope.authentication.profile.email,
                    billingAddress: true,
                    description: description,
                    amount: Math.round(plan['amount'].toFixed(2) * 100),
                    panelLabel: "Subscribe",
                    allowRememberMe: false,
                    token: function (token, args) {
                        $scope.isWaiting = false;

                        data["stripeData"] = token;

                        createSubscription(data);
                    },
                    opened: function (e) {
                        $scope.isWaiting = true;
                    },
                    closed: function (e) {
                        $scope.isWaiting = false;
                    }
                });


                stripeHandler.open();
            }
        }

    }
]);
