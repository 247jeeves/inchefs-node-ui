angular.module('users').controller('CreateCustomerServiceCenterController',
    ['$scope', 'BootStrap', '$state', '$timeout', '$rootScope', 'Menus', '$location', '$http', '$filter', 'Subscription','localStorageService', '$window',
       function ($scope, BootStrap, $state, $timeout, $rootScope, Menus, $location , $http, $filter, Subscription, localStorageService, $window) {
            // This provides BootStrap context.
            console.log("ushakiz csc controller ");
            $scope.authentication = BootStrap;

            $scope.user = $scope.authentication.user;
            console.log("ushakiz csc user  localstorage service ");

            $scope.currentMarket = JSON.parse(JSON.stringify(BootStrap.market));

            $scope.error = {};
            $scope.isWaiting = false;
	  //Random Password for the Jeeves Engine
     	   $scope.randomPassword = Math.random().toString(36).slice(-8);

            $scope.market = {};

            $scope.allServices = BootStrap.services;

            $scope.credentials = {};

            $scope.columns = 'small-8';

            $scope.progress = {
                isFirstStepDone: false,
                isSecondStepDone: false,
                isThirdStepDone: false,
                countDown: 0
            };

            var currentSubscription = undefined;

                console.log("ushakiz current subscription status "+JSON.stringify(BootStrap.market));
            if (typeof BootStrap.market.getCurrentUserSubscription === 'function') {
                currentSubscription = BootStrap.market.getCurrentUserSubscription(BootStrap.market.slug, 'market_subscription'); //'wanteet'
            }
            console.log("ushakiz current subscription status "+currentSubscription);

            var hasSubscription = (currentSubscription) ? true : false;
            var hasMarket = ($scope.user && $scope.user.permissions.indexOf('ROLE_MARKET_ADMIN') != -1);
            console.log("ushakiz current subscription status hasSubscription "+hasSubscription+  " hasMarket "+hasMarket);

            if (!$scope.user) {
                console.log("ushakiz signup not valid user");
                console.log("ushakiz signup not valid user authentication"+$scope.authentication);
                console.log("ushakiz signup not valid user Bootstrap "+BootStrap); 
                $state.go('createYourCustomerServiceCenter.register');
            } else if ($scope.user && !hasSubscription) {
                $scope.columns = 'small-12';
                console.log("ushakiz signup user valid has no subscription");
                $state.go('createYourCustomerServiceCenter.subscriptions');
            } else if ($scope.user && hasSubscription && !hasMarket) {
                console.log("ushakiz signup user valid has subscription go to setup");
                $state.go('createYourCustomerServiceCenter.setupCustomerServiceCenter')
            }

            $scope.initSubscriptions = function () {
                console.log('init subscriptions');
		Subscription.initSubscriptions({
                    marketId: BootStrap.market.slug,
                    //marketId: 'jeeves247';
		    planType: 'market_subscription',
                    gridCols: 4
                });
                console.log('after init subscriptions');
            };

            $scope.signup = function(){
                $scope.credentials["market"] = BootStrap.market.slug;
                $scope.credentials["mcrSid"] = Math.floor(Date.now() / 1000);
		$http.post('/auth/signup', $scope.credentials).then(function (signupResponse)  {
                    // If successful we assign the response to the global user model
                        //console.log("ushakiz signup signin");
                    $http.post('/auth/signin', $scope.credentials).then(function (response) {
		        $scope.authentication.user = response.data;
                        localStorageService.set('user', response.data);
                        //console.log("ushakiz signup signin cscauthentication "+JSON.stringify(response.data));
                        //console.log("ushakiz signup signin csc market "+BootStrap.market.slug);
                        if ($scope.authentication && (BootStrap.market.slug)){
                            //console.log("ushakiz signup signin csc"+$scope.authentication);
                            $http({
                                    url: $scope.authentication.api.baseUrl + "/users/" + $scope.authentication.user.id
                                    + "/bootstrap",
                                method: 'get',
                                headers: {
                                    'X-Market-ID': BootStrap.market.slug,
                                    'X-Auth-Token': $scope.authentication.user.accessToken
                                }
                            })
                            .then(function (_response) {
                                localStorageService.set('profile', _response.data.profile);
                                localStorageService.set('stripe', _response.data.payments.stripe);

                                // And redirect the user to message Page
                                $timeout(function () {
                                   //console.log("ushakiz reload page csc");
                                   $window.location.reload();
                                }, 50);                                

                            }, function(error){
                                 console.log(error);
                            });
                       }
                     }, function (errResponse) {
                         $scope.error = {};
                         for (var idx in response.errors) {
                             $scope.error[response.errors[idx]['field']] = {
                                 value: response.errors[idx]['value'],
                                 message: response.errors[idx]['message']
                             };
                         }
                    });
                });
            };

            $scope.setupCreateCustomerServiceCenter = function () {

                console.log("ushakiz csc  setupcreateCustomerServiceCenter");
                $scope.progress = {
                    isFirstStepDone: false,
                    isSecondStepDone: false,
                    countDown: 0
                };

                $scope.market = {
                    settings: {
                        autoRoute: true,
                        offerType: 'join',
                        owner: {
                            user: $scope.user.username
                        },
                        marketAdmins: [$scope.user.username],
                        services: ['customerService'],
                        addons: {
                            seller_subscription: false,
                            prospect_finder: false,
                            faq: true
                        }
                    }
               };
		
		if(BootStrap.market.name == '247jeeves'){
			 $scope.market.settings.plans = BootStrap.market.settings.plans;
		}

                if ($scope.user.permissions.indexOf('ROLE_ADMIN') == -1) {
		console.log("Current market subscription id is", $scope.market.settings.owner['marketSubscriptionId']);
                    $scope.market.settings.owner['marketSubscriptionId'] = currentSubscription.id
                }

                $timeout(function () {
                    $("#tags").selectize({
                        create: function (input, callback) {
                            callback({
                                name: input,
                                description: input
                            });
                        },
                        valueField: 'name',
                        labelField: 'description',
                        searchField: 'description',
                        delimiter: ',',
                        maxItems: '10',
                        options: $scope.allServices,
                        sortField: {
                            field: 'description',
                            direction: 'asc'
                        },
                        onChange: function (value) {
                            if (value == null) {
                                $scope.market.settings.services = [];
                            } else {
                                $scope.market.settings.services = value;
                            }
                        }
                    });
                }, 5);
            };

            $scope.createCustomerServiceCenter = function () {
                console.log("ushakiz csc  createCustomerServiceCenter");

                $scope.isWaiting = true;

                var data = JSON.parse(JSON.stringify($scope.market));

                data['notify'] = true;

                $http.post($scope.authentication.api.baseUrl + "/users/"
                    + $scope.authentication.user.id + "/markets/",
                    data)
                    .success(function (response) {
                        $scope.market = response;
                        $scope.error = {};
                        console.log("create customer service center"+$scope.market);
                        $scope.progress.isFirstStepDone = true;
                        $scope.isWaiting = false;
                        $scope.createJeevesEngineAccount();
		        $scope.createCustomerServiceCenterStep2();
                    })
                    .error(function (response) {
                        errorHandler(response);
                        $scope.isWaiting = false;
                    });
        };
	//To save the jeeves engine password
	$scope.saveJeevesEnginePassword = function(){
		var req = {
			 method: 'POST',
			 url: "/jeeves/register",
			 headers: {
			   'Content-Type': 'application/json',
			  'X-market-ID': $scope.market.slug
			 },
		       	 data: { 
				data: { 
                                'username':$scope.authentication.user.username,
                                'provider': "jeevesengine",
                                'email':$scope.authentication.user.username,
                                'firstName':$scope.authentication.user.firstName,
                                'LastName': $scope.authentication.user.lastName,
                                'password':$scope.randomPassword,
                                'marketId':$scope.market.slug,
			       }

				}
			}
		//Send Request to save password of jeeves engine as ext profile
		$http(req).then(function(success){
			//console.log("User Password Successfully!: "+ success);
		},function(err){
			console.log("Error "+ err);
		})


	 };

	 $scope.createJeevesEngineAccount = function(){
			$scope.environment = $scope.market.env;
			$scope.engineUrl = "";
			if($scope.environment == "production"){
                                 $scope.engineUrl='https://engine.247jeeves.com';
                        }else{
                                 $scope.engineUrl='https://qa.engine.247jeeves.com';
                        }

                         $http.post($scope.engineUrl+'/api/v2/createUser', {
                           "servicecenter": $scope.market.slug,
                            "username": $scope.authentication.user.username,
                           "password": $scope.randomPassword,
                         }).success(function (response){
                                 if(response.status=='success'){
                                        $scope.saveJeevesEnginePassword();

                                }
                         }).error(function (error){
                                errorHandler(error);
                         });
	  };
  
         $scope.updateSubscription = function(subscriptionId, market){
           var userId = $scope.authentication.user.id;
	   console.log("userId: " + userId + " subscriptionId: " + subscriptionId +  " marketId: " + market)
       	   $http.put($scope.authentication.api.baseUrl + "/marketsubscriptions/"
                    + $scope.authentication.user.id + "/" + subscriptionId + "/" + market
   		    )
                    .success(function (response) {
                        console.log("update subscription "+ response);
                    })
                    .error(function (response) {
                        errorHandler(response);
                    });
           
	 }


	  $scope.createCustomerServiceCenterStep2 = function () {

                var progressBar = setInterval(function () {
                    if ($scope.progress.countDown == 100) {
                        $scope.progress.countDown = 0;
                    }
                    $scope.progress.countDown++;
                    $scope.progress.style = { width: $scope.progress.countDown + '%' };
                    if (!$scope.$$phase) {
                        $scope.$apply();
                    }
                }, 50);

                $http.post('/market-admin/setup-domain', {
                    market: $scope.market.slug,
                    domains: $scope.market.settings.urls
                })
                .success(function (response) {
                    $http.post('/market-admin/setup-pages', {
                        market: $scope.market.slug
                    }).success(function (response) {
		//After creating marketPlace update the subscription using new marketId
		currentSubscription = localStorageService.get('subscription')
		console.log("current subscription - ", JSON.stringify(currentSubscription));
		console.log("Updating subscription - passing subscription id - ", currentSubscription.id)
			$scope.updateSubscription(currentSubscription.id, $scope.market.slug)
			$timeout(function () {
                                $scope.progress.isSecondStepDone = true;
                                $scope.progress.countDown = 100;
                                clearInterval(progressBar);
                            }, 5000);
                    }).error(function (response) {
                        });
                    })
                .error(function (response) {
                });
            };

            $scope.setupMarketForm = function () {

                $scope.market = {};

                if ($scope.markets.length > 0) {
                    for (idx in $scope.markets) {
                        if ($scope.markets[idx].slug == $state.params.marketId) {
                            $timeout(function () {
                                $scope.market = $scope.markets[idx];
                                $scope.isWaiting = false;

                                setupEditUI();
                            }, 5);
                            return;
                        }
                    }
                } else {
                    $http.get($scope.authentication.api.baseUrl + "/users/"
                        + $scope.authentication.user.id + "/markets/"
                        + $state.params.marketId
                    )
                        .success(function (response) {
                            $scope.market = response;
                            $scope.isWaiting = false;

                            setupEditUI();

                        })
                        .error(function (response) {
                            errorHandler(response);
                        });
                }
            };

            var setupEditUI = function () {
                myCodeMirror = undefined;
                myTextArea = undefined;

                if ($scope.market.settings.hasOwnProperty('services')) {
                    var services = JSON.parse(JSON.stringify($scope.market.settings.services));
                    $scope.market.settings.services = [];
                    angular.forEach(services, function (value, key) {
                        this.push(value.name);
                    }, $scope.market.settings.services);
                }

                var myTextArea = angular.element("#codeEditor")[0];
                if (myTextArea) {
                    var myCodeMirror = CodeMirror.fromTextArea(myTextArea, {
                        lineNumbers: true,
                        matchBrackets: true,
                        autoCloseBrackets: true,
                        mode: "application/ld+json",
                        lineWrapping: true
                    });

                    myCodeMirror.getDoc().setValue($filter('json')($scope.market.settings));

                    myCodeMirror.on('change', function (cm, change) {
                        cm.save();
                    });
                }
            };

            var errorHandler = function (response) {

                $scope.error = {};
                if (response && response !== undefined && response.hasOwnProperty('errors')) {
                    for (var idx in response.errors) {
                        $scope.error[response.errors[idx]['field']] = {
                            value: response.errors[idx]['value'],
                            message: response.errors[idx]['message']
                        };
                    }
                }
                $scope.isWaiting = false;
            };

        }
    ]);
