angular.module('users').controller('CreateMarketController',
    ['$scope', 'BootStrap', '$state', '$timeout', '$rootScope', 'Menus', '$http', '$filter', 'localStorageService', 'Subscription', '$window',
        function ($scope, BootStrap, $state, $timeout, $rootScope, Menus, $http, $filter, localStorageService,  Subscription, $window) {
            // This provides BootStrap context.
            $scope.authentication = BootStrap;

            $scope.user = $scope.authentication.user;

            $scope.currentMarket = JSON.parse(JSON.stringify(BootStrap.market));

            $scope.error = {};
            $scope.isWaiting = false;

            $scope.market = {};

            $scope.allServices = BootStrap.services;

            $scope.credentials = {};

            $scope.columns = 'small-8';

            $scope.progress = {
                isFirstStepDone: false,
                isSecondStepDone: false,
                isThirdStepDone: false,
                countDown: 0
            };

            var currentSubscription = undefined;

            if (typeof BootStrap.market.getCurrentUserSubscription === 'function') {
                currentSubscription = BootStrap.market.getCurrentUserSubscription('wanteet', 'market_subscription');
            }

            var hasSubscription = (currentSubscription) ? true : false;
            var hasMarket = ($scope.user && $scope.user.permissions.indexOf('ROLE_MARKET_ADMIN') != -1);

            if (!$scope.user) {
                $state.go('createYourMarket.register');
            } else if ($scope.user && !hasSubscription) {
                $scope.columns = 'small-12';
                $state.go('createYourMarket.subscriptions');
            } else if ($scope.user && hasSubscription && !hasMarket) {
                $state.go('createYourMarket.setupMarket')
            }

            $scope.initSubscriptions = function () {
                console.log('init subscriptions');
                Subscription.initSubscriptions({
                    marketId: BootStrap.market.slug,
                    planType: 'market_subscription',
                    gridCols: 4
                });
            };

            $scope.signup = function () {

                $scope.credentials["market"] = BootStrap.market.slug;
                $scope.credentials["mcrSid"] = Math.floor(Date.now() / 1000);
                $http.post('/auth/signup', $scope.credentials).then(function (response) {
                    // If successful we assign the response to the global user model
                    $http.post('/auth/signin', $scope.credentials).then(function (response) {
                        $scope.authentication.user = response.data;
			localStorageService.set('user', response.data);
                        if ($scope.authentication && (BootStrap.market.slug)){
                            $http({
                                    url: $scope.authentication.api.baseUrl + "/users/" + $scope.authentication.user.id
                                    + "/bootstrap",
                                method: 'get',
                                headers: {
                                    'X-Market-ID': BootStrap.market.slug,
                                    'X-Auth-Token': $scope.authentication.user.accessToken
                                }
                            })
                            .then(function (_response) {
                                localStorageService.set('profile', _response.data.profile);
                                localStorageService.set('stripe', _response.data.payments.stripe);

                                // And redirect the user to message Page
                                $timeout(function () {
                                   $window.location.reload();
                                }, 50);

                            }, function(error){
                                 console.log(error);
                            });
                       }

                    });
                    // And redirect the user to message Page


                }, function (response) { //Error
                    $scope.error = {};
                    for (var idx in response.errors) {
                        $scope.error[response.errors[idx]['field']] = {
                            value: response.errors[idx]['value'],
                            message: response.errors[idx]['message']
                        };
                    }
                });
	      };
          
            $scope.setupCreateMarket = function () {
                $scope.progress = {
                    isFirstStepDone: false,
                    isSecondStepDone: false,
                    countDown: 0
                };

                $scope.market = {
                    settings: {
                        offerType: 'price',
                        owner: {
                            user: $scope.user.username
                        },
                        marketAdmins: [$scope.user.username],
                        services: [],
                        addons: {
                            seller_subscription: false,
                            prospect_finder: false
                        }
                    }
                };

                if($scope.user.permissions.indexOf('ROLE_ADMIN') == -1){
                    $scope.market.settings.owner['marketSubscriptionId'] = currentSubscription.id
                }

                $timeout(function () {
                    $("#tags").selectize({
                        create: function (input, callback) {
                            callback({
                                name: input,
                                description: input
                            });
                        },
                        valueField: 'name',
                        labelField: 'description',
                        searchField: 'description',
                        delimiter: ',',
                        maxItems: '10',
                        options: $scope.allServices,
                        sortField: {
                            field: 'description',
                            direction: 'asc'
                        },
                        onChange: function (value) {
                            if (value == null) {
                                $scope.market.settings.services = [];
                            } else {
                                $scope.market.settings.services = value;
                            }
                        }
                    });
                }, 5);
            };


            $scope.createMarket = function () {

                $scope.isWaiting = true;

                var data = JSON.parse(JSON.stringify($scope.market));

                data['notify'] = true;

                $http.post($scope.authentication.api.baseUrl + "/users/"
                    + $scope.authentication.user.id + "/markets/",
                    data)
                    .success(function (response) {
                        $scope.market = response;
                        $scope.error = {};
                        $scope.progress.isFirstStepDone = true;
                        $scope.isWaiting = false;
                        $scope.createMarketStep2();
                    })
                    .error(function (response) {
                        errorHandler(response);
                        $scope.isWaiting = false;
                    });
            };

            $scope.createMarketStep2 = function () {

                var progressBar = setInterval(function () {
                    if ($scope.progress.countDown == 100) {
                        $scope.progress.countDown = 0;
                    }
                    $scope.progress.countDown++;
                    $scope.progress.style = {width: $scope.progress.countDown + '%'};
                    if (!$scope.$$phase) {
                        $scope.$apply();
                    }
                }, 50);

                $http.post('/market-admin/setup-domain', {
                    market: $scope.market.slug,
                    domains: $scope.market.settings.urls
                })
                    .success(function (response) {
                        $http.post('/market-admin/setup-pages', {
                            market: $scope.market.slug
                        }).success(function (response) {
                            $timeout(function () {
                                $scope.progress.isSecondStepDone = true;
                                $scope.progress.countDown = 100;
                                clearInterval(progressBar);
                            }, 5000);
                        }).error(function (response) {
                            console.log(response);
                        });
                    })
                    .error(function (response) {
                        console.log(response);
                    });
            };

            $scope.setupMarketForm = function () {

                $scope.market = {};

                if ($scope.markets.length > 0) {
                    for (idx in $scope.markets) {
                        if ($scope.markets[idx].slug == $state.params.marketId) {
                            $timeout(function () {
                                $scope.market = $scope.markets[idx];
                                $scope.isWaiting = false;

                                setupEditUI();
                            }, 5);
                            return;
                        }
                    }
                } else {
                    $http.get($scope.authentication.api.baseUrl + "/users/"
                        + $scope.authentication.user.id + "/markets/"
                        + $state.params.marketId
                    )
                        .success(function (response) {
                            $scope.market = response;
                            $scope.isWaiting = false;

                            setupEditUI();

                        })
                        .error(function (response) {
                            errorHandler(response);
                        });
                }
            };

            var setupEditUI = function () {
                myCodeMirror = undefined;
                myTextArea = undefined;

                if ($scope.market.settings.hasOwnProperty('services')) {
                    var services = JSON.parse(JSON.stringify($scope.market.settings.services));
                    $scope.market.settings.services = [];
                    angular.forEach(services, function (value, key) {
                        this.push(value.name);
                    }, $scope.market.settings.services);
                }

                var myTextArea = angular.element("#codeEditor")[0];
                if (myTextArea) {
                    var myCodeMirror = CodeMirror.fromTextArea(myTextArea, {
                        lineNumbers: true,
                        matchBrackets: true,
                        autoCloseBrackets: true,
                        mode: "application/ld+json",
                        lineWrapping: true
                    });

                    myCodeMirror.getDoc().setValue($filter('json')($scope.market.settings));

                    myCodeMirror.on('change', function (cm, change) {
                        cm.save();
                    });
                }
            }

            var errorHandler = function (response) {
                $scope.error = {};
                if (response && response !== undefined && response.hasOwnProperty('errors')) {
                    for (var idx in response.errors) {
                        $scope.error[response.errors[idx]['field']] = {
                            value: response.errors[idx]['value'],
                            message: response.errors[idx]['message']
                        };
                    }
                }
                $scope.isWaiting = false;
            };

        }
    ]);
