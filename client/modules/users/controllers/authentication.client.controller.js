angular.module('users').controller('AuthenticationController', ['$scope', '$http', '$location', 'BootStrap', '$timeout', 'localStorageService', '$window',
	function ($scope, $http, $location, BootStrap, $timeout, localStorageService, $window) {
		$scope.authentication = BootStrap;

		$scope.market = BootStrap.market;

		// If user is signed in then redirect back home
		if ($scope.authentication.user) $location.path('/');
	
     		$scope.signup = function() {
			$scope.credentials["market"] = BootStrap.market.slug;
			$http.post('/auth/signup', $scope.credentials).then(function (response) {
				// If successful we assign the response to the global user model
				$scope.authentication.user = response;
				localStorageService.set('user', response);
				      var marketName = $window._marketId;
					if(marketName && $scope.authentication){
                		                $http({
                                		       url: $scope.authentication.api.baseUrl + "/users/" + $scope.authentication.user.id
		                                       + "/bootstrap",
                		                       method: 'get',
                                		       headers: {
			                                        'X-Market-ID': marketName,
			                                        'X-Auth-Token': $scope.authentication.user.accessToken
                        		               }
		                              })
                		              .then(function (_response) {
                                		          localStorageService.set('profile', _response.data.profile);
		                                          localStorageService.set('stripe', _response.data.payments.stripe);

							// And redirect the user to message Page
		                                $timeout(function(){
		                                        var url = '/registration-success/' + btoa($scope.credentials.username);
		                                        window.location.href = url;
		                                },1000);
								
					      }, function(error){
							console.log(error);
					     });
					}

			}, function(errResponse) {
                                $scope.error = {};
                                for (var idx in errResponse.errors){
                                        $scope.error[errResponse.errors[idx]['field']] = {
                                                value: errResponse.errors[idx]['value'],
                                                message: errResponse.errors[idx]['message']
                                        };
                                }
			});
                };

                $scope.signin = function() {
		    $http.post('/auth/signin', $scope.credentials).then(function (_response){
			// If successful we assign the response to the global user model
			$scope.authentication.user = _response.data;
			localStorageService.set('user', _response.data);
			//Fetch if exists & stores the landing page url
       			var marketName = $window._marketId;
			if (marketName && $scope.authentication){
		            $http({
                		url: $scope.authentication.api.baseUrl + "/users/" + $scope.authentication.user.id
		                + "/bootstrap",
		                method: 'get',
		                headers: {
		                'X-Market-ID': marketName,
	                	'X-Auth-Token': $scope.authentication.user.accessToken
		                }
	                    })
	                    .then(function (response){
	                	localStorageService.set('profile', response.data.profile);
	                        localStorageService.set('stripe', response.data.payments.stripe);
			
				var locredir = localStorageService.get('logredir') || undefined;


                                if (locredir) {
                                    localStorageService.remove('logredir');
                                    location.href = locredir;
                                    $timeout(function () {
                                        location.reload();
                                    }, 3000);
                                    return;
                                 }

                                 if (_response.data.permissions.indexOf('ROLE_SELLER') != -1){
                                     location.href = '/dashboard';
                                 }else{
                                     location.href = '/';
                                     location.hash = "";
                                     //If the current page is '/'
                                     $timeout(function(){
                                         location.reload();
                                     },3000);
                                  }
			       }, 
                               function (_error){
	        	       });
		           }
			}, function(errResponse) {
                                $scope.error = errResponse.message;
                        });
		};
	}
]);
