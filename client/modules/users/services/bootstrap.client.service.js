// BootStrap service for user variables
angular.module('users').factory('BootStrap', [
    function () {
        var _this = this;

        var _user = window.user;
        var _profile = window._profile;
        var _api = window._api;
        var _services = [];
        var _market = window._market;
	var _subscription = window._subsInfo;

        if (typeof _user === 'object') {
            _user["isBuyer"] = function () {
                if (_user !== JSON.stringify({}) && _user.hasOwnProperty('accessToken')) {
                    return (this.permissions.indexOf('ROLE_SELLER') == -1);
                } else {
                    return true;
                }
            };
        }


        if ($('meta[name="_services"]').length) {
            _services = JSON.parse($('meta[name="_services"]').attr('content'));
        }

        //TODO: Refactor this
        if (_market.hasOwnProperty('settings')) {

            if (_market.settings.hasOwnProperty('addons')) {
                if (_profile !== "" && _user !== "") {

                    _market['getCurrentUserSubscription'] = function (marketId, planType) {
                        if (_profile && _profile.subscriptions[marketId || _market.slug]) {
                            if (planType) {
                                return setupSubscription(_profile.subscriptions[marketId || _market.slug])[planType];
                            } else {
                                return setupSubscription(_profile.subscriptions[marketId || _market.slug]);
                            }
                        } else {
                            return undefined;
                        }
                    };

                    _market['showTrialPlan'] = function () {
                        return _profile['showTrial'];
                    };

                    _market['updateCount'] = function (marketId, planType) {
                        console.log("Update Current Subscription Limit");
                        _profile.subscriptions[marketId || _market.slug][planType].leftCount -= 1;
                        $('meta[name="_profile"]').attr('content', JSON.stringify(_profile));
                        return setupSubscription(_profile.subscriptions[marketId || _market.slug][planType]);
                    };

                    var setupSubscription = function (data) {
                        console.log("setup subscription"+data);
                        var subscription = {};
                        subscription[data.type] = (_market.settings.plans && _market.settings.plans[data.type]) ? _market.settings.plans[data.type][data.name] : data;
                        subscription[data.type].name = data.name;
                        subscription[data.type].leftCount = data.leftCount;
                        return subscription;
                    };
                }

                _market['getPlans'] = function () {
                    var currentSubscription = undefined;
                    if (_market['getCurrentUserSubscription']) {
                        currentSubscription = _market.getCurrentUserSubscription();
                    }

                    var plans = [];
                    for (planType in _market['settings']['plans']) {
                        var planTypePlans = [];
                        for (plan in planType) {
                            var data = JSON.parse(JSON.stringify(_market['settings']['plans'][planType]));
                            if (currentSubscription && currentSubscription.name == plan) {
                                data.subscribed = true;
                            } else {
                                data.subscribed = false;
                            }
                            data.name = plan;
                            planTypePlans.push(data);
                        }
                        plans[planType] = planTypePlans;
                    }
                    return plans;
                };
            }
        }

        var synchrnonize = function ($http) {
            $http.get(_api.baseUrl + "/bootstrap?market=" + _market.slug)
                .success(function (resonse) {
                    _market = resonse.market;
                })
                .error(function (response) {
                    console.log("Error Occurred!");
                    console.log(response);
                })
        };

        _this._data = {
            user: _user,
            profile: _profile,
	    subscription: _subscription,
            services: _services,
            market: _market,
            api: _api,
            synchronize: synchrnonize
        };


        return _this._data;
    }
]);
