// BootStrap service for user variables
angular.module('users').factory('Subscription', ['$http', 'BootStrap',
    function ($http, BootStrap) {
        var _this = this;

        var initType = {};


        var initSubscriptions = function (data) {
            initType = data;
        }

        var getInitSubscription = function () {
            return initType;
        }

        _this._data = {
            initType: initType,
            initSubscriptions: initSubscriptions,
            getInitSubscription: getInitSubscription
        };


        return _this._data;
    }
]);
