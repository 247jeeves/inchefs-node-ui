angular.module('yumquotes').controller('YumquotesController',
    ['$scope', 'BootStrap', '$state', '$timeout',
        '$rootScope', '$http', 'localStorageService',
        '$location', 'Loader', '$filter', 'PlaceRequest',
        function ($scope, BootStrap, $state, $timeout,
                  $rootScope, $http, localStorageService,
                  $location, Loader, $filter, PlaceRequest) {
            // This provides BootStrap
            $scope.authentication = BootStrap;
            $scope.service = window.service;
            $scope.formName = $scope.service.replace(/ /g, '-');
            $scope.answers = {};
            $scope.confNumber = 0;
            $scope.noQuestions = false;

            $scope.eventData = localStorageService.get('fullEventBkp') || {};
            $scope.questions = [];
            $scope.displayQuestions = {};

            var exclusiveModal = false;
            $scope.market = BootStrap.market;
            var serviceNames = JSON.parse(JSON.stringify(window._market.settings.services));
            var serviceDesc = {};
            for (var idx in serviceNames) {
                serviceDesc[serviceNames[idx]['name'].toLowerCase()] = serviceNames[idx]['description'];
            }
            console.log(serviceDesc);
            $scope.serviceName = serviceDesc[$scope.service];

            moment.locale('en');

            if ($scope.service) {
                $scope.eventData = localStorageService.get('cReqValObj' + $scope.service) || {};

                //Setup Event Data Object if it's empty
                if (JSON.stringify($scope.eventData) === "{}") {
                    $scope.eventData = {
                        market: $scope.market.slug,
                        services: [
                            {
                                service: $scope.service,
                                answers: []
                            }
                        ]
                    };
                }
                $scope.answers = localStorageService.get('cReqValObjAns' + $scope.service) || {};
            }

            $scope.formatDate = function (date) {
                return moment(date).format('MM/DD/YYYY hh:mm A');
            }

            $scope.formatLocation = function (address) {
                if (typeof address !== 'undefined') {
                    return address.toLowerCase().replace(/[, ]/g, '-');
                }
            }

            $scope.getQuestions = function (success) {

                if (exclusiveModal) {
                    return;
                }

                if (typeof getQueryVariable('l') !== 'undefined') {
                    $scope.eventData.address = getQueryVariable('l');
                }

                $http.get($scope.authentication.api.publicUrl + "/services/" + $scope.service + "/questions/")
                    .success(function (response) {
                        $scope.questions = response.results;
                        if (response.totalCount == 0) {
                            $scope.noQuestions = true;
                        }
                        //Setup an array to log answered state of questions
                        //$scope.answered = [];
                        //for(idx in $scope.questions){
                        //    $scope.answered.push({
                        //        idx: false
                        //    });
                        //}

                        //Set
                        $timeout(function () {
                            // Blocking selection of same day
                            $("input[data-datetime-bind]").datetimepicker({
                                minDate: (new Date()).setDate((new Date()).getDate() + 1),
                                collapse: true,
                                useCurrent: false
                            }).on('dp.change', function (e) {
                                var currentField = $(e.target);
                                currentField = currentField[0];
                                var questionId = currentField.id.match(/([0-9].*)/)[0];
                                $scope.answers[questionId] = $('#' + currentField.id).val();
                            });

                            //for(jdx in $scope.questions){
                            //    var currQuestion = $scope.questions[jdx];
                            //    if(currQuestion.type == 'datetime'){
                            //        $('body').on('dp.change', '#datetime'+currQuestion.id, function (e) {
                            //            console.log(e);
                            //            $scope.answers[currQuestion.id] = $('#datetime'+currQuestion.id).val();
                            //        });
                            //    }
                            //}
                        }, 100);

                        $scope.isWaiting = false;
                        $scope.error = {};

                        if (typeof success !== 'undefined') {
                            success();
                        }

                    }).error(function (response) {
                        console.log(response);
                        $scope.isWaiting = false;
                    });
            };

            $scope.processAnswers = function () {

                console.log($scope.answers);

                if (!$scope[$scope.formName + "Form"].$valid) {
                    $scope.setErrors = true;
                    return;
                }


                //backup to localstorage
                localStorageService.set('cReqValObjAns' + $scope.service, $scope.answers);

                var answers = JSON.parse(JSON.stringify($scope.answers));
                //Set service to be traversed
                var service;
                for (kdx in $scope.eventData.services) {
                    if ($scope.eventData.services[kdx]["service"] === $scope.service) {
                        service = $scope.eventData.services[kdx];
                    }
                }

                if (service.answers.length > 0) {
                    service.answers = [];
                }

                for (idx in answers) {
                    var answer;
                    if (typeof answers[idx] === 'object') {
                        answer = [];
                        for (jdx in answers[idx]) {
                            if (answers[idx][jdx]) {
                                answer.push(answers[idx][jdx]);
                            }
                        }
                        answer = answer.join(',');
                    } else {
                        answer = answers[idx];
                    }

                    service.answers.push({
                        question: idx,
                        answer: answer
                    });
                }


                //Setup exclusive provider
                console.log($state.current.name);
                if ($state.current.name === 'exclusiveSeller' && $state.params.sellerSlug) {
                    console.log("Add Exclusive record for - " + $state.params.sellerSlug);
                    service["exclusive"] = {
                        sellerSlug: $state.params.sellerSlug
                    };
                } else {
                    //For a normal flow, remove the exclusive data if any
                    console.log("Remove Exclusive - Normal Flow");
                    if (service["exclusive"]) {
                        service["exclusive"] = undefined;
                    }
                }

                //backup data to localStorage
                localStorageService.set('cReqValObj' + $scope.service, $scope.eventData);
                $state.go("getEventDetails");
            }

            //Planning to use this to give a feedback to user
            //if a certain question has been answered by him with a valid value
            $scope.isAnswered = function (questionId) {
                return $scope.answered[questionId];
            }

            $scope.initFinalScreen = function () {

                $scope.displayQuestions = {};
                //For display purpose
                if ($scope.questions.length > 0) {
                    setDisplayQuestions();
                    $("#phone").focus();
                } else {
                    $scope.getQuestions(function () {
                        setDisplayQuestions();
                        $("#phone").focus();
                    });
                }

                $timeout(function () {
                    // Blocking selection of same day
                    var ua = navigator.userAgent;

                    if (ua.match(/(iPhone|iPad)/i)) {
                        $("#dateAndTime").attr('type', 'datetime-local');
                        $('body').on('change', '#dateAndTime', function (e) {

                            $scope.eventData.dateTime = moment($('#dateAndTime').val()).
                                format('YYYY-MM-DDTHH:mm:ss.SSSZ');

                            if (!$scope.$$phase) {
                                $scope.$apply();
                            }
                        });
                    } else {
                        $("#dateAndTime").datetimepicker({
                            minDate: (new Date()).setDate((new Date()).getDate() + 1),
                            collapse: true,
                            useCurrent: false
                        });

                        $('body').on('dp.change', '#dateAndTime', function (e) {
                            $scope.dateAndTime = $('#dateAndTime').val();
                            $scope.eventData.dateTime = $('#dateAndTime').data("DateTimePicker")
                                .date().format('YYYY-MM-DDTHH:mm:ss.SSSZ');
                        });
                    }
                }, 10);

            }

            $scope.initResultScreen = function () {

                //Remove local storage data
                localStorageService.remove('cReqValObj' + $scope.service);
                localStorageService.remove('cReqValObjAns' + $scope.service);

            }

            var setDisplayQuestions = function () {
                var questions = $scope.questions.slice();
                for (idx in questions) {
                    $scope.displayQuestions[questions[idx].id] = questions[idx].text;
                }
            };

            var getQueryVariable = function (variable) {
                var query = window.location.search.substring(1);
                var vars = query.split('&');
                for (var i = 0; i < vars.length; i++) {
                    var pair = vars[i].split('=');
                    if (decodeURIComponent(pair[0]) == variable) {
                        return decodeURIComponent(pair[1]);
                    }
                }
                console.log('Query variable %s not found', variable);
            };

            var locationHash = '';
            $scope.processEvent = function () {
                $scope.isWaiting = true;
                console.log($scope.eventData);
                console.log('^ processEvent');
                locationHash = location.hash;
                $http.post($scope.authentication.api.baseUrl + "/users/"
                + ($scope.authentication.user.id || "1") + "/events/", $scope.eventData)
                    .success(function (response) {
                        $scope.eventData = response;

                        localStorageService.remove('fullEventBkp');

                        var hallContent = '<h5>New Request!</h5>';
                        hallContent += '<p>A request has been placed with following details</p>';
                        hallContent += '<table style="width:100%; margin:0; border:0">';
                        hallContent += '<tbody> ';
                        hallContent += '<tr>';
                        hallContent += '<td width="110">Service</td>';
                        hallContent += '<td>' + $scope.eventData.services[0].service + '</td>';
                        hallContent += '</tr>';
                        hallContent += '<tr>';
                        hallContent += '<td width="110">Budget</td>';
                        hallContent += '<td>' + $filter('wCurrency')($scope.eventData.budget) + '</td>';
                        hallContent += '</tr>';
                        if ($scope.eventData.details) {
                            hallContent += '<tr>';
                            hallContent += '<td width="110">Details</td>';
                            hallContent += '<td>' + $scope.eventData.details + '</td>';
                            hallContent += '</tr>';
                        }
                        hallContent += '</tbody></table>';

                        var targetUrl = location.origin + '/' + '/dashboard/#!/';
                        targetUrl += 'requests/request/' + $scope.eventData.services[0].id;
                        targetUrl += '/' + $scope.eventData.services[0].displayId;

                        if (exclusiveModal) {
                            $("#excusiveListingRequest").foundation('reveal', 'close');
                            window.location.href = '/dashboard#!/requests/event/' + $scope.eventData.id;

                            //TODO: Do no post to Hall in case it's an exclusive Request (?) [Might need to refactor]
                            return;
                        }

                        //Due to some bug, not able to transfer to child state, hence saving data to localStorage
                        //& deleting it in next state
                        //TODO: Refactor & fix the bug w.r.t child states
                        localStorageService.set('cReqValObj' + $scope.service, $scope.eventData);

                        $scope.$broadcast('publishToHall', {
                            postType: 0,
                            content: hallContent,
                            targetUrl: targetUrl,
                            success: function () {
                                Loader.stopLoader(true);

                                $scope.isWaiting = false;
                                //Take to result screen
                                $scope.initResultScreen();

                                var url = '/search/' + $scope.service + '?referenceId=' + $scope.eventData.id;
                                url += '&location=' + $scope.eventData.address;

                                window.location.href = url;
                                //$state.go("getResults");
                            }
                        });
                    })
                    .error(function (response) {
                        $scope.isWaiting = false;
                        $scope.error = {};
                        console.log(location);
                        console.log(response);
                        localStorageService.set('fullEventBkp', $scope.eventData);
                        localStorageService.set('logredir', location.pathname + locationHash);

                        if (typeof response !== 'undefined') {
                            for (var idx in response.errors) {
                                $scope.error[response.errors[idx]['field']] = {
                                    value: response.errors[idx]['value'],
                                    message: response.errors[idx]['message']
                                };
                            }
                        }
                    });
            }


            $scope.initExclusiveListingRequest = function () {
                exclusiveModal = true;
                var args = PlaceRequest.getExclusiveRequest();

                console.log(args);

                if ($scope.eventData.services && $scope.eventData.services.length > 0) {
                    var service = $scope.eventData.services[0];

                    if (service.exclusive) {
                        if (!(service.exclusive.sellerSlug == args.sellerSlug &&
                            service.exclusive.listingSlug == args.listingSlug)) {
                            setEventData();
                        }
                    } else {
                        setEventData();
                    }
                } else {
                    setEventData();
                }

                function setEventData() {
                    $scope.eventData = {
                        budget: args.budget,
                        services: [{
                            service: args.service,
                            exclusive: {
                                sellerSlug: args.sellerSlug,
                                listingSlug: args.listingSlug,
                                listingTitle: args.listingTitle
                            }
                        }],
                        market: $scope.market.slug
                    };
                };

                console.log("Exclusive Request set !");
                $scope.initFinalScreen();
            };
        }
    ])
;

