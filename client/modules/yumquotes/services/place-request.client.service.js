// BootStrap service for user variables
angular.module('yumquotes').factory('PlaceRequest', [
    function () {
        var _this = this;

        var exclusiveRequest = {};


        var setupExclusiveRequest = function (data) {
            exclusiveRequest = data;
        }

        var getExclusiveRequest = function () {
            return exclusiveRequest;
        }

        _this._data = {
            exclusiveRequest: exclusiveRequest,
            setupExclusiveRequest: setupExclusiveRequest,
            getExclusiveRequest: getExclusiveRequest
        };


        return _this._data;
    }
]);
