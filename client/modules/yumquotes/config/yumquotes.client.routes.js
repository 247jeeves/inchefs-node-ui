angular.module('yumquotes').config(['$stateProvider', '$urlRouterProvider',
    function ($stateProvider, $urlRouterProvider) {
        // Redirect to home view when route not found
        $urlRouterProvider.otherwise('/');

        // Home state routing
        $stateProvider.
            state('request', {
                url: '/',
                controller: 'YumquotesController',
                templateUrl: 'modules/yumquotes/views/yumquotes/index.client.view.html'
            }).
            state('getEventDetails', {
                url: '/event-info',
                controller: 'YumquotesController',
                templateUrl: 'modules/yumquotes/views/yumquotes/getEventDetails.client.view.html'
            }).
            state('getResults', {
                url: '/success',
                controller: 'YumquotesController',
                templateUrl: 'modules/yumquotes/views/yumquotes/getResults.client.view.html'
            }).
            state('exclusiveSeller', {
                url: '/exclusive/:sellerSlug',
                controller: 'YumquotesController',
                templateUrl: 'modules/yumquotes/views/yumquotes/index.client.view.html'
            });
    }
]);
