angular.module('yumquotes').run(['$http', 'BootStrap',
    function ($http, BootStrap) {

        //$http.defaults.headers.common['Accept'] = 'application/json';
        $http.defaults.headers.common['Content-Type'] = 'text/plain';

        delete $http.defaults.headers.common['X-Requested-With'];

        if (BootStrap.user) {
            $http.defaults.headers.common['X-Auth-Token'] = BootStrap.user.accessToken;
            $http.defaults.headers.common['X-Market-ID'] = BootStrap.market.slug;
        }

    }
]);
