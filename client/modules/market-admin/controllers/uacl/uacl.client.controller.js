angular.module('market-admin').controller('UACLController',
    ['$scope', 'BootStrap', '$state', '$timeout', '$rootScope', 'Menus', '$http', 'Loader',
        function ($scope, BootStrap, $state, $timeout, $rootScope, Menus, $http, Loader) {
            // This provides BootStrap context.
            $scope.authentication = BootStrap;

            var market = $scope.market = BootStrap.market;

            $scope.profile = $scope.authentication.profile;
            //$scope.services = $scope.authentication.services;
            $scope.services = market.settings.services;
            $scope.listings = [];
            $scope.addItems = [];
            $scope.listing = {
                items: []
            };
            $scope.error = {};

            $scope.isWaiting = false;

            $scope.progress = {
                isFirstStepDone: false
            };

            var imagesToUpload = [];
            var flowInst = undefined;


            //Setup Side Nav
            $scope.sidenav = Menus.getMenu('listingsSideNav').items;

            //Action to perform on url routing inside current scope
            $rootScope.$on('$stateChangeSuccess',
                function (event, toState, toParams, fromState, fromParams) {
                    if ((toState.name.match(/\./g) || []).length > 1) {
                        Menus.selectMenuItemByLink('listingsSideNav', toState.parent);
                    } else {
                        Menus.selectMenuItemByLink('listingsSideNav', toState.name);
                    }
                });

            if ($state.current.name === undefined || $state.current.parent === undefined) {
                $state.transitionTo('listings.all');
            } else {
                Menus.selectMenuItemByLink('listingsSideNav', $state.current.name);
            }

            $scope.setupAddListing = function () {
                $scope.addItems = [];
                $scope.listing = {
                    items: []
                };
                $scope.error = {};

                $scope.isWaiting = false;

                $scope.progress = {
                    isFirstStepDone: false
                };
                $timeout(function () {
                    $("#tags").selectize({
                        delimiter: ',',
                        maxItems: 5,
                        persist: true,
                        create: function (input) {
                            return {
                                value: input,
                                text: input
                            }
                        }
                    });
                    setupDatePickers();
                }, 5);
            };

            //Get all Listings - No pagination needed at the moment here
            $scope.getListings = function () {
                $scope.isWaiting = true;
                $scope.listings = [];
                $http.get($scope.authentication.api.baseUrl + "/users/"
                    + $scope.authentication.user.id + "/packages"
                    + "?market=" + market.slug
                )
                    .success(function (response) {
                        $scope.listings = response.results;
                        $scope.isWaiting = false;
                    })
                    .error(function (response) {
                        errorHandler(response);
                    });
            }

            //Get individual Listing
            $scope.getListing = function () {
                $scope.listing = undefined;
                $scope.addItems = [];
                $scope.isWaiting = true;
                if ($scope.listings.length > 0) {
                    for (idx in $scope.listings) {
                        if ($scope.listings[idx].id == $state.params.packageId) {
                            $timeout(function () {
                                $scope.listing = $scope.listings[idx];
                                setupListing();
                                $scope.isWaiting = false;
                            }, 5);
                            return;
                        }
                    }
                } else {
                    $http.get($scope.authentication.api.baseUrl + "/users/"
                        + $scope.authentication.user.id + "/packages/"
                        + $state.params.packageId
                    )
                        .success(function (response) {
                            $scope.listing = response;
                            setupListing();
                            $scope.isWaiting = false;
                        })
                        .error(function (response) {
                            errorHandler(response);
                        });
                }

            }

            $scope.createOrUpdateListing = function () {
                $scope.isWaiting = true;
                var data = JSON.parse(JSON.stringify($scope.listing));
                data.market = market.slug;
                data.images = [];
                if (imagesToUpload.length > 0) {
                    data.images = imagesToUpload;
                }
                if (data.tags !== undefined) {
                    data.tags = data.tags.split(',');
                }
                if (!$scope.listing.hasOwnProperty('id')) {
                    $http.post($scope.authentication.api.baseUrl + "/users/"
                        + $scope.authentication.user.id + "/packages",
                        data)
                        .success(function (response) {
                            console.log(response);
                            $scope.listing = response;
                            $scope.listings.push($scope.listing);
                            $scope.addItem();
                            $scope.isWaiting = false;
                            $scope.progress.isFirstStepDone = true;
                            imagesToUpload = [];
                            if (flowInst !== undefined) {
                                flowInst.files = [];
                            }
                            $scope.error = {};
                        })
                        .error(function (response) {
                            errorHandler(response);
                        });
                } else {
                    $http.put($scope.authentication.api.baseUrl + "/users/"
                        + $scope.authentication.user.id + "/packages/"
                        + $scope.listing.id,
                        data)
                        .success(function (response) {
                            $scope.listing = response;
                            $scope.listings.push($scope.listing);
                            $scope.isWaiting = false;
                            imagesToUpload = [];
                            if (flowInst !== undefined) {
                                flowInst.files = [];
                            }
                            //Setup InstantRequest
                            if ($scope.listing.instantRequest) {
                                setupInstantRequest();
                            }
                            $scope.error = {};
                        })
                        .error(function (response) {
                            errorHandler(response);
                        });
                }
            }

            $scope.addItemsToListing = function (index) {


                var itemName = $scope.addItems[index].name.replace(/^\s+|\s+$/g, '');
                if (itemName == '' ||
                    $scope.isWaiting ||
                    $scope.addItems[index].added ||
                    $scope.addItems[index].isWating) {
                    return;
                }

                $scope.isWaiting = true;
                $scope.addItems[index].added = true;
                $scope.addItems[index].isWating = true;

                $http.patch($scope.authentication.api.baseUrl + "/users/"
                    + $scope.authentication.user.id + "/packages/"
                    + $scope.listing.id,
                    {name: $scope.addItems[index].name})
                    .success(function (response) {
                        $scope.addItems[index] = response;

                        //Adding again - I know too exhaustive
                        $scope.addItems[index].added = true;
                        $scope.addItems[index].isWating = false;

                        //Check if the items is null or if it doesn't exist, act accordingly
                        if ($scope.listing.hasOwnProperty('items') && $scope.listing.items !== null) {
                            $scope.listing.items.push(response);
                        } else {
                            $scope.listing.items = [];
                            $scope.listing.items.push(response);
                        }

                        $scope.addItems[index].isWating = false;
                        addItem();
                        $scope.isWaiting = false;
                        $scope.error = {};
                    })
                    .error(function (response) {
                        $scope.addItems[index].added = false;
                        $scope.addItems[index].isWating = false;
                        errorHandler(response);
                    });
            }

            $scope.deleteItemsFromListing = function (index) {

                if (!$scope.addItems[index].hasOwnProperty('id')) {
                    removeItem(index);
                    return;
                }

                //$scope.isWaiting = true;
                $scope.addItems[index].isWating = true;
                //
                $http.delete($scope.authentication.api.baseUrl + "/users/"
                    + $scope.authentication.user.id + "/packages/"
                    + $scope.listing.id + "/items/"
                    + $scope.addItems[index].id
                )
                    .success(function (response) {
                        $scope.listing.items.splice(index, 1);
                        removeItem(index);
                        $scope.isWaiting = false;
                    })
                    .error(function (response) {
                        $scope.addItems[index].added = false;
                        $scope.addItems[index].isWating = false;
                        errorHandler(response);
                    });
            }

            $scope.deleteListing = function () {
                $("#deleteListing").find('[data-cancel-button]').on('click', function (e) {
                    $("#deleteListing").foundation('reveal', 'close');
                    e.preventDefault();
                });

                $("#deleteListing").find('[data-confirm-button]').on('click', function (e) {
                    $("#deleteListing").foundation('reveal', 'close');

                    $http.delete($scope.authentication.api.baseUrl + "/users/"
                    + $scope.authentication.user.id + "/packages/"
                    + $scope.listing.id)
                        .success(function (response) {
                            $state.go('listings.all');
                            //TODO: Show a success message
                        })
                        .error(function (response) {
                            //TODO: Show an error message
                        });

                    e.preventDefault();
                });
                $timeout(function () {
                    $("#deleteListing").foundation('reveal', 'open');
                }, 10);
            }

            /**
             * For Add Item Input boxes - Begin
             */

            var addItem = function (setFocus) {
                if (setFocus === undefined) {
                    setFocus = true;
                }
                $scope.addItems.push({
                    name: '',
                    added: false,
                    isWaiting: false
                });
                if (setFocus) {
                    $timeout(function () {
                        $("#itemName" + ($scope.addItems.length - 1)).focus();
                    }, 5);
                }
            }

            var removeItem = function (index) {
                $scope.addItems.splice(index, 1);
            }

            //I know it's funny :-/ but angularjs have a lot of untold problems
            //It's just a hack to call addItem() so that angular js doesn't
            //throw error at an untraceable line
            $scope.addItem = function () {
                $timeout(function () {
                    addItem();
                }, 5);
            };


            /**
             * Add Item Input boxes - End
             */

            $scope.initFileUpload = function () {
                $scope.isWaiting = true;
            }

            $scope.uploadSuccess = function ($flow, $file, $message) {
                flowInst = $flow;
                imagesToUpload.push($file.uniqueIdentifier);
                if (flowInst.files.length == imagesToUpload.length) {
                    $scope.isWaiting = false;
                }
            }

            var setupListing = function () {
                var listOfItems = [];
                if ($scope.listing.items !== undefined) {
                    listOfItems = $scope.listing.items.slice();
                }
                $scope.addItems = listOfItems;

                for (idx in $scope.addItems) {
                    $scope.addItems[idx].added = true;
                    $scope.addItems[idx].isWaiting = false;
                }

                addItem(false);

                $timeout(function () {
                    $("#tags-edit").selectize({
                        delimiter: ',',
                        maxItems: 5,
                        persist: true,
                        create: function (input) {
                            return {
                                value: input,
                                text: input
                            }
                        }
                    });
                    setupDatePickers();
                    //Setup InstantRequest
                    if ($scope.listing.instantRequest) {
                        setupInstantRequest();
                    }
                }, 10);

            };

            var setupInstantRequest = function () {
                $scope.listing.instantRequest.isUnlimited = "" + $scope.listing.instantRequest.isUnlimited;
                if ($scope.listing.instantRequest.expiryDate) {
                    $scope.dateAndTime = new moment($scope.listing.instantRequest.expiryDate).format('MM/DD/YYYY hh:mm A')
                }
            }

            var errorHandler = function (response) {
                console.log(response);
                $scope.error = {};
                if (response && response !== undefined && response.hasOwnProperty('errors')) {
                    for (var idx in response.errors) {
                        $scope.error[response.errors[idx]['field']] = {
                            value: response.errors[idx]['value'],
                            message: response.errors[idx]['message']
                        };
                    }
                }
                $scope.isWaiting = false;
            };

            var setupDatePickers = function () {

                $("input[data-datetime-bind]").datetimepicker({
                    minDate: (new Date()).setDate((new Date()).getDate() + 1),
                    collapse: true,
                    useCurrent: false
                });

                var ua = navigator.userAgent;

                if (ua.match(/(iPhone|iPad)/i)) {
                    $("#dateAndTime").attr('type', 'datetime-local');
                    $('body').on('change', '#dateAndTime', function (e) {

                        $scope.listing.instantRequest.expiryDate = moment($('#dateAndTime').val()).
                            format('YYYY-MM-DDTHH:mm:ss.SSSZ');

                        if (!$scope.$$phase) {
                            $scope.$apply();
                        }
                    });
                } else {
                    $("#dateAndTime").datetimepicker({
                        minDate: (new Date()).setDate((new Date()).getDate() + 1),
                        collapse: true,
                        useCurrent: false
                    });

                    $('body').on('dp.change', '#dateAndTime', function (e) {
                        $scope.dateAndTime = $('#dateAndTime').val();
                        $scope.listing.instantRequest.expiryDate = $('#dateAndTime').data("DateTimePicker")
                            .date().format('YYYY-MM-DDTHH:mm:ss.SSSZ');
                    });
                }
            }

            $scope.publishListing = function () {

                Loader.startLoader();
                $scope.isWaiting = true;
                var hallContent = '<h5>' + $scope.listing.name + '</h5>';
                if ($scope.listing.description.length > 0) {
                    hallContent += '<p>' + $scope.listing.description + '</p>';
                }
                var targetUrl = location.origin + '/' + $scope.market.slug + '/p/' + $scope.listing.slug;
                var imageUrl = null;
                if ($scope.listing.images.length > 0) {
                    imageUrl = $scope.listing.images[0].url;
                }

                $scope.$broadcast('publishToHall', {
                    postType: 4,
                    content: hallContent,
                    targetUrl: targetUrl,
                    image: imageUrl,
                    success: function () {
                        Loader.stopLoader('success');
                        $scope.isWaiting = false;
                    }
                });
            }
        }
    ]);
