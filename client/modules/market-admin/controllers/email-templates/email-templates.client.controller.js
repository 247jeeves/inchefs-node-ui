angular.module('market-admin').controller('EmailTemplatesController',
    ['$scope', 'BootStrap', '$state', '$timeout', '$rootScope', 'Menus', '$http', 'Loader',
        function ($scope, BootStrap, $state, $timeout, $rootScope, Menus, $http, Loader) {
            // This provides BootStrap context.
            $scope.authentication = BootStrap;

            var market = $scope.market = BootStrap.market;

            $scope.profile = $scope.authentication.profile;
            //$scope.services = $scope.authentication.services;
            $scope.services = market.settings.services;
            $scope.emailTemplates = [];
            $scope.emailTemplate = {};
            $scope.error = {};
            $scope.defaultET = {
                templates: [],
                selected: {}
            };

            $scope.isWaiting = false;

            var myTextArea = undefined;
            var myCodeMirror = undefined;

            //Setup Side Nav
            $scope.sidenav = Menus.getMenu('emailTemplatesSideNav').items;

            //Action to perform on url routing inside current scope
            $rootScope.$on('$stateChangeSuccess',
                function (event, toState, toParams, fromState, fromParams) {
                    if ((toState.name.match(/\./g) || []).length > 1) {
                        Menus.selectMenuItemByLink('emailTemplatesSideNav', toState.parent);
                    } else {
                        Menus.selectMenuItemByLink('emailTemplatesSideNav', toState.name);
                    }

                    if (toState.name == 'emailTemplates.add') {
                        if (myTextArea) {
                            myTextArea = undefined;
                            myCodeMirror = undefined;
                        }
                    }

                });

            if ($state.current.name === undefined || $state.current.parent === undefined) {
                $state.transitionTo('emailTemplates.all');
            } else {
                Menus.selectMenuItemByLink('emailTemplatesSideNav', $state.current.name);
            }

            //Get all Templates - No pagination needed at the moment here
            $scope.getEmailTemplates = function () {
                $scope.isWaiting = true;
                $scope.emailTemplates = [];
                $http.get($scope.authentication.api.baseUrl + "/users/"
                    + $scope.authentication.user.id + "/markets/"
                    + market.slug + "/email/templates"
                )
                    .success(function (response) {
                        $scope.emailTemplates = response.results;
                        $scope.isWaiting = false;
                    })
                    .error(function (response) {
                        errorHandler(response);
                    });
            }

            //Get individual Template
            $scope.getEmailTemplate = function () {
                $scope.emailTemplate = undefined;
                $scope.isWaiting = true;
                if ($scope.emailTemplates.length > 0) {
                    for (idx in $scope.emailTemplates) {
                        if ($scope.emailTemplates[idx].id == $state.params.emailTemplateId) {
                            $timeout(function () {
                                $scope.emailTemplate = $scope.emailTemplates[idx];

                                $scope.isWaiting = false;

                                myCodeMirror = undefined;
                                myTextArea = undefined;

                                setupEmailTemplate();
                            }, 5);
                            return;
                        }
                    }
                } else {
                    $http.get($scope.authentication.api.baseUrl + "/users/"
                        + $scope.authentication.user.id + "/markets/"
                        + market.slug + "/email/templates/"
                        + $state.params.emailTemplateId
                    )
                        .success(function (response) {
                            $scope.emailTemplate = response;
                            $scope.isWaiting = false;

                            myCodeMirror = undefined;
                            myTextArea = undefined;

                            setupEmailTemplate();
                        })
                        .error(function (response) {
                            errorHandler(response);
                        });
                }

            }

            $scope.createOrUpdateEmailTemplate = function () {
                $scope.isWaiting = true;

                var data = JSON.parse(JSON.stringify($scope.emailTemplate));
                data.content = angular.element("#codeEditor").val();
                data.market = market.slug;

                if (!$scope.emailTemplate.hasOwnProperty('id')) {
                    $http.post($scope.authentication.api.baseUrl + "/users/"
                        + $scope.authentication.user.id + "/markets/"
                        + market.slug + "/email/templates",
                        data)
                        .success(function (response) {
                            $scope.emailTemplate = response;
                            $scope.emailTemplates.push($scope.emailTemplate);
                            $scope.isWaiting = false;
                            $scope.error = {};
                            $state.go('emailTemplates.all');
                        })
                        .error(function (response) {
                            errorHandler(response);
                        });
                } else {
                    $http.put($scope.authentication.api.baseUrl + "/users/"
                        + $scope.authentication.user.id + "/markets/"
                        + market.slug + "/email/templates/" + $scope.emailTemplate.id,
                        data)
                        .success(function (response) {
                            $scope.emailTemplate = response;
                            $scope.emailTemplates.push($scope.emailTemplate);
                            $scope.isWaiting = false;
                            $scope.error = {};
                        })
                        .error(function (response) {
                            errorHandler(response);
                        });
                }
            };

            var errorHandler = function (response) {
                console.log(response);
                $scope.error = {};
                if (response && response !== undefined && response.hasOwnProperty('errors')) {
                    for (var idx in response.errors) {
                        $scope.error[response.errors[idx]['field']] = {
                            value: response.errors[idx]['value'],
                            message: response.errors[idx]['message']
                        };
                    }
                }
                $scope.isWaiting = false;
            };

            var setupEmailTemplate = $scope.setupEmailTemplate = function (reset) {


                if (reset) {
                    $http.get($scope.authentication.api.baseUrl + "/users/"
                        + $scope.authentication.user.id + "/markets/"
                        + market.slug + "/email/templates?defaultTemplates=true"
                    )
                        .success(function (response) {
                            $scope.defaultET.templates = response.results;
                            $scope.isWaiting = false;
                        })
                        .error(function (response) {
                            errorHandler(response);
                        });
                }

                if (typeof myTextArea === 'undefined') {
                    myTextArea = document.getElementById("codeEditor");
                }

                if (typeof myCodeMirror === 'undefined') {
                    myCodeMirror = CodeMirror.fromTextArea(myTextArea, {
                        lineNumbers: true,
                        mode: "text/html",
                        extraKeys: {"Ctrl-Space": "autocomplete"}
                    });
                }

                if (typeof reset !== 'undefined') {
                    $scope.emailTemplate = {
                        needTheme: true
                    };
                } else {
                    myCodeMirror.getDoc().setValue($scope.emailTemplate.content);
                }

                myCodeMirror.on('change', function (cm, change) {
                    cm.save();
                });
            }

            $scope.setUpDefaultEmailTemplate = function () {
                $scope.emailTemplate = $scope.defaultET.selected;
                delete $scope.emailTemplate['id'];
                setupEmailTemplate();
            }
        }
    ]);
