angular.module('market-admin').controller('DashboardController', ['$scope', 'BootStrap', '$state', '$timeout', '$rootScope', 'localStorageService',
    function ($scope, BootStrap, $state, $timeout, $rootScope, localStorageService) {
        // This provides BootStrap context.
        $scope.authentication = BootStrap;

    }
]);
