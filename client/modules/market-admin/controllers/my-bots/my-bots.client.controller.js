angular.module('market-admin')
.filter('trustThis', ['$sce', function($sce) {
    return function(url) {
        return $sce.trustAsResourceUrl(url);
    };
}])
/*
.config(function($sceDelegateProvider) { 
    $sceDelegateProvider.resourceUrlWhitelist([
    // Allow same origin resource loads.
    'self',
    // Allow loading from our assets domain. **.
    'https://engine.247jeeves.com/**'
  ])
*/
.controller('MyBotsController', ['$scope', 'BootStrap', '$state', '$http', '$timeout', '$rootScope', 'localStorageService',
    function ($scope, BootStrap, $state, $http, $timeout, $rootScope, localStorageService) {
        // This provides BootStrap context.
        $scope.authentication = BootStrap;
        $scope.jeevesUser = $scope.authentication.user.username ;
        $scope.jeevesPlainPass = $scope.authentication.user.additionalProvidersData.jeevesengine.jeevesPassword;
	$scope.environment = BootStrap.market.env;
console.log("Environment outside getUserUrl is " + $scope.environment);
        $scope.getUserUrl = function() {
	console.log("Environment inside getUserUrl is " + $scope.environment);
			if($scope.environment == "qa"){
 				 return 'https://qa.engine.247jeeves.com?username='+$scope.jeevesUser + '&password='+$scope.jeevesPlainPass;
			}
			if($scope.environment == "production"){
			         return 'https://engine.247jeeves.com?username='+$scope.jeevesUser + '&password='+$scope.jeevesPlainPass;
			}
        }


    }
]);

