angular.module('market-admin').controller('EmailThemesController',
    ['$scope', 'BootStrap', '$state', '$timeout', '$rootScope', 'Menus', '$http', 'Loader',
        function ($scope, BootStrap, $state, $timeout, $rootScope, Menus, $http, Loader) {
            // This provides BootStrap context.
            $scope.authentication = BootStrap;

            var market = $scope.market = BootStrap.market;

            $scope.profile = $scope.authentication.profile;
            //$scope.services = $scope.authentication.services;
            $scope.services = market.settings.services;
            $scope.emailThemes = [];
            $scope.emailTheme = {};
            $scope.error = {};

            $scope.isWaiting = false;

            //Setup Side Nav
            $scope.sidenav = Menus.getMenu('emailThemesSideNav').items;


            //Action to perform on url routing inside current scope
            $rootScope.$on('$stateChangeSuccess',
                function (event, toState, toParams, fromState, fromParams) {
                    if ((toState.name.match(/\./g) || []).length > 1) {
                        Menus.selectMenuItemByLink('emailThemesSideNav', toState.parent);
                    } else {
                        Menus.selectMenuItemByLink('emailThemesSideNav', toState.name);
                    }
                });

            if ($state.current.name === undefined || $state.current.parent === undefined) {
                $state.transitionTo('emailThemes.all');
            } else {
                Menus.selectMenuItemByLink('emailThemesSideNav', $state.current.name);
            }

            //Get all Themes - No pagination needed at the moment here
            $scope.getEmailThemes = function () {
                $scope.isWaiting = true;
                $scope.emailThemes = [];
                $http.get($scope.authentication.api.baseUrl + "/users/"
                    + $scope.authentication.user.id + "/markets/"
                    + market.slug + "/email/themes"
                )
                    .success(function (response) {
                        $scope.emailThemes = response.results;
                        $scope.isWaiting = false;
                    })
                    .error(function (response) {
                        errorHandler(response);
                    });
            }

            //Get individual Listing
            $scope.getEmailTheme = function () {
                $scope.emailTheme = undefined;
                $scope.isWaiting = true;
                if ($scope.emailThemes.length > 0) {
                    for (idx in $scope.emailThemes) {
                        if ($scope.emailThemes[idx].id == $state.params.emailThemeId) {
                            $timeout(function () {
                                $scope.emailTheme = $scope.emailThemes[idx];
                                setupEmailTheme();
                                $scope.isWaiting = false;
                            }, 5);
                            return;
                        }
                    }
                } else {
                    $http.get($scope.authentication.api.baseUrl + "/users/"
                        + $scope.authentication.user.id + "/markets/"
                        + market.slug + "/email/themes/"
                        + $state.params.emailThemeId
                    )
                        .success(function (response) {
                            $scope.emailTheme = response;
                            $scope.isWaiting = false;
                            setupEmailTheme();
                        })
                        .error(function (response) {
                            errorHandler(response);
                        });
                }

            }

            $scope.createOrUpdateEmailTheme = function () {
                $scope.isWaiting = true;

                var data = JSON.parse(JSON.stringify($scope.emailTheme));
                data.content = angular.element("#codeEditor").val();
                data.market = market.slug;

                if (!$scope.emailTheme.hasOwnProperty('id')) {
                    $http.post($scope.authentication.api.baseUrl + "/users/"
                        + $scope.authentication.user.id + "/markets/"
                        + market.slug + "/email/themes",
                        data)
                        .success(function (response) {
                            $scope.emailTheme = response;
                            $scope.emailThemes.push($scope.emailTheme);
                            $scope.isWaiting = false;
                            $scope.error = {};
                            $state.go('emailThemes.all');
                        })
                        .error(function (response) {
                            errorHandler(response);
                        });
                } else {
                    $http.put($scope.authentication.api.baseUrl + "/users/"
                        + $scope.authentication.user.id + "/markets/"
                        + market.slug + "/email/themes/" + $scope.emailTheme.id,
                        data)
                        .success(function (response) {
                            $scope.emailTheme = response;
                            $scope.emailThemes.push($scope.emailTheme);
                            $scope.isWaiting = false;
                            $scope.error = {};
                        })
                        .error(function (response) {
                            errorHandler(response);
                        });
                }
            };

            var errorHandler = function (response) {
                console.log(response);
                $scope.error = {};
                if (response && response !== undefined && response.hasOwnProperty('errors')) {
                    for (var idx in response.errors) {
                        $scope.error[response.errors[idx]['field']] = {
                            value: response.errors[idx]['value'],
                            message: response.errors[idx]['message']
                        };
                    }
                }
                $scope.isWaiting = false;
            };

            var setupEmailTheme = $scope.setupEmailTheme = function (reset) {

                var myTextArea = document.getElementById("codeEditor");
                var myCodeMirror = CodeMirror.fromTextArea(myTextArea, {
                    lineNumbers: true,
                    mode: "text/html",
                    extraKeys: {"Ctrl-Space": "autocomplete"}
                });

                if (typeof reset !== 'undefined') {
                    $scope.emailTheme = {};
                } else {
                    myCodeMirror.getDoc().setValue($scope.emailTheme.content);
                }

                myCodeMirror.on('change', function (cm, change) {
                    cm.save();
                });
            }
        }
    ]);
