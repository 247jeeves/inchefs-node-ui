angular.module('market-admin').controller('EmailerController',
    ['$scope', 'BootStrap', '$state', '$timeout', '$rootScope', 'Menus', '$http', 'Loader','$window', 
        function ($scope, BootStrap, $state, $timeout, $rootScope, Menus, $http, Loader, $window) {
            // This provides BootStrap context.
            $scope.authentication = BootStrap;

            var market = $scope.market = BootStrap.market;

            $scope.profile = $scope.authentication.profile;
            $scope.services = market.settings.services;
            $scope.emailCampaigns = [];
            $scope.emailCampaign = {};
            $scope.error = {};
	    $scope.isDisabledZohoAuth = true;
	    $scope.isDisabledZohoSync = true;
	    $scope.syncButtonTitle = "Sync Leads & Conversations"
            $scope.isWaiting = false;

            $scope.sendToList = [
                {name: 'ALL_BUYERS', desc: 'All Buyers in the Market'},
                {name: 'ALL_SELLERS', desc: 'All Sellers in the Market'},
                {name: 'ALL_USERS', desc: 'All Users (Buyers + Sellers) of the Market'},
                {name: 'BUYER_PROSPECTS', desc: 'All Buyer Prospects'},
                {name: 'SELLER_PROSPECTS', desc: 'All Seller Prospects'},
                {name: 'CUSTOM', desc: 'Custom'}
            ];

            var myTextArea = undefined;
            var myCodeMirror = undefined;

            //Setup Side Nav
            $scope.sidenav = Menus.getMenu('emailerSideNav').items;

            //Action to perform on url routing inside current scope
            $rootScope.$on('$stateChangeSuccess',
                function (event, toState, toParams, fromState, fromParams) {
                    if ((toState.name.match(/\./g) || []).length > 1) {
                        Menus.selectMenuItemByLink('emailerSideNav', toState.parent);
                    } else {
                        Menus.selectMenuItemByLink('emailerSideNav', toState.name);
                    }

                    if (toState.name == 'emailer.add') {
                        if (myTextArea) {
                            myTextArea = undefined;
                            myCodeMirror = undefined;
                        }
                    }

                });

            if ($state.current.name === undefined || $state.current.parent === undefined) {
                $state.transitionTo('emailer.all');
            } else {
                Menus.selectMenuItemByLink('emailerSideNav', $state.current.name);
            }

            //Get all Templates - No pagination needed at the moment here
            $scope.getEmailCampaigns = function () {
                $scope.isWaiting = true;
                $scope.emailTemplates = [];
                $http.get($scope.authentication.api.baseUrl + "/users/"
                    + $scope.authentication.user.id + "/markets/"
                    + market.slug + "/email/campaigns"
                )
                    .success(function (response) {
                        $scope.emailCampaigns = response.results;
                        $scope.isWaiting = false;
                    })
                    .error(function (response) {
                        errorHandler(response);
                    });
            }

          
	 //Function to sync the leads to the zoho crm panel
          $scope.syncLeadsZoho = function(){
                //Step -1 . Check for the zoho credentials exist or not
                if(market.settings.hasOwnProperty('social') && market.settings.social.hasOwnProperty('zoho')){
					$scope.syncButtonTitle = "Syncing....";
					$scope.isDisabledZohoSync = true;
               	      $window.location.href='/zohosync';
                }else{
                  Loader.startLoader();
                  Loader.stopLoader('zohoError');
                }

          }

         $scope.checkZohoAuth = function() {
		$http.post('/checkzohoauth', {"username": market.settings.owner.user})
    		.success(function (response){
			if(response.status=="success"){
				$scope.isDisabledZohoAuth = true;	// In case of success - enable the button (disable false)
				$scope.isDisabledZohoSync = false;
			}else{
				$scope.isDisabledZohoAuth =  false; // In case error - disable the button
				$scope.isDisabledZohoSync = true;
			}
                });

         }

	$scope.doZohoAuth = function(){
         	//Step -1 . Check for the zoho credentials exist or not
                if(market.settings.hasOwnProperty('social') && market.settings.social.hasOwnProperty('zoho')){
                   $window.location.href='/auth/zohocrm';
		}else{
                  Loader.startLoader();
                  Loader.stopLoader('zohoError');
                }
	}

	$scope.runZohoCampaign = function(){
		 $window.location.href='https://crm.zoho.com';
	}
  
          //Get individual Template
            $scope.getEmailCampaign = function () {

                $scope.emailCampaign = {};
                $scope.isWaiting = true;
                if ($scope.emailCampaigns.length > 0) {
                    for (idx in $scope.emailCampaigns) {
                        if ($scope.emailCampaigns[idx].id == $state.params.emailCampaignId) {
                            $timeout(function () {
                                $scope.emailCampaign = $scope.emailCampaigns[idx];

                                $scope.isWaiting = false;

                                myCodeMirror = undefined;
                                myTextArea = undefined;

                                initAddEmailCampaign();
                            }, 5);
                            return;
                        }
                    }
                } else {
                    $http.get($scope.authentication.api.baseUrl + "/users/"
                        + $scope.authentication.user.id + "/markets/"
                        + market.slug + "/email/campaigns/"
                        + $state.params.emailCampaignId
                    )
                        .success(function (response) {
                            $scope.emailCampaign = response;
                            $scope.isWaiting = false;

                            myCodeMirror = undefined;
                            myTextArea = undefined;

                            initAddEmailCampaign();
                        })
                        .error(function (response) {
                            errorHandler(response);
                        });
                }

            }

            $scope.getLeads= function () {
                $http.get("/getLeads")
                .success(function (response) {
                     //console.log("ushakiz getLeads response "+response);
                     $scope.leads = response;
                })
                .error(function (response) {
                     console.log("ushakiz getLeads error response "+response);
                     errorHandler(response);
                });
            } 

            $scope.deleteLead= function (email) {
				var data = {
					"marketId": market.slug,
					"email": email
				};
				$http.post('/gdpr/lead/delete', data);
				$window.location.reload();
            } 
            $scope.createOrUpdateEmailCampaign = function () {

                console.log($scope.emailCampaign);

                $scope.isWaiting = true;

                var data = JSON.parse(JSON.stringify($scope.emailCampaign));
                data.content = angular.element("#codeEditor").val();
                data.market = market.slug;

                if (!$scope.emailCampaign.hasOwnProperty('id')) {
                    $http.post($scope.authentication.api.baseUrl + "/users/"
                        + $scope.authentication.user.id + "/markets/"
                        + market.slug + "/email/campaigns",
                        data)
                        .success(function (response) {
                            $scope.emailCampaign = response;
                            $scope.emailCampaigns.push($scope.emailCampaign);
                            $scope.isWaiting = false;
                            $scope.error = {};
                            $scope.emailCampaign = {};
                            $state.go('emailer.all');
                        })
                        .error(function (response) {
                            errorHandler(response);
                        });
                } else {
                    $http.put($scope.authentication.api.baseUrl + "/users/"
                        + $scope.authentication.user.id + "/markets/"
                        + market.slug + "/email/campaigns/" + $scope.emailCampaign.id,
                        data)
                        .success(function (response) {
                            $scope.emailCampaign = response;
                            $scope.emailCampaigns.push($scope.emailCampaign);
                            $scope.isWaiting = false;
                            $scope.emailCampaign = {};
                            $scope.error = {};
                        })
                        .error(function (response) {
                            errorHandler(response);
                        });
                }
            };

            var errorHandler = function (response) {
                console.log(response);
                $scope.error = {};
                if (response && response !== undefined && response.hasOwnProperty('errors')) {
                    for (var idx in response.errors) {
                        $scope.error[response.errors[idx]['field']] = {
                            value: response.errors[idx]['value'],
                            message: response.errors[idx]['message']
                        };
                    }
                }
                $scope.isWaiting = false;
            };

            var initAddEmailCampaign = $scope.initAddEmailCampaign = function (reset) {

                //$scope.emailCampaign = {};

                setupDatePickers();


                //$scope.tinymceOptions = {
                //    plugins: 'link image code',
                //    toolbar: 'undo redo | bold italic | alignleft aligncenter alignright | code'
                //};

                //if (typeof reset !== 'undefined') {
                //    $scope.emailCampaign.options = {
                //        needTheme: true
                //    };
                //} else {
                //    $scope.tinymceModel = 'Initial content';
                //}


                if (typeof myTextArea === 'undefined') {
                    myTextArea = document.getElementById("codeEditor");
                }

                if (typeof myCodeMirror === 'undefined') {
                    myCodeMirror = CodeMirror.fromTextArea(myTextArea, {
                        lineNumbers: true,
                        mode: "text/html",
                        extraKeys: {"Ctrl-Space": "autocomplete"}
                    });
                }

                if (typeof reset !== 'undefined') {
                    $scope.emailCampaign.options = {
                        needTheme: true
                    };
                } else {
                    myCodeMirror.getDoc().setValue($scope.emailCampaign.content || "");
                }

                myCodeMirror.on('change', function (cm, change) {
                    cm.save();
                });
            };

            var setupDatePickers = function () {

                $("input[data-datetime-bind]").datetimepicker({
                    minDate: (new Date()).setDate((new Date()).getDate() + 1),
                    collapse: true,
                    useCurrent: false
                });

                var ua = navigator.userAgent;

                if (ua.match(/(iPhone|iPad)/i)) {
                    $("#dateAndTime").attr('type', 'datetime-local');
                    $('body').on('change', '#dateAndTime', function (e) {

                        $scope.listing.instantRequest.expiryDate = moment($('#dateAndTime').val()).
                            format('YYYY-MM-DDTHH:mm:ss.SSSZ');

                        if (!$scope.$$phase) {
                            $scope.$apply();
                        }
                    });
                } else {
                    $("#dateAndTime").datetimepicker({
                        minDate: (new Date()).setDate((new Date()).getDate() + 1),
                        collapse: true,
                        useCurrent: false
                    });

                    $('body').on('dp.change', '#dateAndTime', function (e) {
                        $scope.dateAndTime = $('#dateAndTime').val();
                        $scope.emailCampaign.deferredDateTime = $('#dateAndTime').data("DateTimePicker")
                            .date().format('YYYY-MM-DDTHH:mm:ss.SSSZ');
                    });
                }
            }


        }]);
