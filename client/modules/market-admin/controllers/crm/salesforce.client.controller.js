angular.module('market-admin').controller('SalesforceController',
    ['$scope', 'BootStrap', '$state', '$timeout', '$rootScope', 'Menus', '$http', 'Loader','$window', 
        function ($scope, BootStrap, $state, $timeout, $rootScope, Menus, $http, Loader, $window) {
            // This provides BootStrap context.
            $scope.authentication = BootStrap;

            var market = $scope.market = BootStrap.market;

            $scope.profile = $scope.authentication.profile;
            $scope.services = market.settings.services;
            $scope.error = {};
	    $scope.isDisabledSFAuth = true;
	    $scope.isDisabledSFSync = true;
	    $scope.syncButtonTitle = "Sync Leads & Conversations"
            $scope.isWaiting = false;

            //Setup Side Nav
            $scope.sidenav = Menus.getMenu('emailerSideNav').items;

          $scope.syncLeadsSF = function(){
                //Step -1 . Check for the zoho credentials exist or not
                if(market.settings.hasOwnProperty('social') && market.settings.social.hasOwnProperty('salesforce')){
					$scope.syncButtonTitle = "Syncing....";
					$scope.isDisabledSFSync = true;
               	      $window.location.href='/sfsync';
                }else{
                  Loader.startLoader();
                  Loader.stopLoader('zohoError');
                }

          }

         $scope.checkSFAuth = function() {
		$http.post('/checksfauth', {"username": market.settings.owner.user})
    		.success(function (response){
			if(response.status=="success"){
				$scope.isDisabledSFAuth = true;	// In case of success - enable the button (disable false)
				$scope.isDisabledSFSync = false;
			}else{
				$scope.isDisabledSFAuth =  false; // In case error - disable the button
				$scope.isDisabledSFSync = true;
			}
                });

         }

	$scope.doSFAuth = function(){
         	//Step -1 . Check for the zoho credentials exist or not
                if(market.settings.hasOwnProperty('social') && market.settings.social.hasOwnProperty('salesforce')){
                   $window.location.href='/auth/sfcrm';
		}else{
                  Loader.startLoader();
                  Loader.stopLoader('zohoError');
                }
	}

	$scope.runSFCampaign = function(){
		 $window.location.href='https://login.salesforce.com/';
	}
  

            var errorHandler = function (response) {
                console.log(response);
                $scope.error = {};
                if (response && response !== undefined && response.hasOwnProperty('errors')) {
                    for (var idx in response.errors) {
                        $scope.error[response.errors[idx]['field']] = {
                            value: response.errors[idx]['value'],
                            message: response.errors[idx]['message']
                        };
                    }
                }
                $scope.isWaiting = false;
            };

}]);
