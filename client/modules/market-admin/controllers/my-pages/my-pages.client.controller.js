angular.module('market-admin').controller('MyPagesController',
    ['$scope', 'BootStrap', '$state', '$timeout', '$rootScope', 'Menus', '$http', 'Loader',
        function ($scope, BootStrap, $state, $timeout, $rootScope, Menus, $http, Loader) {
            // This provides BootStrap context.
            $scope.authentication = BootStrap;

            var market = $scope.market = BootStrap.market;

            $scope.pages = [];
            $scope.page = {
                parent: $scope.market.slug
            };
            $scope.error = {};

            $scope.isWaiting = false;

            var myTextArea = undefined;
            var myCodeMirror = undefined;

            //Setup Side Nav
            $scope.sidenav = Menus.getMenu('myPagesSideNav').items;

            //Action to perform on url routing inside current scope
            $rootScope.$on('$stateChangeSuccess',
                function (event, toState, toParams, fromState, fromParams) {
                    if ((toState.name.match(/\./g) || []).length > 1) {
                        Menus.selectMenuItemByLink('myPagesSideNav', toState.parent);
                    } else {
                        Menus.selectMenuItemByLink('myPagesSideNav', toState.name);
                    }

                    if (toState.name == 'myPages.add') {
                        if (myTextArea) {
                            myTextArea = undefined;
                            myCodeMirror = undefined;
                        }
                    }

                });

            if ($state.current.name === undefined || $state.current.parent === undefined) {
                $state.transitionTo('myPages.all');
            } else {
                Menus.selectMenuItemByLink('myPagesSideNav', $state.current.name);
            }

            //Get all Templates - No pagination needed at the moment here
            $scope.getPages = function () {
                $scope.isWaiting = true;
                $scope.pages = [];
                $http.get("/market-admin/pages")
                    .success(function (response) {
                        $scope.pages = response;
                        $scope.isWaiting = false;
                    })
                    .error(function (response) {
                        errorHandler(response);
                    });
            }

            //Get individual Template
            $scope.getPage = function () {
                $scope.page = undefined;
                $scope.isWaiting = true;
                if ($scope.pages.length > 0) {
                    for (idx in $scope.pages) {
                        if ($scope.pages[idx]._id == $state.params.pageId) {
                            $timeout(function () {
                                $scope.page = $scope.pages[idx];

                                $scope.isWaiting = false;

                                myCodeMirror = undefined;
                                myTextArea = undefined;

                                setupPage();
                            }, 5);
                            return;
                        }
                    }
                } else {
                    $http.get("/market-admin/pages/"
                        + $state.params.pageId
                    )
                        .success(function (response) {
                            $scope.page = response;
                            $scope.isWaiting = false;

                            myCodeMirror = undefined;
                            myTextArea = undefined;

                            setupPage();
                        })
                        .error(function (response) {
                            errorHandler(response);
                        });
                }

            }

            $scope.createOrUpdatePage = function () {
                $scope.isWaiting = true;

                var data = JSON.parse(JSON.stringify($scope.page));
                data.content = angular.element("#codeEditor").val();

                if (!$scope.page.hasOwnProperty('_id')) {
                    $http.post("/market-admin/pages",
                        data)
                        .success(function (response) {
                            $scope.page = response;
                            $scope.pages.push($scope.page);
                            $scope.isWaiting = false;
                            $scope.error = {};
                            $state.go('myPages.all');
                        })
                        .error(function (response) {
                            errorHandler(response);
                        });
                } else {
                    $http.put("/market-admin/pages/" + $scope.page._id,
                        data)
                        .success(function (response) {
                            $scope.page = response;
                            $scope.pages.push($scope.page);
                            $scope.isWaiting = false;
                            $scope.error = {};
                        })
                        .error(function (response) {
                            errorHandler(response);
                        });
                }
            };

            var errorHandler = function (response) {
                console.log(response);
                $scope.error = {};
                if (response && response !== undefined && response.hasOwnProperty('errors')) {
                    for (var idx in response.errors) {
                        $scope.error[response.errors[idx]['field']] = {
                            value: response.errors[idx]['value'],
                            message: response.errors[idx]['message']
                        };
                    }
                }
                $scope.isWaiting = false;
            };

            var setupPage = $scope.setupPage = function (reset) {

                if (typeof myTextArea === 'undefined') {
                    myTextArea = document.getElementById("codeEditor");
                }

                if (typeof myCodeMirror === 'undefined') {
                    myCodeMirror = CodeMirror.fromTextArea(myTextArea, {
                        lineNumbers: true,
                        lineWrapping: true,
                        mode: "text/html",
                        extraKeys: {"Ctrl-Space": "autocomplete"}
                    });

                    var charWidth = myCodeMirror.defaultCharWidth(), basePadding = 4;
                    myCodeMirror.on("renderLine", function(cm, line, elt) {
                        var off = CodeMirror.countColumn(line.text, null, cm.getOption("tabSize")) * charWidth;
                        elt.style.textIndent = "-" + off + "px";
                        elt.style.paddingLeft = (basePadding + off) + "px";
                    });
                    myCodeMirror.refresh();
                }

                if (typeof reset !== 'undefined') {
                    $scope.page = {
                        parent: $scope.market.slug
                    }
                } else {
                    myCodeMirror.getDoc().setValue($scope.page.content.data);
                }

                myCodeMirror.on('change', function (cm, change) {
                    cm.save();
                });
            };
        }
    ]);
