angular.module('market-admin').controller('FaqController', ['$scope', 'BootStrap', '$state',
    '$timeout', '$rootScope', 'Menus', '$http',
    '$filter', 'localStorageService',
    function ($scope, BootStrap, $state, $timeout, $rootScope, Menus, $http, $filter, localStorageService) {
        // This provides BootStrap context.
        $scope.authentication = BootStrap;

        var offset = 0;
        var max = 10;

            console.log("ushakiz faq controller 111");
        $scope.total = 0;
        $scope.faqs = [];
        $scope.faq = {};

        $scope.market = BootStrap.market;
        $scope.allServices = BootStrap.services;

            console.log("ushakiz faq controller 222");
        $scope.previousState = 'faq.all';

        $scope.notAllowed = false;

        $scope.marketServices = $scope.market.settings.services;
        $scope.service = $scope.marketServices[0].name;

        $scope.formatDate = function (date) {
            return moment(date).format('MM/DD/YYYY hh:mm A');
        };

        //Setup Side Nav
        $scope.sideNav = Menus.getMenu('faqSideNav').items;

            console.log("ushakiz faq controller after side nav 2222");
        //Action to perform on url routing inside current scope
        $rootScope.$on('$stateChangeSuccess',
            function (event, toState, toParams, fromState, fromParams) {

                if (fromState.parent == "faq") {
                    $scope.previousState = fromState.name;
                }

                if ($state.current.name == 'faq.all') {
                    offset = 0;
                }

                if ((toState.name.match(/\./g) || []).length > 1) {
                    Menus.selectMenuItemByLink('faqSideNav', toState.parent);
                } else {
                    Menus.selectMenuItemByLink('faqSideNav', toState.name);
                }
            });

        if ($state.current.name === undefined || $state.current.parent === undefined) {
            $state.transitionTo('faq.all');
        } else {
            Menus.selectMenuItemByLink('faqSideNav', $state.current.name);
        }

        $scope.setupAddFaq = function () {
            console.log("ushakiz setupaddfaq 333");
            $scope.faq = {
            };
        };

        var setupEditFaq = function () {
            console.log("ushakiz setupfaq 444");
            $scope.faq = {
            };

        }

        $scope.createOrUpdateFaq= function () {
            console.log("ushakiz createOrUpdateFAQ 555");
            if (!$scope.nlp.hasOwnProperty('slug')) {
                $http.post($scope.authentication.api.baseUrl +
                '/users/' + $scope.authentication.user.id + '/faq', $scope.faq)
                    .success(function (response) {
                        console.log("response for createorupdatefaq"+ JSON.stringify(response));
                        $scope.faq = {};
                    }).error(function (response) {
                        console.log(response);
                        errorHandler(response);
                    });
            } else {
                $http.put($scope.authentication.api.baseUrl +
                '/users/' + $scope.authentication.user.id + '/faq/' + $scope.faq.slug, $scope.faq)
                    .success(function (response) {
                        console.log(response);
                        $scope.faqs = {};
                        $state.go('faq.all');
                    }).error(function (response) {
                        console.log(response);
                        errorHandler(response);
                    });
            }
        };

        // Fetch FAQ List
        $scope.faqList = function (data) {

            console.log("ushakiz faqList666");
            if (data && data.refresh) {
                $scope.total = 0;
                offset = 0;
                $scope.faqs = [];
                $scope.isEnd = false;
            }

            if ($scope.isEnd) {
                return;
            }

            $scope.isWaiting = true;
            $scope.success = "";


            var url = $scope.authentication.api.baseUrl + '/users/' + $scope.authentication.user.id + '/faqs/';
            url += '?max=' + max + '&offset=' + offset;


            if ($scope.queryText) {
                url += '&query=' + $scope.queryText.text;
            }

            console.log("faqlist "+url);

            $http.get(url)
                .success(function (response) {

                    $scope.faqs = $scope.faqs.concat(response.searchResults);
                    $scope.total = response.total;
                    offset += max;
                    $scope.isWaiting = false;
                    $scope.error = {};

                }).error(function (response) {
                    console.log("faqlist response error "+response);
                    errorHandler(response);
                });
        }

        // Fetch Single Request with Deep Linking
        $scope.getFaq = function () {

            console.log("ushakiz getfaq 777");
            $scope.faq = undefined;
            if ($scope.faqs.length > 0) {
                for (var idx in $scope.faqs) {
                    if ($scope.faqs[idx].slug == $state.params.id) {
                        $scope.faq = $scope.faqs[idx];

                        setupEditFaq();
                        updateWidgetDisplayFlags();
                        Menus.selectMenuItemByLink('faqSideNav', $scope.previousState);
                        return;
                    }
                }
            } else {
                if ($scope.faq === undefined) {
                    console.log("faq undefined 88888"+$scope.authentication.api.baseUrl +
                    '/users/' + $scope.authentication.user.id + '/faqs/'
                    + $state.params.id);
                    $http.get($scope.authentication.api.baseUrl +
                        '/users/' + $scope.authentication.user.id + '/faqs/'
                        + $state.params.id
                    )
                        .success(function (response) {
                            $scope.faq = response;


                            console.log("faq 888888"+$scope.faq);

                            setupEditFaq();
                            //updateWidgetDisplayFlags();
                            Menus.selectMenuItemByLink('faqSideNav', $scope.previousState);
                            $scope.isWaiting = false;
                            $scope.error = {};
                        }).error(function (response) {
                            console.log("error get faq 8888"+response);
                            errorHandler(response);
                        });
                }
            }
        }
        $scope.deleteFaq = function () {


        };

        var errorHandler = function (response) {
            console.log("ushakiz faq error handler 888"+response);
            $scope.faq = undefined;
            console.log(response);
            if (response == '') {
                $scope.request = {};
                $scope.notAllowed = true;
                return;
            }

            $scope.error = {};
            if (response.errors) {
                for (var idx in response.errors) {
                    $scope.error[response.errors[idx]['field']] = {
                        value: response.errors[idx]['value'],
                        message: response.errors[idx]['message']
                    };
                }
            }
            $scope.isWaiting = false;
        };
        var updateWidgetDisplayFlags = function () {
            if ($scope.market.settings.addons.faq
                && ($scope.authentication.user.permissions.indexOf('ROLE_SELLER') != -1)) {
                var userSubscription = $scope.market.getCurrentUserSubscription();

                if (!userSubscription) {
                    $scope.isSubscribed = false;
                } else {
                    if (userSubscription.leftCount <= 0) {
                        $scope.isUnderSubscriptionLimit = false;
                    }
                }

            }
        }

    }
]);
