angular.module('market-admin').controller('MyMarketController',
    ['$scope', 'BootStrap', '$state', '$timeout', '$rootScope', 'Menus', 'Subscription' ,'$http', '$filter', 'flowFactory', 
        function ($scope, BootStrap, $state, $timeout, $rootScope, Menus, Subscription, $http, $filter, flowFactory) {
            // This provides BootStrap context.
            $scope.flowObj = flowFactory.create({singleFile:true});

            var imageToUpload;
            var flowInst = undefined;
            $scope.authentication = BootStrap;

            $scope.profile = $scope.authentication.profile;

            $scope.market = JSON.parse(JSON.stringify(BootStrap.market));

            $scope.error = {};
            $scope.isWaiting = false;
    	   

            $scope.allServices = BootStrap.services;

            $scope.allCurrencies = window._currencies || [];
            $scope.isRequestPhraseDisabled = true;
            if($scope.market.settings.services.length == 1){
                $scope.isRequestPhraseDisabled = false;
            }
            //Setup Side Nav
            $scope.myMarketSideNav = Menus.getMenu('myMarketSideNav').items;

            if (!$scope.market.settings.hasOwnProperty('urls')) {
                $scope.market.settings.urls = ['', ''];
            }

            var currentDomains = JSON.parse(JSON.stringify($scope.market.settings.urls)).join(',');
            var updateDomains = false;

            //Action to perform on url routing inside current scope
            $rootScope.$on('$stateChangeSuccess',
                function (event, toState, toParams, fromState, fromParams) {
                    if ((toState.name.match(/\./g) || []).length > 1) {
                        Menus.selectMenuItemByLink('myMarketSideNav', toState.parent);
                    } else {
                        Menus.selectMenuItemByLink('myMarketSideNav', toState.name);
                    }
                });

            if ($state.current.name === undefined || $state.current.parent === undefined) {
                $state.transitionTo('myMarket.settings');
            } else {
                Menus.selectMenuItemByLink('myMarketSideNav', $state.current.name);
            }

            $scope.initFileUpload = function () {
                $scope.isWaiting = true;
            }
            $scope.uploadSuccess = function ($flow, $file, $message) {
                flowInst = $flow;
                imageToUpload = $file.uniqueIdentifier;
                $scope.isWaiting = false;
            }
            $scope.getDataUrl = function (file) {
                var fileReader = new FileReader();
                fileReader.readAsDataURL(file.file);
                fileReader.addEventListener("load", function () {
                    $('#' + file.uniqueIdentifier.split('.')[0]).css('background', 'url("' + fileReader.result + '")');
                }, false);
            }

            $scope.deleteImage = function (image) {

                if ($scope.isWaiting) {
                    return;
                }
                $scope.isWaiting = true;
                $http.delete(url)
                    .success(function (response) {
                        console.log("ushakiz flowObj files "+$scope.flowObj.files);
                        console.log("ushakiz flowObj file "+$scope.flowObj.file);
                        $scope.flowObj.files.forEach(function (obj, idx) {
                                if (obj.uniqueIdentifier === image.uniqueIdentifier) {
                                    $scope.flowObj.files.splice(idx, 1);
                                }
                        });
                        if (!$scope.$$phase) {
                            $scope.$apply();
                        }
                        $scope.isWaiting = false;
                    })
                    .error(function (response) {
                        $scope.isWaiting = false;
                    })

            }
            $scope.updateMarket = function () {
                $scope.isWaiting = true;

                console.log("ushakiz updatemarket ");
                var data = JSON.parse(JSON.stringify($scope.market));
                console.log("ushakiz updatemarket ushakiz "+data);
                if (currentDomains != $scope.market.settings.urls.join(',')) {
                    updateDomains = true;
                } else {
                    updateDomains = false;
                }
                $scope.market.settings.logo = imageToUpload;

                console.log("ushakiz updatemarket callingapi "+imageToUpload);
                $http.put($scope.authentication.api.baseUrl + "/users/"
                    + $scope.authentication.user.id + "/markets/"
                    + $scope.market.slug,
                    data)
                    .success(function (response) {
                console.log("ushakiz updatemarket response "+JSON.stringify(response));
                        $scope.market = response;
                        $scope.error = {};
                        if (updateDomains) {
                            $http.post('/market-admin/setup-domain', {
                                market: $scope.market.slug,
                                domains: $scope.market.settings.urls
                            })
                                .success(function (response) {
                                    window.location = window.location.protocol + '//'
                                    + $scope.market.settings.urls[0] + '/market-admin';
                                })
                                .error(function (response) {
                                    console.log(response);
                                });
                        } else {
                            window.location.reload();
                        }
                    })
                    .error(function (response) {
                        errorHandler(response);
                    });
            };

            $scope.setupMarketForm = function () {


                if ($scope.market.settings.hasOwnProperty('services')) {
                    var services = JSON.parse(JSON.stringify($scope.market.settings.services));
                    $scope.market.settings.services = [];
                    angular.forEach(services, function (value, key) {
                        if(value.hasOwnProperty('name')){
                            this.push(value.name);
                        }else{
                            this.push(value);
                        }
                    }, $scope.market.settings.services);
                }

                $timeout(function () {
                    $("#tags").selectize({
                        create: function (input, callback) {
                            callback({
                                name: input,
                                description: input
                            });
                        },
                        valueField: 'name',
                        labelField: 'description',
                        searchField: 'description',
                        delimiter: ',',
                        maxItems: '10',
                        items: $scope.market.settings.services,
                        options: $scope.allServices,
                        sortField: {
                            field: 'description',
                            direction: 'asc'
                        },
                        onChange: function (value) {
                            if (value == null) {
                                $scope.market.settings.services = [];
                            } else {
                                $scope.market.settings.services = value;
                            }
                        }
                    });
                }, 5);

                var myTextArea = angular.element("#codeEditor")[0];
                if (myTextArea) {
                    var myCodeMirror = CodeMirror.fromTextArea(myTextArea, {
                        lineNumbers: true,
                        matchBrackets: true,
                        autoCloseBrackets: true,
                        mode: "application/ld+json",
                        lineWrapping: true
                    });

                    myCodeMirror.getDoc().setValue($filter('json')($scope.market.settings));

                    myCodeMirror.on('change', function (cm, change) {
                        cm.save();
                    });
                }
            };

            var errorHandler = function (response) {
                console.log(response);
                $scope.error = {};
                if (response && response !== undefined && response.hasOwnProperty('errors')) {
                    for (var idx in response.errors) {
                        $scope.error[response.errors[idx]['field']] = {
                            value: response.errors[idx]['value'],
                            message: response.errors[idx]['message']
                        };
                    }
                }
                $scope.isWaiting = false;
            };

        }
    ]);
