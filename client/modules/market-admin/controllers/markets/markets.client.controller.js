angular.module('market-admin').controller('MarketsController',
    ['$scope', 'BootStrap', '$state', '$timeout', '$rootScope', 'Menus', '$http', '$filter',
        function ($scope, BootStrap, $state, $timeout, $rootScope, Menus, $http, $filter) {
            // This provides BootStrap context.
            $scope.authentication = BootStrap;

            $scope.profile = $scope.authentication.profile;

            $scope.currentMarket = JSON.parse(JSON.stringify(BootStrap.market));

            $scope.error = {};
            $scope.isWaiting = false;

            $scope.market = {};
            $scope.markets = [];

            $scope.allServices = BootStrap.services;

            $scope.progress = {
                isFirstStepDone: false,
                isSecondStepDone: false,
                countDown: 0
            };


            //Setup Side Nav
            $scope.marketsSideNav = Menus.getMenu('marketsSideNav').items;

            //Action to perform on url routing inside current scope
            $rootScope.$on('$stateChangeSuccess',
                function (event, toState, toParams, fromState, fromParams) {
                    if ((toState.name.match(/\./g) || []).length > 1) {
                        Menus.selectMenuItemByLink('marketsSideNav', toState.parent);
                    } else {
                        Menus.selectMenuItemByLink('marketsSideNav', toState.name);
                    }
                });

            if ($state.current.name === undefined || $state.current.parent === undefined) {
                $state.transitionTo('markets.all');
            } else {
                Menus.selectMenuItemByLink('marketsSideNav', $state.current.name);
            }


            $scope.getMarkets = function () {
                $http.get(BootStrap.api.baseUrl + "/users/"
                + $scope.authentication.user.id + "/markets/")
                    .success(function (response) {
                        $scope.markets = response.results;
                    })
                    .error(function (response) {
                        console.log(reponse);
                    });
            }

            $scope.updateMarket = function () {
                $scope.isWaiting = true;

                //var data = JSON.parse(JSON.stringify($scope.market));
                //console.log(angular.element("#codeEditor").val());
                //data.settings = JSON.parse(angular.element("#codeEditor").val());

                $http.put($scope.authentication.api.baseUrl + "/users/"
                    + $scope.authentication.user.id + "/markets/"
                    + $scope.market.slug,
                    $scope.market)
                    .success(function (response) {
                        $scope.market = response;
                        if ($scope.market.settings.hasOwnProperty('services')) {
                            var services = JSON.parse(JSON.stringify($scope.market.settings.services));
                            $scope.market.settings.services = [];
                            angular.forEach(services, function (value, key) {
                                this.push(value.name);
                            }, $scope.market.settings.services);
                        }

                        if (angular.element('#updateDomains').prop('checked')) {
                            $http.post('/market-admin/setup-domain', {
                                market: $scope.market.slug,
                                domains: $scope.market.settings.urls
                            })
                                .success(function (response) {
                                    window.location.reload();
                                })
                                .error(function (response) {
                                    console.log(response);
                                });
                        }
                        $scope.error = {};
                        $scope.isWaiting = false;
                    })
                    .error(function (response) {
                        console.log(response);
                        errorHandler(response);
                        $scope.isWaiting = false;
                    });
            };

            $scope.setupCreateMarket = function () {
                $scope.progress = {
                    isFirstStepDone: false,
                    isSecondStepDone: false,
                    countDown: 0
                };

                $scope.market = {
                    settings: {
                        marketType: 'buyer_payment',
                        offerType: 'price',
                        services: [],
                        addons: {
                            seller_subscription: false,
                            prospect_finder: false
                        }
                    }
                };

                $timeout(function () {
                    $("#tags").selectize({
                        valueField: 'name',
                        labelField: 'description',
                        searchField: 'description',
                        delimiter: ',',
                        maxItems: '10',
                        options: $scope.allServices,
                        sortField: {
                            field: 'description',
                            direction: 'asc'
                        },
                        onChange: function (value) {
                            if (value == null) {
                                $scope.market.settings.services = [];
                            } else {
                                $scope.market.settings.services = value;
                            }
                        }

                    });
                }, 5);
            };


            $scope.createMarket = function () {

                $scope.isWaiting = true;

                var data = JSON.parse(JSON.stringify($scope.market));
                //data.settings = JSON.parse(angular.element("#codeEditor").val());

                if (data.settings.marketType == 'seller_subscription') {
                    data.settings.plans = [{
                        free: {
                            amount: 0,
                            max: 3,
                            description: "Free Plan!",
                            features: [
                                "First 3 bids are free",
                                "Try before you subscribe",
                                "Renewed every month"
                            ],
                            type: "month"
                        }
                    }];
                }

                $http.post($scope.authentication.api.baseUrl + "/users/"
                    + $scope.authentication.user.id + "/markets/",
                    data)
                    .success(function (response) {
                        $scope.market = response;
                        $scope.error = {};
                        console.log($scope.market);
                        $scope.progress.isFirstStepDone = true;
                        $scope.isWaiting = false;
                        $scope.createMarketStep2();
                    })
                    .error(function (response) {
                        errorHandler(response);
                        $scope.isWaiting = false;
                    });
            };

            $scope.createMarketStep2 = function () {

                var progressBar = setInterval(function () {
                    if ($scope.progress.countDown == 100) {
                        $scope.progress.countDown = 0;
                    }
                    $scope.progress.countDown++;
                    $scope.progress.style = {width: $scope.progress.countDown + '%'};
                    if (!$scope.$$phase) {
                        $scope.$apply();
                    }
                }, 50);

                $http.post('/market-admin/setup-domain', {
                    market: $scope.market.slug,
                    domains: $scope.market.settings.urls
                })
                    .success(function (response) {
                        $http.post('/market-admin/setup-pages', {
                            market: $scope.market.slug,
                            name: $scope.market.name
                        }).success(function (response) {
                            console.log(response);
                            $timeout(function () {
                                $scope.progress.isSecondStepDone = true;
                                $scope.progress.countDown = 100;
                                clearInterval(progressBar);
                            }, 5000);
                        }).error(function (response) {
                            console.log(response);
                        });
                    })
                    .error(function (response) {
                        console.log(response);
                    });
            };

            $scope.setupMarketForm = function () {

                $scope.market = {};

                if ($scope.markets.length > 0) {
                    for (idx in $scope.markets) {
                        if ($scope.markets[idx].slug == $state.params.marketId) {
                            $timeout(function () {
                                $scope.market = $scope.markets[idx];
                                console.log($scope.market);
                                $scope.isWaiting = false;

                                setupEditUI();
                            }, 5);
                            return;
                        }
                    }
                } else {
                    $http.get($scope.authentication.api.baseUrl + "/users/"
                        + $scope.authentication.user.id + "/markets/"
                        + $state.params.marketId
                    )
                        .success(function (response) {
                            $scope.market = response;
                            $scope.isWaiting = false;

                            setupEditUI();

                        })
                        .error(function (response) {
                            errorHandler(response);
                        });
                }
            };

            var setupEditUI = function () {
                myCodeMirror = undefined;
                myTextArea = undefined;

                if ($scope.market.settings.hasOwnProperty('services')) {
                    var services = JSON.parse(JSON.stringify($scope.market.settings.services));
                    $scope.market.settings.services = [];
                    angular.forEach(services, function (value, key) {
                        this.push(value.name);
                    }, $scope.market.settings.services);
                }

                var myTextArea = angular.element("#codeEditor")[0];
                if (myTextArea) {
                    var myCodeMirror = CodeMirror.fromTextArea(myTextArea, {
                        lineNumbers: true,
                        matchBrackets: true,
                        autoCloseBrackets: true,
                        mode: "application/ld+json",
                        lineWrapping: true
                    });

                    myCodeMirror.getDoc().setValue($filter('json')($scope.market.settings));

                    myCodeMirror.on('change', function (cm, change) {
                        cm.save();
                    });
                }
            }

            var errorHandler = function (response) {
                console.log(response);
                $scope.error = {};
                if (response && response !== undefined && response.hasOwnProperty('errors')) {
                    for (var idx in response.errors) {
                        $scope.error[response.errors[idx]['field']] = {
                            value: response.errors[idx]['value'],
                            message: response.errors[idx]['message']
                        };
                    }
                }
                $scope.isWaiting = false;
            };

        }
    ]);
