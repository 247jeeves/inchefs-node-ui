angular.module('market-admin').controller('QuestionsController',
    ['$scope', 'BootStrap', '$state', '$timeout', '$rootScope', 'Menus', '$http', 'Loader',
        function ($scope, BootStrap, $state, $timeout, $rootScope, Menus, $http, Loader) {
            // This provides BootStrap context.
            $scope.authentication = BootStrap;

            var market = $scope.market = BootStrap.market;

            $scope.profile = $scope.authentication.profile;
            $scope.services = market.settings.services;
            $scope.questionTypes = ["text", "number", "checkbox", "radio", "time", "textarea", "datetime", "file"]; //, "files", "video"];
            $scope.questions = [];
            $scope.question = {};
            $scope.error = {};
            //$scope.isResponseChk = true;
            $scope.currentService = $scope.services[0];

            $scope.isWaiting = false;

            $scope.services.push({
                name: "seller-questions",
                description: "Seller Questions",
                type: "domain-questions"
            });
            $scope.services.push({
                name: "listing-questions",
                description: "Listing Questions",
                type: "domain-questions"
            });

            //Setup Side Nav
            $scope.sidenav = Menus.getMenu('questionsSideNav').items;

            //Action to perform on url routing inside current scope
            $rootScope.$on('$stateChangeSuccess',
                function (event, toState, toParams, fromState, fromParams) {
                    if ((toState.name.match(/\./g) || []).length > 1) {
                        Menus.selectMenuItemByLink('questionsSideNav', toState.parent);
                    } else {
                        Menus.selectMenuItemByLink('questionsSideNav', toState.name);
                    }
                });

            if ($state.current.name === undefined || $state.current.parent === undefined) {
                $state.transitionTo('questions.all');
            } else {
                Menus.selectMenuItemByLink('questionsSideNav', $state.current.name);
            }

            $scope.toggleResponse = function () {
                $timeout(function () {
                    console.log($scope.isResponseChk);
                }, 100);
            }

            //Get all Questions - No pagination needed at the moment here
            $scope.getQuestions = function (service) {
                $scope.isWaiting = true;
                $scope.questions = [];

                //console.log(service);
                if (typeof service !== 'undefined') {
                    $scope.currentService = service;
                }

                $scope.isResponseChk = $('#isResponse').prop('checked');

                var url = $scope.authentication.api.baseUrl + "/users/"
                    + $scope.authentication.user.id + "/markets/"
                    + market.slug + "/questions"
                    + "?category=" + $scope.currentService.name
                    + "&categoryType=" + $scope.currentService.type
                    + "&isResponse=" + $scope.isResponseChk;

                $http.get(url)
                    .success(function (response) {
                        $scope.questions = response.results;
                        $scope.isWaiting = false;
                    })
                    .error(function (response) {
                        errorHandler(response);
                    });
            };

            //Get individual Listing
            $scope.getQuestion = function () {
                $scope.question = undefined;
                $scope.isWaiting = true;
                if ($scope.questions.length > 0) {
                    for (idx in $scope.questions) {
                        if ($scope.questions[idx].id == $state.params.questionId) {
                            $timeout(function () {
                                $scope.question = $scope.questions[idx];
                                setupQuestion();
                                $scope.isWaiting = false;
                            }, 5);
                            return;
                        }
                    }
                } else {
                    $http.get($scope.authentication.api.baseUrl + "/users/"
                        + $scope.authentication.user.id + "/markets/"
                        + market.slug + "/questions/"
                        + $state.params.questionId
                    )
                        .success(function (response) {
                            $scope.question = response;
                            setupQuestion();
                            $scope.isWaiting = false;
                        })
                        .error(function (response) {
                            errorHandler(response);
                        });
                }

            };

            var setupQuestion = function () {
                $scope.services.forEach(function (obj) {
                    if (obj.name === $scope.question.service) {
                        $scope.question.category = obj;
                        return;
                    }
                })
            }

            $scope.createOrUpdateQuestion = function () {
                $scope.isWaiting = true;

                var data = JSON.parse(JSON.stringify($scope.question));
                data.categoryType = $scope.question.category.type || $scope.services[0].type;
                data.category = $scope.question.category.name || $scope.services[0].name;

                if (typeof data.choices !== 'undefined' && typeof data.choices !== 'object') {
                    data.choices = data.choices.split(',');
                }

                console.log(data);

                if (!$scope.question.hasOwnProperty('id')) {
                    $http.post($scope.authentication.api.baseUrl + "/users/"
                        + $scope.authentication.user.id + "/markets/"
                        + market.slug + "/questions",
                        data)
                        .success(function (response) {
                            $scope.question = response;
                            $scope.questions.push($scope.question);
                            $scope.isWaiting = false;
                            $scope.error = {};
                            $state.go('questions.all');
                        })
                        .error(function (response) {
                            errorHandler(response);
                        });
                } else {

                    $http.put($scope.authentication.api.baseUrl + "/users/"
                        + $scope.authentication.user.id + "/markets/"
                        + market.slug + "/questions/" + $scope.question.id,
                        data)
                        .success(function (response) {
                            $scope.question = response;
                            setupQuestion();
                            $scope.questions.push($scope.question);
                            $scope.isWaiting = false;
                            $scope.error = {};
                        })
                        .error(function (response) {
                            errorHandler(response);
                        });
                }
            };

            $scope.deleteQuestion = function () {
                $http.delete($scope.authentication.api.baseUrl + "/users/"
                + $scope.authentication.user.id + "/markets/"
                + market.slug + "/questions/" + $scope.question.id)
                    .success(function (response) {
//                            $scope.question = response;
//                            $scope.questions.push($scope.question);
                        $scope.isWaiting = false;
                        $scope.error = {};
                        $state.go('questions.all');
                    })
                    .error(function (response) {
                        errorHandler(response);
                    });
            };

            $scope.setupAddQuestion = function () {
                $scope.question = {};
            }

            var errorHandler = function (response) {
                console.log(response);
                $scope.error = {};
                if (response && response !== undefined && response.hasOwnProperty('errors')) {
                    for (var idx in response.errors) {
                        $scope.error[response.errors[idx]['field']] = {
                            value: response.errors[idx]['value'],
                            message: response.errors[idx]['message']
                        };
                    }
                }
                $scope.isWaiting = false;
            };
        }
    ]);
