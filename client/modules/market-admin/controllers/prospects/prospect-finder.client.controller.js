angular.module('market-admin').controller('ProspectFinderController', ['$scope', 'BootStrap', '$state',
    '$timeout', '$rootScope', 'Menus', '$http',
    '$filter', 'Subscription',
    function ($scope, BootStrap, $state, $timeout, $rootScope, Menus, $http, $filter, Subscription) {
        // This provides BootStrap context.
        $scope.authentication = BootStrap;

        var offset = 0;
        var max = 10;

        $scope.total = 0;
        $scope.prospectRequests = [];
        $scope.prospectRequest = {
            prospectType: 'prospect-tag-seller'
        };

        $scope.market = BootStrap.market;
        $scope.allServices = BootStrap.services;

        $scope.previousState = 'prospectFinder.all';

        $scope.notAllowed = false;

        $scope.formatDate = function (date) {
            return moment(date).format('MM/DD/YYYY hh:mm A');
        };

        //Setup Side Nav
        $scope.sideNav = Menus.getMenu('prospectFinderSideNav').items;

        $scope.initSubscriptions = function () {
            if ($scope.market.settings.addons && $scope.market.settings.addons['prospect_finder']) {

                Subscription.initSubscriptions({
                    marketId: 'probot',
                    planType: 'prospect_finder_subscription'
                });

            }
        };

        //Action to perform on url routing inside current scope
        $rootScope.$on('$stateChangeSuccess',
            function (event, toState, toParams, fromState, fromParams) {

                if (fromState.parent == "prospectFinder") {
                    $scope.previousState = fromState.name;
                }

                if ((toState.name.match(/\./g) || []).length > 1) {
                    Menus.selectMenuItemByLink('prospectFinderSideNav', toState.parent);
                } else {
                    Menus.selectMenuItemByLink('prospectFinderSideNav', toState.name);
                }
            });

        if ($state.current.name === undefined || $state.current.parent === undefined) {
            $state.transitionTo('prospectFinder.all');
        } else {
            Menus.selectMenuItemByLink('prospectFinderSideNav', $state.current.name);
        }


        $scope.submitSearchQuery = function () {

            $http.post($scope.authentication.api.baseUrl + '/users/' + $scope.authentication.user.id + '/prospects/finder', $scope.prospectRequest)
                .success(function (response) {
                    $scope.prospectRequest = {
                        prospectType: 'prospect-tag-seller'
                    };
                    $state.go('prospectFinder.all');
                }).error(function (response) {
                    console.log(response);
                    errorHandler(response);
                });

        };

        $scope.listSearchQuery = function () {


            $http.get($scope.authentication.api.baseUrl + '/users/' + $scope.authentication.user.id + '/prospects/finder')
                .success(function (response) {
                    $scope.prospectRequests = response.results;
                    if ($scope.prospectRequests.length == 0) {
                        $state.go('prospectFinder.search');
                    }
                }).error(function (response) {
                    console.log(response);
                    errorHandler(response);
                });

        };


        var errorHandler = function (response) {
            console.log(response);
            if (response == '') {
                $scope.request = {};
                $scope.notAllowed = true;
                return;
            }
            ;
            $scope.error = {};
            if (response.errors) {
                for (var idx in response.errors) {
                    $scope.error[response.errors[idx]['field']] = {
                        value: response.errors[idx]['value'],
                        message: response.errors[idx]['message']
                    };
                }
            }
            $scope.isWaiting = false;
        };

        var refreshSubscription = function () {
            if ($scope.market.hasOwnProperty('updateCount')) {
                $scope.currentSubscription = $scope.market.updateCount();
                $scope.market = BootStrap.market;
            }
        }

        var updateWidgetDisplayFlags = function () {
            if ($scope.market.settings.addons.prospect_finder
                && ($scope.authentication.user.permissions.indexOf('ROLE_MARKET_ADMIN') != -1)) {
                var userSubscription = $scope.market.getCurrentUserSubscription('probot', 'prospect_finder_subscription');

                if (!userSubscription) {
                    $scope.isSubscribed = false;
                } else {
                    if (userSubscription.leftCount <= 0) {
                        $scope.isUnderSubscriptionLimit = false;
                    }
                }

            }
        }

    }
]);
