angular.module('market-admin').controller('ProspectsController', ['$scope', 'BootStrap', '$state',
    '$timeout', '$rootScope', 'Menus', '$http',
    '$filter', 'localStorageService',
    function ($scope, BootStrap, $state, $timeout, $rootScope, Menus, $http, $filter, localStorageService) {
        // This provides BootStrap context.
        $scope.authentication = BootStrap;

        var offset = 0;
        var max = 10;

        $scope.listType = 0;
        $scope.total = 0;
        $scope.prospects = [];
        $scope.prospect = {};
        $scope.prospectRequest = {};
        $scope.queryText = {};

        $scope.market = BootStrap.market;
        $scope.allServices = BootStrap.services;

        $scope.previousState = 'prospects.all';

        $scope.notAllowed = false;

        $scope.csvFile = {};
        $scope.csvFile.src = "";
        $scope.prospectType = 'prospect-tag-seller';
        $scope.marketServices = $scope.market.settings.services;
        $scope.service = $scope.marketServices[0].name;

        $scope.formatDate = function (date) {
            return moment(date).format('MM/DD/YYYY hh:mm A');
        };

        //Setup Side Nav
        $scope.sideNav = Menus.getMenu('prospectFinderSideNav').items;

        //Action to perform on url routing inside current scope
        $rootScope.$on('$stateChangeSuccess',
            function (event, toState, toParams, fromState, fromParams) {

                if (fromState.parent == "prospects") {
                    $scope.previousState = fromState.name;
                }

                if ($state.current.name == 'prospects.all') {
                    offset = 0;
                }

                if ((toState.name.match(/\./g) || []).length > 1) {
                    Menus.selectMenuItemByLink('prospectFinderSideNav', toState.parent);
                } else {
                    Menus.selectMenuItemByLink('prospectFinderSideNav', toState.name);
                }
            });

        if ($state.current.name === undefined || $state.current.parent === undefined) {
            $state.transitionTo('prospects.all');
        } else {
            Menus.selectMenuItemByLink('prospectFinderSideNav', $state.current.name);
        }

        $scope.setupAddProspect = function () {
            $scope.prospect = {
                prospectType: 'prospect-tag-buyer'
            };
            $timeout(function () {
                $("#categories").selectize({
                    valueField: 'name',
                    labelField: 'description',
                    searchField: 'description',
                    delimiter: ',',
                    maxItems: '10',
                    options: $scope.allServices,
                    sortField: {
                        field: 'description',
                        direction: 'asc'
                    },
                    onChange: function (value) {
                        if (value == null) {
                            $scope.prospect.categories = [];
                        } else {
                            $scope.prospect.categories = value;
                        }
                    }

                });
            }, 5);
        };

        var setupEditProspect = function () {

            var characteristics = [];
            for (var idx in $scope.prospect.characteristics) {
                characteristics.push($scope.prospect.characteristics[idx]['name']);
            }

            $timeout(function () {
                $("#categories").selectize({
                    valueField: 'name',
                    labelField: 'description',
                    searchField: 'description',
                    delimiter: ',',
                    maxItems: '10',
                    items: characteristics,
                    options: $scope.allServices,
                    sortField: {
                        field: 'description',
                        direction: 'asc'
                    },
                    onChange: function (value) {
                        if (value == null) {
                            $scope.prospect.characteristics = [];
                        } else {
                            $scope.prospect.characteristics = value;
                        }
                    }

                });
            }, 5);
        }

        $scope.createOrUpdateProspect = function () {
            if (!$scope.prospect.hasOwnProperty('slug')) {
                $http.post($scope.authentication.api.baseUrl +
                '/users/' + $scope.authentication.user.id + '/prospects', $scope.prospect)
                    .success(function (response) {
                        console.log(response);
                        $scope.prospect = {};
                        $("#categories").val(null);
                    }).error(function (response) {
                        console.log(response);
                        errorHandler(response);
                    });
            } else {
                $http.put($scope.authentication.api.baseUrl +
                '/users/' + $scope.authentication.user.id + '/prospects/' + $scope.prospect.slug, $scope.prospect)
                    .success(function (response) {
                        console.log(response);
                        $scope.prospect = {};
                        $state.go('prospects.all');
                    }).error(function (response) {
                        console.log(response);
                        errorHandler(response);
                    });
            }
        };

        // Fetch Prospect List
        $scope.prospectList = function (data) {

            if (data && data.refresh) {
                $scope.total = 0;
                offset = 0;
                $scope.prospects = [];
                $scope.isEnd = false;
            }

            if ($scope.isEnd) {
                return;
            }

            $scope.isWaiting = true;
            $scope.success = "";


            var url = $scope.authentication.api.baseUrl + '/users/' + $scope.authentication.user.id + '/prospect-search';
            url += '?max=' + max + '&offset=' + offset;

            console.log(url);
            console.log($scope.queryText);

            if ($scope.queryText.text) {
                url += '&query=' + $scope.queryText.text;
            }

            if (data && data.originQuery) {
                url += '&groupId=' + data.originQuery;
            }

            console.log(url);

            $http.get(url)
                .success(function (response) {

                    $scope.prospects = $scope.prospects.concat(response.searchResults);
                    $scope.total = response.total;
                    offset += max;
                    $scope.isWaiting = false;
                    $scope.error = {};
                    //$scope.queryText = {};

                }).error(function (response) {
                    errorHandler(response);
                });
        }

        // Fetch Single Request with Deep Linking
        $scope.getProspect = function () {

            $scope.prospect = undefined;
            if ($scope.prospects.length > 0) {
                for (var idx in $scope.prospects) {
                    if ($scope.prospects[idx].slug == $state.params.probotId) {
                        $scope.prospect = $scope.prospects[idx];

                        setupEditProspect();
                        updateWidgetDisplayFlags();
                        Menus.selectMenuItemByLink('prospectFinderSideNav', $scope.previousState);
                        return;
                    }
                }
            } else {
                if ($scope.prospect === undefined) {
                    console.log($scope.authentication.api.baseUrl +
                    '/users/' + $scope.authentication.user.id + '/prospects/'
                    + $state.params.probotId);
                    $http.get($scope.authentication.api.baseUrl +
                        '/users/' + $scope.authentication.user.id + '/prospects/'
                        + $state.params.probotId
                    )
                        .success(function (response) {
                            $scope.prospect = response;


                            console.log($scope.prospect);

                            setupEditProspect();
                            updateWidgetDisplayFlags();
                            Menus.selectMenuItemByLink('prospectFinderSideNav', $scope.previousState);
                            $scope.isWaiting = false;
                            $scope.error = {};
                        }).error(function (response) {
                            console.log(response);
                            errorHandler(response);
                        });
                }
            }
        }


        $scope.uploadCsv = function () {

            console.log($scope.csvFile);

            var fd = new FormData();
            fd.append('csvFile', $scope.csvFile.src);
            fd.append('prospectType', $scope.prospectType);
            fd.append('service', $scope.service);
            fd.append('fileName', $scope.csvFile.src.name);

            //console.log(fd);

            $http.post($scope.authentication.api.baseUrl +
                '/users/' + $scope.authentication.user.id + '/prospects/upload',
                fd,
                {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined}
                })
                .success(function (response) {
                    console.log(response);
                    //$scope.csvFile = {};
                    //$scope.prospectType = "prospect-tag-seller";
                })
                .error(function () {
                    console.log(response);
                });
        };

        $scope.downloadCsv = function () {


        };

        $scope.deleteProspect = function () {


        };

        var errorHandler = function (response) {
            console.log(response);
            if (response == '') {
                $scope.request = {};
                $scope.notAllowed = true;
                return;
            }

            $scope.error = {};
            if (response.errors) {
                for (var idx in response.errors) {
                    $scope.error[response.errors[idx]['field']] = {
                        value: response.errors[idx]['value'],
                        message: response.errors[idx]['message']
                    };
                }
            }
            $scope.isWaiting = false;
        };

        var refreshSubscription = function () {
            if ($scope.market.hasOwnProperty('updateCount')) {
                $scope.currentSubscription = $scope.market.updateCount();
                $scope.market = BootStrap.market;
            }
        }

        var updateWidgetDisplayFlags = function () {
            if ($scope.market.settings.addons.prospect_finder
                && ($scope.authentication.user.permissions.indexOf('ROLE_SELLER') != -1)) {
                var userSubscription = $scope.market.getCurrentUserSubscription();

                if (!userSubscription) {
                    $scope.isSubscribed = false;
                } else {
                    if (userSubscription.leftCount <= 0) {
                        $scope.isUnderSubscriptionLimit = false;
                    }
                }

            }

            //$timeout(function () {
            //    angular.element('form[name="makeOffer"]').removeClass('hide');
            //}, 10);

        }

    }
]);
