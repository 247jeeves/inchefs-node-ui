angular.module('market-admin').config(['flowFactoryProvider',
    function (flowFactoryProvider) {

        //Using direct references here as we don't have access to Services here
        var api = window._api;
        var accessToken = window.user.accessToken;

        flowFactoryProvider.defaults = {
            target: api.baseUrl + "/image/upload",
            testChunks: false,
            permanentErrors: [404, 500, 501],
            headers: {'X-Auth-Token': accessToken},
            generateUniqueIdentifier: function (file) {
                var relativePath = file.relativePath || file.webkitRelativePath || file.fileName || file.name;
                return file.size + '-' + relativePath.replace(/[^0-9a-zA-Z._-]/img, '');
            }
        };
        //// You can also set default events:
        //flowFactoryProvider.on('catchAll', function (event) {
        //    console.log(event);
        //});
    }
]);

angular.module('market-admin').run(['$http', 'BootStrap', 'Menus', 'localStorageService',
    function ($http, BootStrap, Menus, localStorageService) {

        //$http.defaults.headers.common['Accept'] = 'application/json';
        $http.defaults.headers.common['Content-Type'] = 'text/plain';

        delete $http.defaults.headers.common['X-Requested-With'];

        if (BootStrap.user) {
            $http.defaults.headers.common['X-Auth-Token'] = BootStrap.user.accessToken;
            $http.defaults.headers.common['X-Market-ID'] = BootStrap.market.slug;
        }

        //Setup Top Menus
        Menus.addMenu('dashboardTop');
        Menus.addMenuItem('dashboardTop', 'User Dashboard', 'dashboard', 'href', null, true, null, ['ROLE_MARKET_ADMIN'], 0, 'fa-home');
        Menus.addMenuItem('dashboardTop', 'Bots', 'myBots', 'ui-sref', null, true, null, ['ROLE_MARKET_ADMIN'], 1);
	Menus.addMenuItem('dashboardTop', 'Market', 'myMarket', 'ui-sref', null, true, null, ['ROLE_MARKET_ADMIN'], 2);
        Menus.addMenuItem('dashboardTop', 'Pages', 'myPages.all', 'ui-sref', null, true, null, ['ROLE_MARKET_ADMIN'], 3);
        Menus.addMenuItem('dashboardTop', 'Questions', 'questions', 'ui-sref', null, true, null, ['ROLE_MARKET_ADMIN'], 4);
        Menus.addMenuItem('dashboardTop', 'Email Themes', 'emailThemes', 'ui-sref', null, true, null, ['ROLE_MARKET_ADMIN'], 5);
        Menus.addMenuItem('dashboardTop', 'Email Templates', 'emailTemplates', 'ui-sref', null, true, null, ['ROLE_MARKET_ADMIN'], 6);
        Menus.addMenuItem('dashboardTop', 'CRM', 'emailer', 'ui-sref', null, true, null, ['ROLE_MARKET_ADMIN'], 7);
        //TODO: Add the logic to check Market Admin's subscription

        Menus.addMenu('prospectFinderSideNav');
        if (BootStrap.market.settings.addons && BootStrap.market.settings.addons['prospect_finder']) {
            var currentSubscription = BootStrap.market.getCurrentUserSubscription('probot', 'prospect_finder_subscription');
            if ((currentSubscription && currentSubscription.leftCount > 0) || (BootStrap.user.permissions.indexOf('ROLE_ADMIN') != -1)) {
                //Menus.addMenuItem('dashboardTop', 'Prospect Finder™', 'prospectFinder.all', 'ui-sref', null, true, null, ['ROLE_MARKET_ADMIN'], 7);
                Menus.addMenuItem('prospectFinderSideNav', 'Prospect Queries', 'prospectFinder.all', 'ui-sref', null, false, null, ['ROLE_MARKET_ADMIN'], 0);
                Menus.addMenuItem('prospectFinderSideNav', 'New Prospect Search', 'prospectFinder.search', 'ui-sref', null, false, null, ['ROLE_MARKET_ADMIN'], 1);
                Menus.addMenuItem('prospectFinderSideNav', 'Prospects', 'prospects.all', 'ui-sref', null, false, null, ['ROLE_MARKET_ADMIN'], 2);
                Menus.addMenuItem('prospectFinderSideNav', 'Add Prospect', 'prospects.add', 'ui-sref', null, false, null, ['ROLE_MARKET_ADMIN'], 3);
                Menus.addMenuItem('prospectFinderSideNav', 'Upload Prospects', 'prospects.upload', 'ui-sref', null, false, null, ['ROLE_MARKET_ADMIN'], 4);
            } else {
                Menus.addMenuItem('dashboardTop', 'Prospect Finder™', 'prospectFinder.subscriptions', 'ui-sref', null, true, null, ['ROLE_MARKET_ADMIN'], 7);
                Menus.addMenuItem('prospectFinderSideNav', 'Prospect Queries', '#', 'href', null, false, null, ['ROLE_MARKET_ADMIN'], 0);
                Menus.addMenuItem('prospectFinderSideNav', 'New Prospect Search', '#', 'href', null, false, null, ['ROLE_MARKET_ADMIN'], 1);
                Menus.addMenuItem('prospectFinderSideNav', 'Prospects', '#', 'href', null, false, null, ['ROLE_MARKET_ADMIN'], 2);
                Menus.addMenuItem('prospectFinderSideNav', 'Add Prospect', '#', 'href', null, false, null, ['ROLE_MARKET_ADMIN'], 3);
                Menus.addMenuItem('prospectFinderSideNav', 'Upload Prospects', '#', 'href', null, false, null, ['ROLE_MARKET_ADMIN'], 4);
            }
            Menus.addMenuItem('prospectFinderSideNav', 'Subscriptions', 'prospectFinder.subscriptions', 'ui-sref', null, false, null, ['ROLE_MARKET_ADMIN'], 5);
        }
        Menus.addMenu('faqSideNav');
        if (BootStrap.market.settings.addons && BootStrap.market.settings.addons['faq']) {
                Menus.addMenuItem('dashboardTop', 'FAQ', 'faq.all', 'ui-sref', null, true, null, ['ROLE_MARKET_ADMIN'], 8);
                Menus.addMenuItem('faqSideNav', 'My FAQs', 'faq.all', 'ui-sref', null, false, null, ['ROLE_MARKET_ADMIN'], 0);
                Menus.addMenuItem('faqSideNav', 'New FAQ', 'faq.add', 'ui-sref', null, false, null, ['ROLE_MARKET_ADMIN'], 1);
        }
        //Setup My Market Menu
        Menus.addMenu('myMarketSideNav');
        Menus.addMenuItem('myMarketSideNav', 'General Settings', 'myMarket.settings', 'ui-sref', null, true, null, ['ROLE_MARKET_ADMIN'], 0);
        Menus.addMenuItem('myMarketSideNav', 'Add-ons', 'myMarket.addons', 'ui-sref', null, true, null, ['ROLE_MARKET_ADMIN'], 1);
        Menus.addMenuItem('myMarketSideNav', 'Branding', 'myMarket.branding', 'ui-sref', null, true, null, ['ROLE_MARKET_ADMIN'], 2);
        Menus.addMenuItem('myMarketSideNav', 'Partner Integration', 'myMarket.network', 'ui-sref', null, true, null, ['ROLE_MARKET_ADMIN'], 3);
        Menus.addMenuItem('myMarketSideNav', 'Requests', 'myMarket.requests', 'ui-sref', null, true, null, ['ROLE_MARKET_ADMIN'], 4);
        Menus.addMenuItem('myMarketSideNav', 'Chat Widget Settings', 'myMarket.chat-widget-settings', 'ui-sref', null, true, null, ['ROLE_MARKET_ADMIN'], 5);
        Menus.addMenuItem('myMarketSideNav', 'Billing', 'myMarket.billing', 'ui-sref', null, true, null, ['ROLE_MARKET_ADMIN'], 5);
        if(BootStrap.market.settings.addons && BootStrap.market.settings.addons['payment']){
            Menus.addMenuItem('myMarketSideNav', 'Payment', 'myMarket.payment', 'ui-sref', null, true, null, ['ROLE_MARKET_ADMIN'], 4);
        }

        //Menus.addMenuItem('marketsSideNav', 'Plans', 'market.plans', 'ui-sref', null, false, null, ['ROLE_MARKET_ADMIN'], 1);
        //Menus.addMenuItem('marketsSideNav', 'Logo and Description', 'market.logo', 'ui-sref', null, true, null, ['ROLE_MARKET_ADMIN'], 3);
        //Menus.addMenuItem('marketsSideNav', 'Miscellaneous', 'market.payment', 'ui-sref', null, true, null, ['ROLE_MARKET_ADMIN'], 4);

        //Email Templates Menu
        Menus.addMenu('emailTemplatesSideNav');
        Menus.addMenuItem('emailTemplatesSideNav', 'List', 'emailTemplates.all', 'ui-sref', null, true, null, ['ROLE_MARKET_ADMIN'], 0);
        Menus.addMenuItem('emailTemplatesSideNav', 'Add New', 'emailTemplates.add', 'ui-sref', null, false, null, ['ROLE_MARKET_ADMIN'], 1);

        //Email Themes Menu
        Menus.addMenu('emailThemesSideNav');
        Menus.addMenuItem('emailThemesSideNav', 'List', 'emailThemes.all', 'ui-sref', null, true, null, ['ROLE_MARKET_ADMIN'], 0);
        Menus.addMenuItem('emailThemesSideNav', 'Add New', 'emailThemes.add', 'ui-sref', null, false, null, ['ROLE_MARKET_ADMIN'], 1);

        //Questions Menu
        Menus.addMenu('questionsSideNav');
        Menus.addMenuItem('questionsSideNav', 'List', 'questions.all', 'ui-sref', null, true, null, ['ROLE_MARKET_ADMIN'], 0);
        Menus.addMenuItem('questionsSideNav', 'Add New', 'questions.add', 'ui-sref', null, false, null, ['ROLE_MARKET_ADMIN'], 1);

        //Market Users Menu
        Menus.addMenu('usersSideNav');
        Menus.addMenuItem('usersSideNav', 'List', 'users.all', 'ui-sref', null, true, null, ['ROLE_MARKET_ADMIN'], 0);
        Menus.addMenuItem('usersSideNav', 'Add New', 'users.add', 'ui-sref', null, false, null, ['ROLE_MARKET_ADMIN'], 1);

        Menus.addMenu('myPagesSideNav');
        Menus.addMenuItem('myPagesSideNav', 'List', 'myPages.all', 'ui-sref', null, true, null, ['ROLE_MARKET_ADMIN'], 0);

        if (BootStrap.user.permissions.indexOf('ROLE_ADMIN') != -1) {
            Menus.addMenuItem('dashboardTop', 'Markets', 'markets', 'ui-sref', null, true, null, ['ROLE_ADMIN'], 5);
            Menus.addMenu('marketsSideNav');
            Menus.addMenuItem('marketsSideNav', 'List', 'markets.all', 'ui-sref', null, true, null, ['ROLE_ADMIN'], 0);
            Menus.addMenuItem('marketsSideNav', 'Create New', 'markets.create', 'ui-sref', null, false, null, ['ROLE_ADMIN'], 1);

            //Add Pages only available to Admin
            Menus.addMenuItem('myPagesSideNav', 'Add New', 'myPages.add', 'ui-sref', null, false, null, ['ROLE_ADMIN'], 1);
        }

        Menus.addMenu('emailerSideNav');
        Menus.addMenuItem('emailerSideNav', 'List Customers', 'emailer.all', 'ui-sref', null, true, null, ['ROLE_MARKET_ADMIN'], 0);
        Menus.addMenuItem('emailerSideNav', 'Add New', 'emailer.add', 'ui-sref', null, false, null, ['ROLE_MARKET_ADMIN'], 1);
        Menus.addMenuItem('emailerSideNav', 'Zoho CRM', 'emailer.zohoCRM', 'ui-sref', null, false, null, ['ROLE_MARKET_ADMIN'], 2);
		Menus.addMenuItem('emailerSideNav', 'Salesforce CRM', 'salesforce.crm', 'ui-sref', null, false, null, ['ROLE_MARKET_ADMIN'], 3);
        Menus.addMenuItem('emailerSideNav', 'List Leads', 'emailer.listLeads', 'ui-sref', null, false, null, ['ROLE_MARKET_ADMIN'], 4);

    }
]);
