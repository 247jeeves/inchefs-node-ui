angular.module('market-admin').config(['$stateProvider', '$urlRouterProvider',
    function ($stateProvider, $urlRouterProvider) {
        // Redirect to home view when route not found
        $urlRouterProvider.otherwise('/my-market');

        // Home state routing
        $stateProvider.
            state('myBots', {
                url: '/my-bots',
                controller: 'MyBotsController',
                templateUrl: 'modules/market-admin/views/my-bots/index.client.view.html'
            }).
	    state('myMarket', {
                url: '/my-market',
                controller: 'MyMarketController',
                templateUrl: 'modules/market-admin/views/my-market/index.client.view.html'
            }).
            state('myMarket.settings', {
                url: '/settings',
                templateUrl: 'modules/market-admin/views/my-market/editOrView.my-market.client.view.html',
                parent: 'myMarket'

            }).
            state('myMarket.addons', {
                url: '/addons',
                templateUrl: 'modules/market-admin/views/my-market/addons.client.view.html',
                parent: 'myMarket'
            }).
            state('myMarket.branding', {
                url: '/branding',
                templateUrl: 'modules/market-admin/views/my-market/branding.client.view.html',
                parent: 'myMarket'
            }).
            state('myMarket.requests', {
                url: '/requests',
                templateUrl: 'modules/market-admin/views/my-market/requests.client.view.html',
                parent: 'myMarket'
            }).
            state('myMarket.chat-widget-settings', {
                url: '/chat-widget-settings',
                templateUrl: 'modules/market-admin/views/my-market/chatWidgetSettings.my-market.client.view.html',
                parent: 'myMarket'
            }).
            state('myMarket.billing', {
                url: '/billing',
                templateUrl: 'modules/market-admin/views/my-market/billing.my-market.client.view.html',
                parent: 'myMarket'
            }).
	    state('myMarket.network', {
                url: '/network',
                templateUrl: 'modules/market-admin/views/my-market/network.client.view.html',
                parent: 'myMarket'
            }).
            state('emailTemplates', {
                url: '/email/templates',
                controller: 'EmailTemplatesController',
                templateUrl: 'modules/market-admin/views/email-templates/index.client.view.html'
            }).
            state('emailTemplates.all', {
                url: '/all',
                templateUrl: 'modules/market-admin/views/email-templates/list.client.view.html',
                parent: 'emailTemplates'
            }).
            state('emailTemplates.add', {
                url: '/add',
                templateUrl: 'modules/market-admin/views/email-templates/add.client.view.html',
                parent: 'emailTemplates'
            }).
            state('emailTemplates.editOrView', {
                url: '/:emailTemplateId',
                templateUrl: 'modules/market-admin/views/email-templates/editOrView.email-template.client.view.html',
                parent: 'emailTemplates'
            }).
            state('emailThemes', {
                url: '/email/themes',
                controller: 'EmailThemesController',
                templateUrl: 'modules/market-admin/views/email-themes/index.client.view.html'
            }).
            state('emailThemes.all', {
                url: '/all',
                controller: 'EmailThemesController',
                templateUrl: 'modules/market-admin/views/email-themes/list.client.view.html',
                parent: 'emailThemes'
            }).
            state('emailThemes.add', {
                url: '/add',
                templateUrl: 'modules/market-admin/views/email-themes/add.client.view.html',
                parent: 'emailThemes'
            }).
            state('emailThemes.editOrView', {
                url: '/:emailThemeId',
                templateUrl: 'modules/market-admin/views/email-themes/editOrView.email-theme.client.view.html',
                parent: 'emailThemes'
            }).
            state('questions', {
                url: '/questions',
                controller: 'QuestionsController',
                templateUrl: 'modules/market-admin/views/questions/index.client.view.html'
            }).
            state('questions.all', {
                url: '/all',
                templateUrl: 'modules/market-admin/views/questions/list.client.view.html',
                parent: 'questions'
            }).
            state('questions.add', {
                url: '/add',
                templateUrl: 'modules/market-admin/views/questions/add.client.view.html',
                parent: 'questions'
            }).
            state('questions.editOrView', {
                url: '/:questionId',
                templateUrl: 'modules/market-admin/views/questions/editOrView.question.client.view.html',
                parent: 'questions'
            }).
            state('myPages', {
                url: '/pages',
                controller: 'MyPagesController',
                templateUrl: 'modules/market-admin/views/my-pages/index.client.view.html'
            }).
            state('myPages.all', {
                url: '/all',
                templateUrl: 'modules/market-admin/views/my-pages/list.client.view.html',
                parent: 'myPages'
            }).
            state('myPages.editOrView', {
                url: '/edit/:pageId',
                templateUrl: 'modules/market-admin/views/my-pages/editOrView.my-pages.client.view.html',
                parent: 'myPages'
            }).
            state('salesforce', {
                url: '/salesforce',
                templateUrl: 'modules/market-admin/views/crm/emailer/index.client.view.html',
                controller: 'SalesforceController'
            }).
	    state('salesforce.crm', {
                url: '/crm',
                templateUrl: 'modules/market-admin/views/crm/emailer/salesforce.client.view.html',
                parent: 'salesforce'
            }).
            state('emailer', {
                url: '/emailer',
                templateUrl: 'modules/market-admin/views/crm/emailer/index.client.view.html',
                controller: 'EmailerController'
            }).
            state('emailer.all', {
                url: '/all',
                templateUrl: 'modules/market-admin/views/crm/emailer/list.client.view.html',
                parent: 'emailer'
            }).
            state('emailer.add', {
                url: '/add',
                templateUrl: 'modules/market-admin/views/crm/emailer/add.client.view.html',
                parent: 'emailer'
            }).
            state('emailer.zohoCRM', {
                url: '/zohoCRM',
                templateUrl: 'modules/market-admin/views/crm/emailer/zoho.client.view.html',
                parent: 'emailer'
            }).
            state('emailer.editOrView', {
                url: '/edit/:emailCampaignId',
                templateUrl: 'modules/market-admin/views/crm/emailer/editOrView.emailer.client.view.html',
                parent: 'emailer'
            }).
            state('emailer.listLeads', {
                url: '/listLeads',
                templateUrl: 'modules/market-admin/views/crm/emailer/listLeads.client.view.html',
                parent: 'emailer'
            }).
            state('faq', {
                url: '/faq',
                controller: 'FaqController',
                templateUrl: 'modules/market-admin/views/faq/index.client.view.html'
            }).
            state('faq.all', {
                 url: '/all',
                 templateUrl: 'modules/market-admin/views/faq/all.client.view.html',
                 parent: 'faq'
            }).
            state('faq.add', {
                 url: '/add',
                 templateUrl: 'modules/market-admin/views/faq/add.client.view.html',
                 parent: 'faq'
            });


        var _market = window._market;

        if(_market.settings.addons && _market.settings.addons['payment']){
            $stateProvider.state('myMarket.payment', {
                url: '/payment',
                templateUrl: 'modules/market-admin/views/my-market/payment.client.view.html',
                parent: 'myMarket'
            });
        }


        var _user = window.user;

        if (_user && _user.permissions.indexOf('ROLE_ADMIN') != -1) {
            $stateProvider.
                state('markets', {
                    url: '/markets',
                    controller: 'MarketsController',
                    templateUrl: 'modules/market-admin/views/markets/index.client.view.html'
                }).
                state('markets.all', {
                    url: '/all',
                    parent: 'markets',
                    templateUrl: 'modules/market-admin/views/markets/list.client.view.html'
                }).
                state('markets.create', {
                    url: '/create',
                    parent: 'markets',
                    templateUrl: 'modules/market-admin/views/markets/create.market.client.view.html'
                }).
                state('customer-creaservice.create', {
                    url: '/createCScenter',
                    parent: 'markets',
                    templateUrl: 'modules/market-admin/views/markets/create.customer.service.client.view.html'
                }).
                state('markets.editOrView', {
                    url: '/edit/:marketId',
                    parent: 'markets',
                    templateUrl: 'modules/market-admin/views/markets/editOrView.market.client.view.html'
                }).
                // ADDING NEW PAGES, Only available to Admin
                state('myPages.add', {
                    url: '/add',
                    templateUrl: 'modules/market-admin/views/my-pages/add.client.view.html',
                    parent: 'myPages'
                });
        }

        //TODO: Add logic for market admin subscription
        var prospectSubscription = undefined;
        if (_profile) {
            prospectSubscription = _profile.subscriptions['probot'];
        }

        if (user.permissions.indexOf('ROLE_ADMIN') != -1) {
            prospectSubscription = {isActive: true};
        }


        $stateProvider.
            state('prospectFinder', {
                url: '/prospect-finder',
                controller: 'ProspectFinderController',
                templateUrl: 'modules/market-admin/views/prospect-finder/index.client.view.html'
            }).
            state('prospectFinder.subscriptions', {
                url: '/subscriptions',
                templateUrl: 'modules/users/views/subscriptions/subscriptions.client.view.html',
                parent: 'prospectFinder'
            });

        if (prospectSubscription && prospectSubscription.isActive) {
            /**
             * Prospects Routes
             */
            $stateProvider.
                state('prospects', {
                    url: '/prospects',
                    controller: 'ProspectsController',
                    templateUrl: 'modules/market-admin/views/prospects/index.client.view.html'
                }).
                state('prospects.all', {
                    url: '/all',
                    templateUrl: 'modules/market-admin/views/prospects/prospects.client.view.html',
                    parent: 'prospects'
                }).
                state('prospects.search', {
                    url: '/search',
                    templateUrl: 'modules/market-admin/views/prospects/search.client.view.html',
                    parent: 'prospects'
                }).
                state('prospects.request', {
                    url: '/request',
                    templateUrl: 'modules/market-admin/views/prospects/request.client.view.html',
                    parent: 'prospects'
                }).
                state('prospects.upload', {
                    url: '/upload',
                    templateUrl: 'modules/market-admin/views/prospects/upload.client.view.html',
                    parent: 'prospects'
                }).
                state('prospects.add', {
                    url: '/add',
                    templateUrl: 'modules/market-admin/views/prospects/add.prospect.client.view.html',
                    parent: 'prospects'
                }).
                state('prospects.editOrView', {
                    url: '/:probotId',
                    templateUrl: 'modules/market-admin/views/prospects/editOrView.prospect.client.view.html',
                    parent: 'prospects'
                }).
                state('prospectFinder.all', {
                    url: '/all',
                    templateUrl: 'modules/market-admin/views/prospect-finder/all.client.view.html',
                    parent: 'prospectFinder'
                }).
                state('prospectFinder.search', {
                    url: '/search',
                    templateUrl: 'modules/market-admin/views/prospect-finder/search.client.view.html',
                    parent: 'prospectFinder'
                });
        }
    }
]);
