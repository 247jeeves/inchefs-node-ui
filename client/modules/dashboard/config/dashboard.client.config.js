angular.module('dashboard').config(['flowFactoryProvider',
    function (flowFactoryProvider) {

        //Using direct references here as we don't have access to Services here
        var api = window._api;
        var accessToken = window.user.accessToken;
        var market = window._market.slug;

        flowFactoryProvider.defaults = {
            target: api.baseUrl + "/image/upload",
            testChunks: false,
            permanentErrors: [404, 500, 501],
            headers: {'X-Auth-Token': accessToken, 'X-Market-ID': market},
            generateUniqueIdentifier: function (file) {
                var relativePath = file.relativePath || file.webkitRelativePath || file.fileName || file.name;
                return file.size + '-' + relativePath.replace(/[^0-9a-zA-Z._-]/img, '');
            }
        };
        //// You can also set default events:
        //flowFactoryProvider.on('catchAll', function (event) {
        //    console.log(event);
        //});
    }
]);

angular.module('dashboard').run(['$http', 'BootStrap', 'Menus', 'localStorageService',
    function ($http, BootStrap, Menus, localStorageService) {

        $http.defaults.headers.common['Content-Type'] = 'text/plain';

        delete $http.defaults.headers.common['X-Requested-With'];

        if (BootStrap.user) {
            $http.defaults.headers.common['X-Auth-Token'] = BootStrap.user.accessToken;
            $http.defaults.headers.common['X-Market-ID'] = BootStrap.market.slug;
        }

        //Setup Top Menus
        Menus.addMenu('dashboardTop');

        //Menus.addMenuItem('dashboardTop', 'Home', '', 'href', null, true, null, ['ROLE_BUYER'], 0, 'fa-home');

        if (BootStrap.user.isBuyer()) {
            Menus.addMenuItem('dashboardTop', 'Requests', 'requests.byou', 'ui-sref', null, true, null, ['ROLE_BUYER'], 1);
        } else {
            Menus.addMenuItem('dashboardTop', 'Requests', 'requests.ayou', 'ui-sref', null, true, null, ['ROLE_BUYER'], 1);
        }

        //Menus.addMenuItem('dashboardTop', 'Orders', 'orders', 'ui-sref', null, false, null, ['ROLE_BUYER'], 2);
        Menus.addMenuItem('dashboardTop', 'Listings', 'listings.all', 'ui-sref', null, false, null, ['ROLE_SELLER'], 3);

        if(BootStrap.market.settings.addons && BootStrap.market.settings.addons['event_calendar']){
            Menus.addMenuItem('dashboardTop', 'Calendar', 'eventCalendar', 'ui-sref', null, false, null, ['ROLE_SELLER'], 6);
        }

        Menus.addMenuItem('dashboardTop', 'CRM', 'customers.all', 'ui-sref', null, false, null, ['ROLE_SELLER'], 8);

        Menus.addMenuItem('dashboardTop', 'Keystone', 'keystone', 'href', null, false, null, ['ROLE_ADMIN'], 7);
        Menus.addMenuItem('dashboardTop', 'Proxy Login', 'ppp', 'ui-sref', null, false, null, ['ROLE_ADMIN'], 8);
        Menus.addMenuItem('dashboardTop', 'Market >', 'market-admin', 'href', null, false, null, ['ROLE_MARKET_ADMIN'], 9);
        
	//Setup Profile Menus
        Menus.addMenu('profileSideNav');
        Menus.addMenuItem('profileSideNav', 'Basic Information', 'profile.basic', 'ui-sref', null, true, null, ['ROLE_BUYER'], 0);
        Menus.addMenuItem('profileSideNav', 'Photo', 'profile.photo', 'ui-sref', null, false, null, ['ROLE_BUYER'], 1);
        Menus.addMenuItem('profileSideNav', 'Trust and Verification', 'profile.trust', 'ui-sref', null, false, null, ['ROLE_BUYER'], 2);
        //Menus.addMenuItem('profileSideNav', 'Preferences', 'profile.preferences', 'ui-sref', null, false, null, ['ROLE_BUYER'], 3);
        Menus.addMenuItem('profileSideNav', 'Provider Profile', 'profile.business', 'ui-sref', null, false, null, ['ROLE_SELLER'], 4);
        Menus.addMenuItem('profileSideNav', 'Extended Profile', 'profile.extended', 'ui-sref', null, false, null, ['ROLE_SELLER'], 5);
        Menus.addMenuItem('profileSideNav', 'Promotion', 'profile.promotion', 'ui-sref', null, false, null, ['ROLE_SELLER'], 6);
        //Menus.addMenuItem('profileSideNav', 'Transaction Setup', 'profile.transaction', 'ui-sref', null, false, null, ['ROLE_SELLER'], 5);
        if (!BootStrap.user.isBuyer() && BootStrap.market.settings.addons && BootStrap.market.settings.addons['seller_subscription']) {
            var currentSubscription = BootStrap.market.getCurrentUserSubscription(BootStrap.market.slug, 'seller_subscription');
            var menuName = "Subscriptions ";
            if (currentSubscription) {
                menuName += "(" + currentSubscription.name + ")";
            }
            Menus.addMenuItem('profileSideNav', menuName, 'profile.subscriptions', 'ui-sref', null, false, null, ['ROLE_SELLER'], 7);
        }

        //Setup Review Menus
        Menus.addMenu('reviewsTopNav');
        Menus.addMenuItem('reviewsTopNav', 'Reviews about you', 'profile.reviews.ayou', 'ui-sref', null, true, null, ['ROLE_BUYER'], 0);
        Menus.addMenuItem('reviewsTopNav', 'Reviews by you', 'profile.reviews.byou', 'ui-sref', null, false, null, ['ROLE_BUYER'], 1);

        //Setup Account Menus
        Menus.addMenu('accountSideNav');
        Menus.addMenuItem('accountSideNav', 'Notifications', 'account.notifications', 'ui-sref', null, true, null, ['ROLE_BUYER'], 0);
        Menus.addMenuItem('accountSideNav', 'Security', 'account.security', 'ui-sref', null, false, null, ['ROLE_BUYER'], 3);


	var requestType = "Place Request"
	var requestWord = "Request"
	if(typeof BootStrap.market.settings.branding !== 'undefined' && BootStrap.market.settings.services.length == 1){
		if(typeof BootStrap.market.settings.branding.request_phrase1 !== 'undefined' && typeof BootStrap.market.settings.branding.request_phrase2 !== 'undefined'){
			if(BootStrap.market.settings.branding.request_phrase1 && BootStrap.market.settings.branding.request_phrase2){
				   requestType = BootStrap.market.settings.branding.request_phrase1 + " " + BootStrap.market.settings.branding.request_phrase2
     				    requestWord = BootStrap.market.settings.branding.request_phrase2
			}	
		}
	}

	//Setup Request Menus
        Menus.addMenu('requestsSideNav');
        Menus.addMenuItem('requestsSideNav', requestWord+' for You', 'requests.ayou', 'ui-sref', null, false, null, ['ROLE_SELLER'], 1);
        Menus.addMenuItem('requestsSideNav', requestWord+' by You', 'requests.byou', 'ui-sref', null, false, null, ['ROLE_BUYER'], 2);
        Menus.addMenuItem('requestsSideNav', requestType, 'place-request', 'href', null, false, null, ['ROLE_BUYER'], 3);


        //Setup Orders Menus
        Menus.addMenu('ordersSideNav');
        //Menus.addMenuItem('ordersSideNav', 'Orders for You', 'orders.ayou', 'ui-sref', null, false, null, ['ROLE_SELLER'], 0);
        //Menus.addMenuItem('ordersSideNav', 'Orders by You', 'orders.byou', 'ui-sref', null, false, null, ['ROLE_BUYER'], 1);


        //Setup Listings Menus
        Menus.addMenu('listingsSideNav');
        Menus.addMenuItem('listingsSideNav', 'Current Listings', 'listings.all', 'ui-sref', null, false, null, ['ROLE_SELLER'], 0);
        Menus.addMenuItem('listingsSideNav', 'Add new Listing', 'listings.add', 'ui-sref', null, false, null, ['ROLE_SELLER'], 1);

        //Setup Market Menus
        Menus.addMenu('marketsSideNav');
        Menus.addMenuItem('marketsSideNav', 'Current Markets', 'markets.all', 'ui-sref', null, false, null, ['ROLE_ADMIN'], 0);
        Menus.addMenuItem('marketsSideNav', 'Create new Market', 'markets.add', 'ui-sref', null, false, null, ['ROLE_ADMIN'], 1);

        Menus.addMenu('customersSideNav');
        Menus.addMenuItem('customersSideNav', 'List Customers', 'customers.all', 'ui-sref', null, false, null, ['ROLE_SELLER'], 0);
        Menus.addMenuItem('customersSideNav', 'Add Customer', 'customers.add', 'ui-sref', null, false, null, ['ROLE_SELLER'], 1);
        Menus.addMenuItem('customersSideNav', 'Emailer', 'customers.emailerAdd', 'ui-sref', null, false, null, ['ROLE_SELLER'], 2);
        //Menus.addMenuItem('customersSideNav', 'Zoho CRM', 'customers.zohoCRM', 'ui-sref', null, false, null, ['ROLE_SELLER'], 2);
        //Menus.addMenuItem('customersSideNav', 'DialogFlow', 'customers.dialogFlow', 'ui-sref', null, false, null, ['ROLE_SELLER'], 2);
    }
]);
