angular.module('dashboard').directive('flowImageResize', ['$q',function($q) {
    return {
        'require': 'flowInit',
        'link': function(scope, element, attrs) {

            var toWidth = 225, toHeight = 225;
            console.log(attrs.flowImageResize != '');
            if (typeof attrs.flowImageResize !== undefined && attrs.flowImageResize != '') {
                var resizeArgs = attrs.flowImageResize.split(',');
                toWidth = resizeArgs[0].split(':')[1] || 225;
                toHeight = resizeArgs[1].split(':')[1] || 225;
            }


            scope.$flow.opts.preprocess = function (chunk) {
                chunk.fileObj.promise.then(function () {
                    if (!chunk.fileObj.resized) {
                        chunk.fileObj.resized = true;
                        chunk.fileObj.retry();
                    }
                });
                if (chunk.fileObj.resized) {
                    chunk.preprocessFinished();
                }
            };
            scope.$flow.on('filesSubmitted', function (files) {
                angular.forEach(files, function (file) {
                    var nativeFile = file.file;// instance of File, same as here: https://github.com/flowjs/ng-flow/blob/master/src/directives/img.js#L13
                    file.file = null;// do not display it
                    var deferred = $q.defer();
                    file.promise = deferred.promise;

                    loadImage(
                        nativeFile,
                        function (canvas) {
                            canvas.toBlob(function (blob) {
                                file.file = blob;
                                file.size = blob.size;
                                deferred.resolve();
                                scope.$digest();
                            });
                        },
                        {
                            canvas: true,
                            crop: true,
                            contain: true,
                            maxWidth: toWidth,
                            maxHeight: toHeight,
                            top: 0
                        }
                    );
                });
            })
        }
    };
}]);

angular.module('dashboard').directive('resizedImg', [function() {
    return {
        //'scope': true,
        //'require': '^flowInit',
        'link': function(scope, element, attrs) {
            var file = attrs.resizedImg;
            scope.$watch(file + '.file', function (file) {
                if (!file) {
                    return ;
                }
                var fileReader = new FileReader();
                fileReader.readAsDataURL(file);
                fileReader.onload = function (event) {
                    scope.$apply(function () {
                        attrs.$set('src', event.target.result);
                    });
                };
            });
        }
    };
}]);

angular.module('dashboard').directive('moreDetails', [function() {
    return {
        scope: false,
        restrict: 'E',
        controller: 'EventsController',
        templateUrl: 'modules/dashboard/views/requests/more-details.client.view.html',
        link : function(scope, element, attrs, ctrl, transclude){
            console.log('directive Init');
            console.log(element);
            ctrl.getDetailQuestions();

        }
    };
}]);
