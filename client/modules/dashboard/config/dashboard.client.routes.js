angular.module('dashboard').config(['$stateProvider', '$urlRouterProvider',
    function ($stateProvider, $urlRouterProvider) {
        // Redirect to home view when route not found
        $urlRouterProvider.otherwise('/requests');

        // Home state routing
        $stateProvider.
            //state('dashboard', {
            //    url: '/dash',
            //    controller: 'DashboardController',
            //    templateUrl: 'modules/dashboard/views/dashboard/index.client.view.html'
            //}).
            state('ppp', {
                url: '/ppp-login',
                controller: 'ProxyController',
                templateUrl: 'modules/users/views/authentication/proxy.client.view.html'
            }).
        /**
         * Profile Routes
         */
            state('profile', {
                url: '/profile',
                controller: 'ProfileController',
                templateUrl: 'modules/dashboard/views/profile/index.client.view.html'
            }).
            state('profile.basic', {
                url: '/basic',
                controller: 'BasicController',
                templateUrl: 'modules/dashboard/views/profile/basic.client.view.html',
                parent: 'profile'
            }).
            state('profile.photo', {
                url: '/photo',
                controller: 'PhotoController',
                templateUrl: 'modules/dashboard/views/profile/photo.client.view.html',
                parent: 'profile'
            }).
            state('profile.trust', {
                url: '/trust',
                controller: 'TrustController',
                templateUrl: 'modules/dashboard/views/profile/trust.client.view.html',
                parent: 'profile'
            }).
            state('profile.preferences', {
                url: '/preferences',
                templateUrl: 'modules/dashboard/views/profile/preferences.client.view.html',
                parent: 'profile'
            }).
            state('profile.promotion', {
                url: '/promotion',
                controller: 'PromotionsController',
                templateUrl: 'modules/dashboard/views/profile/promotions.client.view.html',
                parent: 'profile'
            }).
            state('profile.reviews', {
                url: '/',
                controller: 'ReviewController',
                templateUrl: 'modules/dashboard/views/profile/reviews.client.view.html',
                parent: 'profile'
            }).
            state('profile.reviews.ayou', {
                url: '/ayou',
                templateUrl: 'modules/dashboard/views/profile/reviews.ayou.client.view.html',
                parent: 'profile.reviews'
            }).
            state('profile.reviews.byou', {
                url: '/byou',
                templateUrl: 'modules/dashboard/views/profile/reviews.byou.client.view.html',
                parent: 'profile.reviews'
            }).
            state('profile.business', {
                url: '/business',
                controller: 'BusinessController',
                templateUrl: 'modules/dashboard/views/profile/business.client.view.html',
                parent: 'profile'
            }).
            state('profile.extended', {
                url: '/extended',
                controller: 'BusinessController',
                templateUrl: 'modules/dashboard/views/profile/extended.client.view.html',
                parent: 'profile'
            }).
            state('profile.transaction', {
                url: '/transaction',
                controller: 'TransactionController',
                templateUrl: 'modules/dashboard/views/profile/transaction.client.view.html',
                parent: 'profile'
            }).
            state('profile.sellerIntro', {
                url: '/become-a-seller',
                templateUrl: 'modules/dashboard/views/profile/seller-intro.client.view.html',
                parent: 'profile'
            }).
            state('profile.sellerCreate', {
                url: '/create-business-profile',
                controller: 'BusinessController',
                templateUrl: 'modules/dashboard/views/profile/business.create.client.view.html',
                parent: 'profile'
            }).
            state('profile.sellerWelcome', {
                url: '/welcome',
                templateUrl: 'modules/dashboard/views/profile/business.welcome.client.view.html',
                parent: 'profile'
            }).
            state('profile.subscriptions', {
                url: '/subscriptions',
                templateUrl: 'modules/users/views/subscriptions/subscriptions.client.view.html',
                parent: 'profile'
            }).
        /**
         * Account Routes
         */
            state('account', {
                url: '/account',
                controller: 'AccountController',
                templateUrl: 'modules/dashboard/views/account/index.client.view.html'
            }).
            state('account.notifications', {
                url: '/notifications',
                controller: 'NotificationsController',
                templateUrl: 'modules/dashboard/views/account/notifications.client.view.html',
                parent: 'account'
            }).
            state('account.security', {
                url: '/security',
                controller: 'SecurityController',
                templateUrl: 'modules/dashboard/views/account/security.client.view.html',
                parent: 'account'
            }).
            state('account.payment', {
                url: '/security',
                templateUrl: 'modules/dashboard/views/account/payment.client.view.html',
                parent: 'account'
            }).
            state('account.privacy', {
                url: '/privacy',
                templateUrl: 'modules/dashboard/views/account/privacy.client.view.html',
                parent: 'account'
            }).
            state('account.settings', {
                url: '/settings',
                templateUrl: 'modules/dashboard/views/account/settings.client.view.html',
                parent: 'account'
            }).
        /**
         * Request Routes
         */
            state('requests', {
                url: '/requests',
                controller: 'RequestsController',
                templateUrl: 'modules/dashboard/views/requests/index.client.view.html'
            }).
            state('requests.ayou', {
                url: '/ayou',
                templateUrl: 'modules/dashboard/views/requests/requests.ayou.client.view.html',
                parent: 'requests'
            }).
            state('requests.byou', {
                url: '/byou',
                controller: 'EventsController',
                templateUrl: 'modules/dashboard/views/requests/requests.byou.client.view.html',
                parent: 'requests'
            }).
            state('requests.rdetail', {
                url: '/request/:id/:displayId',
                templateUrl: 'modules/dashboard/views/requests/requests.detail.client.view.html',
                parent: 'requests'
            }).
            state('requests.edetail', {
                url: '/event/:id',
                controller: 'EventsController',
                templateUrl: 'modules/dashboard/views/requests/events.detail.client.view.html',
                parent: 'requests'
            }).
            state('requests.odetail', {
                url: '/event/:id/:offerId/:serviceId',
                controller: 'EventsController',
                templateUrl: 'modules/dashboard/views/requests/offer.detail.client.view.html',
                parent: 'requests'
            }).
            state('requests.response', {
                url: '/request/:id/:displayId/response',
                //controller: 'RequestsController',
                templateUrl: 'modules/dashboard/views/requests/requests.response.client.view.html',
                parent: 'requests'
            }).
        /**
         * Order Routes
         */
            state('orders', {
                url: '/orders',
                controller: 'OrdersController',
                templateUrl: 'modules/dashboard/views/orders/index.client.view.html'
            }).
            //state('orders.ayou', {
            //    url: '/ayou',
            //    templateUrl: 'modules/dashboard/views/orders/orders.ayou.client.view.html',
            //    parent: 'orders'
            //}).
            //state('orders.byou', {
            //    url: '/byou',
            //    templateUrl: 'modules/dashboard/views/orders/orders.byou.client.view.html',
            //    parent: 'orders'
            //}).
            //state('orders.odetail', {
            //    url: '/:id',
            //    templateUrl: 'modules/dashboard/views/orders/orders.detail.client.view.html',
            //    parent: 'orders'
            //}).
            state('orders.pay', {
                url: '/pay/:id',
                templateUrl: 'modules/dashboard/views/orders/orders.pay.client.view.html',
                parent: 'orders'
            }).
            state('orders.timeSuccess', {
                url: '/time-success/:id',
                templateUrl: 'modules/dashboard/views/orders/orders.time.success.client.view.html',
                parent: 'orders'
            }).
            state('orders.paySuccess', {
                url: '/pay-success',
                templateUrl: 'modules/dashboard/views/orders/orders.pay.success.client.view.html',
                parent: 'orders'
            }).
            state('orders.paySubscription', {
                url: '/pay-success-subscription',
                templateUrl: 'modules/dashboard/views/orders/orders.pay.subscription.client.view.html',
                parent: 'orders'
            }).

        /**
         * Listing Routes
         */
            state('listings', {
                url: '/listings',
                controller: 'ListingsController as listingCtrl',
                templateUrl: 'modules/dashboard/views/listings/index.client.view.html'
            }).
            state('listings.all', {
                url: '/all',
                templateUrl: 'modules/dashboard/views/listings/listings.client.view.html',
                parent: 'listings'
            }).
            state('listings.add', {
                url: '/add',
                templateUrl: 'modules/dashboard/views/listings/add.listing.client.view.html',
                parent: 'listings'
            }).
            state('listings.editOrView', {
                url: '/:packageId',
                templateUrl: 'modules/dashboard/views/listings/editOrView.listing.client.view.html',
                parent: 'listings'
            }).

        /**
         * Event Calendar
         */
            state('eventCalendar', {
                url: '/my-event-calendar',
                controller: 'EventCalendarController',
                templateUrl: 'modules/dashboard/views/event-calendar/index.client.view.html'
            }).
        /**
         * Seller CRM
         */
            state('customers', {
                url: '/crm/customers',
                controller: 'CustomersController as customerCtrl',
                templateUrl: 'modules/dashboard/views/crm/customers/index.client.view.html'
            }).
            state('customers.all', {
                url: '/all',
                parent: 'customers',
                templateUrl: 'modules/dashboard/views/crm/customers/customers.client.view.html'
            }).
            state('customers.add', {
                url: '/add',
                parent: 'customers',
                templateUrl: 'modules/dashboard/views/crm/customers/add.customer.client.view.html'
            }).
            state('customers.editOrView', {
                url: '/customer/:customerId',
                parent: 'customers',
                templateUrl: 'modules/dashboard/views/crm/customers/editOrView.customer.client.view.html'
            }).

            state('customers.dialogFLow', {
                url: '/dialogFlow',
                templateUrl: 'modules/dashboard/views/crm/dialogFlow/dialogFlow.client.view.html',
                parent: 'customers',
                controller: 'DialogFlowController'
            }).
            state('customers.emailerAdd', {
                url: '/emailer',
                templateUrl: 'modules/dashboard/views/crm/emailer/add.client.view.html',
                parent: 'customers',
                controller: 'SellerEmailerController'
            }).
            state('customers.zohoCRM', {
                url: '/zohoCRM',
                templateUrl: 'modules/dashboard/views/crm/emailer/zoho.client.view.html',
                parent: 'customers',
                controller: 'SellerEmailerController'
            });

    }
]);
