angular.module('dashboard').controller('TrustController', ['$scope', 'BootStrap', '$state', '$timeout', '$http',
    function ($scope, BootStrap, $state, $timeout, $http) {
        // This provides BootStrap context.
        $scope.authentication = BootStrap;

        $scope.profile = $scope.authentication.profile;
        $scope.isWaiting = false;
        $scope.market = BootStrap.market;
        var image = "";
        var flowInst;

        $scope.isNotVerified = function(type){
            if(typeof $scope.profile.tags !== 'undefined'){
                for(idx in $scope.profile.tags.verify){
                    if($scope.profile.tags.verify[idx].name == 'verified-'+type){
                        return false;
                    }
                }
            }
            return true;
        }

        $scope.update = function () {
            $scope.isWaiting = true;
            $http.put($scope.authentication.api.baseUrl + "/users/"
                + $scope.authentication.user.id + "/profile/buyer/" + $scope.authentication.profile.buyerProfile.id,
                {
                    image: image
                })
                .success(function (response) {
                    console.log(response);
                    console.log($scope.profile);
                    $scope.profile.image = response.image;
                    $scope.success = "Saved!";
                    $timeout(function () {
                        $scope.success = "";
                    }, 5000);
                    $scope.isWaiting = false;
                    $scope.error = {};
                    console.log($scope.profile);

                }).error(function (response) {
                    console.log(response);
                    $scope.error = {};
                    for (var idx in response.errors) {
                        $scope.error[response.errors[idx]['field']] = {
                            value: response.errors[idx]['value'],
                            message: response.errors[idx]['message']
                        };
                    }
                    $scope.isWaiting = false;
                });
        };

        $scope.uploadSuccess = function($flow, $file, $message){
            $scope.isWaiting = true;
            flowInst = $flow;
            image = $file.uniqueIdentifier;
            console.log($flow);
            $scope.update();
        }
    }
]);
