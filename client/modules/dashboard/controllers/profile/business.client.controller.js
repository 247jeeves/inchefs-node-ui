angular.module('dashboard').controller('BusinessController', ['$scope', 'BootStrap', '$state', '$timeout', '$http', 'Answers',
    function ($scope, BootStrap, $state, $timeout, $http, Answers) {
        // This provides BootStrap context.
        $scope.authentication = BootStrap;

        $scope.profile = $scope.authentication.profile.sellerProfile || {};


        $scope.formName = 'businessProfile';
        $scope.isWaiting = false;
        $scope.answers = {};
        $scope.questions = [];

        $scope.save = function () {
            var l = window.location;
            $scope.isWaiting = true;
            $scope.success = '';
            $http.post($scope.authentication.api.baseUrl + '/users/'
                + $scope.authentication.user.id + '/profile/seller',
                $scope.profile)
                .success(function (response) {

                    $scope.success = 'Please Wait...';
                    $timeout(function () {
                        $scope.success = '';
                    }, 5000);
                    $scope.isWaiting = false;
                    $scope.error = {};

                    var reqUrl = encodeURIComponent(l.href);
                    var redrUrl = encodeURIComponent(l.origin + l.pathname + '#!/profile/welcome');

                    location.href = l.origin + '/' + BootStrap.market.slug + '/rs?from=' + reqUrl + '&to=' + redrUrl;

                }).error(function (response) {
                    $scope.isWaiting = false;
                    $scope.error = {};
                    for (var idx in response.errors) {
                        $scope.error[response.errors[idx]['field']] = {
                            value: response.errors[idx]['value'],
                            message: response.errors[idx]['message']
                        };
                        angular.element('#' + response.errors[idx]['field']).focus();
                    }
                });
        };

        $scope.update = function (isNewMarket) {
            var l = window.location;
            $scope.isWaiting = true;
            $scope.success = '';

            if ($state.current.name == 'profile.extended') {
                if (!processAnswers()) {
                    $scope.isWaiting = false;
                    return;
                }
                $scope.profile.propertyList = processAnswers();
            }

            $http.put($scope.authentication.api.baseUrl + '/users/'
                + $scope.authentication.user.id + '/profile/seller/' + $scope.authentication.profile.sellerProfile.id,
                $scope.profile)
                .success(function (response) {

                    $scope.success = 'Saved!';
                    $timeout(function () {
                        $scope.success = '';
                    }, 5000);
                    $scope.isWaiting = false;
                    $scope.error = {};

                    $scope.authentication.profile.sellerProfile = response;

                    if ($scope.authentication.user.isBuyer()) {
                        var reqUrl = encodeURIComponent(l.href);
                        var redrUrl = encodeURIComponent(l.origin + l.pathname + '#!/profile/welcome');
                        location.href = l.origin + '/' + BootStrap.market.slug + '/rs?from=' + reqUrl + '&to=' + redrUrl;
                    }
                }).error(function (response) {
                    $scope.isWaiting = false;
                    $scope.error = {};
                    for (var idx in response.errors) {
                        $scope.error[response.errors[idx]['field']] = {
                            value: response.errors[idx]['value'],
                            message: response.errors[idx]['message']
                        };
                        angular.element('#' + response.errors[idx]['field']).focus();
                    }
                });
        };

        $scope.getQuestions = function () {

            $http.get($scope.authentication.api.publicUrl + "/services/questions/?"
                + "category=seller-questions&categoryType=domain-questions"
            )
                .success(function (response) {
                    $scope.questions = response.results;
                    if (response.totalCount == 0) {
                        $scope.noQuestions = true;
                    }
                    $timeout(function () {
                        // Blocking selection of same day
                        $("input[data-datetime-bind]").datetimepicker({
                            minDate: (new Date()).setDate((new Date()).getDate() + 1),
                            collapse: true,
                            useCurrent: false
                        }).on('dp.change', function (e) {
                            var currentField = $(e.target);
                            currentField = currentField[0];
                            var questionId = currentField.id.match(/([0-9].*)/)[0];
                            $scope.answers[questionId] = $('#' + currentField.id).val();
                        });
                    }, 100);

                    $scope.isWaiting = false;
                    $scope.error = {};

                    $scope.answers = Answers.setupAnswers($scope.questions, $scope.profile.propertyList);

                }).error(function (response) {
                    console.log(response);
                    $scope.isWaiting = false;
                });
        };

        var processAnswers = function () {
            if (!$scope['businessProfile'].$valid) {
                $scope.setErrors = true;
                return false;
            }

            return Answers.processAnswers(
                JSON.parse(JSON.stringify($scope.answers))
            );
        }

    }
]);
