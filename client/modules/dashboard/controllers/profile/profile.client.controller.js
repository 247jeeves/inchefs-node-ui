angular.module('dashboard').controller('ProfileController', ['$scope', 'BootStrap', '$state', '$timeout', '$rootScope', 'Menus', 'Subscription',
    function ($scope, BootStrap, $state, $timeout, $rootScope, Menus, Subscription) {
        // This provides BootStrap context.
        $scope.authentication = BootStrap;

        $scope.profile = $scope.authentication.profile;
        $scope.market = BootStrap.market;

        //Setup Side Nav
        $scope.sidenav = Menus.getMenu('profileSideNav').items;

        //Action to perform on url routing inside current scope
        $rootScope.$on('$stateChangeSuccess',
            function (event, toState, toParams, fromState, fromParams) {
                if ((toState.name.match(/\./g) || []).length > 1) {
                    Menus.selectMenuItemByLink('profileSideNav', toState.parent);
                } else {
                    Menus.selectMenuItemByLink('profileSideNav', toState.name);
                }
            });

        if ($state.current.name === undefined || $state.current.parent === undefined) {
            $state.transitionTo('profile.basic');
        } else {
            Menus.selectMenuItemByLink('profileSideNav', $state.current.name);
        }

        Menus.selectMenuItemByLink('dashboardTop', 'profile.basic');

        $scope.initSubscriptions = function () {
            //console.log('init subscriptions');
            if ($scope.market.settings.addons && $scope.market.settings.addons['seller_subscription']) {
                //console.log('Setup seller Subs');

                Subscription.initSubscriptions({
                    marketId: $scope.market.slug,
                    planType: 'seller_subscription'
                });

            }
        }

    }
]);
