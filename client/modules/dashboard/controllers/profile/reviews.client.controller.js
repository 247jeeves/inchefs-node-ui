angular.module('dashboard').controller('ReviewController', ['$scope', 'BootStrap', '$state', '$timeout', '$rootScope', 'Menus',
    function ($scope, BootStrap, $state, $timeout, $rootScope, Menus) {
        // This provides BootStrap context.
        $scope.authentication = BootStrap;

        //Setup Side Nav
        $scope.reviewsTopNav = Menus.getMenu('reviewsTopNav').items;

        //Action to perform on url routing inside current scope
        $rootScope.$on('$stateChangeSuccess',
            function (event, toState, toParams, fromState, fromParams) {
                Menus.selectMenuItemByLink('reviewsTopNav', toState.name);
            });

        if ($state.current.name === undefined || $state.current.parent !== 'profile.reviews') {
            $state.transitionTo('profile.reviews.ayou');
        }

    }
]);
