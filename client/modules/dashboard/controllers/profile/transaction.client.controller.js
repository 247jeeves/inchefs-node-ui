angular.module('dashboard').controller('TransactionController', ['$scope', 'BootStrap', '$state', '$timeout', '$http',
    function ($scope, BootStrap, $state, $timeout, $http) {
        // This provides BootStrap context.
        $scope.authentication = BootStrap;

        $scope.profile = $scope.authentication.profile.sellerProfile;

        $scope.isWaiting = false;

        $scope.update = function () {
            $scope.isWaiting = true;
            $scope.success = "";
            $http.put($scope.authentication.api.baseUrl + "/users/"
                + $scope.authentication.user.id + "/profile/seller/" + $scope.authentication.profile.sellerProfile.id,
                $scope.profile)
                .success(function (response) {

                    $scope.success = "Saved!";
                    $timeout(function () {
                        $scope.success = "";
                    }, 5000);
                    $scope.isWaiting = false;
                    $scope.error = {};

                }).error(function (response) {
                    $scope.error = {};
                    for (var idx in response.errors) {
                        $scope.error[response.errors[idx]['field']] = {
                            value: response.errors[idx]['value'],
                            message: response.errors[idx]['message']
                        };
                    }
                    $scope.isWaiting = false;
                });
        };
    }
]);
