angular.module('dashboard').controller('BasicController', ['$scope', 'BootStrap', '$state', '$timeout', '$http',
    function ($scope, BootStrap, $state, $timeout, $http) {
        // This provides BootStrap context.
        $scope.authentication = BootStrap;

        $scope.profile = $scope.authentication.profile;
        $scope.isWaiting = false;

        $scope.update = function () {
            $scope.isWaiting = true;
            $http.put($scope.authentication.api.baseUrl + "/users/"
                + $scope.authentication.user.id + "/profile/buyer/" + $scope.authentication.profile.buyerProfile.id,
                {
                    firstName: $scope.profile.firstName,
                    lastName: $scope.profile.lastName,
                    address: $scope.profile.address
                })
                .success(function (response) {

                    $scope.success = "Saved!";
                    $timeout(function () {
                        $scope.success = "";
                    }, 5000);
                    $scope.isWaiting = false;
                    $scope.error = {};

                }).error(function (response) {
                    console.log(response);
                    $scope.error = {};
                    for (var idx in response.errors) {
                        $scope.error[response.errors[idx]['field']] = {
                            value: response.errors[idx]['value'],
                            message: response.errors[idx]['message']
                        };
                    }
                    $scope.isWaiting = false;
                });
        };
    }
]);
