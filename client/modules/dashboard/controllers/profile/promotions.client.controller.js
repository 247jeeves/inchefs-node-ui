angular.module('dashboard').controller('PromotionsController',
    ['$scope', 'BootStrap', '$state', '$timeout', '$rootScope', 'Menus', '$http', '$sce',
        function ($scope, BootStrap, $state, $timeout, $rootScope, Menus, $http, $sce) {
            // This provides BootStrap context.
            $scope.authentication = BootStrap;

            var market = $scope.market = BootStrap.market;

            $scope.profile = $scope.authentication.profile;
            //$scope.services = $scope.authentication.services;
            $scope.services = market.settings.services;
            $scope.promotions = [];
            $scope.promotion = {
                title: '',
                content: ''
            };
            $scope.error = {};
            $scope.formattedContent = '';

            //var prefix = '<div class="panel" style="background: #ffffcc">';
            //var suffix = '</div>';

            var prefix = '';
            var suffix = '';

            var watchChanges = function () {
                $scope.formattedContent = $sce.trustAsHtml(
                    prefix + '<h4>' + $scope.promotion.title + '</h4><p class="no-space">'
                    + $scope.promotion.content + '</p>' + suffix
                );
            };

            $scope.$watch('promotion.title', watchChanges);
            $scope.$watch('promotion.content', watchChanges);

            $scope.isWaiting = false;

            $scope.getPromotions = function () {
                $scope.isWaiting = true;
                $scope.promotions = [];
                $http.get($scope.authentication.api.baseUrl + "/users/"
                    + $scope.authentication.user.id + "/promotions"
                )
                    .success(function (response) {
                        $scope.promotions = response.results;
                        if (response.totalCount > 0) {
                            $scope.promotion = $scope.promotions[0];
                        }
                        $scope.isWaiting = false;
                    })
                    .error(function (response) {
                        errorHandler(response);
                        $scope.isWaiting = false;
                    });
            };

            $scope.createOrUpdatePromotion = function () {
                $scope.isWaiting = true;
                var data = JSON.parse(JSON.stringify($scope.promotion));

                if (!$scope.promotion.hasOwnProperty('id')) {
                    $http.post($scope.authentication.api.baseUrl + "/users/"
                        + $scope.authentication.user.id + "/promotions",
                        data)
                        .success(function (response) {
                            console.log(response);
                            $scope.promotion = response;
                            $scope.promotions.push($scope.promotion);
                            $scope.isWaiting = false;
                            $scope.error = {};
                        })
                        .error(function (response) {
                            errorHandler(response);
                            $scope.isWaiting = false;
                        });
                } else {
                    $http.put($scope.authentication.api.baseUrl + "/users/"
                        + $scope.authentication.user.id + "/promotions/"
                        + $scope.promotion.id,
                        data)
                        .success(function (response) {
                            $scope.promotion = response;
                            $scope.isWaiting = false;
                            $scope.error = {};
                        })
                        .error(function (response) {
                            errorHandler(response);
                            $scope.isWaiting = false;
                        });
                }
            };

            $scope.deletePromotion = function () {
                $("#deleteListing").find('[data-cancel-button]').on('click', function (e) {
                    $("#deleteListing").foundation('reveal', 'close');
                    e.preventDefault();
                });

                $("#deleteListing").find('[data-confirm-button]').on('click', function (e) {
                    $("#deleteListing").foundation('reveal', 'close');

                    $http.delete($scope.authentication.api.baseUrl + "/users/"
                    + $scope.authentication.user.id + "/promotions/"
                    + $scope.promotion.id)
                        .success(function (response) {
                            $scope.promotion = {};
                            $scope.promotions = [];
                            $scope.isWaiting = false;
                            //TODO: Show a success message
                        })
                        .error(function (response) {
                            //TODO: Show an error message
                            $scope.isWaiting = false;
                        });

                    e.preventDefault();
                });
                $timeout(function () {
                    $("#deleteListing").foundation('reveal', 'open');
                }, 10);
            };


            var errorHandler = function (response) {
                console.log(response);
                $scope.error = {};
                if (response && response !== undefined && response.hasOwnProperty('errors')) {
                    for (var idx in response.errors) {
                        $scope.error[response.errors[idx]['field']] = {
                            value: response.errors[idx]['value'],
                            message: response.errors[idx]['message']
                        };
                    }
                }
                $scope.isWaiting = false;
            };
        }
    ]);
