angular.module('dashboard').controller('MessagePhotoController', ['$scope', 'BootStrap', '$state', '$timeout', '$http',
    function ($scope, BootStrap, $state, $timeout, $http) {
        // This provides BootStrap context.
        $scope.authentication = BootStrap;

        $scope.profile = $scope.authentication.profile;
        $scope.messageId = $scope.message;
        $scope.isWaiting = false;
        var image = "";
        var flowInst;

        $scope.update = function () {
            $scope.isWaiting = true;
            $http.put($scope.authentication.api.baseUrl + "/users/"
                + $scope.authentication.user.id + "/message/" + messageId,
                {
                    image: image
                })
                .success(function (response) {
                    $scope.message.image = response.image;
                    $scope.success = "Saved!";
                    $timeout(function () {
                        $scope.success = "";
                    }, 5000);
                    $scope.isWaiting = false;
                    $scope.error = {};

                }).error(function (response) {
                    console.log(response);
                    $scope.error = {};
                    for (var idx in response.errors) {
                        $scope.error[response.errors[idx]['field']] = {
                            value: response.errors[idx]['value'],
                            message: response.errors[idx]['message']
                        };
                    }
                    $scope.isWaiting = false;
                });
        };

        $scope.uploadSuccess = function($flow, $file, $message){
            $scope.isWaiting = true;
            flowInst = $flow;
            image = $file.uniqueIdentifier;
            console.log($flow);
            $scope.update();
        }
    }
]);
