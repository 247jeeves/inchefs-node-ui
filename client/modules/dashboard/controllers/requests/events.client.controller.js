angular.module('dashboard').controller('EventsController', ['$scope', 'BootStrap', '$state', '$timeout', '$rootScope', 'Menus', '$http', 'Answers',
    function ($scope, BootStrap, $state, $timeout, $rootScope, Menus, $http, Answers) {
        // This provides BootStrap context.
        $scope.authentication = BootStrap;

        var offset = 0;
        var max = 10;

        $scope.listType = 0;
        $scope.total = 0;
        $scope.events = [];
        $scope.offer = {};
        $scope.answers = {};
        $scope.questions = [];

        $scope.previousState = 'requests.byou';
        $scope.requestType = "Place Request";
        $scope.requestWord = "Request"
        if(typeof $scope.market.settings.branding !== 'undefined' && $scope.market.settings.services.length == 1){
            if(typeof $scope.market.settings.branding.request_phrase1 !== 'undefined' && typeof $scope.market.settings.branding.request_phrase2 !== 'undefined'){
                if($scope.market.settings.branding.request_phrase1 && $scope.market.settings.branding.request_phrase2){
                    $scope.requestType = $scope.market.settings.branding.request_phrase1 + " " + $scope.market.settings.branding.request_phrase2
                    $scope.requestWord = $scope.market.settings.branding.request_phrase2
                }	
            }
        }	
        $scope.cancelReason = '';
        $scope.formName = 'buyerRequest';

        $scope.market = BootStrap.market;
        $scope.selectedOffers = {};
        $scope.selectedOffersDisplay = [];
        $scope.showConfirm = false;

        var globals = JSON.parse($("meta[name='_globals']").attr('content'));
        var wanteetPercentage = globals.WANTEET_FEE_PERCENTAGE || 0.00;
        var taxPercentage = globals.TAX_FEE_PERCENTAGE || 0.00;
        var paymentFeePercentage = globals.PAYMENT_FEE_PERCENTAGE || 0.00;

        $scope.taxes = function (offerPrice) {
            if (isNaN(offerPrice)) {
                return 0;
            }
            offerPrice = parseFloat(offerPrice);
            var wanteetFee = offerPrice * parseFloat(wanteetPercentage) / 100;
            var priceInclTax = wanteetFee + offerPrice + (offerPrice * parseFloat(taxPercentage) / 100);
            var paymentFee = priceInclTax * parseFloat(paymentFeePercentage) / 100;

            return ((priceInclTax + paymentFee) - offerPrice);
        };

        $scope.formatDate = function (date) {
            return moment(date).format('MM/DD/YYYY hh:mm A');
        };

        $scope.selectOffer = function (id) {

            if (typeof $scope.selectedOffers[id] === 'undefined') {
                $scope.selectedOffers[id] = true;
            } else {
                $scope.selectedOffers[id] = !$scope.selectedOffers[id];
            }

            if ($scope.selectedOffers[id]) {
                $scope.selectedOffersDisplay.push(this.offer);
            } else {
                $scope.selectedOffersDisplay.splice($scope.selectedOffersDisplay.indexOf(this.offer), 1);
            }

            //console.log($scope.selectedOffersDisplay);

            for (oIdx in Object.keys($scope.selectedOffers)) {
                var keys = Object.keys($scope.selectedOffers);
                if ($scope.selectedOffers[keys[oIdx]]) {
                    $scope.showConfirm = true;
                    break;
                }
                $scope.showConfirm = false;
            }

        };


        $scope.$watch('showConfirm', function (newValue, oldValue) {

            $timeout(function () {
                if (newValue) {
                    $('.floating-bottom').width($('.floating-bottom').parent().width());
                }
            }, 1);

        }, true);


        if (BootStrap.user.isBuyer()) {
            Menus.selectMenuItemByLink('dashboardTop', 'requests.byou');
        } else {
            Menus.selectMenuItemByLink('dashboardTop', 'requests.ayou');
        }

        //Setup Side Nav
        $scope.requestsSideNav = Menus.getMenu('requestsSideNav').items;

        //Action to perform on url routing inside current scope
        $rootScope.$on('$stateChangeSuccess',
            function (event, toState, toParams, fromState, fromParams) {

                if (fromState.parent == "requests") {
                    $scope.previousState = fromState.name;
                }

                if ((toState.name.match(/\./g) || []).length > 1) {
                    Menus.selectMenuItemByLink('requestsSideNav', toState.parent);
                } else {
                    Menus.selectMenuItemByLink('requestsSideNav', toState.name);
                }
            });

        if ($state.current.name === undefined || $state.current.parent === undefined) {
            if ($scope.authentication.user.isBuyer()) {
                $state.transitionTo('requests.byou');
            }
            //} else {
            //    $state.transitionTo('requests.ayou');
            //}
        } else {
            Menus.selectMenuItemByLink('requestsSideNav', $state.current.name);
        }

        // Fetch Request List
        $scope.eventList = function (type) {
            $scope.isWaiting = true;
            $scope.success = "";

                //console.log(" event list b4 comapring "+$scope.listType +":"+type);
            if ($scope.listType != type) {
                //console.log(" event list "+$scope.listType +":"+type);
                offset = 0;
                $scope.listType = type || 0;
                $scope.events = [];
            }

            $scope.event = undefined;
            $scope.offer = {};

            $http.get($scope.authentication.api.baseUrl + "/users/"
                + $scope.authentication.user.id + "/events"
                + "?max=" + max + "&offset=" + offset
                + "&type=" + $scope.listType + "&market=" + $scope.market.slug
            )
                .success(function (response) {
                    $scope.total = response.totalCount;
                    $scope.events = $scope.events.concat(response.results);
                    offset += max;
                    $scope.isWaiting = false;
                    $scope.error = {};

                }).error(function (response) {
                    errorHandler(response);
                });
        }

        // Fetch Single Request
        $scope.getEvent = function () {
            $scope.previousState = 'requests.byou';
            $scope.event = undefined;
            if ($scope.events.length > 0) {
                for (var idx in $scope.events) {
                    if ($scope.events[idx].id == $state.params.id) {
                        $scope.event = $scope.events[idx];

                        setupEvent();
                        return;
                    }
                }
            } else {
                if ($scope.event === undefined) {
                    $http.get($scope.authentication.api.baseUrl + "/users/"
                        + $scope.authentication.user.id + "/events/"
                        + $state.params.id
                    )
                        .success(function (response) {
                            $scope.event = response;
                            //console.log(response);
                            setupEvent();

                            $scope.isWaiting = false;
                            $scope.error = {};
                        }).error(function (response) {
                            errorHandler(response);
                        });
                }
            }
        }

        $scope.getOffer = function () {
            $scope.offer = {};
            $scope.offer = this.offer;
            initMessages(function () {
                $("#showMessages").foundation('reveal', 'open');
            });

        };

        // Make/Modify an Offer
        $scope.acceptOffer = function () {

            $("#acceptOfferDialog")
                .find('[data-cancel-button]').on('click', function (e) {
                    $("#acceptOfferDialog").foundation('reveal', 'close');
                    e.preventDefault();
                });

            $("#acceptOfferDialog").find('[data-confirm-button]').on('click', function (e) {
                $("#acceptOfferDialog").foundation('reveal', 'close');
                doAcceptOffer();
                e.preventDefault();
            });


            $timeout(function () {
                $("#acceptOfferDialog").foundation('reveal', 'open');
            }, 10);
            return false;
        }

        var doAcceptOffer = $scope.doAcceptOffer = function () {
            $scope.isWaiting = true;
            $scope.success = "";

            var offerIds = $scope.selectedOffersDisplay.map(function (obj) {
                return obj.id;
            });

            offerIds = offerIds.join(',');

            $http.patch($scope.authentication.api.baseUrl + "/users/"
            + $scope.authentication.user.id + "/requests/"
            + $scope.service.id + "/offers/"
            + offerIds)
                .success(function (response) {

                    $scope.event.services[0].offers = response;
                    setupEvent();
                    $scope.selectedOffersDisplay = [];
                    $scope.selectedOffers = {};
                    $scope.showConfirm = false;
                })
                .error(function (response) {
                    errorHandler(response);
                });
        };

        $scope.payForOffer = function (orderId) {
            //console.log(orderId);
            $state.go('orders.pay', {"id": orderId});
        };

        var errorHandler = function (response) {
            $scope.error = {};
            for (var idx in response.errors) {
                $scope.error[response.errors[idx]['field']] = {
                    value: response.errors[idx]['value'],
                    message: response.errors[idx]['message']
                };
            }
            $scope.isWaiting = false;
        }

        var setupEvent = function () {
            Menus.selectMenuItemByLink('requestsSideNav', $scope.previousState);
            for (var idx in $scope.event.services) {
                $scope.service = $scope.event.services[idx];
                $scope.formName = $scope.service.service.replace(/ /g, '-');
                var offers = $scope.event.services[idx].offers;
                for (var kdx in offers) {
                    if (offers[kdx]['type'] == 'price') {
                        $scope.event.services[idx].offers[kdx]['value'] = parseFloat($scope.event.services[idx].offers[kdx]['value']);
                    }
                }
            }
        }

        var setupOffer = function () {
            for (var jdx in $scope.event.services) {
                if ($scope.event.services[jdx].displayId === $state.params.serviceId) {
                    $scope.service = $scope.event.services[jdx];
                    for (var kdx in $scope.service.offers) {
                        if ($scope.service.offers[kdx].id == $state.params.offerId) {
                            $scope.offer = $scope.service.offers[kdx];
                            if ($scope.offer.type == 'price') {
                                $scope.offer.value = parseFloat($scope.offer.value);
                            }
                        }
                    }
                }
            }
        }

        var initMessages = function (cb) {
            $timeout(function () {
                $scope.$broadcast('initMessages', {
                    subject: $scope.service.displayId + "-" + $scope.offer.provider.id + $scope.authentication.user.id,
                    fromId: $scope.authentication.user.id,
                    toId: $scope.offer.provider.id,
                    fromName: $scope.authentication.profile.displayName,
                    toName: $scope.offer.provider.displayName,
                    isClosed: ($scope.offer.status === 'IGNORED')
                });
            }, 10);
            cb();
        }

        var doCancelEvent = function (cb) {

            var url = $scope.authentication.api.baseUrl + "/users/" + $scope.authentication.user.id + "/events/" + $state.params.id;

            $http({
                url: url,
                method: 'DELETE',
                data: {
                    reason: $scope.reason
                },
                headers: {
                    'X-Auth-Token': $scope.authentication.user.accessToken,
                    'X-Market-ID': $scope.market.slug,
                    'Content-Type': "application/json;charset=utf-8"
                }
            })
                .success(function (response) {
                    //console.log('Success');
                    //console.log(response);
                    $scope.error = {};
                    cb();
                })
                .error(function (response) {
                    //console.log('Failure');
                    console.log(response);
                    $scope.error['reason'] = {
                        message: 'Reason must be specified!'
                    };
                });
        };

        $scope.cancelEvent = function () {

            $("#cancelEventDialog")
                .find('[data-cancel-button]').on('click', function (e) {
                    $("#cancelEventDialog").foundation('reveal', 'close');
                    e.preventDefault();
                });

            $("#cancelEventDialog").find('[data-confirm-button]').on('click', function (e) {
                doCancelEvent(function () {
                    $("#cancelEventDialog").foundation('reveal', 'close');
                    $state.go('requests.byou');
                });
                e.preventDefault();
            });


            $timeout(function () {
                $("#cancelEventDialog").foundation('reveal', 'open');
            }, 10);
            return false;
        }

        var setupAnswers = function () {
            if ($scope.service.answers) {
                $scope.answers = Answers.setupAnswers($scope.questions, $scope.service.answers);
            }
        };

        var updateRequest = function () {
            var eventObj = JSON.parse(JSON.stringify($scope.event));
            eventObj['market'] = $scope.market.slug;
            $http.put($scope.authentication.api.baseUrl + "/users/"
                + $scope.authentication.user.id + "/events/"
                + $state.params.id
                , eventObj)
                .success(function (response) {
                    $scope.isWaiting = false;
                    $scope.error = {};
                }).error(function (response) {
                    errorHandler(response);
                });
            return;
        };

        this.getDetailQuestions = function () {
            if (!$scope.service) {
                return;
            }

            if ($scope.questions.length > 0) {
                return;
            }

            $http.get($scope.authentication.api.publicUrl + "/services/questions?category=" + $scope.service.service)
                .success(function (response) {
                    $scope.questions = response.results;
                    if (response.totalCount == 0) {
                        $scope.noQuestions = true;
                    }

                    $timeout(function () {
                        // Blocking selection of same day
                        $("input[data-datetime-bind]").datetimepicker({
                            minDate: (new Date()).setDate((new Date()).getDate() + 1),
                            collapse: true,
                            useCurrent: false
                        }).on('dp.change', function (e) {
                            var currentField = $(e.target);
                            currentField = currentField[0];
                            var questionId = currentField.id.match(/([0-9].*)/)[0];
                            $scope.answers[questionId] = $('#' + currentField.id).val();
                        });

                    }, 100);

                    $scope.isWaiting = false;
                    $scope.error = {};
                    $timeout(function () {
                        setupAnswers();
                    }, 100);

                }).error(function (response) {
                    console.log(response);
                    $scope.isWaiting = false;
                });
        };

        $scope.processDetailAnswers = function () {
            var service;

            if (!$scope[$scope.formName + 'Form'].$valid) {
                $scope.setErrors = true;
                return false;
            }

            for (kdx in $scope.event.services) {
                if ($scope.event.services[kdx]["service"] === $scope.service.service) {
                    service = JSON.parse(angular.toJson($scope.event.services[kdx]));
                }

                if (typeof service['answers'] === 'undefined') {
                    service['answers'] = [];
                }
                $scope.event.services[kdx].answers = Answers.processAnswers(
                    JSON.parse(JSON.stringify($scope.answers))
                );
            }
            updateRequest();
        };

        $scope.fileComplete = function (res) {
            //console.log(res);
        }

        $(window).resize(function () {
            $('.floating-bottom').width($('.floating-bottom').parent().width());
        });

    }
]);
