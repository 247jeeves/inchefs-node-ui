angular.module('dashboard').controller('RequestsController', ['$scope', 'BootStrap', '$state',
    '$timeout', '$rootScope', 'Menus', '$http',
    '$filter', 'localStorageService',
    function ($scope, BootStrap, $state, $timeout, $rootScope, Menus, $http, $filter, localStorageService) {
        // This provides BootStrap context.
        $scope.authentication = BootStrap;

        var offset = 0;
        var max = 10;

        $scope.listType = 0;
        $scope.total = 0;
        $scope.requests = [];

        $scope.market = BootStrap.market;
        $scope.offer = {type: $scope.market.settings.offerType};
        $scope.answers = {};
        $scope.declineReasonText = "Unfortunately I have to decline at this time";
        $scope.sorters = {
            'date': 'desc',
            'address': 'asc'
        };
        $scope.statuses = ["ACCEPTED", "DECLINED"];
        $scope.acceptedStatuses = ["ACCEPTED", "COMPLETED", "CLOSED", "VERIFIED"];
        $scope.keywords = "";

        $scope.previousState = 'requests.ayou';

        $scope.notAllowed = false;

        // Handle displaying Make Offer widget based on market
        $scope.isSubscribed = true;
        $scope.isUnderSubscriptionLimit = true;
        $scope.isSocialVerified = $scope.authentication.profile.isSocialVerified;
        $scope.isOfferExisting = false;
        $scope.isSellerVerificationReqd = ($scope.market.settings.requests) ? $scope.market.settings.requests.seller_verification || false : false;
        $scope.isOfferValid = false;
	$scope.requestType = "Place Request";
	if(typeof $scope.market.settings.branding !== 'undefined'){
                if(typeof $scope.market.settings.branding.request_phrase1 !== 'undefined' && typeof $scope.market.settings.branding.request_phrase2 !== 'undefined'){
                        if($scope.market.settings.branding.request_phrase1 && $scope.market.settings.branding.request_phrase2){
                                   $scope.requestType = $scope.market.settings.branding.request_phrase1 + " " + $scope.market.settings.branding.request_phrase2
                        }
                }
        } 


        var globals = JSON.parse($("meta[name='_globals']").attr('content'));
        var wanteetPercentage = globals.WANTEET_FEE_PERCENTAGE || 0.00;
        var taxPercentage = globals.TAX_FEE_PERCENTAGE || 0.00;
        var paymentFeePercentage = globals.PAYMENT_FEE_PERCENTAGE || 0.00;

        $scope.taxes = function (offerPrice) {
            if (isNaN(offerPrice)) {
                return 0;
            }
            offerPrice = parseFloat(offerPrice);
            var wanteetFee = offerPrice * parseFloat(wanteetPercentage) / 100;
            var priceInclTax = wanteetFee + offerPrice + (offerPrice * parseFloat(taxPercentage) / 100);
            var paymentFee = priceInclTax * parseFloat(paymentFeePercentage) / 100;

            return ((priceInclTax + paymentFee) - offerPrice);
        };

        $scope.formatDate = function (date) {
            return moment(date).format('MM/DD/YYYY hh:mm A');
        };

        //Setup Side Nav
        $scope.requestsSideNav = Menus.getMenu('requestsSideNav').items;

        //Action to perform on url routing inside current scope
        $rootScope.$on('$stateChangeSuccess',
            function (event, toState, toParams, fromState, fromParams) {

                if (fromState.parent == "requests") {
                    $scope.previousState = fromState.name;
                }

                if ((toState.name.match(/\./g) || []).length > 1) {
                    Menus.selectMenuItemByLink('requestsSideNav', toState.parent);
                } else {
                    Menus.selectMenuItemByLink('requestsSideNav', toState.name);
                }
            });

        if ($state.current.name === undefined || $state.current.parent === undefined) {
            if ($scope.authentication.user.isBuyer()) {
                $state.transitionTo('requests.byou');
            } else {
                $state.transitionTo('requests.ayou');
            }
        } else {
            Menus.selectMenuItemByLink('requestsSideNav', $state.current.name);
        }

        Menus.selectMenuItemByLink('dashboardTop', 'requests.ayou');


        $scope.addSorter = function (type) {
            if ($scope.sorters[type] === 'desc') {
                $scope.sorters[type] = 'asc';
            } else {
                $scope.sorters[type] = 'desc';
            }
            $scope.requests = [];
            $scope.total = 0;
            offset = 0;
            $scope.requestList($scope.listType);
        };

        $scope.filterRequests = function () {
            $scope.keywords = this.keywords;
            $scope.requests = [];
            $scope.total = 0;
            offset = 0;
            $scope.requestList($scope.listType);
        };

        // Fetch Request List
        $scope.requestList = function (type) {
            $scope.isWaiting = true;
            $scope.success = "";

            if ($scope.listType != type) {
                offset = 0;
                $scope.listType = type || 0;
                $scope.requests = [];
            }

            $scope.request = undefined;
            $scope.offer = {};
            var sorterList = [];
            for (var k in $scope.sorters) {
                sorterList.push(k + '-' + $scope.sorters[k]);
            }

            var url = $scope.authentication.api.baseUrl + "/users/"
                + $scope.authentication.user.id + "/requests"
                + "?max=" + max + "&offset=" + offset + "&type=" + $scope.listType
                + "&market=" + $scope.market.slug + "&sorters=" + sorterList.join(',');

            if ($scope.keywords) {
                url += '&query=' + $scope.keywords;
            }

            $http.get(url)
                .success(function (response) {

                    //if ($scope.keywords) {
                        $scope.requests = $scope.requests.concat(response.searchResults);
                    //else
                     //   $scope.requests = $scope.requests.concat(response.results);
                    $scope.total = response.total;
                    offset += max;
                    $scope.isWaiting = false;
                    $scope.error = {};

                }).error(function (response) {
                    errorHandler(response);
                });
        }

        // Fetch Single Request with Deep Linking
        $scope.getRequest = function () {
            $scope.request = undefined;
            if ($scope.requests.length > 0) {
            //console.log("ushakiz available request "+$scope.requests);
                for (var idx in $scope.requests) {
                    if ($scope.requests[idx].id == $state.params.id) {
                        $scope.request = $scope.requests[idx];
            //console.log("ushakiz available request "+$scope.request);
                        if ($scope.request.hasOwnProperty('offers') && $scope.request.offers.length > 0) {
                            $scope.offer = $scope.request.offers[0];
            //console.log("ushakiz available offer "+$scope.offer);
                            removeOfferPriceIfZero();
                        } else {
                            $scope.isOfferExisting = false;
                        }
                        updateWidgetDisplayFlags();
                        initMessages();
                        Menus.selectMenuItemByLink('requestsSideNav', $scope.previousState);
            //console.log("ushakiz available offer before return "+$scope.offer);
                        return;
                    }
                }
            } else {
                if ($scope.request === undefined) {
                    $http.get($scope.authentication.api.baseUrl + "/users/"
                        + $scope.authentication.user.id + "/requests/"
                        + $state.params.id
                    )
                        .success(function (response) {
                            $scope.request = response;
            //console.log("ushakiz undefined fetch request "+$scope.request);
                            if ($scope.request.hasOwnProperty('offers') && $scope.request.offers.length > 0) {
                                $scope.offer = $scope.request.offers[0];
            //console.log("ushakiz undefined available offer "+$scope.offer);
                                removeOfferPriceIfZero();
                            } else {
                                $scope.isOfferExisting = false;
                            }
                            updateWidgetDisplayFlags();
                            initMessages();
                            Menus.selectMenuItemByLink('requestsSideNav', $scope.previousState);
                            $scope.isWaiting = false;
                            $scope.error = {};
                        }).error(function (response) {
               // console.log("ushakiz get request error response "+JSON.stringify(response));
                            errorHandler(response);
                        });
                }
            }
        }


        $scope.offerService = function () {

             //console.log("ushakiz offer service ");
            if (!$scope.offer.hasOwnProperty('id') && $scope.offer.status !== 'TENTATIVE') {
                $("#offerService").find('[data-cancel-button]').on('click', function (e) {
                    $("#offerService").foundation('reveal', 'close');
                    e.preventDefault();
                });

                $("#offerService").find('[data-confirm-button]').on('click', function (e) {
                    $("#offerService").foundation('reveal', 'close');
                    $scope.offerServiceOrDecline(false);
                    e.preventDefault();
                });
                $timeout(function () {
                    $("#offerService").foundation('reveal', 'open');
                }, 10);
            } else {
             //console.log("else ushakiz offer service ");
                $scope.offerServiceOrDecline(false);
            }
            return false;
        }
        $scope.processRequest = function () {

                //console.log("ushakiz offer status entered "+$scope.offer.status);
            if ($scope.offer.type == 'join') {
                if ($scope.offer.status == 'DECLINED') { 
                    $scope.declineRequest();
                }
                else if ($scope.offer.status == 'COMPLETED'){ 
                    //console.log("ushakiz completed ");
                    $scope.offerStatus('COMPLETED');   
                }
                else if ($scope.offer.status == 'ACCEPTED') { 
                    $scope.offerServiceOrDecline(false);            
                }
                else if ($scope.offer.status == 'VERIFIED') { 
                    $scope.offerStatus('VERIFIED');   
                }
                else if ($scope.offer.status == 'CLOSED') { 
                    $scope.offerStatus('CLOSED');   
                }            
            }
        }

        $scope.declineRequest = function () {

            if ($scope.offer.type == 'join') {
                $scope.offerServiceOrDecline(true);
                return false;
            }

            if (!$scope.offer.hasOwnProperty('id')) {
                $("#declineRequest").find('[data-cancel-button]').on('click', function (e) {
                    $("#declineRequest").foundation('reveal', 'close');
                    e.preventDefault();
                });

                $("#declineRequest").find('[data-confirm-button]').on('click', function (e) {
                    $("#declineRequest").foundation('reveal', 'close');
                    $scope.offerServiceOrDecline(true);
                    e.preventDefault();
                });
                $timeout(function () {
                    $("#declineRequest").foundation('reveal', 'open');
                }, 10);
            }
            return false;
        }

        var isOfferValid = function () {


            if ($scope.offer.type == 'join') {
                $scope.isOfferValid = false;
                return;
            }

            if (JSON.stringify($scope.offer) !== "{}") {
                if ($scope.offer.hasOwnProperty('status') && $scope.offer.status !== 'TENTATIVE') {
                    $scope.isOfferValid = true;
                    return;
                }
            }
            $scope.isOfferValid = false;

        }

        $scope.$on('messageSentSuccess', function () {
            //Submit an EOI
            if (!$scope.offer.hasOwnProperty('value') && typeof $scope.request !== 'undefined') {

                $http.post($scope.authentication.api.baseUrl + "/users/"
                    + $scope.authentication.user.id + "/requests/"
                    + $scope.request.id + "/offers",
                    {"value": null, "type": $scope.offer.type, "market": $scope.market.slug})//TODO: This is hard reference! Need to refactor based on new Offer Value & Type Logic
                    .success(function (response) {
                        response.price = parseFloat(response.price);
                        $scope.offer = response;
                        removeOfferPriceIfZero();
                        if ($scope.request.offers.length == 0){
                            $scope.request.offers.push($scope.offer);
                        }
                        $scope.isWaiting = false;
                        $scope.error = {};
                    }).error(function (response) {
                //console.log("ushakiz message sent error response "+JSON.stringify(response));
                        errorHandler(response);
                    });
            }

            if ($state.current.name == 'requests.response') {
                $state.go('^.rdetail', {id: $scope.request.id, displayId: $scope.request.displayId});
            }
        });

        $scope.getQuestions = function () {
            $http.get($scope.authentication.api.publicUrl + "/services/" + $scope.request.service + "/questions?response=true")
                .success(function (response) {
                    $scope.questions = response.results;

                    $timeout(function () {
                        // Blocking selection of same day
                        $("input[data-datetime-bind]").datetimepicker({
                            minDate: (new Date()).setDate((new Date()).getDate() + 1),
                            collapse: true,
                            useCurrent: false
                        });
                    }, 100);
                    initMessages();

                    //Hack to set the Questions & answers
                    if ($scope.offer.hasOwnProperty('answers')) {
                        for (ans in $scope.offer.answers) {
                            var ansObj = $scope.offer.answers[ans];
                            var ques;
                            for (q in $scope.questions) {
                                ques = $scope.questions[q];
                                if (ques.text == ansObj.question) {
                                    ques = ques;
                                    break;
                                }
                            }
                            $scope.answers[ques.id] = ansObj.answer;
                            if (ques.type == 'checkbox') {
                                var answerObj = {};
                                var answerArr = ansObj.answer.split(',');
                                for (i in answerArr) {
                                    var ansInst = answerArr[i];
                                    for (j in ques.choices) {
                                        var choice = ques.choices[j];
                                        if (choice == ansInst) {
                                            answerObj[j] = ansInst;
                                        }
                                    }
                                }
                                $scope.answers[ques.id] = answerObj;
                            }
                        }
                    }

                }).error(function (response) {
                    console.log("error "+JSON.stringify(response));
                });
        };

        $scope.processAnswers = function () {
            var answers = JSON.parse(JSON.stringify($scope.answers));
            var processedAnswers = [];

            for (idx in answers) {
                var answer;
                if (typeof answers[idx] === 'object') {
                    answer = [];
                    for (jdx in answers[idx]) {
                        if (answers[idx][jdx]) {
                            answer.push(answers[idx][jdx]);
                        }
                    }
                    answer = answer.join(',');
                } else {
                    answer = answers[idx];
                }

                processedAnswers.push({
                    question: idx,
                    answer: answer
                });
            }
            $scope.offer.answers = processedAnswers;
            $scope.offerServiceOrDecline(false);
        }

        $scope.offerStatus = function (status) {

            $scope.success = "";

            var api;

                //console.log("ushakiz offer status -=-=-=-=-=-entered "+status);
            $scope.offer['market'] = $scope.market.slug;

            if ($scope.offer.type == 'join') {
                $scope.offer.value = status;
                $scope.offer.status = 'ACCEPTED';
            }
                //console.log("ushakiz offer status -=-=-=-=-=-entered "+$scope.offer.status+":value "+$scope.offer.value);
            if ($scope.offer.hasOwnProperty('id')) {
                //update
                api = $http.put($scope.authentication.api.baseUrl + "/users/"
                    + $scope.authentication.user.id + "/requests/"
                    + $scope.request.id + "/offers/" + $scope.offer.id,
                    $scope.offer);
            } else {
                //create a new offer
                api = $http.post($scope.authentication.api.baseUrl + "/users/"
                    + $scope.authentication.user.id + "/requests/"
                    + $scope.request.id + "/offers",
                    $scope.offer);
            }

            api.success(function (response) {
                $scope.offer = response;
                if ($scope.request.offers.length == 0) {
                    $scope.request.offers.push($scope.offer);
                } else {
                    $scope.request.offers[0] = response;
                }
                //console.log("ushakiz-=-=-=-=-=- offer status "+status);
                var msg = status;

                if ($scope.offer.hasOwnProperty('answers')) {
                    msg = 'response';
                }

                $timeout(function () {
                    $scope.$broadcast('sendDirectMessage', {
                        text: setCompleteMessage(msg)
                    });
                }, 100);

                $scope.isWaiting = false;
                $scope.error = {};
            }).error(function (response) {
                //console.log("error response "+JSON.stringify(response));
                errorHandler(response);
            });
        }

         var setCompleteMessage =  function(message){
             var messageStr = "";
             switch(message){
                 default:
                     messageStr = "Request "+$scope.request.id+" is "+message;
                     break;
             }
             return messageStr;
         }
        // Make/Modify an Offer
        $scope.offerServiceOrDecline = function (isDeclined) {
             //console.log("ushakiz offer service or decline "+isDeclined);
            $scope.isWaiting = true;
            $scope.success = "";

            var api;

            $scope.offer['market'] = $scope.market.slug;

            // If Decline is pressed.
            if (isDeclined) {
                $scope.offer.isDeclined = isDeclined;
                if ($scope.offer.type == 'join') {
                    $scope.offer.value = 'DECLINED';
                }
            }
            else {
                if ($scope.offer.type == 'join') {
                    $scope.offer.status = 'ACCEPTED';
                    $scope.offer.value = 'WAITING';
                }
            }

                //console.log("ushakiz offerServiceOrDecline "+$scope.offer.status+":value "+$scope.offer.value);

            if ($scope.offer.hasOwnProperty('id')) {
                //update
                api = $http.put($scope.authentication.api.baseUrl + "/users/"
                    + $scope.authentication.user.id + "/requests/"
                    + $scope.request.id + "/offers/" + $scope.offer.id,
                    $scope.offer);
            } else {
                //create a new offer
                api = $http.post($scope.authentication.api.baseUrl + "/users/"
                    + $scope.authentication.user.id + "/requests/"
                    + $scope.request.id + "/offers",
                    $scope.offer);
            }

            api.success(function (response) {
                $scope.offer = response;
                removeOfferPriceIfZero();
                if ($scope.request.offers.length == 0) {
                    $scope.request.offers.push($scope.offer);
                } else {
                    $scope.request.offers[0] = response;
                }

                var msg = 'offer';
                if (isDeclined) {
                    msg = 'decline';
                } else {
                    msg = 'offer';
                }

                if ($scope.offer.hasOwnProperty('answers')) {
                    msg = 'response';
                }

                $timeout(function () {
                    $scope.$broadcast('sendDirectMessage', {
                        text: setOfferServiceOrDeclineMessage(msg)
                    });
                }, 100);

                $scope.isWaiting = false;
                $scope.error = {};

                refreshSubscription();

            }).error(function (response) {
                console.log("ushakiz error response "+JSON.stringify(response));
                errorHandler(response);
            });
        }

        var errorHandler = function (response) {
            //console.log("ushakiz response "+JSON.stringify(response));
            if (response == '') {
                $scope.request = {};
                $scope.notAllowed = true;
                return;
            }
            ;
            $scope.error = {};
            if (response.errors) {
                for (var idx in response.errors) {
                    $scope.error[response.errors[idx]['field']] = {
                        value: response.errors[idx]['value'],
                        message: response.errors[idx]['message']
                    };
                }
            }
            $scope.isWaiting = false;
        }

        var initMessages = function () {
            $timeout(function () {
                $scope.$broadcast('initMessages', {
                    subject: $scope.request.displayId + "-" + $scope.authentication.user.id + $scope.request.buyer.id,
                    fromId: $scope.authentication.user.id,
                    toId: $scope.request.buyer.id,
                    fromName: $scope.authentication.profile.displayName,
                    toName: $scope.request.buyer.displayName,
                    isClosed: ($scope.offer.status === 'IGNORED')
                });
            }, 800);
        };

        var removeOfferPriceIfZero = function () {
            $scope.isOfferExisting = true;
            if ($scope.offer.type == 'price') {
                $scope.offer.value = parseFloat($scope.offer.value);
                if ($scope.offer.value == 0.00) {
                    $scope.offer.value = undefined;
                }
            } else if ($scope.offer.type == 'time') {
                $scope.dateAndTime = new moment($scope.offer.value).format('MM/DD/YYYY h:mm A');
                $('#dateAndTime').val($scope.dateAndTime);
            }
            isOfferValid();
        }

        var setOfferServiceOrDeclineMessage = function (message) {
            //console.log("set offer service or decline mesage "+message);
            var messageStr = "";
            switch (message) {
                case 'decline':

                    messageStr = $scope.declineReasonText;
                    break;
                case 'response':
                    messageStr = '<div class="panel response-msg">';
                    messageStr += '<h5>Response to your request</h5>';
                    messageStr += '<ul class="no-bullet">';
                    for (ans in $scope.offer.answers) {
                        var ansObj = $scope.offer.answers[ans];
                        messageStr += '<li>';
                        messageStr += '<b>Q: ' + ansObj.question + '</b><br/>';
                        messageStr += '<p>A: ' + ansObj.answer + '</p>';
                        messageStr += '</li>';
                    }
                    messageStr += '</ul></div>';
                    break;
                case 'offer':
                default:
                    messageStr = "Hello ";
                    if ($scope.request.buyer != null)
                        messageStr += $scope.request.buyer.displayName;
                    messageStr += "! \nI will be happy to ";
                    if ($scope.offer.type == 'price') {
                        messageStr += "provide my services with quote price of <b>";
                        messageStr += $filter('wCurrency')(parseFloat($scope.offer.value) + $scope.taxes($scope.offer.value)) + "</b> (inclusive of all taxes)";
                    }
                    else if ($scope.offer.type == 'time') {
                        messageStr += "provide my services on <b>";
                        messageStr += new moment($scope.offer.value).format('MM/DD/YYYY [at] h:mm A');
                    }
                    else if ($scope.offer.type == 'join') {
                        messageStr += "offer my services";
                    }
            }
            //console.log("set offer service or decline mesagestr "+messageStr);
            return messageStr;
        }

        var refreshSubscription = function () {
            if (($scope.offer.type !== 'join') && ($scope.market.hasOwnProperty('updateCount'))) {
                $scope.currentSubscription = $scope.market.updateCount($scope.market.slug, 'seller_subscription');
                $scope.market = BootStrap.market;
            }
        }

        var updateWidgetDisplayFlags = function () {
            if ($scope.market.settings.addons && $scope.market.settings.addons['seller_subscription']
                && ($scope.authentication.user.permissions.indexOf('ROLE_SELLER') != -1)) {
                var userSubscription = $scope.market.getCurrentUserSubscription($scope.market.slug, 'seller_subscription');

                if (!userSubscription) {
                    $scope.isSubscribed = false;
                } else {
                    if (userSubscription.leftCount <= 0) {
                        $scope.isUnderSubscriptionLimit = false;
                    }
                }

            }

            //Set offerType if it doesn't exist
            if (!$scope.offer.hasOwnProperty('type')) {
                $scope.offer.type = $scope.market.settings.offerType;
            }

            //Setup dateTime selector
            if ($scope.offer.type == 'time') {
                $timeout(function () {
                    // Blocking selection of same day
                    var ua = navigator.userAgent;

                    if (ua.match(/(iPhone|iPad)/i)) {
                        $("#dateAndTime").attr('type', 'datetime-local');
                        $('body').on('change', '#dateAndTime', function (e) {

                            $scope.offer.value = moment($('#dateAndTime').val()).
                                format('YYYY-MM-DDTHH:mm:ss.SSSZ');

                            if (!$scope.$$phase) {
                                $scope.$apply();
                            }
                        });
                    } else {
                        $("#dateAndTime").datetimepicker({
                            minDate: (new Date()).setDate((new Date()).getDate() + 1),
                            collapse: true,
                            useCurrent: false
                        });

                        $('body').on('dp.change', '#dateAndTime', function (e) {
                            $scope.dateAndTime = $('#dateAndTime').val();
                            $scope.offer.value = $('#dateAndTime').data("DateTimePicker")
                                .date().format('YYYY-MM-DDTHH:mm:ss.SSSZ');
                        });
                    }
                }, 10);
            }

            $timeout(function () {
                angular.element('form[name="makeOffer"]').removeClass('hide');
            }, 10);

            //Setup File Type of answers
            if ($scope.request.answers && $scope.request.answers.length > 0) {
                $scope.request.answers.forEach(function (value, idx) {
                    if ($scope.request.answers[idx].answer == null) {
                        $scope.request.answers[idx].isFile = true;
                    }
                    if ($scope.request.answers[idx].answer && $scope.request.answers[idx].answer.indexOf('{') == 0) {
                        $scope.request.answers[idx].answer = JSON.parse($scope.request.answers[idx].answer);
                        $scope.request.answers[idx].isFile = true;
                    }

                });
            }
        }
    }
]);
