angular.module('dashboard').controller('EventCalendarController',
    ['$scope', 'BootStrap', '$state', '$timeout', '$rootScope', 'Menus', '$http', 'Loader',
        function ($scope, BootStrap, $state, $timeout, $rootScope, Menus, $http, Loader) {
            // This provides BootStrap context.
            $scope.authentication = BootStrap;

            var market = $scope.market = BootStrap.market;

            $scope.profile = $scope.authentication.profile;
            $scope.calendarEvents = [];
            $scope.calendarEvent = {};
            $scope.error = {};
            $scope.startDateTime = '';
            $scope.endDateTime = '';
            $scope.displayDateTime = {};

            $scope.isWaiting = false;

            var currentTimeZone = false;


            Menus.selectMenuItemByLink('dashboardTop', 'eventCalendar');

            moment.locale('en');

            function renderCalendar() {
                //Setup Calendar
                $('#calendar').fullCalendar({
                    header: {
                        left: 'prev,next today',
                        center: 'title',
                        right: 'month,agendaWeek,agendaDay'
                    },
                    fixedWeekCount: false,
                    selectable: true,
                    //editable: true,
                    selectHelper: true,
                    eventLimit: true,
                    timezone: 'local',
                    select: select,
                    //eventRender: eventRender,
                    eventClick: eventClick
                });
            }

            var setupTimePicker = function(){
                $("input[data-datetime-bind]").datetimepicker({
                    minDate: (new Date()).setDate((new Date()).getDate() + 1),
                    collapse: true,
                    useCurrent: false
                }).on('dp.change', function (e) {
                    var currentField = $(e.target);
                    currentField = currentField[0];
                    var questionId = currentField.id.match(/([0-9].*)/)[0];
                    $scope.answers[questionId] = $('#' + currentField.id).val();
                });
            };

            setupTimePicker();

            function setupModal(start, end, isEditMode){
                console.log(isEditMode);
                console.log(start);
                console.log(end);
                $scope.error = {};
                $scope.startDateTime = (typeof start !== 'string')?start.local():moment(start);
                $scope.endDateTime = (typeof end !== 'string')?end.local() : moment(end);

                if(!isEditMode){
                    $scope.endDateTime = $scope.endDateTime.subtract(1, 'minutes');
                }

                $scope.calendarEvent['start'] = moment($scope.startDateTime).format('YYYY-MM-DDTHH:mm:ss.SSSZ');
                $scope.calendarEvent['end'] = moment($scope.endDateTime).format('YYYY-MM-DDTHH:mm:ss.SSSZ');

                $scope.displayDateTime = {
                    startDate: $scope.startDateTime.format('MM/DD/YYYY, dddd'),
                    startTime: $scope.startDateTime.format('hh:mm A'),
                    endDate: $scope.endDateTime.format('MM/DD/YYYY, dddd'),
                    endTime: $scope.endDateTime.format('hh:mm A')
                };

                $scope.showModal(isEditMode);
            }

            function select(start, end, jsEvent, view) {
                $scope.calendarEvent = {};
                console.log('Select Event');
                setupModal(start, end, false);
            }

            //function eventRender(event, view) {
            //    if (event.start.hasZone()) {
            //        view.find('.fc-title').after(
            //            $('<div class="tzo"/>').text(event.start.format('Z'))
            //        );
            //    }
            //}

            function eventClick(event, jsEvent, view) {
                $scope.calendarEvent = event;
                console.log("Edit Event");
                setupModal(event.start, event.end, true);
            }

            $("#calendarEventModal").find('[data-cancel-button]').on('click', function (e) {
                $("#calendarEventModal").foundation('reveal', 'close');
                e.preventDefault();
            });

            $("#calendarEventModal").find('[data-confirm-button]').on('click', function (e) {
                $scope.createOrUpdateCalendarEvent(function () {
                    $scope.calendarEvent = {};
                    $("#calendarEventModal").foundation('reveal', 'close');
                });
                e.preventDefault();
            });

            $scope.showModal = function (editMode) {

                if (editMode) {
                    $scope.isEdit = true;
                } else {
                    $scope.isEdit = false;
                }

                $timeout(function () {
                    $("#calendarEventModal").foundation('reveal', 'open');
                }, 10);

                return false;
            };


            //Get all Listings - No pagination needed at the moment here
            $scope.getCalendarEvents = function () {
                $('#calendar').fullCalendar('destroy');
                renderCalendar();

                $scope.isWaiting = true;
                $scope.calendarEvents = [];
                $http.get($scope.authentication.api.baseUrl + "/users/"
                    + $scope.authentication.user.id + "/calendar/events"
                )
                    .success(function (response) {
                        $scope.calendarEvents = response.results;

                        for (idx in $scope.calendarEvents) {
                            $('#calendar').fullCalendar('renderEvent', $scope.calendarEvents[idx], true);
                        }

                        $scope.isWaiting = false;
                    })
                    .error(function (response) {
                        errorHandler(response);
                    });
            };

            $scope.createOrUpdateCalendarEvent = function (cb) {
                $scope.isWaiting = true;

                var data = {
                    title: $scope.calendarEvent.title,
                    start: $scope.calendarEvent.start,
                    end: $scope.calendarEvent.end
                };

                if($scope.calendarEvent.id){
                    data['id'] = $scope.calendarEvent.id;
                }

                if (!$scope.calendarEvent.hasOwnProperty('id')) {
                    $http.post($scope.authentication.api.baseUrl + "/users/"
                        + $scope.authentication.user.id + "/calendar/events",
                        data)
                        .success(function (response) {
                            $scope.calendarEvent = response;
                            $('#calendar').fullCalendar('renderEvent', $scope.calendarEvent, true);
                            $scope.calendarEvents.push($scope.calendarEvent);
                            $scope.isWaiting = false;
                            $scope.error = {};

                            cb();

                        })
                        .error(function (response) {
                            errorHandler(response);
                        });
                } else {
                    $http.put($scope.authentication.api.baseUrl + "/users/"
                        + $scope.authentication.user.id + "/calendar/events/"
                        + $scope.calendarEvent.id,
                        data)
                        .success(function (response) {
                            $scope.getCalendarEvents();

                            cb();
                        })
                        .error(function (response) {
                            errorHandler(response);
                        });
                }
            };


            $scope.deleteCalendarEvent = function () {

                $http.delete($scope.authentication.api.baseUrl + "/users/"
                + $scope.authentication.user.id + "/calendar/events/"
                + $scope.calendarEvent.id)
                    .success(function (response) {
                        $scope.getCalendarEvents();
                        $("#calendarEventModal").foundation('reveal', 'close');
                        $scope.calendarEvent = {};
                    })
                    .error(function (response) {
                        console.log(response);
                    });


            }
	    
	
	/****************************************
	*Function to sync the outlook event
	*****************************************/ 	

            $scope.syncWithOutlook = function(){
		Loader.startLoader();		
		outlookRefreshToken(function(res){
			console.log("!!!!! Event Calendar Success - ");
			console.log(res);
		                   $http.get('/outlooksync/'+$scope.authentication.user.id)
				   .success(function(response){
	                        	console.log(response);i
					Loader.stopLoader('success');
        	        	    }).error(function(err){
        		                console.log(err);
					Loader.stopLoader('error');
	                	    });
		}, function(err){
			console.log(err);
		});
	}

	   var outlookRefreshToken = function(success, failure){
                    $http.get('/auth/microsoft/refresh')
		    .success(function(response){
				success(response);
                    }).error(function(err){
                        	failure(err);
                    });
	   }

            var errorHandler = function (response) {
                console.log(response);

                $scope.error = {};
                if (response && response !== undefined && response.hasOwnProperty('errors')) {
                    for (var idx in response.errors) {
                        $scope.error[response.errors[idx]['field']] = {
                            value: response.errors[idx]['value'],
                            message: response.errors[idx]['message']
                        };
                    }
                }
                $scope.isWaiting = false;
            };
        }
    ]);
