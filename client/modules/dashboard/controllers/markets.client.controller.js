angular.module('dashboard').controller('MarketsController',
    ['$scope', 'BootStrap', '$state', '$timeout', '$rootScope', 'Menus', '$http',
        function ($scope, BootStrap, $state, $timeout, $rootScope, Menus, $http) {
            // This provides BootStrap context.
            $scope.authentication = BootStrap;

            //var market = $scope.market = BootStrap.market;

            $scope.profile = $scope.authentication.profile;
            $scope.markets = [];

            $scope.market = {};
            $scope.error = {};

            $scope.isWaiting = false;

            $scope.progress = {
                isFirstStepDone: false
            };


            //Setup Side Nav
            $scope.sidenav = Menus.getMenu('marketsSideNav').items;

            //Action to perform on url routing inside current scope
            $rootScope.$on('$stateChangeSuccess',
                function (event, toState, toParams, fromState, fromParams) {
                    if ((toState.name.match(/\./g) || []).length > 1) {
                        Menus.selectMenuItemByLink('marketsSideNav', toState.parent);
                    } else {
                        Menus.selectMenuItemByLink('marketsSideNav', toState.name);
                    }
                });

            if ($state.current.name === undefined || $state.current.parent === undefined) {
                $state.transitionTo('markets.all');
            } else {
                Menus.selectMenuItemByLink('marketsSideNav', $state.current.name);
            }

            $scope.setupAddListing = function () {
                $scope.addItems = [];
                $scope.market = {
                    items: []
                };
                $scope.error = {};

                $scope.isWaiting = false;

                $scope.progress = {
                    isFirstStepDone: false
                };
                $timeout(function () {
                    $("#tags").selectize({
                        delimiter: ',',
                        maxItems: 5,
                        persist: true,
                        create: function (input) {
                            return {
                                value: input,
                                text: input
                            }
                        }
                    });
                }, 5);
            };

            //Get all Listings - No pagination needed at the moment here
            $scope.getMarkets = function () {
                $scope.isWaiting = true;
                $scope.markets = [];
                $http.get($scope.authentication.api.baseUrl + "/users/"
                    + $scope.authentication.user.id + "/markets"
                )
                    .success(function (response) {
                        $scope.markets = response.results;
                        $scope.isWaiting = false;
                    })
                    .error(function (response) {
                        errorHandler(response);
                    });
            }

            //Get individual Listing
            $scope.getListing = function () {
                $scope.market = undefined;
                $scope.addItems = [];
                $scope.isWaiting = true;
                if ($scope.markets.length > 0) {
                    for (idx in $scope.markets) {
                        if ($scope.markets[idx].id == $state.params.packageId) {
                            $timeout(function () {
                                $scope.market = $scope.markets[idx];
                                setupListing();
                                $scope.isWaiting = false;
                            }, 5);
                            return;
                        }
                    }
                } else {
                    $http.get($scope.authentication.api.baseUrl + "/users/"
                        + $scope.authentication.user.id + "/packages/"
                        + $state.params.packageId
                    )
                        .success(function (response) {
                            $scope.market = response;
                            setupListing();
                            $scope.isWaiting = false;
                        })
                        .error(function (response) {
                            errorHandler(response);
                        });
                }

            }

            $scope.createOrUpdateListing = function () {
                $scope.isWaiting = true;
                var data = JSON.parse(JSON.stringify($scope.market));
                data.market = market.slug;
                data.images = [];
                if (imagesToUpload.length > 0) {
                    data.images = imagesToUpload;
                }
                if (data.tags !== undefined) {
                    data.tags = data.tags.split(',');
                }
                if (!$scope.market.hasOwnProperty('id')) {
                    $http.post($scope.authentication.api.baseUrl + "/users/"
                        + $scope.authentication.user.id + "/packages",
                        data)
                        .success(function (response) {
                            console.log(response);
                            $scope.market = response;
                            $scope.markets.push($scope.market);
                            $scope.addItem();
                            $scope.isWaiting = false;
                            $scope.progress.isFirstStepDone = true;
                            imagesToUpload = [];
                            if (flowInst !== undefined) {
                                flowInst.files = [];
                            }
                            $scope.error = {};
                        })
                        .error(function (response) {
                            errorHandler(response);
                        });
                } else {
                    $http.put($scope.authentication.api.baseUrl + "/users/"
                        + $scope.authentication.user.id + "/packages/"
                        + $scope.market.id,
                        data)
                        .success(function (response) {
                            $scope.market = response;
                            $scope.markets.push($scope.market);
                            $scope.isWaiting = false;
                            imagesToUpload = [];
                            if (flowInst !== undefined) {
                                flowInst.files = [];
                            }
                            $scope.error = {};
                        })
                        .error(function (response) {
                            errorHandler(response);
                        });
                }
            }

            $scope.addItemsToListing = function (index) {


                var itemName = $scope.addItems[index].name.replace(/^\s+|\s+$/g, '');
                if (itemName == '' ||
                    $scope.isWaiting ||
                    $scope.addItems[index].added ||
                    $scope.addItems[index].isWating) {
                    return;
                }

                $scope.isWaiting = true;
                $scope.addItems[index].added = true;
                $scope.addItems[index].isWating = true;

                $http.patch($scope.authentication.api.baseUrl + "/users/"
                    + $scope.authentication.user.id + "/packages/"
                    + $scope.market.id,
                    {name: $scope.addItems[index].name})
                    .success(function (response) {
                        $scope.addItems[index] = response;

                        //Adding again - I know too exhaustive
                        $scope.addItems[index].added = true;
                        $scope.addItems[index].isWating = false;

                        //Check if the items is null or if it doesn't exist, act accordingly
                        if ($scope.market.hasOwnProperty('items') && $scope.market.items !== null) {
                            $scope.market.items.push(response);
                        } else {
                            $scope.market.items = [];
                            $scope.market.items.push(response);
                        }

                        $scope.addItems[index].isWating = false;
                        addItem();
                        $scope.isWaiting = false;
                        $scope.error = {};
                    })
                    .error(function (response) {
                        $scope.addItems[index].added = false;
                        $scope.addItems[index].isWating = false;
                        errorHandler(response);
                    });
            }

            $scope.deleteItemsFromListing = function (index) {

                if (!$scope.addItems[index].hasOwnProperty('id')) {
                    removeItem(index);
                    return;
                }

                //$scope.isWaiting = true;
                $scope.addItems[index].isWating = true;
                //
                $http.delete($scope.authentication.api.baseUrl + "/users/"
                    + $scope.authentication.user.id + "/packages/"
                    + $scope.market.id + "/items/"
                    + $scope.addItems[index].id
                )
                    .success(function (response) {
                        $scope.market.items.splice(index, 1);
                        removeItem(index);
                        $scope.isWaiting = false;
                    })
                    .error(function (response) {
                        $scope.addItems[index].added = false;
                        $scope.addItems[index].isWating = false;
                        errorHandler(response);
                    });
            }

            $scope.deleteListing = function () {
                $("#deleteListing").find('[data-cancel-button]').on('click', function (e) {
                    $("#deleteListing").foundation('reveal', 'close');
                    e.preventDefault();
                });

                $("#deleteListing").find('[data-confirm-button]').on('click', function (e) {
                    $("#deleteListing").foundation('reveal', 'close');

                    $http.delete($scope.authentication.api.baseUrl + "/users/"
                    + $scope.authentication.user.id + "/packages/"
                    + $scope.market.id)
                        .success(function (response) {
                            $state.go('markets.all');
                            //TODO: Show a success message
                        })
                        .error(function (response) {
                            //TODO: Show an error message
                        });

                    e.preventDefault();
                });
                $timeout(function () {
                    $("#deleteListing").foundation('reveal', 'open');
                }, 10);
            }

            /**
             * For Add Item Input boxes - Begin
             */

            var addItem = function (setFocus) {
                if (setFocus === undefined) {
                    setFocus = true;
                }
                $scope.addItems.push({
                    name: '',
                    added: false,
                    isWaiting: false
                });
                if (setFocus) {
                    $timeout(function () {
                        $("#itemName" + ($scope.addItems.length - 1)).focus();
                    }, 5);
                }
            }

            var removeItem = function (index) {
                $scope.addItems.splice(index, 1);
            }

            //I know it's funny :-/ but angularjs have a lot of untold problems
            //It's just a hack to call addItem() so that angular js doesn't
            //throw error at an untraceable line
            $scope.addItem = function () {
                $timeout(function () {
                    addItem();
                }, 5);
            }
            /**
             * Add Item Input boxes - End
             */

            $scope.initFileUpload = function () {
                $scope.isWaiting = true;
            }

            $scope.uploadSuccess = function ($flow, $file, $message) {
                flowInst = $flow;
                imagesToUpload.push($file.uniqueIdentifier);
                if (flowInst.files.length == imagesToUpload.length) {
                    $scope.isWaiting = false;
                }
            }

            var setupListing = function () {
                var listOfItems = [];
                if ($scope.market.items !== undefined) {
                    listOfItems = $scope.market.items.slice();
                }
                $scope.addItems = listOfItems;

                for (idx in $scope.addItems) {
                    $scope.addItems[idx].added = true;
                    $scope.addItems[idx].isWaiting = false;
                }

                addItem(false);

                $timeout(function () {
                    $("#tags-edit").selectize({
                        delimiter: ',',
                        maxItems: 5,
                        persist: true,
                        create: function (input) {
                            return {
                                value: input,
                                text: input
                            }
                        }
                    });
                }, 10);


            };

            var errorHandler = function (response) {
                console.log(response);
                $scope.error = {};
                if (response && response !== undefined && response.hasOwnProperty('errors')) {
                    for (var idx in response.errors) {
                        $scope.error[response.errors[idx]['field']] = {
                            value: response.errors[idx]['value'],
                            message: response.errors[idx]['message']
                        };
                    }
                }
                $scope.isWaiting = false;
            };

        }
    ]);
