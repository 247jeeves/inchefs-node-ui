angular.module('dashboard').controller('CustomersController',
    ['$scope', 'BootStrap', '$state', '$timeout', '$rootScope', 'Menus', '$http', 'Loader',
        function ($scope, BootStrap, $state, $timeout, $rootScope, Menus, $http, Loader) {
            // This provides BootStrap context.


            $scope.authentication = BootStrap;

            var market = $scope.market = BootStrap.market;

            var vm = this;

            $scope.profile = $scope.authentication.profile;
            $scope.customers = [];
            $scope.customer = {};
            $scope.error = {};

            $scope.isWaiting = false;

            $scope.formName = 'customerCtrl.customersForm';
            $scope.isWaiting = false;

            //Setup Side Nav
            $scope.sidenav = Menus.getMenu('customersSideNav').items;

            var offset = 0;
            var max = 10;
            $scope.total = 0;

            //Action to perform on url routing inside current scope
            $rootScope.$on('$stateChangeSuccess',
                function (event, toState, toParams, fromState, fromParams) {
                    if ((toState.name.match(/\./g) || []).length > 1) {
                        Menus.selectMenuItemByLink('customersSideNav', toState.parent);
                    } else {
                        Menus.selectMenuItemByLink('customersSideNav', toState.name);
                    }
                    if (toState.name != 'customers.add' && toState.name != 'customers.all') {
                        Menus.disableMenuItemByLink('customersSideNav', 'customers.add');
                    } else {
                        Menus.enableMenuItemByLink('customersSideNav', 'customers.add');
                    }
                });

            if ($state.current.name === undefined || $state.current.parent === undefined) {
                $state.transitionTo('customers.all');
                Menus.enableMenuItemByLink('customersSideNav', 'customers.add');
            } else {
                Menus.selectMenuItemByLink('customersSideNav', $state.current.name);
            }

            Menus.selectMenuItemByLink('dashboardTop', 'customers.all');

            $scope.setupAddCustomer = function () {
                $scope.customer = {};
            };

            //Get Customers
            $scope.getCustomers = function () {
               console.log("!!!!!!!!!!!!!!!!!!!! Sanjeet - inside customer client controller for apiurl  "+$scope.authentication.api.baseUrl + "/users/"
                    + $scope.authentication.user.id + "/customers"
                    + "?&max=" + max + "&offset" + offset);
		 $scope.isWaiting = true;
                $scope.customers = [];
                $http.get($scope.authentication.api.baseUrl + "/users/"
                    + $scope.authentication.user.id + "/customers"
                    + "?&max=" + max + "&offset" + offset
                ).success(function (response) {
                        $scope.customers = response.results;
                        $scope.isWaiting = false;
                    })
                    .error(function (response) {
                        errorHandler(response);
                    });
            };

            //Get individual Listing
            $scope.getCustomer = function () {
                $scope.customer = {};
                $scope.isWaiting = true;
                if ($scope.customers.length > 0) {
                    for (idx in $scope.customers) {
                        if ($scope.customers[idx].id == $state.params.customerId) {
                            $timeout(function () {
                                $scope.customer = $scope.customers[idx];
                                $scope.isWaiting = false;
                            }, 5);
                            return;
                        }
                    }
                } else {
                    $http.get($scope.authentication.api.baseUrl + "/users/"
                        + $scope.authentication.user.id + "/customers/"
                        + $state.params.customerId
                    )
                        .success(function (response) {
                            $scope.customer = response;
                            $scope.isWaiting = false;
                        })
                        .error(function (response) {
                            errorHandler(response);
                        });
                }

            }

            $scope.createOrUpdateCustomer = function () {
                $rootScope.$on('$stateChangeSuccess',
                    function (event, toState, toParams, fromState, fromParams) {
                        if ((toState.name.match(/\./g) || []).length > 1) {
                            Menus.selectMenuItemByLink('customersSideNav', toState.parent);
                        } else {
                            Menus.selectMenuItemByLink('customersSideNav', toState.name);
                        }
                        if (toState.name != 'customers.add' && toState.name != 'customers.all') {
                            Menus.disableMenuItemByLink('customersSideNav', 'customers.add');
                        } else {
                            Menus.enableMenuItemByLink('customersSideNav', 'customers.add');
                        }
                    });

                if ($state.current.name === undefined || $state.current.parent === undefined) {
                    $state.transitionTo('customers.all');
                    Menus.enableMenuItemByLink('customersSideNav', 'customers.add');
                } else {
                    Menus.selectMenuItemByLink('customersSideNav', $state.current.name);
                }

                $scope.isWaiting = true;
                var data = JSON.parse(JSON.stringify($scope.customer));

                if (!$scope.customer.hasOwnProperty('id')) {
                    $http.post($scope.authentication.api.baseUrl + "/users/"
                        + $scope.authentication.user.id + "/customers",
                        data)
                        .success(function (response) {
                            $scope.customer = response;
                            $scope.customers.push($scope.customer);
                            $scope.isWaiting = false;
                            $scope.error = {};
                        })
                        .error(function (response) {
                            errorHandler(response);
                        });
                } else {
                    $http.put($scope.authentication.api.baseUrl + "/users/"
                        + $scope.authentication.user.id + "/customers/"
                        + $scope.customer.id,
                        data)
                        .success(function (response) {
                            $scope.customer = response;
                            $scope.customers.push($scope.customer);
                            $scope.isWaiting = false;
                            $scope.error = {};
                            $state.go('customers.all');
                        })
                        .error(function (response) {
                            errorHandler(response);
                        });
                }
            };


            $scope.deleteCustomer = function () {
                $("#deleteCustomer").find('[data-cancel-button]').on('click', function (e) {
                    $("#deleteCustomer").foundation('reveal', 'close');
                    e.preventDefault();
                });

                $("#deleteCustomer").find('[data-confirm-button]').on('click', function (e) {
                    $("#deleteCustomer").foundation('reveal', 'close');

                    $http.delete($scope.authentication.api.baseUrl + "/users/"
                    + $scope.authentication.user.id + "/customers/"
                    + $scope.customer.id)
                        .success(function (response) {
                            $state.go('customers.all');
                            //TODO: Show a success message
                        })
                        .error(function (response) {
                            //TODO: Show an error message
                        });

                    e.preventDefault();
                });
                $timeout(function () {
                    $("#deleteCustomer").foundation('reveal', 'open');
                }, 10);
            }


            var setupCustomer = function () {

            };


            var errorHandler = function (response) {
                console.log(response);
                $scope.error = {};
                if (response && response !== undefined && response.hasOwnProperty('errors')) {
                    for (var idx in response.errors) {
                        $scope.error[response.errors[idx]['field']] = {
                            value: response.errors[idx]['value'],
                            message: response.errors[idx]['message']
                        };
                    }
                }
                $scope.isWaiting = false;
            };

        }
    ]);
