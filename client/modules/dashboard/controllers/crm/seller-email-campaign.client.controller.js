angular.module('dashboard').controller('SellerEmailerController',
    ['$scope', 'BootStrap', '$state','$window', '$timeout', '$rootScope', 'Menus', '$http', 'localStorageService','$location', 'Loader',
        function ($scope, BootStrap, $state, $window, $timeout, $rootScope, Menus, $http, localStorageService ,$location, Loader) {
            // This provides BootStrap context.
            $scope.authentication = BootStrap;

            var market = $scope.market = BootStrap.market;

            $scope.profile = $scope.authentication.profile;
            $scope.services = market.settings.services;
	    localStorageService.set('id', $scope.authentication.user.id);
            localStorageService.set('accessToken', $scope.authentication.user.accessToken);
	    $scope.emailCampaigns = [];
            $scope.emailCampaign = {};
            $scope.error = {};
            $scope.allCustomers = [];

            $scope.isWaiting = false;

            $scope.sendToList = [
                {name: 'ALL_BUYERS', desc: 'All Buyers in the Market'},
                {name: 'ALL_SELLERS', desc: 'All Sellers in the Market'},
                {name: 'ALL_USERS', desc: 'All Users (Buyers + Sellers) of the Market'},
                {name: 'BUYER_PROSPECTS', desc: 'All Buyer Prospects'},
                {name: 'SELLER_PROSPECTS', desc: 'All Seller Prospects'},
                {name: 'CUSTOM', desc: 'Custom'}
            ];

            var customerDropDown;

            var myTextArea = undefined;
            var myCodeMirror = undefined;

            //Setup Side Nav
            $scope.sidenav = Menus.getMenu('customersSideNav').items;

            //Action to perform on url routing inside current scope
            $rootScope.$on('$stateChangeSuccess',
                function (event, toState, toParams, fromState, fromParams) {
                    if ((toState.name.match(/\./g) || []).length > 1) {
                        Menus.selectMenuItemByLink('customersSideNav', toState.parent);
                    } else {
                        Menus.selectMenuItemByLink('customersSideNav', toState.name);
                    }

                    if (toState.name == 'customers.emailerAdd') {
                        if (myTextArea) {
                            myTextArea = undefined;
                            myCodeMirror = undefined;
                        }
                    }

                });

            if ($state.current.name === undefined || $state.current.parent === undefined) {
                $state.transitionTo('customers.all');
            } else {
                Menus.selectMenuItemByLink('customersSideNav', $state.current.name);
            }

            $('#sendAll').change(function (value) {
                console.log(this.checked);
                if (this.checked) {
                    $scope.emailCampaign.sendTo = 'ALL_SELLER_BUYERS';
                    $scope.emailCampaign.receiver = null;
                    $scope.emailCampaign.receipentName = null;
                    customerDropDown[0].selectize.disable();
                } else {
                    $scope.emailCampaign.sendTo = 'SELLER_BUYER';
                    customerDropDown[0].selectize.enable();
                }

            });

            //Get all Templates - No pagination needed at the moment here
            $scope.getEmailCampaigns = function () {
                $scope.isWaiting = true;
                $scope.emailTemplates = [];
                $http.get($scope.authentication.api.baseUrl + "/users/"
                    + $scope.authentication.user.id + "/markets/"
                    + market.slug + "/email/campaigns"
                )
                    .success(function (response) {
                        $scope.emailCampaigns = response.results;
                        $scope.isWaiting = false;
                    })
                    .error(function (response) {
                        errorHandler(response);
                    });
	   }

	   $scope.syncWithZoho = function(){
		/****
		* Passing zoho clientID and clientSecret to trigger the zoho OAuth2
		* @param id = clientID
		* @param token = clientSecret
		**/

		$window.open('http://'
		+market.slug+'.com/auth/zohocrm?id='+market.settings.social.zoho.clientID+'&token='
                +market.settings.social.zoho.clientSecret,'_target');	
	   };

            //Get individual Template
            $scope.getEmailCampaign = function () {

                $scope.emailCampaign = {};
                $scope.isWaiting = true;
                if ($scope.emailCampaigns.length > 0) {
                    for (idx in $scope.emailCampaigns) {
                        if ($scope.emailCampaigns[idx].id == $state.params.emailCampaignId) {
                            $timeout(function () {
                                $scope.emailCampaign = $scope.emailCampaigns[idx];

                                $scope.isWaiting = false;

                                myCodeMirror = undefined;
                                myTextArea = undefined;

                                initAddEmailCampaign();
                            }, 5);
                            return;
                        }
                    }
                } else {
                    $http.get($scope.authentication.api.baseUrl + "/users/"
                        + $scope.authentication.user.id + "/markets/"
                        + market.slug + "/email/campaigns/"
                        + $state.params.emailCampaignId
                    )
                        .success(function (response) {
                            $scope.emailCampaign = response;
                            $scope.isWaiting = false;

                            myCodeMirror = undefined;
                            myTextArea = undefined;

                            initAddEmailCampaign();
                        })
                        .error(function (response) {
                            errorHandler(response);
                        });
                }

            }

            $scope.createOrUpdateEmailCampaign = function () {

                console.log($scope.emailCampaign);

                $scope.isWaiting = true;

                var data = JSON.parse(JSON.stringify($scope.emailCampaign));
                data.content = angular.element("#codeEditor").val();
                data.market = market.slug;

                if (!$scope.emailCampaign.hasOwnProperty('id')) {
                    $http.post($scope.authentication.api.baseUrl + "/users/"
                        + $scope.authentication.user.id + "/markets/"
                        + market.slug + "/email/campaigns",
                        data)
                        .success(function (response) {
                            $scope.emailCampaign = response;
                            $scope.emailCampaigns.push($scope.emailCampaign);
                            $scope.isWaiting = false;
                            $scope.error = {};
                            $scope.emailCampaign = {};
                            $state.go('customers.all');
                        })
                        .error(function (response) {
                            errorHandler(response);
                        });
                } else {
                    $http.put($scope.authentication.api.baseUrl + "/users/"
                        + $scope.authentication.user.id + "/markets/"
                        + market.slug + "/email/campaigns/" + $scope.emailCampaign.id,
                        data)
                        .success(function (response) {
                            $scope.emailCampaign = response;
                            $scope.emailCampaigns.push($scope.emailCampaign);
                            $scope.isWaiting = false;
                            $scope.emailCampaign = {};
                            $scope.error = {};
                        })
                        .error(function (response) {
                            errorHandler(response);
                        });
                }
            };

            var errorHandler = function (response) {
                console.log(response);
                $scope.error = {};
                if (response && response !== undefined && response.hasOwnProperty('errors')) {
                    for (var idx in response.errors) {
                        $scope.error[response.errors[idx]['field']] = {
                            value: response.errors[idx]['value'],
                            message: response.errors[idx]['message']
                        };
                    }
                }
                $scope.isWaiting = false;
            };

            var initAddEmailCampaign = $scope.initAddEmailCampaign = function (reset) {

                //$scope.emailCampaign = {};

                setupDatePickers();


                //$scope.tinymceOptions = {
                //    plugins: 'link image code',
                //    toolbar: 'undo redo | bold italic | alignleft aligncenter alignright | code'
                //};

                //if (typeof reset !== 'undefined') {
                //    $scope.emailCampaign.options = {
                //        needTheme: true
                //    };
                //} else {
                //    $scope.tinymceModel = 'Initial content';
                //}


                if (typeof myTextArea === 'undefined') {
                    myTextArea = document.getElementById("codeEditor");
                }

                if (typeof myCodeMirror === 'undefined') {
                    myCodeMirror = CodeMirror.fromTextArea(myTextArea, {
                        lineNumbers: true,
                        mode: "text/html",
                        extraKeys: {"Ctrl-Space": "autocomplete"}
                    });
                }

                if (typeof reset !== 'undefined') {
                    $scope.emailCampaign.options = {
                        needTheme: true
                    };
                } else {
                    myCodeMirror.getDoc().setValue($scope.emailCampaign.content || "");
                }

                myCodeMirror.on('change', function (cm, change) {
                    cm.save();
                });


                $http.get($scope.authentication.api.baseUrl + "/users/"
                    + $scope.authentication.user.id + "/customers"
                    + "?&max=9999&offset=0"
                ).success(function (response) {
                        $scope.allCustomers = response.results;
                        $scope.isWaiting = false;

                        $timeout(function () {
                            customerDropDown = $("#receiver").selectize({
                                persist: false,
                                maxItems: 1,
                                valueField: 'email',
                                labelField: 'fullName',
                                searchField: ['fullName', 'email'],
                                options: $scope.allCustomers,
                                render: {
                                    item: function (item, escape) {
                                        return '<div>' +
                                            (item.fullName ? '<span class="name">' + escape(item.fullName) + '</span>' : '') +
                                            (item.email ? '<span class="email"> &lt;' + escape(item.email) + '&gt;</span>' : '') +
                                            '</div>';
                                    },
                                    option: function (item, escape) {
                                        var label = item.fullName || item.email;
                                        var caption = item.fullName ? item.email : null;
                                        return '<div>' +
                                            '<span >' + escape(label) + '</span>' +
                                            (caption ? '<span class="caption"> &lt;' + escape(caption) + '&gt;</span>' : '') +
                                            '</div>';
                                    }
                                },
                                createFilter: function (input) {
                                    var match, regex;

                                    // email@address.com
                                    regex = new RegExp('^' + REGEX_EMAIL + '$', 'i');
                                    match = input.match(regex);
                                    if (match) return !this.options.hasOwnProperty(match[0]);

                                    // name <email@address.com>
                                    regex = new RegExp('^([^<]*)<' + REGEX_EMAIL + '>$', 'i');
                                    match = input.match(regex);
                                    if (match) return !this.options.hasOwnProperty(match[2]);

                                    return false;
                                },
                                onChange: function (value) {
                                    if (value === null || value === '') {
                                        $scope.emailCampaign.receiver = null;
                                        $scope.emailCampaign.receipentName = null;
                                        $scope.emailCampaign.sendTo = null;
                                    } else {
                                        $scope.emailCampaign.receiver = value;
                                        $scope.allCustomers.map(function (obj) {
                                            if (obj.email === value) {
                                                $scope.emailCampaign.receipentName = obj.fullName;
                                            }
                                        });
                                        $scope.emailCampaign.sendTo = 'SELLER_BUYER';
                                    }
                                }
                            });
                        }, 5);
                    })
                    .error(function (response) {
                        errorHandler(response);
                        $scope.isWaiting = false;
                    });
            };

            var setupDatePickers = function () {

                $("input[data-datetime-bind]").datetimepicker({
                    minDate: (new Date()).setDate((new Date()).getDate() + 1),
                    collapse: true,
                    useCurrent: false
                });

                var ua = navigator.userAgent;

                if (ua.match(/(iPhone|iPad)/i)) {
                    $("#dateAndTime").attr('type', 'datetime-local');
                    $('body').on('change', '#dateAndTime', function (e) {

                        $scope.listing.instantRequest.expiryDate = moment($('#dateAndTime').val()).
                            format('YYYY-MM-DDTHH:mm:ss.SSSZ');

                        if (!$scope.$$phase) {
                            $scope.$apply();
                        }
                    });
                } else {
                    $("#dateAndTime").datetimepicker({
                        minDate: (new Date()).setDate((new Date()).getDate() + 1),
                        collapse: true,
                        useCurrent: false
                    });

                    $('body').on('dp.change', '#dateAndTime', function (e) {
                        $scope.dateAndTime = $('#dateAndTime').val();
                        $scope.emailCampaign.deferredDateTime = $('#dateAndTime').data("DateTimePicker")
                            .date().format('YYYY-MM-DDTHH:mm:ss.SSSZ');
                    });
                }
            }

        }]);
