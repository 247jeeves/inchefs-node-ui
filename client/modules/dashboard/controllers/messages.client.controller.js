angular.module('dashboard').controller('MessagesController', ['$scope', 'BootStrap', '$state', '$timeout', '$http',
    function ($scope, BootStrap, $state, $timeout, $http) {
        // This provides BootStrap context.
        $scope.authentication = BootStrap;
        $scope.market = window._market;

        $scope.messages = [];
        $scope.message = {};
        $scope.isClosed = false;

        var subject = undefined;

        $scope.formatDate = function (date) {
            return moment(date).format('MM/DD/YYYY hh:mm A');
        }

        $scope.formatText = function(text){
            return text.replace(/\n/g,'<br>');
        }

        //Event Listener to Start Conv Loop
        $scope.$on('initMessages', function (data, args) {
            $scope.isClosed = args.isClosed || false;
            subject = args.subject;
            $scope.message = {
                fromId: args.fromId,
                toId: args.toId,
                fromName: args.fromName,
                toName: args.toName,
                subject: subject
            };
     
            loadMessages();
        });

        //Event Listener to send message from anywhere in the system, provided that initMessages is already called
        $scope.$on('sendDirectMessage', function(data, args){
            console.log("ushakiz message args "+JSON.stringify(args));
            $scope.sendMessage(args.text);
        });

        // Fetch Event Liscope.offer.answers = processedAnswers;
        //             $scope.offerServiceOrDecline(false);
        //                     }
        //
        //                             var offerStatus = function (status) {
        //
        var loadMessages = function () {

            if(subject === undefined){
                console.log("Subject can't be null");
                return;
            }

            $scope.isWaiting = true;
            $scope.success = "";
            $http.get($scope.authentication.api.baseUrl + "/users/"
                + $scope.authentication.user.id + "/messages"
                + "?subject=" + subject
            )
                .success(function (response) {
                    $scope.messages = response;
                    $scope.isWaiting = false;
                    $scope.error = {};

                }).error(function (response) {
                    errorHandler(response);
                });
        }

        // Send Message
        $scope.sendMessage = function (isDirect) {
            $scope.isWaiting = true;
            $scope.success = "";

            // To avoid displaying a message flash in the textarea field
            var message = $scope.message;
            isDirect = (isDirect === undefined)?false:isDirect;
            if(isDirect){
                message = {
                    fromId: $scope.message.fromId,
                    toId: $scope.message.toId,
                    fromName: $scope.message.fromName,
                    toName: $scope.message.toName,
                    subject: subject,
                    text: isDirect
                };
            }

            $http.post($scope.authentication.api.baseUrl + "/users/"
                + $scope.authentication.user.id + "/messages",
                message)
                .success(function (response) {
                    addMessageToList(response);
                    $scope.message.text = undefined;
                    $scope.isWaiting = false;
                    $scope.error = {};

                    $scope.$emit('messageSentSuccess');

                }).error(function (response) {
                    errorHandler(response);
                });
        }

        var addMessageToList = function(message){
            $scope.messages.push(message);
        }

        var errorHandler = function (response) {
            alert("Something went wrong, please notify this to support@wanteet.com");
            $scope.error = {};
            for (var idx in response.errors) {
                $scope.error[response.errors[idx]['field']] = {
                    value: response.errors[idx]['value'],
                    message: response.errors[idx]['message']
                };
            }
            $scope.isWaiting = false;
        }


    }
]);


angular.module('dashboard').controller('DialogFlowController', ['$scope', 'BootStrap', '$state', '$timeout', '$http', 
    function ($scope, BootStrap, $state, $timeout, $http) { 

        $scope.market = window._market;
        console.dir(window._market);
        $scope.dfMessage;
        $scope.messages = [];

        function newMessage(text, isReply, timeStamp) {
            this.text       = text;
            this.isReply    = isReply; 
            this.timeStamp  = timeStamp;
        }
       
        function moveToBottom() {
            var objDiv = document.getElementById("messagesContainer");
            objDiv.scrollTop = objDiv.scrollHeight;
        }
        ////Please remove this are backend is added with autoRoute
       
        $scope.sendDFMessage = function() {
            var accessToken = window.user.accessToken;
            $scope.market = window._market;
            $scope.messages.push(new newMessage( $scope.dfMessage, false, ""));
            moveToBottom();
           $http({
            method: 'GET',
            url : 'https://api.dialogflow.com/v1/query?'
                +'&lang=en'
                +'&query=' +$scope.dfMessage+' @accessToken=' +accessToken+ " @marketId=" +window._market.slug+ " @marketNo=" +window.user.id
                +'&sessionId=12345',
            headers: {
                'X-Auth-Token': undefined, 
                'X-Market-ID': undefined,
                'Authorization' : 'Bearer e9e680634d4842a685c3c114e36ae8c8'        
            }
          }).success(function (response) {
              console.dir(response);
                $scope.messages.push(new newMessage( response.result.speech, true, ""));
                setTimeout (moveToBottom(), 500);
            }).error(function (response) {
                console.dir(response);
            });
            $scope.dfMessage = "";
        }
    }
]);
