angular.module('dashboard').controller('AccountController', ['$scope', 'BootStrap', '$state', '$timeout', '$rootScope', 'Menus',
    function ($scope, BootStrap, $state, $timeout, $rootScope, Menus) {
        // This provides BootStrap context.
        $scope.authentication = BootStrap;

        //Setup Side Nav
        $scope.accountSideNav = Menus.getMenu('accountSideNav').items;

        //Action to perform on url routing inside current scope
        $rootScope.$on('$stateChangeSuccess',
            function (event, toState, toParams, fromState, fromParams) {
                if ((toState.name.match(/\./g) || []).length > 1) {;
                    Menus.selectMenuItemByLink('accountSideNav', toState.parent);
                }else{
                    Menus.selectMenuItemByLink('accountSideNav', toState.name);
                }
            });

        if ($state.current.name === undefined || $state.current.parent === undefined) {
            $state.transitionTo('account.notifications');
        }else{
            Menus.selectMenuItemByLink('accountSideNav', $state.current.name);
        }

        Menus.selectMenuItemByLink('dashboardTop', 'account.notifications');
    }
]);
