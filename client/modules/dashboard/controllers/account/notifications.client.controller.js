angular.module('dashboard').controller('NotificationsController', ['$scope', 'BootStrap', '$state', '$timeout', '$http',
    function ($scope, BootStrap, $state, $timeout, $http) {
        // This provides BootStrap context.
        $scope.authentication = BootStrap;

        $scope.notifications = $scope.authentication.profile.notifications;
        $scope.isWaiting = false;

        $scope.update = function () {
            $scope.isWaiting = true;
            $http.put($scope.authentication.api.baseUrl + "/users/"
                + $scope.authentication.user.id + "/profile/buyer/" + $scope.authentication.profile.buyerProfile.id,
                {"notifications": $scope.notifications})
                .success(function (response) {
                    $scope.authentication.profile.notifications = $scope.notifications;

                    $scope.success = "Saved!";
                    $timeout(function(){
                        $scope.success = "";
                    },5000);
                    $scope.isWaiting = false;
                    $scope.error = {};

                }).error(function (response) {
                    $scope.error = {};
                    for (var idx in response.errors) {
                        $scope.error[response.errors[idx]['field']] = {
                            value: response.errors[idx]['value'],
                            message: response.errors[idx]['message']
                        };
                    }
                    $scope.isWaiting = false;
                });
        };

    }
]);
