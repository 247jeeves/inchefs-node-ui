angular.module('dashboard').controller('SecurityController', ['$scope', 'BootStrap', '$state', '$timeout', '$http',
    function ($scope, BootStrap, $state, $timeout, $http) {
        // This provides BootStrap context.
        $scope.authentication = BootStrap;

        $scope.credentials;
        $scope.isWaiting = false;

        $scope.update = function () {
            $scope.isWaiting = true;
            $http.put($scope.authentication.api.baseUrl + "/users/" + $scope.authentication.user.id,
                $scope.credentials)
                .success(function (response) {

                    for(var idx in $scope.credentials){
                        $scope.credentials[idx] = "";
                    }

                    $scope.success = "Password changed successfully!";
                    $timeout(function(){
                        $scope.success = "";
                    },5000);
                    $scope.isWaiting = false;
                    $scope.error = {};

                }).error(function (response) {
                    $scope.error = {};
                    for (var idx in response.errors) {
                        $scope.error[response.errors[idx]['field']] = {
                            value: response.errors[idx]['value'],
                            message: response.errors[idx]['message']
                        };
                    }
                    $scope.isWaiting = false;
                });
        };
    }
]);
