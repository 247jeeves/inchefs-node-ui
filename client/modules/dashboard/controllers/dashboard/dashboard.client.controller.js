angular.module('dashboard').controller('DashboardController', ['$scope', 'BootStrap', 'WS',
    function ($scope, BootStrap, WS) {
        // This provides BootStrap context.
        $scope.authentication = BootStrap;

    }
]);
