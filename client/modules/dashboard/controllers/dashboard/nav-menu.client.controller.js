angular.module('dashboard').controller('NavMenuController', ['$scope', 'BootStrap', '$state',
    '$timeout', '$rootScope', 'Menus',
    function ($scope, BootStrap, $state, $timeout, $rootScope, Menus) {
        // This provides BootStrap context.
        $scope.authentication = BootStrap;

        $scope.nav = Menus.getMenu('dashboardTop').items;
        $scope.currentSubs = BootStrap.subscription
	$scope.subsInfo = (typeof $scope.currentSubs["market-subscription-info"] !== "undefined") ? $scope.currentSubs["market-subscription-info"] : ''
        $scope.subsInfo = ($scope.subsInfo != '') ? JSON.parse($scope.subsInfo) : ''
        $scope.currentPlan = ($scope.subsInfo != '') ? $scope.subsInfo[0].plan.name : ''
        $scope.planStatus = ($scope.subsInfo != '') ? $scope.subsInfo[0].status: ''
	
    /*
	if($scope.planStatus != "trialing" && BootStrap.market.name != "247jeeves" && BootStrap.market.name != "inChefs" && BootStrap.market.name != "wanteet" && BootStrap.market.name != "codoods" ){
		if($scope.planStatus != "active" &&  $state.current.name != "myMarket.billing"){
			window.location.href = "market-admin#!/my-market/billing"	
		}
	}
    */

        //Action to perform on url routing inside current scope
        $rootScope.$on('$stateChangeSuccess',
            function (event, toState, toParams, fromState, fromParams) {
/*
		if($scope.planStatus != "trialing" && BootStrap.market.name != "247jeeves"  && BootStrap.market.name != "inChefs" && BootStrap.market.name != "wanteet" && BootStrap.market.name != "codoods"){
			if($scope.planStatus != "active" &&  $state.current.name != "myMarket.billing"){
				window.location.href = "market-admin#!/my-market/billing"	
			}
		}
*/

	        if ($state.current.parent === undefined) {

                    Menus.selectMenuItemByLink('dashboardTop', toState.name);
                    if (fromState.name.indexOf(toState.name) != -1) {
                        $state.transitionTo(fromState.name);
                    }

                } else {
                    if ($state.current.parent.indexOf('.') == -1) {
                        Menus.selectMenuItemByLink('dashboardTop', $state.current.parent);
                    } else {
                        Menus.selectMenuItemByLink('dashboardTop', $state.current.parent.substring(0,
                            $state.current.parent.indexOf('.')));
                    }
                }
            });

        //TODO: Refactor this code to efficiently set the default state
        if (window.location.hash == '#!/') {
            if ($state.current.name === undefined || $state.current.parent === undefined) {
                if (BootStrap.user.isBuyer()) {
                    $state.transitionTo('requests.byou');
                } else {
                    $state.transitionTo('requests.ayou');
                }
            }
        }
    }
]);
