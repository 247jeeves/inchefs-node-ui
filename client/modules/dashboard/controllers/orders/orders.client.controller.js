angular.module('dashboard').controller('OrdersController',
    ['$scope', 'BootStrap', '$state',
        '$timeout', '$rootScope', 'Menus', '$http', '$filter',
        '$interval', 'localStorageService',
        function ($scope, BootStrap, $state, $timeout,
                  $rootScope, Menus, $http, $filter,
                  $interval, localStorageService) {
            // This provides BootStrap context.
            $scope.authentication = BootStrap;

            var offset = 0;
            var max = 10;

            var embeddedPPFlow;
            var checkiFrame;
            $scope.market = BootStrap.market;
            $scope.total = 0;
            $scope.orders = [];
            $scope.offer = {};

            //$scope.previousState = 'orders.ayou';

            $scope.formatDate = function (date) {
                return moment(date).format('MM/DD/YYYY hh:mm A');
            }

            // Fetch Single Order with Deep Linking
            $scope.getOrder = function () {
                if (isNaN($state.params.id)) {
                    return;
                }
                $scope.order = undefined;
                if ($scope.orders.length > 0) {
                    for (var idx in $scope.orders) {
                        if ($scope.orders[idx].id == $state.params.id) {
                            $scope.order = $scope.orders[idx];
                            if ($scope.order.hasOwnProperty('request')) {
                                //getRequest($scope.order.request.id);
                            }
                            //initMessages();
                            Menus.selectMenuItemByLink('ordersSideNav', $scope.previousState);
                            return;
                        }
                    }
                } else {
                    if ($scope.order === undefined) {
                        $http.get($scope.authentication.api.baseUrl + "/users/"
                            + $scope.authentication.user.id + "/orders/"
                            + $state.params.id
                        )
                            .success(function (response) {
                                $scope.order = response;
                                console.log($scope.order);
                                if ($scope.order.hasOwnProperty('request')) {
                                    //getRequest($scope.order.request.id);
                                    //initMessages();
                                }


                                Menus.selectMenuItemByLink('ordersSideNav', $scope.previousState);
                                $scope.isWaiting = false;
                                $scope.error = {};
                            }).error(function (response) {
                                errorHandler(response);
                            });
                    }
                }
            }

            $scope.initTimeSuccess = function () {
                $scope.getOrder();
            }


            $scope.initOrderPayment = function () {
                $scope.getOrder();
            }


            $scope.doPayment = function () {
                if (!$scope.offer.hasOwnProperty('id') && $scope.offer.status !== 'TENTATIVE') {
                    $("#offerService").find('[data-cancel-button]').on('click', function (e) {
                        $("#offerService").foundation('reveal', 'close');
                        e.preventDefault();
                    });

                    $("#offerService").find('[data-confirm-button]').on('click', function (e) {
                        $("#offerService").foundation('reveal', 'close');
                        offerServiceOrDecline(false);
                        e.preventDefault();
                    });
                    $timeout(function () {
                        $("#offerService").foundation('reveal', 'open');
                    }, 10);
                } else {
                    offerServiceOrDecline(false);
                }
                return false;
            };


            var errorHandler = function (response) {
                $scope.isWaiting = false;
                $scope.error = {};
                if (response.errors) {
                    for (var idx in response.errors) {
                        $scope.error[response.errors[idx]['field']] = {
                            value: response.errors[idx]['value'],
                            message: response.errors[idx]['message']
                        };
                    }
                }
            };

            var getRequest = function (requestId) {
                $http.get($scope.authentication.api.baseUrl + "/users/"
                    + $scope.authentication.user.id + "/requests/"
                    + requestId
                )
                    .success(function (response) {
                        $scope.request = response;

                        $scope.isWaiting = false;
                        $scope.error = {};
                    }).error(function (response) {
                        errorHandler(response);
                    });
            };

            var initMessages = function () {
                $timeout(function () {

                    var toId = ($scope.authentication.user.id == $scope.order.buyer.id) ? $scope.order.seller.id : $scope.order.buyer.id;
                    var toName = ($scope.authentication.user.id == $scope.order.buyer.id) ? $scope.order.seller.displayName : $scope.order.buyer.displayName;

                    $scope.$broadcast('initMessages', {
                        subject: $scope.order.request.displayId + "-" + $scope.order.seller.id + $scope.order.buyer.id,
                        fromId: $scope.authentication.user.id,
                        toId: toId,
                        fromName: $scope.authentication.profile.displayName,
                        toName: toName,
                        isClosed: ($scope.order.status === 'COMPLETE')
                    });
                }, 800);
            }

            $scope.pay = function (provider) {
                switch (provider) {
                    case 'paypal':
                        initPayPalPayment();
                        break;
                    case 'stripe':
                        initStripePayment();
                        break;
                }
            }

            var initStripePayment = function () {

                var stripePubKey = window._stripe.public;
                var img = $scope.market.settings.logo || '/assets/img/wanteet.png';

                if (typeof stripePubKey !== 'undefined') {
                    var stripeHandler = StripeCheckout.configure({
                        key: stripePubKey,
                        name: $scope.market.name,
                        image: img,
                        email: $scope.authentication.profile.email,
                        billingAddress: true,
                        bitcoin: true,
                        token: function (token, args) {
                            $scope.isWaiting = false;

                            var orderData = {
                                amount: $scope.order.invoice.payableAmount.toFixed(2) * 100 / 100,
                                paymentInfo: {
                                    provider: 'stripe',
                                    token: token
                                }
                            };

                            commitOrder(orderData);
                        },
                        opened: function (e) {
                            $scope.isWaiting = true;
                        },
                        closed: function (e) {
                            $scope.isWaiting = false;
                        }
                    });

                    stripeHandler.open({
                        description: 'Payable Amount : ($' + $scope.order.invoice.payableAmount.toFixed(2) * 100 / 100 + ')',
                        amount: Math.round($scope.order.invoice.payableAmount.toFixed(2) * 100)
                    });
                }
            }

            var initPayPalPayment = function () {

                $http.get($scope.authentication.api.baseUrl + "/payment/paypal" +
                    "?amount=" + $scope.order.invoice.payableAmount +
                    "&orderId=" + $scope.order.id
                )
                    .success(function (response) {

                        $scope.paypalPayKey = response.payKey;
                        embeddedPPFlow = new PAYPAL.apps.DGFlow({expType: 'light'});
                        embeddedPPFlow.startFlow("https://www.sandbox.paypal.com/webapps/adaptivepayment/flow/pay?payKey=" + $scope.paypalPayKey);

                        checkiFrame = $interval(function () {
                            try {
                                var elems = $('#PPDGFrame iframe').contents().find('.result');
                                if (elems) {
                                    if (elems[0].id.indexOf('success') != -1) {
                                        paypalFlowCallback(true);
                                    } else {
                                        paypalFlowCallback(false);
                                    }
                                }
                            } catch (ex) {
                                //Nothing to catch
                            }
                        }, 100);


                    }).error(function (response) {
                        errorHandler(response);
                    });
            }

            var paypalFlowCallback = function (success) {
                $interval.cancel(checkiFrame);
                embeddedPPFlow.closeFlow();
                $("#PPDGFrame").remove();
                if (success) {
                    console.log("Success! Call Details API for " + $scope.paypalPayKey);

                    var orderData = {
                        amount: $scope.order.invoice.payableAmount,
                        paymentInfo: {
                            provider: 'paypal',
                            payKey: $scope.paypalPayKey
                        }
                    };

                    commitOrder(orderData);

                } else {
                    console.log("Paypal just got cancelled :(");
                }
            }

            var commitOrder = function (orderData) {

                var orderId = $scope.order.id;
                if (isNaN($scope.order.id)) {
                    orderId = $state.params.id;
                }

                $scope.paymentConfirmed = true;
                $http.patch($scope.authentication.api.baseUrl + "/users/"
                    + $scope.authentication.user.id + "/orders/"
                    + orderId,
                    orderData).success(function (response) {
                        $scope.order = response;

                        if ($scope.order.subscription) {
                            $state.go("orders.paySubscription");
                        } else {
                            $state.go("orders.paySuccess");
                        }

                    }).error(function (response) {
                        console.log(response);
                    });
            }

            $scope.initSubscriptionResultScreen = function () {
                if (!$scope.order) {
                    $scope.order = localStorageService.get('freeSubscriptionOrder');
                    localStorageService.remove('freeSubscriptionOrder');
                }

                $timeout(function () {
                    location.href = "/" + BootStrap.market.slug + "/dashboard/#!/profile/subscriptions";
                }, 4000);
            }

        }
    ]);
