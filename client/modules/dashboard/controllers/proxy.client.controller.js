angular.module('dashboard').controller('ProxyController', ['$scope', '$http', '$location', 'BootStrap', '$timeout',
    function ($scope, $http, $location, BootStrap, $timeout) {
        $scope.authentication = BootStrap;

        $scope.proxy = function () {
            console.log($scope.credentials);
            $scope.credentials["token"] = BootStrap.user.accessToken;
            $http.post('/auth/proxy', $scope.credentials).success(function (response) {
                // If successful we assign the response to the global user model
                $scope.authentication.user = response;

                // And redirect the user
                if (response.permissions.indexOf('ROLE_SELLER') != -1) {
                    location.href = '/dashboard';
                } else {
                    location.href = '/';
                    location.hash = "";
                    //If the current page is '/'
                    $timeout(function () {
                        location.reload();
                    }, 10);
                }

            }).error(function (response) {
                $scope.error = response.message;
            });
        };
    }
]);
