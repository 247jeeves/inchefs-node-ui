var keystone = require('keystone'),
    Types = keystone.Field.Types;

/**
 * Post Model
 * ==========
 */

var Page = new keystone.List('Page', {
    map: {name: 'title'},
    autokey: {path: 'slug', from: 'title', unique: 'false'}
});

Page.add({
    title: {type: String, required: true},
    state: {type: Types.Select, options: 'draft, published, archived', default: 'published', index: true},
    parent: {type: String, default: 'wanteet', initial: true},
    keywords: {type: String},
    description: {type: String},
    image: {type: Types.CloudinaryImage},
    content: {
        data: {type: Types.Html, wysiwyg: true, height: 400}
    }
});

Page.schema.virtual('content.full').get(function () {
    return this.content.data;
});

Page.schema.index({slug: 1, parent: 1}, {unique: true});

Page.defaultColumns = 'title, slug, parent|20%';
Page.register();
