'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
		Schema = mongoose.Schema,
		crypto = require('crypto');

/**
 * A Validation function for local strategy properties
 */
var validateLocalStrategyProperty = function(property) {
	return ((this.provider !== 'local' && !this.updated) || property.length);
};

/**
 * User Schema
 */
var UserSchema = new Schema({
	id: {
		type: Number,
		unique: true
	},
	firstName: {
		type: String,
		trim: true,
		default: '',
		validate: [validateLocalStrategyProperty, 'Please fill in your first name']
	},
	lastName: {
		type: String,
		trim: true,
		default: '',
		validate: [validateLocalStrategyProperty, 'Please fill in your last name']
	},
	displayName: {
		type: String,
		trim: true
	},
	username: {
		type: String,
		unique: 'Duplicate User in Cache',
		required: 'Please fill in a username',
		match: [/.+\@.+\..+/, 'Please fill a valid email address'],
		trim: true
	},
	provider: {
		type: String,
		required: 'Provider is required'
	},
	providerData: {},
	additionalProvidersData: {},
	permissions: {
		type: [{
			type: String,
			enum: ['ROLE_BUYER', 'ROLE_ADMIN', 'ROLE_SELLER']
		}],
		default: ['ROLE_BUYER']
	},
	updated: {
		type: Date
	},
	created: {
		type: Date,
		default: Date.now
	},
	memberSince: {
		type: Date
	},
	accessToken:{
		type: String
	},
	zohoToken:{
		type: String
	}	
});


/**
 * Find possible not used username
 */
UserSchema.statics.findUniqueUsername = function(username, suffix, callback) {
	var _this = this;
	var possibleUsername = username + (suffix || '');

	_this.findOne({
		username: possibleUsername
	}, function(err, user) {
		if (!err) {
			if (!user) {
				callback(possibleUsername);
			} else {
				return _this.findUniqueUsername(username, (suffix || 0) + 1, callback);
			}
		} else {
			callback(null);
		}
	});
};

var keystone = require('keystone')
var User = new keystone.List('User',{
	hidden: true
});

User.schema = UserSchema;

User.defaultColumns = 'firstName, lastName, username';

User.schema.virtual('canAccessKeystone').get(function() {
	return (this.permissions.indexOf('ROLE_ADMIN') != -1);
});

User.register();

