'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
		Schema = mongoose.Schema;

var EventSchema = new Schema({
	startDate:{
		type: String,
        required: [true, 'Event start time required']
	},
	endDate:{
        type: String,
        required: [true, 'Event end time required']
    },
    eventTitle:{
		type: String,
		required: [true, 'Event Title required']
    },
    updated: {
         type: Date,
         default: '0'
     },
	createdAt:{
		type: Date,
		default: Date.now()
	}

}, {_id: false});

var conversationSchema = new Schema({
	conversationId: {
		type: String,
		unique: true,
		required: [true, 'conversation Id required']
        },
	createdAt:{
		type: Date,
		default: Date.now()
	}


}, {_id: false});



/**
 * Lead Schema
 */
var LeadSchema = new Schema({
	firstName: {
		type: String,
		trim: true,
		default: '',
		required: 'Please fill in first name',
	},
	lastName: {
		type: String,
		trim: true,
		default: '',
		required: 'Please fill in last name',
	},
	email: {
		type: String,
		required: 'Please fill in email',
		match: [/.+\@.+\..+/, 'Please fill a valid email address'],
		trim: true
	},
	marketId: {
		type: String,
		required: 'Service Center as marketId is required'
	},
	employeesNumber:{
		type: String,
		default:''
	},
	designation:{
		type: String,
		default: ''
	},
	timezone:{
		type: String,
		default: ''
	},
    events:[EventSchema],
    conversations: [conversationSchema],
	zohoId:{
		type:String,
	},
	sfId:{
		type:String,
	},
	updated: {
		type: Date,
		default: '0'
	},
	created: {
		type: Date,
		default: Date.now
	}
});


mongoose.model('Lead',LeadSchema);
