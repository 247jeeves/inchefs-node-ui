'use strict';

module.exports = {
	db: process.env.MONGOHQ_URL || process.env.MONGOLAB_URI || 'mongodb://' + (process.env.DB_1_PORT_27017_TCP_ADDR || 'localhost') + '/247jeeves',
	//db: 'mongodb://localhost/247jeeves',
	app: {
		title: '247jeeves - Development Environment'
	},
	inchefs: {
		baseUrl : 'https://api.247jeeves.com/api/v1',
		publicUrl: 'https://api.247jeeves.com/public/api/v1',
		subscribeUrl: 'ws://push.247jeeves.com/subscribe',
		// baseUrl : 'http://localhost:8080/api/v1',
		// publicUrl: 'http://localhost:8080/public/api/v1',
		// subscribeUrl: 'ws://localhost:7999/subscribe',
		hall: '247jeeves-hall'
	},
	facebook: {
		clientID: process.env.FACEBOOK_ID || '315517558572235',
		clientSecret: process.env.FACEBOOK_SECRET || 'e0bd29435e54f9d9646b356e75b86aa8',
		callbackURL: '/auth/facebook/callback'
	},
	zohocrm: {
		clientID: process.env.ZOHO_ID || '1000.OMCZDPLXZRUA84309S2LJ6CET03B53',
		clientSecret: process.env.ZOHO_SECRET || 'e61978b01b6b8fcbc5c674d51c93ce0e56cf7c7ac3',
		callbackURL: '/auth/zoho/callback'
	},
	twitter: {
		clientID: process.env.TWITTER_KEY || 'CONSUMER_KEY',
		clientSecret: process.env.TWITTER_SECRET || 'CONSUMER_SECRET',
		callbackURL: '/auth/twitter/callback'
	},
	google: {
		clientID: process.env.GOOGLE_ID || '648529906295-bsl1jvdptje6oogfptn9nalh576og2d2.apps.googleusercontent.com',
		clientSecret: process.env.GOOGLE_SECRET || 'i0kXwQEpr_2TVZheca_FpjOF',
		callbackURL: '/auth/google/callback'
	},
	linkedin: {
		clientID: process.env.LINKEDIN_ID || '758olcw902ymy5',
		clientSecret: process.env.LINKEDIN_SECRET || 'q6TrDlCckQ0r2xFI',
		callbackURL: '/auth/linkedin/callback'
	},
	github: {
		clientID: process.env.GITHUB_ID || 'APP_ID',
		clientSecret: process.env.GITHUB_SECRET || 'APP_SECRET',
		callbackURL: '/auth/github/callback'
	},
	mailer: {
		from: process.env.MAILER_FROM || 'MAILER_FROM',
		options: {
			service: process.env.MAILER_SERVICE_PROVIDER || 'MAILER_SERVICE_PROVIDER',
			auth: {
				user: process.env.MAILER_EMAIL_ID || 'MAILER_EMAIL_ID',
				pass: process.env.MAILER_PASSWORD || 'MAILER_PASSWORD'
			}
		}
	},
	nginx: {
		configPath: '/usr/local/etc/nginx/servers/',
		tempPath: '/tmp/',
		logPath: '/tmp/nginx/',
		appRootPath: '/Users/v1p/nodejs/inchefs-foundation/build',
		appUrl: '127.0.0.1:3000',
		port: '7000'
	}
};
