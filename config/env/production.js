'use strict';

module.exports = {
	db: process.env.MONGOHQ_URL || process.env.MONGOLAB_URI || 'mongodb://' + (process.env.DB_1_PORT_27017_TCP_ADDR || 'localhost') + '/247jeeves',
	assets: {
		lib: {
			css: [
				'public/lib/bootstrap/dist/css/bootstrap.min.css',
				'public/lib/bootstrap/dist/css/bootstrap-theme.min.css',
			],
			js: [
				'public/lib/angular/angular.min.js',
				'public/lib/angular-resource/angular-resource.js', 
				'public/lib/angular-cookies/angular-cookies.js', 
				'public/lib/angular-animate/angular-animate.js', 
				'public/lib/angular-touch/angular-touch.js', 
				'public/lib/angular-sanitize/angular-sanitize.js', 
				'public/lib/angular-ui-router/release/angular-ui-router.min.js',
				'public/lib/angular-ui-utils/ui-utils.min.js',
				'public/lib/angular-bootstrap/ui-bootstrap-tpls.min.js'
			]
		},
		css: 'public/dist/application.min.css',
		js: 'public/dist/application.min.js'
	},
	inchefs: {
		baseUrl : 'https://api.247jeeves.com/api/v1',
		publicUrl: 'https://api.247jeeves.com/public/api/v1',
		subscribeUrl: 'ws://push.247jeeves.com/subscribe',
		hall: '247jeeves-hall'
	},
	facebook: {
		clientID: process.env.FACEBOOK_ID || '1464760997187947',
		clientSecret: process.env.FACEBOOK_SECRET || 'a4885db53da12794669da509691dd5da',
		callbackURL: '/auth/facebook/callback'
	},
	zohocrm: {
		clientID: process.env.ZOHO_ID || '1000.OMCZDPLXZRUA84309S2LJ6CET03B53',
		clientSecret: process.env.ZOHO_SECRET || 'e61978b01b6b8fcbc5c674d51c93ce0e56cf7c7ac3',
		callbackURL: '/auth/zoho/callback'
	},
	twitter: {
		clientID: process.env.TWITTER_KEY || 'CONSUMER_KEY',
		clientSecret: process.env.TWITTER_SECRET || 'CONSUMER_SECRET',
		callbackURL: '/auth/twitter/callback'
	},
	google: {
		clientID: process.env.GOOGLE_ID || '190735178357-kh6fcf94v8urmdiivgb9epmof2ge81oc.apps.googleusercontent.com',
		clientSecret: process.env.GOOGLE_SECRET || 'DyCc6jRFv5JBAbC8_Jba179b',
		callbackURL: '/auth/google/callback'
	},
	linkedin: {
		clientID: process.env.LINKEDIN_ID || '758olcw902ymy5',
		clientSecret: process.env.LINKEDIN_SECRET || 'q6TrDlCckQ0r2xFI',
		callbackURL: '/auth/linkedin/callback'
	},
	github: {
		clientID: process.env.GITHUB_ID || 'APP_ID',
		clientSecret: process.env.GITHUB_SECRET || 'APP_SECRET',
		callbackURL: '/auth/github/callback'
	},
	mailer: {
		from: process.env.MAILER_FROM || 'MAILER_FROM',
		options: {
			service: process.env.MAILER_SERVICE_PROVIDER || 'MAILER_SERVICE_PROVIDER',
			auth: {
				user: process.env.MAILER_EMAIL_ID || 'MAILER_EMAIL_ID',
				pass: process.env.MAILER_PASSWORD || 'MAILER_PASSWORD'
			}
		}
	},
	nginx: {
		configPath: '/etc/nginx/conf.d/',
		tempPath: '/tmp/',
		logPath: '/var/log/nginx/',
		appRootPath: '/home/ec2-user/build/247jeeves-node-ui/build',
		appUrl: '127.0.0.1:3000',
		port: '80'
	}
};
