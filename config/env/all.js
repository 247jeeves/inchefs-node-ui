'use strict';

module.exports = {
	app: {
		title: '247Jeeves | Context-aware, Hyper-local Marketplaces',
		description: 'The Marketplace factory!',
		keywords: 'context,aware,marketplace,local,hyperlocal,social,network'
	},
	port: process.env.PORT || 3000,
	templateEngine: 'swig',
	sessionSecret: 'WanteetNodeApp',
	sessionCollection: 'sessions',
	assets: {
		lib: {
			css: [
				'build/assets/css/app.css'
			],
			js: [
				'build/assets/js/foundation.js'
			]
		},
		css: [
			'public/modules/**/css/*.css'
		],
		js: [
			'public/config.js',
			'public/application.js',
			'public/modules/*/*.js',
			'public/modules/*/*[!tests]*/*.js',
			'build/assets/js/app.js',
			'build/assets/js/routes.js',
		],
		tests: [
			'public/lib/angular-mocks/angular-mocks.js',
			'public/modules/*/tests/*.js'
		]
	}
};
