'use strict';

module.exports = {
	db: process.env.MONGOHQ_URL || process.env.MONGOLAB_URI || 'mongodb://' + (process.env.DB_1_PORT_27017_TCP_ADDR || 'localhost') + '/247jeeves',
	app: {
		title: '247jeeves - QA Environment'
	},
	assets: {
		lib: {
			css: [
				'public/lib/bootstrap/dist/css/bootstrap.min.css',
				'public/lib/bootstrap/dist/css/bootstrap-theme.min.css',
			],
			js: [
				'public/lib/angular/angular.min.js',
				'public/lib/angular-resource/angular-resource.js', 
				'public/lib/angular-cookies/angular-cookies.js', 
				'public/lib/angular-animate/angular-animate.js', 
				'public/lib/angular-touch/angular-touch.js', 
				'public/lib/angular-sanitize/angular-sanitize.js', 
				'public/lib/angular-ui-router/release/angular-ui-router.min.js',
				'public/lib/angular-ui-utils/ui-utils.min.js',
				'public/lib/angular-bootstrap/ui-bootstrap-tpls.min.js'
			]
		},
		css: 'public/dist/application.min.css',
		js: 'public/dist/application.min.js'
	},
	inchefs: {
		baseUrl : 'https://qa.api.247jeeves.com/api/v1',
		publicUrl : 'https://qa.api.247jeeves.com/public/api/v1',
		subscribeUrl: 'ws://push.app.247jeeves.com/subscribe',
		hall: '247jeeveshallqadev'
	},
	facebook: {
		clientID: process.env.FACEBOOK_ID || '1493625717598886',
		clientSecret: process.env.FACEBOOK_SECRET || '22bdf31ed42052b53941c7fb2f79bfa2',
		callbackURL: '/auth/facebook/callback'
	},
	zohocrm: {
		clientID: process.env.ZOHO_ID || '1000.OMCZDPLXZRUA84309S2LJ6CET03B53',
		clientSecret: process.env.ZOHO_SECRET || 'e61978b01b6b8fcbc5c674d51c93ce0e56cf7c7ac3',
		callbackURL: '/auth/zoho/callback'
	},
	twitter: {
		clientID: process.env.TWITTER_KEY || 'CONSUMER_KEY',
		clientSecret: process.env.TWITTER_SECRET || 'CONSUMER_SECRET',
		callbackURL: '/auth/twitter/callback'
	},
	google: {
		clientID: process.env.GOOGLE_ID || '663766406928-6o7g17smla42hofutspdv25db1i6gtq8.apps.googleusercontent.com',
		clientSecret: process.env.GOOGLE_SECRET || '6OsHiZluz_wHRa3s8GjxPffX',
		callbackURL: '/auth/google/callback'
	},
	linkedin: {
		clientID: process.env.LINKEDIN_ID || '758olcw902ymy5',
		clientSecret: process.env.LINKEDIN_SECRET || 'q6TrDlCckQ0r2xFI',
		callbackURL: '/auth/linkedin/callback'
	},
	github: {
		clientID: process.env.GITHUB_ID || 'APP_ID',
		clientSecret: process.env.GITHUB_SECRET || 'APP_SECRET',
		callbackURL: '/auth/github/callback'
	},
	mailer: {
		from: process.env.MAILER_FROM || 'MAILER_FROM',
		options: {
			service: process.env.MAILER_SERVICE_PROVIDER || 'MAILER_SERVICE_PROVIDER',
			auth: {
				user: process.env.MAILER_EMAIL_ID || 'MAILER_EMAIL_ID',
				pass: process.env.MAILER_PASSWORD || 'MAILER_PASSWORD'
			}
		}
	},
	nginx: {
		configPath: '/etc/nginx/conf.d/',
		tempPath: '/tmp/',
		logPath: '/var/log/nginx/',
		appRootPath: '/home/ec2-user/build/247jeeves-node-ui/build',
		appUrl: '127.0.0.1:3000',
		port: '80'
	}
};
