'use strict';

/**
 * Module dependencies.
 */
var passport = require('passport'),
	url = require('url'),
	ZohoCRMStrategy = require('passport-zoho-crm').Strategy,
	config = require('../config'),
	users = require('../../server/controllers/users.server.controller');

module.exports = function() {
	// Use zoho strategy
	passport.use(new ZohoCRMStrategy({
        clientID: config.zohocrm.clientID,
        clientSecret: config.zohocrm.clientSecret,
        redirect_URL: config.zohocrm.callbackURL,
        scope: 'ZohoCRM.crmdataaccess.ALL',
		response_type: 'code',
		passReqToCallback: true,
        access_type: 'offline'
		},
		function(req, accessToken, refreshToken, profile, done) {

			var debug = require('debug')('247jeeves| zohocrm |strategy');
			debug(req.market);
			debug('---------------------------------- Level 2');

			// Set the provider data and include tokens
			var providerData = profile._json;
			providerData.accessToken = accessToken;
            providerData.refreshToken = refreshToken;
            
            console.log("!!!!!!!!!! Sanjeet- Inside Zoho Strategy"+ JSON.stringify(profile,"",2));

			// Create the user OAuth profile
			var providerUserProfile = {
				firstName: profile.name.givenName,
				lastName: profile.name.familyName,
				displayName: profile.displayName,
				email: profile.emails[0].value,
				username: profile.username,
				provider: 'zohocrm',
				providerIdentifierField: 'id',
				providerData: providerData
			};

			// Save the user OAuth profile
			users.saveOAuthUserProfile(req, providerUserProfile, done);
		}
	));
};
