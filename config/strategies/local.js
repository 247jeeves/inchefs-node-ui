'use strict';

/**
 * Module dependencies.
 */
var passport = require('passport'),
    LocalStrategy = require('passport-local').Strategy,
    User = require('mongoose').model('User'),
    request = require('request'),
    config = require('../config.js'),
    debug = require('debug')('247jeeves|LocalStrategy');


module.exports = function () {
    // Use local strategy
    passport.use('local', new LocalStrategy({
            usernameField: 'username',
            passwordField: 'password',
            passReqToCallback: true
        },
        function (req, username, password, done) {
	var marketId = req.headers['x-market-id'] || req.market || 'wanteet';
	 request.post(config.inchefs.baseUrl + "/login", {
                headers: {
                    'Content-Type': 'application/json',
                    'X-Market-ID': marketId
                },
                json: true,
                body: {
                    username: username,
                    password: password
                }
            }, function (err, response, body) {
                if (err) {
                    debug(err);
                    return done(err);
                }
                if (response.statusCode == 401) {
                    debug(401);
                    return done(null, false, {
                        message: 'Unknown user or invalid password'
                    });
                }
                if (!body) {
                    debug("No Body!");
                    return done(null, false, {
                        message: 'Unknown user or invalid password'
                    });
                }

                User.findOne({username: body.username}, function(err, user){
                    if(err){
                        console.log(err);
                    }else{
                        // If no user found in cache - create new one
                        if(!user){
                            user = new User(body);
                            user.save(function(err) {
                                if (err) {
                                    console.log(err);
                                    return done(null, false, {
                                        message: 'Server failure! Please try after some time'
                                    });
                                }
                            });
                            return done(null, user);
                        }
                        // Update existing user with latest accessToken
                        else{
                            for(var key in body){
                                if(body.hasOwnProperty(key)){
                                    user[key] = body[key];
                                }
                            }

                            user.save(function(err) {
                                if (err) {
                                    console.log("Error in Update");
                                    return done(null, false, {
                                        message: 'Server failure! Please try after some time'
                                    });
                                }
                            })
                        }
                        return done(null, user);
                    }
                });
            });
        }
    ));
};
