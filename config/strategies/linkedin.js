'use strict';

/**
 * Module dependencies.
 */
var passport = require('passport'),
	url = require('url'),
	LinkedInStrategy = require('passport-linkedin-oauth2').Strategy,
	config = require('../config'),
	users = require('../../server/controllers/users.server.controller');

module.exports = function() {
	// Use linkedin strategy
	passport.use(new LinkedInStrategy({
			clientID: config.linkedin.clientID,
			clientSecret: config.linkedin.clientSecret,
			callbackURL: config.linkedin.callbackURL,
			passReqToCallback: true,
			scope: ['r_emailaddress', 'r_basicprofile']
		},
		function(req, accessToken, refreshToken, profile, done) {
			// Set the provider data and include tokens
			var debug = require('debug')('247jeeves|linkedin|strategy');
			debug(req.user);
			debug(req.market);

			var providerData = profile._json;
			providerData.accessToken = accessToken;
			providerData.refreshToken = refreshToken;

			// Create the user OAuth profile
			var providerUserProfile = {
				firstName: profile.name.givenName,
				lastName: profile.name.familyName,
				displayName: profile.displayName,
				email: profile.emails[0].value,
				username: profile.username,
				provider: 'linkedin',
				providerIdentifierField: 'id',
				providerData: providerData
			};

			// Save the user OAuth profile
			users.saveOAuthUserProfile(req, providerUserProfile, done);
		}
	));
};
