'use strict';

/**
 * Module dependencies.
 */
var passport = require('passport'),
    LocalStrategy = require('passport-local').Strategy,
    User = require('mongoose').model('User'),
    request = require('request'),
    config = require('../config.js');
var debug = require('debug')('247jeeves|proxy|login');


module.exports = function () {
    // Use local strategy
    passport.use('proxy', new LocalStrategy({
            usernameField: 'username',
            passwordField: 'token',
            passReqToCallback: true
        },
        function (req, username, token, done) {

            request.post(config.inchefs.baseUrl + "/proxy/login", {
                headers: {
                    'Content-Type': 'application/json',
                    'X-Auth-Token': token,
                    'X-Market-ID': req.headers['x-market-id']
                },
                json: true,
                body: {
                    username: username
                }
            }, function (err, response, body) {
                if (err) {
                    return done(err);
                }
                if (response.statusCode == 401) {
                    return done(null, false, {
                        message: 'Unknown user or invalid password'
                    });
                }
                if (!body) {
                    return done(null, false, {
                        message: 'Unknown user or invalid password'
                    });
                }

                debug(body);

                User.findOne({username: body.username}, function (err, user) {
                    if (err) {
                        console.log(err);
                    } else {
                        // If no user found in cache - create new one
                        if (!user) {
                            user = new User(body);
                            user.save(function (err) {
                                if (err) {
                                    console.log(err);
                                    return done(null, false, {
                                        message: 'Server failure! Please try after some time'
                                    });
                                }
                            });
                            return done(null, user);
                        }
                        // Update existing user with latest accessToken
                        else {
                            for (var key in body) {
                                if (body.hasOwnProperty(key)) {
                                    user[key] = body[key];
                                }
                            }

                            user.save(function (err) {
                                if (err) {
                                    console.log("Error in Update");
                                    return done(null, false, {
                                        message: 'Server failure! Please try after some time'
                                    });
                                }
                            })
                        }
                        return done(null, user);
                    }
                });
            });
        }
    ));
};
