'use strict';

/**
 * Module dependencies.
 */
var fs = require('fs'),
    http = require('http'),
    https = require('https'),
    express = require('express'),
    morgan = require('morgan'),
    bodyParser = require('body-parser'),
    session = require('express-session'),
    compress = require('compression'),
    methodOverride = require('method-override'),
    cookieParser = require('cookie-parser'),
    helmet = require('helmet'),
    passport = require('passport'),
    mongoStore = require('connect-mongo')({
        session: session
    }),
    flash = require('connect-flash'),
    config = require('./config'),
    consolidate = require('consolidate'),
    path = require('path'),
    multer = require('multer'),
    keystone = require('keystone');

var cors = require('cors')

module.exports = function (db) {
    // Initialize express app
    var app = express();
    //debug statement
    console.log('In express.js');
    // Globbing model files
    config.getGlobbedFiles('./server/models/**/*.js').forEach(function (modelPath) {
        require(path.resolve(modelPath));
    });

    // Setting application local variables
    app.locals.title = config.app.title;
    app.locals.description = config.app.description;
    app.locals.keywords = config.app.keywords;
    app.locals.facebookAppId = config.facebook.clientID;
    app.locals.jsFiles = config.getJavaScriptAssets();
    app.locals.cssFiles = config.getCSSAssets();
    app.locals.api = config.inchefs;
    // Passing the request url to environment locals
    app.use(function (req, res, next) {
        res.locals.url = req.protocol + '://' + req.headers.host + req.url;
    console.log('res locals url '+res.locals.url);
        next();
    });

    // Should be placed before express.static
    app.use(compress({
        filter: function (req, res) {
            return (/json|text|javascript|css/).test(res.getHeader('Content-Type'));
        },
        level: 9
    }));

    console.log('after compress usha');
    // Showing stack errors
    app.set('showStackError', true);

    // Set swig as the template engine
    app.engine('server.view.html', consolidate[config.templateEngine]);

    var swig = require('swig');
    Number.prototype.formatMoney = function (c, d, t) {
        var n = this,
            c = isNaN(c = Math.abs(c)) ? 2 : c,
            d = d == undefined ? "." : d,
            t = t == undefined ? "," : t,
            s = n < 0 ? "-" : "",
            i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
            j = (j = i.length) > 3 ? j % 3 : 0;
        return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
    };

    var usd = function (value, args) {
        return '$' + value.formatMoney(2);
    };
    swig.setFilter('usd', usd);

    //debug statement
    console.log('In before view engine');

    // Set views path and view engine
    app.set('view engine', 'server.view.html');
    app.set('views', './server/views');

    // Environment dependent middleware
    if (process.env.NODE_ENV === 'development') {
        // Enable logger (morgan)
        app.use(morgan('dev'));

        // Disable views cache
        app.set('view cache', false);
    } else if (process.env.NODE_ENV === 'production' || process.env.NODE_ENV === 'qa') {
        app.locals.cache = 'memory';
    }
   
    // Request body parsing middleware should be above methodOverride
    app.use(bodyParser.urlencoded({
        extended: true
    }));
    app.use(bodyParser.json());
    app.use(methodOverride());

    //Allow cors
    app.use(cors());


    //debug statement
    console.log('before app use session');

    // CookieParser should be above session
    app.use(cookieParser());
    app.use(multer({
        includeEmptyFields: true
    }));

    // Express MongoDB session storage
    app.use(session({
        saveUninitialized: true,
        resave: true,
        secret: config.sessionSecret,
        store: new mongoStore({
            db: db.connection.db,
            collection: config.sessionCollection
        })
    }));

   // Allow Origin

    //debug statement
    console.log('after app use session');

    // use passport session
    app.use(passport.initialize());
    app.use(passport.session());

    // connect flash for flash messages
    app.use(flash());

    // Use helmet to secure Express headers
    app.use(helmet.xframe());
    app.use(helmet.xssFilter());
    app.use(helmet.nosniff());
    app.use(helmet.ienoopen());
    app.disable('x-powered-by');


    /**
     * Keystone js configuration starts
     */
    keystone.set('app', app);
    keystone.set('mongoose', db);
    keystone.set('cookie secret', config.sessionSecret);
    keystone.set('session store', 'connect-mongo');
    keystone.set('cloudinary config', {
        cloud_name: 'wanteet-pages',
        api_key: '296198548993374',
        api_secret: 'rm7k5YOuRtCjxvIC3HM3J0lByy8'
    });

    // Let keystone know where your models are defined. Here we have it at the `/models`
    keystone.import('../content/models');

    console.log("Usha Keystone Model - 1");

    keystone.set('nav', {
        'pages': ['pages'],
        'posts': ['posts', 'post-categories'],
        'galleries': 'galleries',
        'enquiries': 'enquiries'
    });

    console.log("Usha Keystone Init start - 2");

    keystone.init({
        'name': 'Wanteet Pages',
        'brand': 'Wanteet Pages',
        'user model': 'User',
        'mongo': 'mongodb://localhost/wanteet-pages',
        'session': true,
        'wysiwyg additional options': {
            valid_elements: '*[*]',
            invalid_elements: '',
            valid_children: '+body[style],+body[script]'
        },
        'wysiwyg importcss': 'http://www.247jeeves.com/assets/css/inchefs.css'
    });

    console.log("Usha Keystone init end - 3");

    keystone.set('auth', function (req, res, next) {
        if (req.user) {
            keystone.list('User').model.findOne()
                .where('id', req.user.id).exec(function (err, user) {
                    if(err){
                        console.error(err);
                        res.redirect('/#!signin');
                    }
                    if (user && user.canAccessKeystone) {
                        next();
                    }else{
                        res.redirect('/#!signin');
                    }
                });
        }else{
    console.log("Usha Keystone auth no user root ");
            res.redirect('/');
        }
    });

    // Set keystone's to serve it's own public files. for instance, its logo's and stylesheets
    keystone.static(app);
    // Set keystone routes for the admin panel, located at '/keystone'
    keystone.routes(app);
    console.log("Usha Keystone Routes end - 1");
    /**
     * Keystone js configuration starts
     */

        // Setting the app router and static folder
    app.use(express.static(path.resolve('./build')));


    // Globbing routing files
    config.getGlobbedFiles('./server/routes/**/*.js').forEach(function (routePath) {
        require(path.resolve(routePath))(app);
        console.log("Usha Inside foreach "+routePath);
    });

    // Assume 'not found' in the error msgs is a 404. this is somewhat silly, but valid, you can do whatever you like, set properties, use instanceof etc.
    app.use(function (err, req, res, next) {
        // If the error object doesn't exists
        if (!err) return next();

        // Log it
        console.error(err.stack);

        // Error page
        res.status(500).render('500', {
            error: err.stack,
            user: req.user || null
        });
    });

    // Assume 404 since no middleware responded
    app.use(function (req, res) {
        console.log(req.user);
        res.status(404).render('404', {
            url: req.originalUrl,
            error: 'Not Found',
            user: req.user || null
        });
    });

    // error handlers

    // development error handler
    // will print stacktrace
    if (app.get('env') === 'development') {
        app.use(function (err, req, res, next) {
            res.status(err.status || 500);
            res.status(500).render('500', {
                message: err.message,
                error: err
            });
        });
    }

    // production error handler
    // no stacktraces leaked to user
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        console.log('EROR ERROR ERROR error message '+err.message);
    });

    process.on('uncaughtException', function (ex) {
        console.log('Caught exception: ');
        console.log(ex);
    });


    if (process.env.NODE_ENV === 'secure') {
        // Log SSL usage
        console.log('Securely using https protocol');

        // Load SSL key and certificate
        var privateKey = fs.readFileSync('./config/sslcerts/key.pem', 'utf8');
        var certificate = fs.readFileSync('./config/sslcerts/cert.pem', 'utf8');

        // Create HTTPS Server
        var httpsServer = https.createServer({
            key: privateKey,
            cert: certificate
        }, app);

        // Return HTTPS server instance
        return httpsServer;
    }


    console.log("Usha At the end of the app"+app);
    // Return Express server instance
    return app;
};
